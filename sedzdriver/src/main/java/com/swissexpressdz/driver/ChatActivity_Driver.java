package com.swissexpressdz.driver;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.swissexpressdz.driver.adapter.ChatAdapter;
import com.swissexpressdz.driver.models.datamodels.Message;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.Utils;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class ChatActivity_Driver extends BaseAppCompatActivity implements GoogleApiClient
        .OnConnectionFailedListener {
    private ChatAdapter chatAdapter;
    private RecyclerView rcvChat;
    private DatabaseReference firebaseDatabaseReference;
    private Button btnSend;
    private EditText messageEditText;
    private String MESSAGES_CHILD = "Demo";
    private SimpleDateFormat webFormat;

    final private String FCM_API = "https://fcm.googleapis.com/fcm/send";
    final private String serverKey = "key=" + "AAAAMj74PmQ:APA91bEpvxrgn6riXPWwYfcapLQRJNIugGQQcbzFlhN91dwjP4xEly23GBAF8hM5gSG2QetPLu0v_mg2wpK0OzqAmMtkxu-rUyudEJkXi8IzxhOdHoQOvj4fFUwf4cJmR-3p6ou1kGnV";
    final private String contentType = "application/json";
    final String TAG = "NOTIFICATION TAG";
    String NOTIFICATION_TITLE;
    String NOTIFICATION_MESSAGE;
    String TOPIC;
    String topic_value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_driver);
        initToolBar();
        setTitleOnToolbar(getResources().getString(R.string.text_chat_to_user) + " " + CurrentTrip
                .getInstance().getUserFirstName() + " " + CurrentTrip
                .getInstance().getUserLastName());
        initFirebaseChat();
        rcvChat = findViewById(R.id.rcvChat);
        btnSend = findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);
        btnSend.setEnabled(false);
        btnSend.setAlpha(0.5f);
        messageEditText = findViewById(R.id.messageEditText);
        messageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    btnSend.setEnabled(true);
                    btnSend.setAlpha(1f);
                } else {
                    btnSend.setEnabled(false);
                    btnSend.setAlpha(0.5f);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        initChatRcv();

    }


    @Override
    public void onPause() {
        if (chatAdapter != null) {
            chatAdapter.stopListening();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (chatAdapter != null) {
            chatAdapter.startListening();
        }
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    public void goWithBackArrow() {
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSend:
                sendMessage();
                break;
            default:
                // do with default
                break;
        }

    }

    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }


    private void initChatRcv() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        SnapshotParser<Message> parser = new SnapshotParser<Message>() {
            @Override
            public Message parseSnapshot(DataSnapshot dataSnapshot) {
                Message chatMessage = dataSnapshot.getValue(Message.class);
              /*  if (chatMessage != null) {
                    chatMessage.setId(dataSnapshot.getKey());

                }*/
                return chatMessage;
            }
        };
        DatabaseReference messagesRef = firebaseDatabaseReference.child(MESSAGES_CHILD);
        final FirebaseRecyclerOptions<Message> options =
                new FirebaseRecyclerOptions.Builder<Message>()
                        .setQuery(messagesRef, parser)
                        .build();

        chatAdapter = new ChatAdapter(this, options);
        chatAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = chatAdapter.getItemCount();


                int lastVisiblePosition = linearLayoutManager
                        .findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the user is at the bottom of
                // the list, scroll
                // to the bottom of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) && lastVisiblePosition ==
                                (positionStart - 1))) {
                    rcvChat.scrollToPosition(positionStart);


                }else {
                    rcvChat.scrollToPosition(friendlyMessageCount - 1);
                }
            }
        });
        rcvChat.setLayoutManager(linearLayoutManager);
        rcvChat.setAdapter(chatAdapter);


    }

    private void initFirebaseChat() {
        webFormat = new SimpleDateFormat(Const.DATE_TIME_FORMAT_WEB, Locale.US);
        webFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        firebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        MESSAGES_CHILD = preferenceHelper.getTripId();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Utils.showToast("Google Connection Failed", this);
    }

    public void setAsReadMessage(String id) {
        DatabaseReference chatMessage = firebaseDatabaseReference.child(MESSAGES_CHILD).child(id)
                .child("is_read");
        chatMessage.setValue(true);
    }


    private void sendMessage() {
        if (firebaseDatabaseReference != null) {
            String key = firebaseDatabaseReference.child(MESSAGES_CHILD).push().getKey();
            if (!TextUtils.isEmpty(key)) {
                Message chatMessage = new Message(key,
                        messageEditText.getText()
                                .toString().trim(), webFormat.format(new
                        Date()), Const.PROVIDER_UNIQUE_NUMBER, false);
                AppLog.Log(ChatActivity_Driver.class.getSimpleName(), chatMessage + "");
                firebaseDatabaseReference.child(MESSAGES_CHILD).child(key).setValue(chatMessage);
                messageEditText.setText("");





            }
        }
    }


}
