package com.swissexpressdz.driver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.swissexpressdz.driver.models.responsemodels.IsSuccessResponse;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.LanguageHelper;
import com.swissexpressdz.driver.utils.PreferenceHelper;
import com.swissexpressdz.driver.utils.Utils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Groofl on 30-03-2016.
 * <p>
 * This Class is handle a Notification which send by Google FCM server.
 */
public class FcmMessagingService extends FirebaseMessagingService {

    public static final String MESSAGE = "message";
    public static final String PROVIDER_DECLINE = "208";
    public static final String PROVIDER_APPROVED = "207";
    public static final String PROVIDER_HAVE_NEW_TRIP = "201";
    public static final String USER_CANCEL_TRIP = "205";
    public static final String USER_DESTINATION_UPDATE = "210";
    public static final String PAYMENT_CASH = "211";
    public static final String PAYMENT_CARD = "212";
    public static final String LOG_OUT = "230";
    public static final String PROVIDER_TRIP_END = "241";
    public static final String PROVIDER_OFFLINE = "242";
    public static final String TRIP_ACCEPTED_BY_ANOTHER_PROVIDER = "232";
    private static final String CHANNEL_ID = "channel_01";
    private LocalBroadcastManager localBroadcastManager;
    MediaPlayer mp;
    Sound sound = new Sound();
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        System.out.println("Remote message Driver###" + remoteMessage);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);


        if (remoteMessage != null) {

            AppLog.Log(Const.Tag.FCM_MESSAGING_SERVICE, "From:" + remoteMessage.getFrom());
            AppLog.Log(Const.Tag.FCM_MESSAGING_SERVICE, "Data:" + remoteMessage.getData());


            String message = remoteMessage.getData().get(MESSAGE);
            if (message != null && !message.isEmpty()) {
                tripStatus(message);
                AppLog.Log("onMessageReceived", message);
            }

        }
    }

    private void sendNotification(String message) {

        //Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.popup_notification);  //Here is FILE_NAME is the name of file that you want to play
        //   Uri sound = Uri. parse (ContentResolver. SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/raw/popup_notification.mp3" ) ;

        String path = "android.resource://" + getPackageName() + "/" + R.raw.popup_notification;

        System.out.println("Uri is Notiication "+path);

        final NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name,
                    NotificationManager.IMPORTANCE_DEFAULT);

            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();

            //    mChannel.enableLights(true);
            //  mChannel.enableVibration(true);
            //    mChannel.setSound(Uri.parse(path), attributes);
            notificationManager.createNotificationChannel(mChannel);
        }
        //mp = MediaPlayer.create(this, R.raw.popup_notification);

        Intent notificationIntent = new Intent(getApplicationContext(), MainDrawerActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                .FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("media",path);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainDrawerActivity.class);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        System.out.println("Notification Intent "+stackBuilder.getIntents());
        final Notification.Builder notificationBuilder = new Notification.Builder
                (this).setPriority(Notification.PRIORITY_MAX).setContentTitle(this.getResources().getString(R.string
                .app_name)).setContentText(message)
                .setAutoCancel(true).setSmallIcon(getNotificationIcon())
                .setContentIntent(notificationPendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID); // Channel ID
        }


        if (PreferenceHelper.getInstance(this).getIsPushNotificationSoundOn()) {


//            notificationBuilder.setDefaults(Notification.DEFAULT_SOUND
//                    | Notification.DEFAULT_LIGHTS);
            // mp.start();

            System.out.println("Message is  "+message);
            if(message.equalsIgnoreCase(getResources().getString(R.string.push_message_201)))
            {
                System.out.println("Paypal Modde "+PreferenceHelper.getInstance(this).getIsPaypalPayment());
                sound.PlaySong(R.raw.popup_notification,true,getApplicationContext());

            }

            else

            {
                notificationBuilder.setDefaults(Notification.DEFAULT_SOUND
                        | Notification.DEFAULT_LIGHTS);
            }

        }


        final int notificationId = Const.PUSH_NOTIFICATION_ID;
        assert notificationManager != null;
        notificationManager.notify(Const.PUSH_NOTIFICATION_ID, notificationBuilder
                .build());


        // This method will get rid of the notification AND the message after 1 day
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            //  mp.release();
                            sound.StopSong();
                        }
                        catch (Exception e)
                        {

                        }
                        notificationManager.cancel(notificationId);
                        // Preference_Manager.getInstance(mCtx).deleteKeyMessageid(NOTIFICATION);
                    }
                }, 60000/*howMany */);

            }
        });
    }
    @Override
    public void onDestroy ()
    {
        mp.release();
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build
                .VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.app_icon : R.mipmap.app_icon;
    }

    private void tripStatus(String status) {
        switch (status) {
            case PROVIDER_APPROVED:
                sendNotification(getMessage(status));
                sendGlobalBroadcast(Const.ACTION_APPROVED_PROVIDER);
                break;
            case PROVIDER_DECLINE:
                sendNotification(getMessage(status));
                sendGlobalBroadcast(Const.ACTION_DECLINE_PROVIDER);
                break;
            case USER_CANCEL_TRIP:
                sendNotification(getMessage(status));
                sendLocalBroadcast(Const.ACTION_CANCEL_TRIP);
                break;
            case PROVIDER_HAVE_NEW_TRIP:
                sendNotification(getMessage(status));
                sendBroadcast(new Intent(Const.ACTION_NEW_TRIP));
                break;
            case USER_DESTINATION_UPDATE:
                sendNotification(getMessage(status));
                sendLocalBroadcast(Const.ACTION_DESTINATION_UPDATE);
                break;
            case PAYMENT_CARD:
                sendNotification(getMessage(status));
                sendLocalBroadcast(Const.ACTION_PAYMENT_CARD);
                break;
            case PAYMENT_CASH:
                sendNotification(getMessage(status));
                sendLocalBroadcast(Const.ACTION_PAYMENT_CASH);
                break;
            case LOG_OUT:
                sendNotification(getMessage(status));
                goToMainActivity();
                break;
            case PROVIDER_TRIP_END:
                sendLocalBroadcast(Const.ACTION_PROVIDER_TRIP_END);
                sendNotification(getMessage(status));
                break;
            case PROVIDER_OFFLINE:
                sendNotification(getMessage(status));
                break;
            case TRIP_ACCEPTED_BY_ANOTHER_PROVIDER:
                sendLocalBroadcast(Const.ACTION_TRIP_ACCEPTED_BY_ANOTHER_PROVIDER);
                sendNotification(getMessage(status));
                break;
            default:
                sendNotification(status);
                break;
        }
    }

    private String getMessage(String code) {

        System.out.println("Language Code is FCM###"+code);
        String msg = "";
        String messageCode = Const.PUSH_MESSAGE_PREFIX + code;
        System.out.println("Language Code is FCM Msgcode###"+messageCode);

        msg = this.getResources().getString(this.getResources().getIdentifier(messageCode, Const
                .STRING, this.getPackageName()));
        System.out.println("Language Code is FCM Msg###"+msg);

        return msg;
    }

    private void sendLocalBroadcast(String action) {
        Intent intent = new Intent(action);
        localBroadcastManager.sendBroadcast(intent);
    }

    private void sendGlobalBroadcast(String action) {
        Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    public void goToMainActivity() {
        PreferenceHelper preferenceHelper = PreferenceHelper.getInstance(this);
        preferenceHelper.logout();
        try {
            mp.release();

        }
        catch (Exception e)
        {

        }
        Intent sigInIntent = new Intent(this, SignInActivity_Driver.class);
        sigInIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sigInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sigInIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        sigInIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(sigInIntent);
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        AppLog.Log(FcmMessagingService.class.getSimpleName(), "FCM Token Refresh = " + token);
        PreferenceHelper.getInstance(this).putDeviceToken(token);
        if (PreferenceHelper.getInstance(this).getSessionToken() != null) {
            updateDeviceTokenOnServer(token);
        }
    }


    private void updateDeviceTokenOnServer(String deviceToken) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.accumulate(Const.Params.TOKEN, PreferenceHelper.getInstance(this)
                    .getSessionToken());
            jsonObject.accumulate(Const.Params.PROVIDER_ID, PreferenceHelper.getInstance(this)
                    .getProviderId());
            jsonObject.accumulate(Const.Params.DEVICE_TOKEN, deviceToken);

            Call<IsSuccessResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .updateDeviceToken(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    if (ParseContent.getInstance().isSuccessful(response)) {
                        if (response.body().isSuccess()) {
                            Utils.showMessageToast(response.body().getMessage(),
                                    FcmMessagingService.this);
                        } else {
                            Utils.showErrorToast(response.body().getErrorCode(),
                                    FcmMessagingService.this);
                        }
                    }

                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(FcmMessagingService.class.getSimpleName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException("FCM Token Refresh", e);
        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(LanguageHelper.wrapper(newBase, PreferenceHelper.getInstance
                (newBase).getLanguageCode()));
    }
}