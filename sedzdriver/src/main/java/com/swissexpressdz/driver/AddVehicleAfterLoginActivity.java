package com.swissexpressdz.driver;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
 import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.swissexpressdz.driver.components.CustomPhotoDialog;
import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.models.datamodels.VehicleDetail;
import com.swissexpressdz.driver.models.datamodels.VehicleDetailsNew;
import com.swissexpressdz.driver.models.responsemodels.AddVehicleResponse;
import com.swissexpressdz.driver.models.responsemodels.EditVTCResponse;
import com.swissexpressdz.driver.models.responsemodels.IsSuccessResponse;
import com.swissexpressdz.driver.models.responsemodels.ProviderSeriveSelctionModel;
import com.swissexpressdz.driver.models.responsemodels.ServiceTypeModel;
import com.swissexpressdz.driver.models.responsemodels.SubServiceTypeModel;
import com.swissexpressdz.driver.models.responsemodels.VehicleDetailResponse;
import com.swissexpressdz.driver.models.responsemodels.VehicleDetailResponseNew;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextView_MontserratBold;
import com.swissexpressdz.driver.utils.ImageCompression;
import com.swissexpressdz.driver.utils.ImageHelper;
import com.swissexpressdz.driver.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVehicleAfterLoginActivity  extends BaseAppCompatActivity {


    CardView login_crd;
    ImageView img_crd;
    CustomPhotoDialog customPhotoDialog;
    private Uri picUri;
    private ImageHelper imageHelper;
    private String uploadImageFilePath = "";

    MyFontEdittextView etVehicleName,etBrand,etmodel,etyear,etplate_number,et_colur,etkilo_meters,etnum_seats
            ,etdistanceBasePrice,etBaseprice,etPriceperdistance,etWaitingtime,etWaitingtime_price,etCancellation_fees,
            etcarrent_hourlyprice,etcarrent_dayprice,etcarrent_depositprice;
    RelativeLayout rr_continue;
    private String msg;
    ArrayList<String> arrayList_files=new ArrayList<>();
    String file_ext2;
    Spinner spn_slctservice_type,spn_slctsubservice_type;
    List<ServiceTypeModel.ServiceType> serviceTypes = new ArrayList<>();
    List<SubServiceTypeModel.SubType> subTypeList = new ArrayList<>();
    LinearLayout ll_subtype,ll_taxi,ll_vtc,ll_carremtal;
    String str_service_id ="", str_subserive_id = "";
    boolean bol_taxi , bol_vtc,bol_carrental;
    EditVTCResponse editVTCResponse;
    RelativeLayout rr_back;
    private boolean isAddVehicle = true;
    private String vehicleId = "";
    CustomTextView_MontserratBold login_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addvehicle);
        System.out.println("Provider Id AddVehicle "+preferenceHelper.getProviderId());
        if (getIntent() != null) {
            isAddVehicle = getIntent().getExtras().getBoolean(Const.IS_ADD_VEHICLE);
            vehicleId = getIntent().getExtras().getString(Const.VEHICLE_ID);
        }


        initUi();


        getserviceTypeList();

        if (!TextUtils.isEmpty(vehicleId)) {
            getProviderVehicleDetail();
        }

    }

    private void getProviderVehicleDetail() {
        Utils.showCustomProgressDialog(this, "", false, null);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.VEHICLE_ID, vehicleId);
            Call<VehicleDetailResponseNew> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .getVehicleDetailnew(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<VehicleDetailResponseNew>() {
                @Override
                public void onResponse(Call<VehicleDetailResponseNew> call,
                                       Response<VehicleDetailResponseNew>
                                               response) {
                    if (parseContent.isSuccessful(response)) {
                        str_service_id = response.body().getVehicleDetail().getTypeid();

                        for (int i = 0; i < serviceTypes.size(); i++) {
                            if (TextUtils.equals(serviceTypes.get(i).getId(), str_service_id)) {
                                spn_slctservice_type.setSelection(i);
                            }
                        }



                   setVehicleDetail(response.body());


                    }

                }

                @Override
                public void onFailure(Call<VehicleDetailResponseNew> call, Throwable t) {
                    AppLog.handleThrowable(AddVehicleDetailActivity_Driver.class.getSimpleName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException("GET_PROVIDER_VEHICLE_DETAIL", e);
        }

    }

  private void setVehicleDetail(VehicleDetailResponseNew response) {
    if (response.isSuccess()) {
        VehicleDetailsNew vehicleDetail = response.getVehicleDetail();
//            edtVehicleName.setText(vehicleDetail.getName());
//            edtVehicleModel.setText(vehicleDetail.getModel());
//            edtVehiclePlateNo.setText(vehicleDetail.getPlateNo());
//            edtVehicleColor.setText(vehicleDetail.getColor());
//            vehicleId = vehicleDetail.getId();
//
//
//
//
        uploadImageFilePath = vehicleDetail.getImage();
        System.out.println("Boll Taxi "+bol_taxi);


        if(bol_taxi == true)
        {
            etVehicleName.setText(vehicleDetail.getName()+"");
    etBrand.setText(vehicleDetail.getBrand());
            etmodel.setText(vehicleDetail.getModel()+"");
            etyear.setText(vehicleDetail.getPassingYear()+"");
            etplate_number.setText(vehicleDetail.getPlateNo()+"");
            et_colur.setText(vehicleDetail.getColor()+"");
            etkilo_meters.setText(vehicleDetail.getKm()+"");
            etnum_seats.setText(vehicleDetail.getSeats()+"");


            PicassoTrustAll.getInstance(AddVehicleAfterLoginActivity.this)
                    .load(ApiClient.Base_URL +vehicleDetail.getImage())
                    .error(R.drawable.car_placeholder)
                    .into(img_crd);

        }

        if(bol_vtc == true)
        {
            etdistanceBasePrice.setText(vehicleDetail.getBasePriceDistance()+"");
            etBaseprice.setText(vehicleDetail.getBasePrice()+"");
            etPriceperdistance.setText(vehicleDetail.getPricePerUnitDistance()+"");
            etWaitingtime.setText(vehicleDetail.getWaitingTimeStartAfterMinute()+"");
            etWaitingtime_price.setText(vehicleDetail.getPriceForWaitingTime()+"");
            etCancellation_fees.setText(vehicleDetail.getCancellationFee()+"");

        }
        if(bol_carrental == true)
        {
            etcarrent_hourlyprice.setText(vehicleDetail.getHourlyPriceCarRent()+"");
            etcarrent_dayprice.setText(vehicleDetail.getDayPriceCarRent()+"");

        }

//            updateEditIcon(false);
        }
        Utils.hideCustomProgressDialog();
   }

    private void initUi() {

        imageHelper = new ImageHelper(this);
        login_crd = (CardView) findViewById(R.id.login_crd);
        login_crd.setOnClickListener(this);

        img_crd = (ImageView) findViewById(R.id.img_crd);
        img_crd.setOnClickListener(this);

        rr_back = (RelativeLayout) findViewById(R.id.rr_back);

        etVehicleName = (MyFontEdittextView) findViewById(R.id.etVehicleName);
        etBrand = (MyFontEdittextView) findViewById(R.id.etBrand);
        etmodel = (MyFontEdittextView) findViewById(R.id.etmodel);
        etyear = (MyFontEdittextView) findViewById(R.id.etyear);
        etplate_number = (MyFontEdittextView) findViewById(R.id.etplate_number);
        et_colur = (MyFontEdittextView) findViewById(R.id.et_colur);
        etkilo_meters = (MyFontEdittextView) findViewById(R.id.etkilo_meters);
        etnum_seats = (MyFontEdittextView) findViewById(R.id.etnum_seats);

        login_submit =(CustomTextView_MontserratBold) findViewById(R.id.login_submit);


        etdistanceBasePrice = (MyFontEdittextView) findViewById(R.id.etdistanceBasePrice);
        etBaseprice = (MyFontEdittextView) findViewById(R.id.etBaseprice);
        etPriceperdistance = (MyFontEdittextView) findViewById(R.id.etPriceperdistance);
        etWaitingtime = (MyFontEdittextView) findViewById(R.id.etWaitingtime);
        etWaitingtime_price = (MyFontEdittextView) findViewById(R.id.etWaitingtime_price);
        etCancellation_fees = (MyFontEdittextView) findViewById(R.id.etCancellation_fees);

        //FOr CarRental
        etcarrent_hourlyprice  = (MyFontEdittextView) findViewById(R.id.etcarrent_hourlyprice);
        etcarrent_dayprice  = (MyFontEdittextView) findViewById(R.id.etcarrent_dayprice);
        etcarrent_depositprice  = (MyFontEdittextView) findViewById(R.id.etcarrent_depositprice);

        rr_continue = (RelativeLayout) findViewById(R.id.rr_continue);
        rr_continue.setOnClickListener(this);
        spn_slctservice_type = (Spinner) findViewById(R.id.spn_slctservice_type);
        spn_slctsubservice_type =(Spinner) findViewById(R.id.spn_slctsubservice_type);

        ll_subtype = (LinearLayout) findViewById(R.id.ll_subtype);

        ll_taxi = (LinearLayout) findViewById(R.id.ll_taxi);
        ll_vtc = (LinearLayout) findViewById(R.id.ll_vtc);
        ll_carremtal = (LinearLayout) findViewById(R.id.ll_carremtal);

        spn_slctservice_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                if(serviceTypes!=null && serviceTypes.size()>0) {

                    str_service_id= serviceTypes.get(arg2).getId();

                    System.out.println("Selected Item Id%%%" + str_service_id);
                    getVTCType();
                    getSubserviceTypeList();
                    if (!TextUtils.isEmpty(vehicleId)) {
                        getProviderVehicleDetail();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }

        });
        spn_slctsubservice_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                if(subTypeList!=null && subTypeList.size()>0) {

                    str_subserive_id= subTypeList.get(arg2).getId();

                    System.out.println("Selected Item Id%%%" + str_subserive_id);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }

        });


        rr_back.setOnClickListener(this);

    }


    private void getserviceTypeList() {
        Utils.showCustomProgressDialog(this, "", false, null);
        Call<ServiceTypeModel> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                .getServiceType();
        call.enqueue(new Callback<ServiceTypeModel>() {
            @Override
            public void onResponse(Call<ServiceTypeModel> call, Response<ServiceTypeModel>
                    response) {
                if (parseContent.isSuccessful(response)) {
                    Utils.hideCustomProgressDialog();
                    System.out.println("Model List  "+response.body().getServiceType().size());

                    serviceTypes.addAll(response.body().getServiceType());
                    System.out.println("Service type size "+serviceTypes.size());
                    ArrayList<String> arr_servicelist = new ArrayList<>();
                    for(int k=0; k<serviceTypes.size(); k++)
                    {
                        arr_servicelist.add(response.body().getServiceType().get(k).getTypename());
                    }



                    ArrayAdapter<String> dealAdapter = new ArrayAdapter<String>(
                            AddVehicleAfterLoginActivity.this,
                            R.layout.detailspinnertext, arr_servicelist);
                    dealAdapter
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spn_slctservice_type.setAdapter(dealAdapter);



                }
            }

            @Override
            public void onFailure(Call<ServiceTypeModel> call, Throwable t) {
                AppLog.handleThrowable(ProfileActivity_Driver.class.getSimpleName(), t);
            }
        });
    }


    private void getSubserviceTypeList() {
        Utils.showCustomProgressDialog(this, "", false, null);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.TYPEID, str_service_id);

            Call<SubServiceTypeModel> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .getSubServiceType(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<SubServiceTypeModel>() {
                @Override
                public void onResponse(Call<SubServiceTypeModel> call, Response<SubServiceTypeModel>
                        response) {

                    if (parseContent.isSuccessful(response)) {
                        Utils.hideCustomProgressDialog();
                        System.out.println("SubType List  " + response.body().getSubType());
                        if(response.body().getSubType()!= null && response.body().getSubType().size()>0) {

                            ll_subtype.setVisibility(View.VISIBLE);
                            subTypeList.addAll(response.body().getSubType());


                            ArrayList<String> arr_subservicelist = new ArrayList<>();
                            for (int k = 0; k < response.body().getSubType().size(); k++) {
                                arr_subservicelist.add(response.body().getSubType().get(k).getSubtypename());
                            }

                            ArrayAdapter<String> dealAdapter2 = new ArrayAdapter<String>(
                                    AddVehicleAfterLoginActivity.this,
                                    R.layout.detailspinnertext, arr_subservicelist);
                            dealAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spn_slctsubservice_type.setAdapter(dealAdapter2);
                        }
                        else
                        {
                            ll_subtype.setVisibility(View.GONE);

                        }


                    }
                }

                @Override
                public void onFailure(Call<SubServiceTypeModel> call, Throwable t) {
                    AppLog.handleThrowable(ProfileActivity_Driver.class.getSimpleName(), t);
                }


            });
        }
        catch (JSONException e) {
            AppLog.handleException(Const.Tag.BANK_DETAIL_ACTIVITY, e);
        }

    }
    private void getProviderServiceStatus() {
        Utils.showCustomProgressDialog(this, "", false, null);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());

            Call<ProviderSeriveSelctionModel> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .provider_vehicle_status(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<ProviderSeriveSelctionModel>() {
                @Override
                public void onResponse(Call<ProviderSeriveSelctionModel> call, Response<ProviderSeriveSelctionModel>
                        response) {

                    if (parseContent.isSuccessful(response)) {
                        Utils.hideCustomProgressDialog();
                        System.out.println("ProviderVehicle List  " + response.body().getSuccess());
                        if(response.body().getSuccess()) {

                          //  bol_taxi = response.body().getTaxi();
                            bol_taxi = true;

                            if(bol_taxi == true)
                            {
                                ll_taxi.setVisibility(View.VISIBLE);
                            }

                            bol_vtc = response.body().getVtc();

                            if(bol_vtc == true)
                            {
                                ll_vtc.setVisibility(View.VISIBLE);

                            }

                            bol_carrental = response.body().getCarRent();

                            if(bol_carrental == true)
                            {
                                ll_carremtal.setVisibility(View.VISIBLE);

                            }



                        }
                        else
                        {

                        }


                    }
                }

                @Override
                public void onFailure(Call<ProviderSeriveSelctionModel> call, Throwable t) {
                    AppLog.handleThrowable(ProfileActivity_Driver.class.getSimpleName(), t);
                }


            });
        }
        catch (JSONException e) {
            AppLog.handleException(Const.Tag.BANK_DETAIL_ACTIVITY, e);
        }

    }



    private void getVTCType() {
        Utils.showCustomProgressDialog(AddVehicleAfterLoginActivity.this, getResources().getString(R.string
                .msg_waiting_for_getting_credit_cards), false, null);
        JSONObject jsonObject = new JSONObject();
        try {
            //  jsonObject.put(Const.Params.TYPE_ID, preferenceHelper.getTypeid());
            jsonObject.put(Const.Params.TYPE_ID, str_service_id);


            Call<EditVTCResponse> call = ApiClient.getClient(AddVehicleAfterLoginActivity.this)
                    .create(ApiInterface.class).provider_get_vtc_city_type(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<EditVTCResponse>() {
                @Override
                public void onResponse(Call<EditVTCResponse> call, Response<EditVTCResponse> response) {
                    if (parseContent.isSuccessful(response)) {

                        if (response.body().getSuccess()) {
                            System.out.println("Provider CityType  "+response.body().getVtcType().getDeposit());


                            editVTCResponse = response.body();


                            etcarrent_depositprice.setText(response.body().getVtcType().getDeposit()+"");
                            getProviderServiceStatus();
                            Utils.hideCustomProgressDialog();
                            rr_continue.setClickable(true);

                            rr_continue.setFocusable(true);


                        }
                        else {
                            ll_taxi.setVisibility(View.GONE);
                            ll_vtc.setVisibility(View.GONE);
                            ll_carremtal.setVisibility(View.GONE);
                            rr_continue.setClickable(false);

                            rr_continue.setFocusable(false);
                            Utils.hideCustomProgressDialog();
                            Utils.showErrorToast(response.body().getErrorCode(), AddVehicleAfterLoginActivity.this);
                        }

                    }

                }

                @Override
                public void onFailure(Call<EditVTCResponse> call, Throwable t) {
                    AppLog.handleThrowable(GetVTCActivity.class.getSimpleName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.VIEW_AND_ADD_PAYMENT_ACTIVITY, e);
        }
    }

    @Override
    protected boolean isValidate() {
        msg = null;
        if(bol_taxi == true)
        {
            if (TextUtils.isEmpty(etVehicleName.getText().toString().trim())) {
                msg = getString(R.string.this_field_requiredd);
                etVehicleName.requestFocus();
                etVehicleName.setError(msg);
            }
            else  if (TextUtils.isEmpty(etBrand.getText().toString().trim())) {
                msg = getString(R.string.this_field_requiredd);
                etBrand.requestFocus();
                etBrand.setError(msg);
            } else if (TextUtils.isEmpty(etmodel.getText().toString().trim())) {
                msg = getString(R.string.this_field_requiredd);
                etmodel.requestFocus();
                etmodel.setError(msg);
            }
            else if (TextUtils.isEmpty(etyear.getText().toString().trim())) {
                msg = getString(R.string.this_field_requiredd);
                etyear.requestFocus();
                etyear.setError(msg);
            }
            else if (TextUtils.isEmpty(etplate_number.getText().toString().trim())) {
                msg = getString(R.string.this_field_requiredd);
                etplate_number.requestFocus();
                etplate_number.setError(msg);
            }
            else if (TextUtils.isEmpty(et_colur.getText().toString().trim())) {
                msg = getString(R.string.this_field_requiredd);
                et_colur.requestFocus();
                et_colur.setError(msg);
            }
            else if (TextUtils.isEmpty(et_colur.getText().toString().trim())) {
                msg = getString(R.string.this_field_requiredd);
                et_colur.requestFocus();
                et_colur.setError(msg);
            }
            else if (TextUtils.isEmpty(etkilo_meters.getText().toString().trim())) {
                msg = getString(R.string.this_field_requiredd);
                etkilo_meters.requestFocus();
                etkilo_meters.setError(msg);
            }
            else if (TextUtils.isEmpty(etnum_seats.getText().toString().trim())) {
                msg = getString(R.string.this_field_requiredd);
                etnum_seats.requestFocus();
                etnum_seats.setError(msg);
            }

            else if (uploadImageFilePath== null || uploadImageFilePath.equalsIgnoreCase("") || uploadImageFilePath.equalsIgnoreCase("null")) {
                msg = getString(R.string.this_field_requiredd);
                img_crd.requestFocus();
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.plz_slt_photo),Toast.LENGTH_SHORT).show();
            }
        }

        if(bol_vtc == true)
        {

            if(TextUtils.isEmpty(etdistanceBasePrice.getText().toString()))
            {
                msg = getString(R.string.this_field_requiredd);
                etdistanceBasePrice.requestFocus();
                etdistanceBasePrice.setError(msg);
            }

            else
            {
                if(etdistanceBasePrice.getText().toString()!= null)
                {

                    try {
                        //FOr Distance Base Price
                        double in_mindistancebaseprice =  editVTCResponse.getVtcType().getBasePriceDistanceMin();
                        double in_maxdistancebaseprice =  editVTCResponse.getVtcType().getBasePriceDistanceMax();

                        double dub_distance_base_price = Double.parseDouble(etdistanceBasePrice.getText().toString());

                        //    else if(dub_distance_base_price  < in_mindistancebaseprice)
//    {
//        msg = getString(R.string.this_field_requiredd);
//        etdistanceBasePrice.requestFocus();
//        etdistanceBasePrice.setError(msg);
//    }
//    else if(dub_distance_base_price  > in_maxdistancebaseprice)
//    {
//        msg = getString(R.string.this_field_requiredd);
//        etdistanceBasePrice.requestFocus();
//        etdistanceBasePrice.setError(msg);
//    }
//
                    }
                    catch (Exception e)
                    {

                    }

                }

            }


            if(TextUtils.isEmpty(etBaseprice.getText().toString()))
            {
                msg = getString(R.string.this_field_requiredd);
                etBaseprice.requestFocus();
                etBaseprice.setError(msg);
            }




            else
            {
                try {
                    // For Base Price
                    double in_minbaseprice =  editVTCResponse.getVtcType().getBasePriceMin();
                    double in_maxbaseprice =  editVTCResponse.getVtcType().getBasePriceMax();

                    double dub_base_price = Double.parseDouble(etBaseprice.getText().toString());
                    if(dub_base_price  < in_minbaseprice)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_minbaseprice+"-"+in_maxbaseprice+"";
                        etBaseprice.requestFocus();
                        etBaseprice.setError(msg);
                    }
                    else if(dub_base_price  > in_maxbaseprice)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_minbaseprice+"-"+in_maxbaseprice+"";
                        etBaseprice.requestFocus();
                        etBaseprice.setError(msg);
                    }
                }
                catch (Exception e)
                {

                }


            }

            if(TextUtils.isEmpty(etPriceperdistance.getText().toString()))
            {
                msg = getString(R.string.this_field_requiredd);
                etPriceperdistance.requestFocus();
                etPriceperdistance.setError(msg);
            }
            else  {
                try {
                    // For  Price Per Distance
                    double in_minpriceperdistance = editVTCResponse.getVtcType().getPricePerUnitDistanceMin();
                    double in_maxpriceperdistance = editVTCResponse.getVtcType().getPricePerUnitDistanceMax();

                    double dub_priceperdistance = Double.parseDouble(etPriceperdistance.getText().toString());
                    if(dub_priceperdistance  < in_minpriceperdistance)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_minpriceperdistance+"-"+in_maxpriceperdistance+"";
                        etPriceperdistance.requestFocus();
                        etPriceperdistance.setError(msg);
                    }
                    else if(dub_priceperdistance  > in_maxpriceperdistance)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_minpriceperdistance+"-"+in_maxpriceperdistance+"";
                        etPriceperdistance.requestFocus();
                        etPriceperdistance.setError(msg);
                    }

                } catch (Exception e) {

                }

            }


            if(TextUtils.isEmpty(etWaitingtime.getText().toString()))
            {
                msg = getString(R.string.this_field_requiredd);
                etWaitingtime.requestFocus();
                etWaitingtime.setError(msg);
            }
            else
            {
                try {
                    // For  Waiting Time
                    double in_minwaitingTime =  editVTCResponse.getVtcType().getWaitingTimeStartAfterMinuteMin();
                    double in_maxwaitingTime  =  editVTCResponse.getVtcType().getWaitingTimeStartAfterMinuteMax();

                    double dub_waitingTime = Double.parseDouble(etWaitingtime.getText().toString());
                    if(dub_waitingTime  < in_minwaitingTime)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_minwaitingTime+"-"+in_maxwaitingTime+"";
                        etWaitingtime.requestFocus();
                        etWaitingtime.setError(msg);
                    }
                    else if(dub_waitingTime  > in_maxwaitingTime)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_minwaitingTime+"-"+in_maxwaitingTime+"";
                        etWaitingtime.requestFocus();
                        etWaitingtime.setError(msg);
                    }

                }
                catch (Exception e)
                {

                }

            }


            if(TextUtils.isEmpty(etWaitingtime_price.getText().toString()))
            {
                msg = getString(R.string.this_field_requiredd);
                etWaitingtime_price.requestFocus();
                etWaitingtime_price.setError(msg);
            }

            else
            {

                try {
                    // For  Waiting Time Price
                    double in_minwaitingPrice =  editVTCResponse.getVtcType().getPriceForWaitingTimeMin();
                    double in_maxwaitingPrice  =  editVTCResponse.getVtcType().getPriceForWaitingTimeMax();

                    double dub_waitingPrice = Double.parseDouble(etWaitingtime_price.getText().toString());
                    if(dub_waitingPrice  < in_minwaitingPrice)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_minwaitingPrice+"-"+in_maxwaitingPrice+"";
                        etWaitingtime_price.requestFocus();
                        etWaitingtime_price.setError(msg);
                    }
                    else if(dub_waitingPrice  > in_maxwaitingPrice)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_minwaitingPrice+"-"+in_maxwaitingPrice+"";
                        etWaitingtime_price.requestFocus();
                        etWaitingtime_price.setError(msg);
                    }
                }
                catch (Exception e)
                {

                }

            }

            if(TextUtils.isEmpty(etCancellation_fees.getText().toString()))
            {
                msg = getString(R.string.this_field_requiredd);
                etCancellation_fees.requestFocus();
                etCancellation_fees.setError(msg);
            }

            else
            {
                try {
                    // FOr Cancellation Fees
                    double in_minCacellationFee =  editVTCResponse.getVtcType().getCancellationFeeMin();
                    double in_maxCacellationFee  =  editVTCResponse.getVtcType().getCancellationFeeMax();

                    System.out.println("Cacellation text "+etCancellation_fees);
                    double dub_CacellationFee = Double.parseDouble(etCancellation_fees.getText().toString());

                    if(dub_CacellationFee  < in_minCacellationFee)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_minCacellationFee+"-"+in_maxCacellationFee+"";
                        etCancellation_fees.requestFocus();
                        etCancellation_fees.setError(msg);
                    }
                    else if(dub_CacellationFee  > in_maxCacellationFee)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_minCacellationFee+"-"+in_maxCacellationFee+"";
                        etCancellation_fees.requestFocus();
                        etCancellation_fees.setError(msg);
                    }
                }
                catch (Exception e)
                {

                }

            }




        }



        if(bol_carrental == true) {





            if(TextUtils.isEmpty(etcarrent_hourlyprice.getText().toString()))
            {
                msg = getString(R.string.this_field_requiredd);
                etcarrent_hourlyprice.requestFocus();
                etcarrent_hourlyprice.setError(msg);
            }

            else {
                // FOr  CarRental Hour
                try {
                    double in_minhourlyprice = editVTCResponse.getVtcType().getHourlyPriceCarRentMin();
                    double in_maxhourlyprice = editVTCResponse.getVtcType().getHourlyPriceCarRentMax();

                    double dub_hourlyprice = Double.parseDouble(etcarrent_hourlyprice.getText().toString());


                    if (dub_hourlyprice < in_minhourlyprice) {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between) + " " + in_minhourlyprice + "-" + in_maxhourlyprice + "";
                        etcarrent_hourlyprice.requestFocus();
                        etcarrent_hourlyprice.setError(msg);
                    } else if (dub_hourlyprice > in_maxhourlyprice) {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between) + " " + in_minhourlyprice + "-" + in_maxhourlyprice + "";
                        etcarrent_hourlyprice.requestFocus();
                        etcarrent_hourlyprice.setError(msg);
                    }
                } catch (Exception e) {

                }
            }





            if(TextUtils.isEmpty(etcarrent_dayprice.getText().toString()))
            {
                msg = getString(R.string.this_field_requiredd);
                etcarrent_dayprice.requestFocus();
                etcarrent_dayprice.setError(msg);
            }

            else

            {
                try {
                    //FOr  CarRental Day
                    double in_mindayprice =  editVTCResponse.getVtcType().getDayPriceCarRentMin();
                    double in_maxdayprice=  editVTCResponse.getVtcType().getDayPriceCarRentMax();

                    double dub_dayprice = Double.parseDouble(etcarrent_dayprice.getText().toString());


                    if(dub_dayprice  < in_mindayprice)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_mindayprice+"-"+in_maxdayprice+"";
                        etcarrent_dayprice.requestFocus();
                        etcarrent_dayprice.setError(msg);
                    }
                    else if(dub_dayprice  > in_maxdayprice)
                    {
                        msg = getResources().getString(R.string.the_vlaue_shouldbe_between)+" "+in_mindayprice+"-"+in_maxdayprice+"";                etcarrent_dayprice.requestFocus();
                        etcarrent_dayprice.setError(msg);
                    }
                }
                catch (Exception e)
                {

                }



            }


        }

        return TextUtils.isEmpty(msg);

    }

    @Override
    public void goWithBackArrow() {

    }


    @Override
    public void onClick(View view) {

        if(view == rr_back)
        {
            onBackPressed();
        }
        if(view == login_crd)
        {
            openPhotoDialog();
        }
        if(view == img_crd)
        {
            openPhotoDialog();
        }

        if(view == rr_continue)
        {
            if (isValidate()) {
                try {

                    System.out.println("Is ADDVehicle "+isAddVehicle);
                    System.out.println("Is Vehicleid "+vehicleId);
                    if(isAddVehicle)
                    {
                        System.out.println("Is AddVehicle inside "+isAddVehicle);

                        if(vehicleId == null || vehicleId.equalsIgnoreCase("null")
                                || vehicleId.equalsIgnoreCase(""))
                        {
                            System.out.println("Is Vehicleid inside "+vehicleId);
                            if (isValidate())
                            {
                                addVehicle();

                            }



                        }
                        else
                        {

                        }
                    }

//                    if(isAddVehicle && vehicleId.equalsIgnoreCase(""))
//                    {
//                        addVehicle();
//                    }
                    else
                    {

                        if (isValidate())

                        {
                            editVehicle();

                        }
                    }

                }
                catch (Exception e)

                {
                    System.out.println("Exception is "+e);
                }

            }

        }


    }


    private void editVehicle() {


        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_registering), false, null);

        ArrayList<String> arrayList_files=new ArrayList<>();

        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register valid");

        HashMap<String, RequestBody> map = new HashMap<>();

        MultipartBody.Part[] surveyImagesParts = null;
        map.put(Const.Params.PROVIDER_ID, ApiClient.makeTextRequestBody(preferenceHelper.getProviderId()));
        map.put(Const.Params.VEHICLE_ID, ApiClient.makeTextRequestBody(vehicleId));


        if(bol_taxi == true)
        {

            map.put(Const.Params.VEHICLE_NAME, ApiClient.makeTextRequestBody(etVehicleName.getText().toString()));
            map.put(Const.Params.BRAND, ApiClient.makeTextRequestBody(etBrand.getText().toString()));

            map.put(Const.Params.VEHICLE_MODEL, ApiClient.makeTextRequestBody(etmodel.getText().toString()));

            map.put(Const.Params.VEHICLE_YEAR, ApiClient.makeTextRequestBody(etyear.getText().toString()));

            map.put(Const.Params.VEHICLE_PLATE, ApiClient.makeTextRequestBody(etplate_number.getText().toString()));
            map.put(Const.Params.VEHICLE_COLOR, ApiClient.makeTextRequestBody(et_colur.getText().toString()));
            map.put(Const.Params.KM, ApiClient.makeTextRequestBody(etkilo_meters.getText().toString()));

            map.put(Const.Params.SEATS, ApiClient.makeTextRequestBody(etnum_seats.getText().toString()));

         if(file_ext2!= null && !file_ext2.equalsIgnoreCase("") && !file_ext2.equalsIgnoreCase("null"))
         {
             map.put(Const.Params.Ext, ApiClient.makeTextRequestBody(file_ext2));



             arrayList_files.add(uploadImageFilePath);
             surveyImagesParts = new MultipartBody.Part[arrayList_files.size()];

             File file = new File(uploadImageFilePath);
             RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
             surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.image, file.getName(), surveyBody);
         }

         else
         {

         }



        }

        else if(bol_taxi == false)
        {
            map.put(Const.Params.VEHICLE_NAME, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.BRAND, ApiClient.makeTextRequestBody(""));

            map.put(Const.Params.VEHICLE_MODEL, ApiClient.makeTextRequestBody(""));

            map.put(Const.Params.VEHICLE_YEAR, ApiClient.makeTextRequestBody(""));

            map.put(Const.Params.VEHICLE_PLATE, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.VEHICLE_COLOR, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.KM, ApiClient.makeTextRequestBody(""));

            map.put(Const.Params.SEATS, ApiClient.makeTextRequestBody(""));
        }


        if(bol_vtc == true)
        {
            map.put(Const.Params.COUNTRY_ID, ApiClient.makeTextRequestBody(editVTCResponse.getVtcType().getCountryid()));

            //  map.put(Const.Params.CITY_ID, ApiClient.makeTextRequestBody(preferenceHelper.getCityId()));
            map.put(Const.Params.TYPEID , ApiClient.makeTextRequestBody(str_service_id));
            double dub_distancebasePrice = Double.parseDouble(etdistanceBasePrice.getText().toString());


            map.put(Const.Params.BASE_PRICE_DISTANCE, ApiClient.makeTextRequestBody(dub_distancebasePrice));
            map.put(Const.Params.BASE_PRICE, ApiClient.makeTextRequestBody(etBaseprice.getText().toString()));
            map.put(Const.Params.PRICE_PER_UNIT_DISTANCE, ApiClient.makeTextRequestBody(etPriceperdistance.getText().toString()));

            map.put(Const.Params.WAITING_TIME_MINUTES, ApiClient.makeTextRequestBody(etWaitingtime.getText().toString()));
            map.put(Const.Params.WAITING_TIME_PRICE, ApiClient.makeTextRequestBody(etWaitingtime_price.getText().toString()));
            map.put(Const.Params.CACELLATION_FEES, ApiClient.makeTextRequestBody(etCancellation_fees.getText().toString()));



        }
        else if(bol_vtc == false)
        {
            map.put(Const.Params.COUNTRY_ID, ApiClient.makeTextRequestBody(""));

            //  map.put(Const.Params.CITY_ID, ApiClient.makeTextRequestBody(preferenceHelper.getCityId()));
            map.put(Const.Params.TYPEID , ApiClient.makeTextRequestBody(""));


            map.put(Const.Params.BASE_PRICE_DISTANCE, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.BASE_PRICE, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.PRICE_PER_UNIT_DISTANCE, ApiClient.makeTextRequestBody(""));

            map.put(Const.Params.WAITING_TIME_MINUTES, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.WAITING_TIME_PRICE, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.CACELLATION_FEES, ApiClient.makeTextRequestBody(""));

        }


        if(bol_carrental == true)
        {
            map.put(Const.Params.HOURLY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(etcarrent_hourlyprice.getText().toString()));
            map.put(Const.Params.DAY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(etcarrent_dayprice.getText().toString()));
            map.put(Const.Params.DEPOSIT, ApiClient.makeTextRequestBody(etcarrent_depositprice.getText().toString()));
        }
        else if(bol_vtc == false)

        {
            map.put(Const.Params.HOURLY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.DAY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.DEPOSIT, ApiClient.makeTextRequestBody(""));
        }

        //For First Document
        Call<IsSuccessResponse> userDataResponseCall;
        userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                (ApiInterface.class).updateVehicleDetailNew(
                surveyImagesParts
                , map);

        System.out.println("Map is###"+map.toString());


        userDataResponseCall.enqueue(new Callback<IsSuccessResponse>() {
            @Override
            public void onResponse(Call<IsSuccessResponse> call,
                                   Response<IsSuccessResponse> response) {
                if (parseContent.isSuccessful(response)) {


                        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register Success");

                        Utils.hideCustomProgressDialog();
                        onBackPressed();
                        //    moveWithUserSpecificPreference();


                }

                else
                {
                    Utils.hideCustomProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                AppLog.handleThrowable(RegisterActivity_Driver.class.getSimpleName(), t);
            }
        });


    }
    private void addVehicle() {


        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_registering), false, null);

        ArrayList<String> arrayList_files=new ArrayList<>();

        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register valid");

        HashMap<String, RequestBody> map = new HashMap<>();

        MultipartBody.Part[] surveyImagesParts = null;
        map.put(Const.Params.PROVIDER_ID, ApiClient.makeTextRequestBody(preferenceHelper.getProviderId()));
        if(bol_taxi == true)
        {

            map.put(Const.Params.VEHICLE_NAME, ApiClient.makeTextRequestBody(etVehicleName.getText().toString()));
            map.put(Const.Params.BRAND, ApiClient.makeTextRequestBody(etBrand.getText().toString()));

            map.put(Const.Params.VEHICLE_MODEL, ApiClient.makeTextRequestBody(etmodel.getText().toString()));

            map.put(Const.Params.VEHICLE_YEAR, ApiClient.makeTextRequestBody(etyear.getText().toString()));

            map.put(Const.Params.VEHICLE_PLATE, ApiClient.makeTextRequestBody(etplate_number.getText().toString()));
            map.put(Const.Params.VEHICLE_COLOR, ApiClient.makeTextRequestBody(et_colur.getText().toString()));
            map.put(Const.Params.KM, ApiClient.makeTextRequestBody(etkilo_meters.getText().toString()));

            map.put(Const.Params.SEATS, ApiClient.makeTextRequestBody(etnum_seats.getText().toString()));
            map.put(Const.Params.Ext, ApiClient.makeTextRequestBody(file_ext2));



            arrayList_files.add(uploadImageFilePath);
            surveyImagesParts = new MultipartBody.Part[arrayList_files.size()];

            File file = new File(uploadImageFilePath);
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.image, file.getName(), surveyBody);


        }
        else if(bol_taxi == false)
        {
            map.put(Const.Params.VEHICLE_NAME, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.BRAND, ApiClient.makeTextRequestBody(""));

            map.put(Const.Params.VEHICLE_MODEL, ApiClient.makeTextRequestBody(""));

            map.put(Const.Params.VEHICLE_YEAR, ApiClient.makeTextRequestBody(""));

            map.put(Const.Params.VEHICLE_PLATE, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.VEHICLE_COLOR, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.KM, ApiClient.makeTextRequestBody(""));

            map.put(Const.Params.SEATS, ApiClient.makeTextRequestBody(""));
        }

        if(bol_vtc == true)
        {
            map.put(Const.Params.COUNTRY_ID, ApiClient.makeTextRequestBody(editVTCResponse.getVtcType().getCountryid()));

            //  map.put(Const.Params.CITY_ID, ApiClient.makeTextRequestBody(preferenceHelper.getCityId()));
            map.put(Const.Params.TYPEID , ApiClient.makeTextRequestBody(str_service_id));


            map.put(Const.Params.BASE_PRICE_DISTANCE, ApiClient.makeTextRequestBody(etdistanceBasePrice.getText()));
            map.put(Const.Params.BASE_PRICE, ApiClient.makeTextRequestBody(etBaseprice.getText().toString()));
            map.put(Const.Params.PRICE_PER_UNIT_DISTANCE, ApiClient.makeTextRequestBody(etPriceperdistance.getText().toString()));

            map.put(Const.Params.WAITING_TIME_MINUTES, ApiClient.makeTextRequestBody(etWaitingtime.getText().toString()));
            map.put(Const.Params.WAITING_TIME_PRICE, ApiClient.makeTextRequestBody(etWaitingtime_price.getText().toString()));
            map.put(Const.Params.CACELLATION_FEES, ApiClient.makeTextRequestBody(etCancellation_fees.getText().toString()));



        }

        else if(bol_vtc == false)
        {
            map.put(Const.Params.COUNTRY_ID, ApiClient.makeTextRequestBody(""));

            //  map.put(Const.Params.CITY_ID, ApiClient.makeTextRequestBody(preferenceHelper.getCityId()));
            map.put(Const.Params.TYPEID , ApiClient.makeTextRequestBody(""));


            map.put(Const.Params.BASE_PRICE_DISTANCE, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.BASE_PRICE, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.PRICE_PER_UNIT_DISTANCE, ApiClient.makeTextRequestBody(""));

            map.put(Const.Params.WAITING_TIME_MINUTES, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.WAITING_TIME_PRICE, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.CACELLATION_FEES, ApiClient.makeTextRequestBody(""));

        }

        if(bol_carrental == true)
        {
            map.put(Const.Params.HOURLY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(etcarrent_hourlyprice.getText().toString()));
            map.put(Const.Params.DAY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(etcarrent_dayprice.getText().toString()));
            map.put(Const.Params.DEPOSIT, ApiClient.makeTextRequestBody(etcarrent_depositprice.getText().toString()));
        }
        else if(bol_vtc == false)

        {
            map.put(Const.Params.HOURLY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.DAY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.DEPOSIT, ApiClient.makeTextRequestBody(""));
        }
        //For First Document
        Call<AddVehicleResponse> userDataResponseCall;
        userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                (ApiInterface.class).provider_vehicle(
                surveyImagesParts
                , map);

        System.out.println("Map is###"+map.toString());


        userDataResponseCall.enqueue(new Callback<AddVehicleResponse>() {
            @Override
            public void onResponse(Call<AddVehicleResponse> call,
                                   Response<AddVehicleResponse> response) {
                if (parseContent.isSuccessful(response)) {

                    if(response.body().getSuccess())
                    {
                        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register Success");

                        CurrentTrip.getInstance().clear();
                        Utils.hideCustomProgressDialog();

                        Intent ii =new Intent(AddVehicleAfterLoginActivity.this,DocumentNewActivity.class);
                        startActivity(ii);
                   //     onBackPressed();
                    //    moveWithUserSpecificPreference();
                    }
                    else
                    {
                        Utils.hideCustomProgressDialog();

                    }

                }

                else
                {

                }
            }

            @Override
            public void onFailure(Call<AddVehicleResponse> call, Throwable t) {
                AppLog.handleThrowable(RegisterActivity_Driver.class.getSimpleName(), t);
            }
        });


    }

    public  void validate() {
        boolean isValidate = false;





    }
    protected void openPhotoDialog() {
        if (ContextCompat.checkSelfPermission(AddVehicleAfterLoginActivity.this, Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission
                (AddVehicleAfterLoginActivity.this, Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AddVehicleAfterLoginActivity.this, new String[]{Manifest
                    .permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
        }
        else {

            customPhotoDialog = new CustomPhotoDialog(this) {
                @Override
                public void clickedOnCamera() {
                    customPhotoDialog.dismiss();
                    takePhotoFromCamera();
                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog.dismiss();
                    choosePhotoFromGallery();
                }
            };
            customPhotoDialog.show();
        }


    }
    private void choosePhotoFromGallery() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Utils.isNougat()) {
            picUri = FileProvider.getUriForFile(AddVehicleAfterLoginActivity.this, getApplicationContext().getPackageName(), imageHelper.createImageFile());
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            picUri = Uri.fromFile(imageHelper.createImageFile());
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, Const.ServiceCode.TAKE_PHOTO);
    }

    @Override
    public void onBackPressed() {
  super.onBackPressed();
      //  setResult(Activity.RESULT_OK);

        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Const.ServiceCode.TAKE_PHOTO:

                if (resultCode == RESULT_OK) {
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        file_ext2 = mime.getExtensionFromMimeType(cR.getType(picUri));

                    }
                    catch (Exception e)
                    {

                    }
                    onCaptureImageResult();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO:
                Uri selectedImage = data.getData();
                try {
                    ContentResolver cR = getContentResolver();
                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                    file_ext2 = mime.getExtensionFromMimeType(cR.getType(selectedImage));

                }
                catch (Exception e)
                {

                }
                onSelectFromGalleryResult(data);
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                handleCrop(resultCode, data);
                break;


        }
    }

    /**
     * This method is used for  handel crop result after crop the image.
     */
    private void handleCrop(int resultCode, Intent result) {
        final CropImage.ActivityResult activityResult = CropImage.getActivityResult(result);
        if (resultCode == RESULT_OK) {
            uploadImageFilePath = imageHelper.getFromMediaUriPfd(this, getContentResolver(), activityResult.getUri()).getPath();
            System.out.println("File Name 1st@@@"+uploadImageFilePath);

            new ImageCompression(this).setImageCompressionListener(new ImageCompression
                    .ImageCompressionListener() {
                @Override
                public void onImageCompression(String compressionImagePath) {

                    setProfileImage(activityResult.getUri());
                    System.out.println("File Name 1st!!!"+compressionImagePath);

                    uploadImageFilePath = compressionImagePath;
                    uploadImageFilePath = imageHelper.getFromMediaUriPfd(AddVehicleAfterLoginActivity.this, getContentResolver(), activityResult.getUri()).getPath();

                    System.out.println("File Name 1st$$$"+uploadImageFilePath);

                    System.out.println("File Ext 1st$$$"+file_ext2);


                }
            }).execute(uploadImageFilePath);
        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Utils.showToast(activityResult.getError().getMessage(), this);
        }
    }



    private void setProfileImage(Uri imageUri) {


        PicassoTrustAll.getInstance(getApplicationContext())
                .load(imageUri)
                .error(R.drawable.ellipse)
                .into(img_crd);
    }


    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            picUri = data.getData();
            beginCrop(picUri);
        }
    }
    private void beginCrop(Uri sourceUri) {
        CropImage.activity(sourceUri).setGuidelines(com.theartofdev.edmodo.cropper.CropImageView
                .Guidelines.ON).start(this);
    }
    /**
     * This method is used for handel result after captured image from camera .
     */
    private void onCaptureImageResult() {
        beginCrop(picUri);
    }



    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }
}
