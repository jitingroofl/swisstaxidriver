package com.swissexpressdz.driver;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.models.responsemodels.AddVTCResponse;
import com.swissexpressdz.driver.models.responsemodels.EditVTCResponse;
import com.swissexpressdz.driver.models.responsemodels.GetVtcResponse;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextView_MontserratBold;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditVTCActivity extends BaseAppCompatActivity {
    Bundle b1;
String type_id="",provider_city_type_vtc_id="";
    MyFontEdittextView etdistanceBasePrice,etBaseprice,etPriceperdistance,etWaitingtime,etWaitingtime_price,
            etHourly_price,etCancellation_fees,etProvider_profit,etcarrent_hourlyprice,etcarrent_dayprice;
    private String msg;
    CustomTextView_MontserratBold btnaddvtc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edtvtc);
        getBundleData();
//        initToolBar();
//        setTitleOnToolbar(getResources().getString(R.string.text_add_vehicle));
        getVTCType();
        initUI2();

    }

    private void initUI2() {
        etdistanceBasePrice = (MyFontEdittextView)findViewById(R.id.etdistanceBasePrice);
        etBaseprice = (MyFontEdittextView)findViewById(R.id.etBaseprice);
        etPriceperdistance = (MyFontEdittextView)findViewById(R.id.etPriceperdistance);
        etWaitingtime = (MyFontEdittextView)findViewById(R.id.etWaitingtime);
        etWaitingtime_price = (MyFontEdittextView)findViewById(R.id.etWaitingtime_price);
        etHourly_price = (MyFontEdittextView)findViewById(R.id.etHourly_price);
        etCancellation_fees = (MyFontEdittextView)findViewById(R.id.etCancellation_fees);
        etProvider_profit = (MyFontEdittextView)findViewById(R.id.etProvider_profit);
        etcarrent_hourlyprice = (MyFontEdittextView)findViewById(R.id.etcarrent_hourlyprice);
        etcarrent_dayprice = (MyFontEdittextView)findViewById(R.id.etcarrent_dayprice);

        btnaddvtc = (CustomTextView_MontserratBold) findViewById(R.id.btnaddvtc);
        btnaddvtc.setOnClickListener(this);
    }

    private void getBundleData() {
        b1 = getIntent().getExtras();
        if(b1!= null)
        {
            type_id = b1.getString("type_id");
            provider_city_type_vtc_id = b1.getString("provider_city_type_vtc_id");
        }
    }

    private void getVTCType() {
        Utils.showCustomProgressDialog(EditVTCActivity.this, getResources().getString(R.string
                .msg_waiting_for_getting_credit_cards), false, null);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());



            Call<GetVtcResponse> call = ApiClient.getClient(EditVTCActivity.this).create(ApiInterface.class).provider_get_city_type_vtc_value
                    (ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<GetVtcResponse>() {
                @Override
                public void onResponse(Call<GetVtcResponse> call, Response<GetVtcResponse> response) {
                    if (parseContent.isSuccessful(response)) {

                        if (response.body().getSuccess()) {
                            System.out.println("Provider CityType  "+response.body().getProviderCityTypeVtc());
                            if(response.body().getProviderCityTypeVtc()!= null) {

                                etdistanceBasePrice.setText(response.body().getProviderCityTypeVtc().get(0).getBasePriceDistance()+"");
                                etBaseprice.setText(response.body().getProviderCityTypeVtc().get(0).getBasePrice()+"");
                                etPriceperdistance.setText(response.body().getProviderCityTypeVtc().get(0).getPricePerUnitDistance()+"");
                                etWaitingtime.setText(response.body().getProviderCityTypeVtc().get(0).getWaitingTimeStartAfterMinute()+"");
                                etWaitingtime_price.setText(response.body().getProviderCityTypeVtc().get(0).getPriceForWaitingTime()+"");
                                etHourly_price.setText(response.body().getProviderCityTypeVtc().get(0).getHourlyPrice()+"");
                                etCancellation_fees.setText(response.body().getProviderCityTypeVtc().get(0).getCancellationFee()+"");
                                etProvider_profit.setText(response.body().getProviderCityTypeVtc().get(0).getProviderProfit()+"");
                                etcarrent_hourlyprice.setText(response.body().getProviderCityTypeVtc().get(0).getHourlyPriceCarRent()+"");
                                etcarrent_dayprice.setText(response.body().getProviderCityTypeVtc().get(0).getDayPriceCarRent()+"");

                                Utils.hideCustomProgressDialog();
                            }
                        }
                        else {
                            Utils.hideCustomProgressDialog();
                            Utils.showErrorToast(response.body().getErrorCode(), EditVTCActivity.this);
                        }

                    }

                }

                @Override
                public void onFailure(Call<GetVtcResponse> call, Throwable t) {
                    AppLog.handleThrowable(GetVTCActivity.class.getSimpleName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.VIEW_AND_ADD_PAYMENT_ACTIVITY, e);
        }
    }
    @Override
    protected boolean isValidate() {
        msg = null;

        if (TextUtils.isEmpty(etdistanceBasePrice.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etdistanceBasePrice.requestFocus();
            etdistanceBasePrice.setError(msg);
        } else if (TextUtils.isEmpty(etBaseprice.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etBaseprice.requestFocus();
            etBaseprice.setError(msg);
        }
        else if (TextUtils.isEmpty(etPriceperdistance.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etPriceperdistance.requestFocus();
            etPriceperdistance.setError(msg);
        }
        else if (TextUtils.isEmpty(etWaitingtime.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etWaitingtime.requestFocus();
            etWaitingtime.setError(msg);
        }
        else if (TextUtils.isEmpty(etWaitingtime_price.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etWaitingtime_price.requestFocus();
            etWaitingtime_price.setError(msg);
        }


        else if (TextUtils.isEmpty(etHourly_price.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etHourly_price.requestFocus();
            etHourly_price.setError(msg);
        }
        else if (TextUtils.isEmpty(etCancellation_fees.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etCancellation_fees.requestFocus();
            etCancellation_fees.setError(msg);
        }
        else if (TextUtils.isEmpty(etProvider_profit.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etProvider_profit.requestFocus();
            etProvider_profit.setError(msg);
        }
        else if (TextUtils.isEmpty(etcarrent_hourlyprice.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etcarrent_hourlyprice.requestFocus();
            etcarrent_hourlyprice.setError(msg);
        }
        else if (TextUtils.isEmpty(etcarrent_dayprice.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etcarrent_dayprice.requestFocus();
            etcarrent_dayprice.setError(msg);
        }


        return TextUtils.isEmpty(msg);    }

    @Override
    public void goWithBackArrow() {

    }

    @Override
    public void onClick(View view) {
        if(view == btnaddvtc)
        {
            if(isValidate())
            {
                editVTC();
            }
        }
    }
    private void editVTC() {




        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register valid");

        HashMap<String, RequestBody> map = new HashMap<>();

        map.put(Const.Params.PROVIDER_ID, ApiClient.makeTextRequestBody(preferenceHelper.getProviderId()));
        map.put(Const.Params.TYPEID , ApiClient.makeTextRequestBody(preferenceHelper.getTypeid()));
        map.put(Const.Params.PROVIDER_CITY_TYPE_VTC_ID , ApiClient.makeTextRequestBody(provider_city_type_vtc_id));



        map.put(Const.Params.BASE_PRICE_DISTANCE, ApiClient.makeTextRequestBody(etdistanceBasePrice.getText().toString()));
        map.put(Const.Params.BASE_PRICE, ApiClient.makeTextRequestBody(etBaseprice.getText().toString()));
        map.put(Const.Params.PRICE_PER_UNIT_DISTANCE, ApiClient.makeTextRequestBody(etPriceperdistance.getText().toString()));

        map.put(Const.Params.WAITING_TIME_MINUTES, ApiClient.makeTextRequestBody(etWaitingtime.getText().toString()));
        map.put(Const.Params.WAITING_TIME_PRICE, ApiClient.makeTextRequestBody(etWaitingtime_price.getText().toString()));
        map.put(Const.Params.HOURLY_PRICE, ApiClient.makeTextRequestBody(etHourly_price.getText().toString()));
        map.put(Const.Params.CACELLATION_FEES, ApiClient.makeTextRequestBody(etCancellation_fees.getText().toString()));
        map.put(Const.Params.PROVIDER_PROFIT, ApiClient.makeTextRequestBody(etProvider_profit.getText().toString()));
        map.put(Const.Params.HOURLY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(etcarrent_hourlyprice.getText().toString()));
        map.put(Const.Params.DAY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(etcarrent_dayprice.getText().toString()));
        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_registering), false, null);

        ArrayList<String> arrayList_files=new ArrayList<>();

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[arrayList_files.size()];



        //For First Document
        Call<AddVTCResponse> userDataResponseCall;
        userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                (ApiInterface.class).provider_edit_city_type_vtc_value(
                surveyImagesParts
                , map);

        System.out.println("Map is###"+map.toString());


        userDataResponseCall.enqueue(new Callback<AddVTCResponse>() {
            @Override
            public void onResponse(Call<AddVTCResponse> call,
                                   Response<AddVTCResponse> response) {
                if (parseContent.isSuccessful(response)) {

                    if(response.body().getSuccess())
                    {
                        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register Success");

                        CurrentTrip.getInstance().clear();
                        Utils.hideCustomProgressDialog();
                        onBackPressed();
                    }
                    else {
                        Utils.hideCustomProgressDialog();
                        Utils.showErrorToast(response.body().getErrorCode(), EditVTCActivity.this);
                    }



                }

                else
                {

                }
            }

            @Override
            public void onFailure(Call<AddVTCResponse> call, Throwable t) {
                AppLog.handleThrowable(RegisterActivity_Driver.class.getSimpleName(), t);
            }
        });


    }

    @Override
    public void onBackPressed() {
        //  super.onBackPressed();
        Intent intent = new Intent(this, GetVTCActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }
}
