package com.swissexpressdz.driver;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.swissexpressdz.driver.adapter.DocumentAdaptor;
import com.swissexpressdz.driver.components.CustomDialogEnable;
import com.swissexpressdz.driver.components.CustomPhotoDialog;
import com.swissexpressdz.driver.components.CustomPhotoDialog2;
import com.swissexpressdz.driver.components.MyAppTitleFontTextView;
import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.interfaces.ClickListener;
import com.swissexpressdz.driver.interfaces.RecyclerTouchListener;
import com.swissexpressdz.driver.models.datamodels.Document;
import com.swissexpressdz.driver.models.datamodels.VehicleDetail;
import com.swissexpressdz.driver.models.responsemodels.DocumentNew;
import com.swissexpressdz.driver.models.responsemodels.IsSuccessResponse;
import com.swissexpressdz.driver.models.responsemodels.VehicleDetailResponse;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.ImageCompression;
import com.swissexpressdz.driver.utils.ImageHelper;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddVehicleDetailActivity_Driver extends BaseAppCompatActivity implements AdapterView
        .OnItemSelectedListener {

    public ImageHelper imageHelper;
    private Spinner spinnerYear;
    private ArrayList<String> listYear;
    private ArrayAdapter<String> adapterYear;
    private int currentYear;
    private String passingYear;
    private boolean isAddVehicle = true;
    private MyFontEdittextView edtVehicleName, edtVehicleModel, edtVehiclePlateNo, edtVehicleColor,tvselect_document,tvselect_document2,tvselect_document3,tvselect_document4;
    private LinearLayout llVehicleDocument;
    private RecyclerView rcvVehicleDocument;
    private ArrayList<Document> docList;
    private DocumentAdaptor documentAdaptor;
    private int docListPosition = 0;
    private Dialog documentDialog;
    private ImageView ivDocumentImage;
    private MyFontEdittextView etDocumentNumber, etExpireDate;
    private MyAppTitleFontTextView tvDocumentTitle;
    private TextInputLayout tilDocumentNumber;
    private String uploadImageFilePath = "", expireDate;
    private CustomPhotoDialog customPhotoDialog;
    ArrayList<String> arrayList_files=new ArrayList<>();

    private CustomDialogEnable customDialogEnable;
  //  private Uri picUri;
    private String vehicleId = "";
    private ImageView ivAddVehicleTypeImage;
    private CheckBox cbHandicap, cbBabySeat, cbHotspot;
    String str_document_type1,str_document_file1,str_document_ext1,str_document_type2,str_document_file2,
            str_document_ext2,str_document_type3,str_document_file3,str_document_ext3,
            str_document_type4,str_document_file4,str_document_ext4;
    private Uri picUri,picUri2,picUri3,picUri4,picUri5;

    private CustomPhotoDialog2 customPhotoDialog2,customPhotoDialog3,customPhotoDialog4,customPhotoDialog5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle_detail_driver);
        initToolBar();
        setTitleOnToolbar(getString(R.string.text_manage_vehicles));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        edtVehicleName = (MyFontEdittextView) findViewById(R.id.edtVehicleName);
        edtVehicleModel = (MyFontEdittextView) findViewById(R.id.edtVehicleModel);
        edtVehiclePlateNo = (MyFontEdittextView) findViewById(R.id.edtVehiclePlateNo);
        edtVehicleColor = (MyFontEdittextView) findViewById(R.id.edtVehicleColor);
//        tvselect_document=(MyFontEdittextView)findViewById(R.id.tvselect_document);
//        tvselect_document2=(MyFontEdittextView)findViewById(R.id.tvselect_document2);
//        tvselect_document3=(MyFontEdittextView)findViewById(R.id.tvselect_document3);
//        tvselect_document4=(MyFontEdittextView)findViewById(R.id.tvselect_document4);
//        tvselect_document.setOnClickListener(this);
//        tvselect_document2.setOnClickListener(this);
//        tvselect_document3.setOnClickListener(this);
//        tvselect_document4.setOnClickListener(this);

        llVehicleDocument = (LinearLayout) findViewById(R.id.llVehicleDocument);
        rcvVehicleDocument = (RecyclerView) findViewById(R.id.rcvVehicleDocument);
        ivAddVehicleTypeImage = (ImageView) findViewById(R.id.ivAddVehicleTypeImage);
        cbBabySeat = findViewById(R.id.cbBabySeat);
        cbHandicap = findViewById(R.id.cbHandicap);
        cbHotspot = findViewById(R.id.cbHotspot);
        imageHelper = new ImageHelper(this);
        initVehicleDocumentList();
        initYearSpinner();
        setYearList(currentYear);
        if (getIntent() != null) {
            isAddVehicle = getIntent().getExtras().getBoolean(Const.IS_ADD_VEHICLE);
            vehicleId = getIntent().getExtras().getString(Const.VEHICLE_ID);
        }
        updateEditIcon(isAddVehicle);
        if (!TextUtils.isEmpty(vehicleId)) {
            getProviderVehicleDetail();
        }
    }


    private void initVehicleDocumentList() {
        docList = new ArrayList<>();
        rcvVehicleDocument.setLayoutManager(new LinearLayoutManager(this));
        documentAdaptor = new DocumentAdaptor(this, docList);
        rcvVehicleDocument.setAdapter(documentAdaptor);
        rcvVehicleDocument.addOnItemTouchListener(new RecyclerTouchListener(this,
                rcvVehicleDocument, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                docListPosition = position;
                if (preferenceHelper.getProviderType() != Const.ProviderStatus
                        .PROVIDER_TYPE_PARTNER) {
                    //openDocumentUploadDialog(position);
                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }


    private void initYearSpinner() {
        spinnerYear = (Spinner) findViewById(R.id.spinnerYear);
        listYear = new ArrayList<>();
        currentYear = Calendar.getInstance().get(Calendar.YEAR);
        spinnerYear.setOnItemSelectedListener(this);
    }


    private void setYearList(int Year) {
        listYear.clear();
        for (int i = 0; i < 20; i++) {
            listYear.add(String.valueOf(Year - i));
        }
        adapterYear = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                listYear);
        spinnerYear.setAdapter(adapterYear);
    }


    @Override
    protected boolean isValidate() {
        String msg = null;

        if (TextUtils.isEmpty(edtVehicleName.getText().toString().trim())) {
            msg = getString(R.string.text_message_provide_vehicle_name);
            edtVehicleName.requestFocus();
        } else if (TextUtils.isEmpty(edtVehicleModel.getText().toString().trim())) {
            msg = getString(R.string.text_message_provide_vehicle_model);
            edtVehicleModel.requestFocus();
        } else if (TextUtils.isEmpty(edtVehiclePlateNo.getText().toString().trim())) {
            msg = getString(R.string.text_message_provide_vehicle_plate_no);
            edtVehiclePlateNo.requestFocus();
        } else if (edtVehiclePlateNo.getText().toString().trim().length() > 15) {
            msg = getString(R.string.text_message_provide_vehicle_plate_no);
            edtVehiclePlateNo.requestFocus();
        } else if (TextUtils.isEmpty(edtVehicleColor.getText().toString().trim())) {
            msg = getString(R.string.text_message_provide_vehicle_color);
            edtVehicleColor.requestFocus();
        }

//        else if (TextUtils.isEmpty(tvselect_document.getText().toString().trim())) {
//            msg = getString(R.string.this_field_required);
//            tvselect_document.requestFocus();
//        }
//        else if (TextUtils.isEmpty(tvselect_document2.getText().toString().trim())) {
//            msg = getString(R.string.this_field_required);
//            tvselect_document2.requestFocus();
//        }
//
//        else if (TextUtils.isEmpty(tvselect_document3.getText().toString().trim())) {
//            msg = getString(R.string.this_field_required);
//            tvselect_document3.requestFocus();
//        }
//
//        else if (TextUtils.isEmpty(tvselect_document4.getText().toString().trim())) {
//            msg = getString(R.string.this_field_required);
//            tvselect_document4.requestFocus();
//        }


        if (msg != null) {
            Utils.showToast(msg, this);
            return false;
        }

        return true;
    }


    private void updateUiVehicleDocument(boolean isShow) {
        if (isShow)
            llVehicleDocument.setVisibility(View.VISIBLE);
        else
            llVehicleDocument.setVisibility(View.GONE);
    }


    private void setEditable(boolean isEditable) {
        edtVehicleName.setEnabled(isEditable);
        edtVehicleColor.setEnabled(isEditable);
        edtVehiclePlateNo.setEnabled(isEditable);
        edtVehicleModel.setEnabled(isEditable);
        spinnerYear.setEnabled(isEditable);
        cbBabySeat.setEnabled(isEditable);
        cbHotspot.setEnabled(isEditable);
        cbHandicap.setEnabled(isEditable);
    }

    @Override
    public void goWithBackArrow() {
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivToolbarIcon:
                if (edtVehicleName.isEnabled()) {
                    if (isValidate()) {
                        if (isAddVehicle && TextUtils.isEmpty(vehicleId)) {
                            addVehicle();
                        } else {
                            updateVehicleDetail();
                        }
                    }
                } else {
                    updateEditIcon(true);
                }
                break;

            case R.id.tvselect_document:

                openPhotoDialog2();
                break;

            case R.id.tvselect_document2:
                openPhotoDialog3();
                break;

            case R.id.tvselect_document3:
                openPhotoDialog4();
                break;

            case R.id.tvselect_document4:
                openPhotoDialog5();
                break;
        }
    }

    protected void openPhotoDialog2() {

        if (ContextCompat.checkSelfPermission(AddVehicleDetailActivity_Driver.this, Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission
                (AddVehicleDetailActivity_Driver.this, Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AddVehicleDetailActivity_Driver.this, new String[]{Manifest
                    .permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE2);
        }
        else {

            customPhotoDialog2 = new CustomPhotoDialog2(this) {

                @Override
                public void clickedOnCamera() {
                    customPhotoDialog2.dismiss();
                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog2.dismiss();
                    choosePhotoFromGallery2();
                }
            };
            customPhotoDialog2.show();
        }

    }
    protected void openPhotoDialog3() {


        customPhotoDialog3 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                customPhotoDialog3.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog3.dismiss();
                choosePhotoFromGallery3();
            }
        };
        customPhotoDialog3.show();


    }
    protected void openPhotoDialog4() {


        customPhotoDialog4 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                customPhotoDialog4.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog4.dismiss();
                choosePhotoFromGallery4();
            }
        };
        customPhotoDialog4.show();


    }

    protected void openPhotoDialog5() {


        customPhotoDialog5 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                customPhotoDialog5.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog5.dismiss();
                choosePhotoFromGallery5();
            }
        };
        customPhotoDialog5.show();


    }

    private void choosePhotoFromGallery2() {

        /*Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);*/
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO2);
    }
    private void choosePhotoFromGallery3() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO3);
    }


    private void choosePhotoFromGallery4() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO4);
    }

    private void choosePhotoFromGallery5() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO5);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Const.PIC_URI, picUri);
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        picUri = savedInstanceState.getParcelable(Const.PIC_URI);

    }

    @Override
    public void onAdminApproved() {
        goWithAdminApproved();
    }

    @Override
    public void onAdminDeclined() {
        goWithAdminDecline();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            closedEnableDialogInternet();
        } else {
            openInternetDialog();
        }
    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(Activity.RESULT_OK);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        passingYear = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private void addVehicle() {

        HashMap<String, RequestBody> map = new HashMap<>();

        JSONObject jsonObject = new JSONObject();
        Utils.showCustomProgressDialog(this, "", false, null);
        try {
            map.put(Const.Params.PROVIDER_ID, ApiClient.makeTextRequestBody(preferenceHelper.getProviderId()));
            map.put(Const.Params.TOKEN, ApiClient.makeTextRequestBody(preferenceHelper.getSessionToken()));
            map.put(Const.Params.VEHICLE_NAME, ApiClient.makeTextRequestBody(edtVehicleName.getText().toString()
                    .trim()));
            map.put(Const.Params.PLATE_NO, ApiClient.makeTextRequestBody(edtVehiclePlateNo.getText().toString()
                    .trim()));
            map.put(Const.Params.MODEL, ApiClient.makeTextRequestBody(edtVehicleModel.getText().toString().trim()));
            map.put(Const.Params.COLOR, ApiClient.makeTextRequestBody(edtVehicleColor.getText().toString().trim()));
            map.put(Const.Params.PASSING_YEAR, ApiClient.makeTextRequestBody(passingYear));
            map.put(Const.Params.ACCESSIBILITY, ApiClient.makeTextRequestBody(getAccessibility()));
            MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[arrayList_files.size()];
//            arrayList_files.add(str_document_file1);
//            arrayList_files.add(str_document_file2);
//            arrayList_files.add(str_document_file3);
//            arrayList_files.add(str_document_file4);
//
//
//            map.put(Const.Params.VEHICLE_DOC_EXT, ApiClient.makeTextRequestBody(str_document_ext1));
//            map.put(Const.Params.VEHICLE_DOC_TYPE, ApiClient.makeTextRequestBody(tvselect_document.getText().toString()));
//
//            map.put(Const.Params.VEHICLE_DOC_EXT2, ApiClient.makeTextRequestBody(str_document_ext2));
//            map.put(Const.Params.VEHICLE_DOC_TYPE2, ApiClient.makeTextRequestBody(tvselect_document.getText().toString()));
//
//            map.put(Const.Params.VEHICLE_DOC_EXT3, ApiClient.makeTextRequestBody(str_document_ext3));
//            map.put(Const.Params.VEHICLE_DOC_TYPE3, ApiClient.makeTextRequestBody(tvselect_document.getText().toString()));
//
//            map.put(Const.Params.VEHICLE_DOC_EXT4, ApiClient.makeTextRequestBody(str_document_ext4));
//            map.put(Const.Params.VEHICLE_DOC_TYPE4, ApiClient.makeTextRequestBody(tvselect_document4.getText().toString()));
//            MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[arrayList_files.size()];
//
//            File file = new File(arrayList_files.get(0));
//            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
//            surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.VEHICLE_DOC_DATA, file.getName(), surveyBody);
//
//            File file2 = new File(arrayList_files.get(1));
//            RequestBody surveyBody2 = RequestBody.create(MediaType.parse("image/*"), file2);
//            surveyImagesParts[1] = MultipartBody.Part.createFormData(Const.Params.VEHICLE_DOC_DATA2, file2.getName(), surveyBody2);
//
//            File file3 = new File(arrayList_files.get(2));
//            RequestBody surveyBody3 = RequestBody.create(MediaType.parse("image/*"), file3);
//            surveyImagesParts[2] = MultipartBody.Part.createFormData(Const.Params.VEHICLE_DOC_DATA3, file3.getName(), surveyBody3);
//
//            File file4 = new File(arrayList_files.get(3));
//            RequestBody surveyBody4 = RequestBody.create(MediaType.parse("image/*"), file4);
//            surveyImagesParts[3] = MultipartBody.Part.createFormData(Const.Params.VEHICLE_DOC_DATA4, file4.getName(), surveyBody4);

            Call<VehicleDetailResponse> vehicleDetailResponseCall;
            vehicleDetailResponseCall = ApiClient.getClient(getApplicationContext()).create
                    (ApiInterface.class).addVehicleDetail_Doc(
                    surveyImagesParts
                    , map);

//            Call<VehicleDetailResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
//                    .addVehicleDetail_Doc(ApiClient.makeJSONRequestBody(jsonObject));
            vehicleDetailResponseCall.enqueue(new Callback<VehicleDetailResponse>() {
                @Override
                public void onResponse(Call<VehicleDetailResponse> call,
                                       Response<VehicleDetailResponse> response) {
                    if (parseContent.isSuccessful(response)) {
                        setVehicleDetail(response.body());
                      //  setVehicleData(response.body().getDocumentList());
                        getProviderVehicleDetail();
                    }

                }

                @Override
                public void onFailure(Call<VehicleDetailResponse> call, Throwable t) {
                    AppLog.handleThrowable(AddVehicleDetailActivity_Driver.class.getSimpleName(), t);

                }
            });
        } catch (Exception e) {
            AppLog.handleException("PROVIDER_ADD_VEHICLE", e);
        }
    }

    private void setVehicleData(DocumentNew docList) {
        try {

            //For First Document
            str_document_type1=docList.getVehicleDocumentType();
            str_document_file1=docList.getVehicleDocumentData();
            str_document_ext1=docList.getVehicleDocumentExt();
            tvselect_document.setText(str_document_type1+"");
            //   String replace_str_one=docList.get(0).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");
            tvselect_document.setOnClickListener(this);

            //For Second Document
            str_document_type2=docList.getVehicleDocumentType2();
            str_document_file2=docList.getVehicleDocumentData2();
            str_document_ext2=docList.getVehicleDocumentExt2();
            tvselect_document2.setText(str_document_type2+"");
            //  String replace_str_two=docList.get(1).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");
            tvselect_document2.setOnClickListener(this);



            //For Third Document
            str_document_type3=docList.getVehicleDocumentType3();
            str_document_file3=docList.getVehicleDocumentData3();
            str_document_ext4=docList.getVehicleDocumentExt3();
            tvselect_document3.setText(str_document_type3+"");
            tvselect_document3.setOnClickListener(this);

            //For Fourth Document
            str_document_type4=docList.getVehicleDocumentType4();
            str_document_file4=docList.getVehicleDocumentData4();
            str_document_ext4=docList.getVehicleDocumentExt4();
            tvselect_document4.setText(str_document_type4+"");

            tvselect_document4.setOnClickListener(this);
        }

        catch (Exception e)
        {

        }



    }


    private void setVehicleDetail(VehicleDetailResponse response) {
        if (response.isSuccess()) {
            VehicleDetail vehicleDetail = response.getVehicleDetail();
          //  setAccessibility(vehicleDetail.getAccessibility());
            edtVehicleName.setText(vehicleDetail.getName());
            edtVehicleModel.setText(vehicleDetail.getModel());
            edtVehiclePlateNo.setText(vehicleDetail.getPlateNo());
            edtVehicleColor.setText(vehicleDetail.getColor());
            vehicleId = vehicleDetail.getId();
            for (int i = 0; i < listYear.size(); i++) {
                if (TextUtils.equals(listYear.get(i), vehicleDetail
                        .getPassingYear())) {
                    spinnerYear.setSelection(i);
                }
            }

            if (TextUtils.isEmpty(vehicleDetail.getServiceType())) {
                ivAddVehicleTypeImage.setVisibility(View.GONE);
            } else {
                ivAddVehicleTypeImage.setVisibility(View.VISIBLE);
//                Glide.with(AddVehicleDetailActivity_Driver.this).load(ApiClient.Base_URL +
//                        vehicleDetail.getTypeImageUrl())
//                        .placeholder(R.drawable.car_placeholder)
//                        .into(ivAddVehicleTypeImage);

                PicassoTrustAll.getInstance(AddVehicleDetailActivity_Driver.this)
                        .load(ApiClient.Base_URL +vehicleDetail.getTypeImageUrl())
                        .error(R.drawable.car_placeholder)
                        .into(ivAddVehicleTypeImage);
            }
           // docList.addAll(response.getDocumentList());
            if (docList.size() > 0) {
                updateUiVehicleDocument(true);
                documentAdaptor.notifyDataSetChanged();
            } else {
                updateUiVehicleDocument(false);
            }

            updateEditIcon(false);
        } else {
            updateUiVehicleDocument(false);
        }
        Utils.hideCustomProgressDialog();
    }

    private void updateVehicleDetail() {
        Utils.showCustomProgressDialog(this, "", false, null);

        HashMap<String, RequestBody> map = new HashMap<>();

        JSONObject jsonObject = new JSONObject();
        Utils.showCustomProgressDialog(this, "", false, null);
        try {
            map.put(Const.Params.PROVIDER_ID, ApiClient.makeTextRequestBody(preferenceHelper.getProviderId()));
            map.put(Const.Params.TOKEN, ApiClient.makeTextRequestBody(preferenceHelper.getSessionToken()));
            map.put(Const.Params.VEHICLE_ID, ApiClient.makeTextRequestBody(vehicleId));
            map.put(Const.Params.VEHICLE_NAME, ApiClient.makeTextRequestBody(edtVehicleName.getText().toString()
                    .trim()));
            map.put(Const.Params.PLATE_NO, ApiClient.makeTextRequestBody(edtVehiclePlateNo.getText().toString()
                    .trim()));
            map.put(Const.Params.MODEL, ApiClient.makeTextRequestBody(edtVehicleModel.getText().toString().trim()));
            map.put(Const.Params.COLOR, ApiClient.makeTextRequestBody(edtVehicleColor.getText().toString().trim()));
            map.put(Const.Params.PASSING_YEAR, ApiClient.makeTextRequestBody(passingYear));
            map.put(Const.Params.ACCESSIBILITY, ApiClient.makeTextRequestBody(getAccessibility()));
            arrayList_files.add(str_document_file1);
            arrayList_files.add(str_document_file2);
            arrayList_files.add(str_document_file3);
            arrayList_files.add(str_document_file4);


            map.put(Const.Params.VEHICLE_DOC_EXT, ApiClient.makeTextRequestBody(str_document_ext1));
            map.put(Const.Params.VEHICLE_DOC_TYPE, ApiClient.makeTextRequestBody(tvselect_document.getText().toString()));

            map.put(Const.Params.VEHICLE_DOC_EXT2, ApiClient.makeTextRequestBody(str_document_ext2));
            map.put(Const.Params.VEHICLE_DOC_TYPE2, ApiClient.makeTextRequestBody(tvselect_document.getText().toString()));

            map.put(Const.Params.VEHICLE_DOC_EXT3, ApiClient.makeTextRequestBody(str_document_ext3));
            map.put(Const.Params.VEHICLE_DOC_TYPE3, ApiClient.makeTextRequestBody(tvselect_document.getText().toString()));

            map.put(Const.Params.VEHICLE_DOC_EXT4, ApiClient.makeTextRequestBody(str_document_ext4));
            map.put(Const.Params.VEHICLE_DOC_TYPE4, ApiClient.makeTextRequestBody(tvselect_document4.getText().toString()));
            MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[arrayList_files.size()];

            File file = new File(arrayList_files.get(0));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.VEHICLE_DOC_DATA, file.getName(), surveyBody);

            File file2 = new File(arrayList_files.get(1));
            RequestBody surveyBody2 = RequestBody.create(MediaType.parse("image/*"), file2);
            surveyImagesParts[1] = MultipartBody.Part.createFormData(Const.Params.VEHICLE_DOC_DATA2, file2.getName(), surveyBody2);

            File file3 = new File(arrayList_files.get(2));
            RequestBody surveyBody3 = RequestBody.create(MediaType.parse("image/*"), file3);
            surveyImagesParts[2] = MultipartBody.Part.createFormData(Const.Params.VEHICLE_DOC_DATA3, file3.getName(), surveyBody3);

            File file4 = new File(arrayList_files.get(3));
            RequestBody surveyBody4 = RequestBody.create(MediaType.parse("image/*"), file4);
            surveyImagesParts[3] = MultipartBody.Part.createFormData(Const.Params.VEHICLE_DOC_DATA4, file4.getName(), surveyBody4);

            Call<IsSuccessResponse> vehicleDetailResponseCall;
            vehicleDetailResponseCall = ApiClient.getClient(getApplicationContext()).create
                    (ApiInterface.class).updateVehicleDetailNew(
                    surveyImagesParts
                    , map);

//            Call<VehicleDetailResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
//                    .addVehicleDetail_Doc(ApiClient.makeJSONRequestBody(jsonObject));
            vehicleDetailResponseCall.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call,
                                       Response<IsSuccessResponse> response) {
                    if (parseContent.isSuccessful(response)) {
                        if (response.body().isSuccess()) {
                            Utils.showToast(getString(R.string
                                            .message_detail_vehicle_successfully_added),
                                    AddVehicleDetailActivity_Driver.this);
                            onBackPressed();
                        }
                        Utils.hideCustomProgressDialog();
                    //  setVehicleData(response.body().getDocumentList());
                    }

                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(AddVehicleDetailActivity_Driver.class.getSimpleName(), t);

                }
            });
        } catch (Exception e) {
            AppLog.handleException("PROVIDER_ADD_VEHICLE", e);
        }

        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.VEHICLE_NAME, edtVehicleName.getText().toString()
                    .trim());
            jsonObject.put(Const.Params.VEHICLE_ID, vehicleId);
            jsonObject.put(Const.Params.PLATE_NO, edtVehiclePlateNo.getText().toString()
                    .trim());
            jsonObject.put(Const.Params.MODEL, edtVehicleModel.getText().toString().trim());
            jsonObject.put(Const.Params.COLOR, edtVehicleColor.getText().toString().trim());
            jsonObject.put(Const.Params.PASSING_YEAR, passingYear);
        //    jsonObject.put(Const.Params.ACCESSIBILITY, getAccessibility());


            Call<IsSuccessResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .updateVehicleDetail(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    if (parseContent.isSuccessful(response)) {
                        if (response.body().isSuccess()) {
                            Utils.showToast(getString(R.string
                                            .message_detail_vehicle_successfully_added),
                                    AddVehicleDetailActivity_Driver.this);
                            onBackPressed();
                        }
                        Utils.hideCustomProgressDialog();
                    }


                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(AddVehicleDetailActivity_Driver.class.getSimpleName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException("PROVIDER_ADD_VEHICLE", e);
        }
    }


    private void getProviderVehicleDetail() {
        Utils.showCustomProgressDialog(this, "", false, null);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.TOKEN, preferenceHelper.getSessionToken());
            jsonObject.put(Const.Params.VEHICLE_ID, vehicleId);
            Call<VehicleDetailResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .getVehicleDetail(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<VehicleDetailResponse>() {
                @Override
                public void onResponse(Call<VehicleDetailResponse> call,
                                       Response<VehicleDetailResponse>
                                               response) {
                    if (parseContent.isSuccessful(response)) {
                        setVehicleDetail(response.body());
                   //     getProviderVehicleDetail();
               //       setVehicleData(response.body().getDocumentList());

                    }

                }

                @Override
                public void onFailure(Call<VehicleDetailResponse> call, Throwable t) {
                    AppLog.handleThrowable(AddVehicleDetailActivity_Driver.class.getSimpleName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException("GET_PROVIDER_VEHICLE_DETAIL", e);
        }

    }


/*
    private void openDocumentUploadDialog(final int position) {

        if (documentDialog != null && documentDialog.isShowing()) {
            return;
        }

        final Document document = docList.get(position);
        expireDate = "";
        documentDialog = new Dialog(this);
        documentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        documentDialog.setContentView(R.layout.dialog_document_upload);
        ivDocumentImage = (ImageView) documentDialog.findViewById(R.id.ivDocumentImage);
        etDocumentNumber = (MyFontEdittextView) documentDialog.findViewById(R.id.etDocumentNumber);
        etExpireDate = (MyFontEdittextView) documentDialog.findViewById(R.id.etExpireDate);
        tilDocumentNumber = (TextInputLayout) documentDialog.findViewById(R.id.tilDocumentNumber);
        tvDocumentTitle = (MyAppTitleFontTextView) documentDialog.findViewById(R.id
                .tvDocumentTitle);
        tvDocumentTitle.setText(document.getName());
//        Glide.with(this).load(ApiClient.Base_URL + document.getDocumentPicture()).dontAnimate().fallback(R
//                .drawable.ellipse).override(200, 200).placeholder(R
//                .drawable.ellipse)
//                .into(ivDocumentImage);

        PicassoTrustAll.getInstance(AddVehicleDetailActivity_Driver.this)
                .load(ApiClient.Base_URL +document.getDocumentPicture())
                .error(R.drawable.car_placeholder)
                .resize(200,200)
                .into(ivDocumentImage);

        if (document.isIsExpiredDate()) {
            etExpireDate.setVisibility(View.VISIBLE);


            try {
                etExpireDate.setText(ParseContent.getInstance().dateFormat.format(ParseContent
                        .getInstance()
                        .webFormatWithLocalTimeZone
                        .parse(document.getExpiredDate())));
                Date date = parseContent.dateFormat.parse(etExpireDate.getText().toString());
                date.setTime(date.getTime() + 86399000);// 86399000 add 24 hour
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Const
                        .DATE_TIME_FORMAT_WEB, Locale.US);
                expireDate = simpleDateFormat.format(date);
            } catch (ParseException e) {
                AppLog.handleException(TAG, e);
            }
        }
        if (document.isIsUniqueCode()) {
            tilDocumentNumber.setVisibility(View.VISIBLE);
            etDocumentNumber.setText(document.getUniqueCode());
        }

        documentDialog.findViewById(R.id.btnDialogDocumentSubmit).setOnClickListener(new View
                .OnClickListener() {


            @Override
            public void onClick(View view) {
                Date date = null;
                if (!TextUtils.isEmpty(expireDate)) {
                    try {
                        date = parseContent.dateFormat.parse(expireDate);
                    } catch (ParseException e) {
                        AppLog.handleException(TAG, e);
                    }
                }
                if ((TextUtils.isEmpty(expireDate) && document
                        .isIsExpiredDate()) || isExpiredDate(date)) {
                    Utils.showToast(getResources().getString(R.string
                            .msg_plz_enter_document_expire_date), AddVehicleDetailActivity_Driver.this);

                } else if (TextUtils.isEmpty(etDocumentNumber.getText().toString().trim()) &&
                        document.isIsUniqueCode()) {
                    Utils.showToast(getResources().getString(R.string
                            .msg_plz_enter_document_unique_code), AddVehicleDetailActivity_Driver.this);

                } else if (TextUtils.isEmpty(uploadImageFilePath) && TextUtils.isEmpty(document
                        .getDocumentPicture())) {
                    Utils.showToast(getResources().getString(R.string
                            .msg_plz_select_document_image), AddVehicleDetailActivity_Driver.this);
                } else {
                    documentDialog.dismiss();
                    uploadVehicleDocument(expireDate, etDocumentNumber
                            .getText().toString(), document.getId(), position);
                    expireDate = "";
                }
            }
        });

        documentDialog.findViewById(R.id.btnDialogDocumentCancel).setOnClickListener(new View
                .OnClickListener() {


            @Override
            public void onClick(View view) {
                documentDialog.dismiss();
            }
        });
        ivDocumentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPhotoDialog();
            }
        });
        etExpireDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePickerDialog();
            }
        });
        WindowManager.LayoutParams params = documentDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        documentDialog.getWindow().setAttributes(params);
        documentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        documentDialog.setCancelable(false);
        documentDialog.show();

    }
*/

    protected void openPhotoDialog() {

        if (ContextCompat.checkSelfPermission(AddVehicleDetailActivity_Driver.this, android.Manifest
                .permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission
                (AddVehicleDetailActivity_Driver.this, android.Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AddVehicleDetailActivity_Driver.this, new
                    String[]{android.Manifest
                    .permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
        } else {
            customPhotoDialog = new CustomPhotoDialog(this) {
                @Override
                public void clickedOnCamera() {
                    customPhotoDialog.dismiss();
                    takePhotoFromCamera();
                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog.dismiss();
                    choosePhotoFromGallery();
                }
            };
            customPhotoDialog.show();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                    goWithCameraAndStoragePermission(grantResults);
                    break;
                default:
                    // do with default
                    break;
            }
        }

    }

    private void goWithCameraAndStoragePermission(int[] grantResults) {
        if (grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            openPhotoDialog();
        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest
                    .permission.CAMERA)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
                openPermissionNotifyDialog(Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
            }
        } else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest
                    .permission.READ_EXTERNAL_STORAGE)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
                openPermissionNotifyDialog(Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
            }
        }
    }

    private void openCameraPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogEnable(this, getResources().getString(R.string
                .msg_reason_for_camera_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void doWithEnable() {
                ActivityCompat.requestPermissions(AddVehicleDetailActivity_Driver.this, new
                        String[]{android.Manifest
                        .permission
                        .CAMERA, android.Manifest
                        .permission
                        .READ_EXTERNAL_STORAGE}, Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
                closedPermissionDialog();
            }

            @Override
            public void doWithDisable() {
                closedPermissionDialog();
            }
        };
        customDialogEnable.show();
    }

    private void closedPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            customDialogEnable.dismiss();
            customDialogEnable = null;

        }
    }


    private void openPermissionNotifyDialog(final int code) {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogEnable(this, getResources()
                .getString(R.string
                        .msg_permission_notification), getResources()
                .getString(R
                        .string.text_exit_caps), getResources().getString(R
                .string
                .text_settings)) {
            @Override
            public void doWithEnable() {
                closedPermissionDialog();
                startActivityForResult(getIntentForPermission(), code);

            }

            @Override
            public void doWithDisable() {
                closedPermissionDialog();
                finishAffinity();
            }
        };
        customDialogEnable.show();
    }


    private void choosePhotoFromGallery() {

       /* Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);*/

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Utils.isNougat()) {
            picUri = FileProvider.getUriForFile(this, getPackageName(), imageHelper
                    .createImageFile());
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            picUri = Uri.fromFile(imageHelper.createImageFile());
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, Const.ServiceCode.TAKE_PHOTO);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.ServiceCode.TAKE_PHOTO:
                if (resultCode == RESULT_OK) {
                    onCaptureImageResult();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO:
                onSelectFromGalleryResult(data);
                break;
            case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                openPhotoDialog();
                break;
            case Const.ServiceCode.CHOOSE_PHOTO2:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext1 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        //str_document_ext1 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        str_document_file1 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri2=selectedImage;

                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext1);

                          tvselect_document.setText(str_document_file1+ "");


                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                break;

            case Const.ServiceCode.CHOOSE_PHOTO3:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext2 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext2);

                        str_document_file2 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri3=selectedImage;


                        tvselect_document2.setText(str_document_file2 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case Const.ServiceCode.CHOOSE_PHOTO4:
                try {
                    Uri selectedImage = data.getData();
                    try {

                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext3 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext3);

                        // str_document_file3 = selectedImage.toString() + "." + str_document_ext3;
                        str_document_file3 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri4=selectedImage;
                        tvselect_document3.setText(str_document_file3 + "");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO5:
                try {
                    Uri selectedImage = data.getData();
                    try {

                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext4 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext4);
                        //  str_document_file4 = selectedImage.toString() + "." + str_document_ext4;
                        str_document_file4 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri5=selectedImage;
                        tvselect_document4.setText(str_document_file4 + "");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            default:
                // do with default
                break;

        }
    }

    private void onCaptureImageResult() {
        uploadImageFilePath = ImageHelper.getFromMediaUriPfd(this, getContentResolver
                (), picUri).getPath();
        setDocumentImage(picUri);
    }

    private void setDocumentImage(final Uri imageUri) {

        new ImageCompression(this).setImageCompressionListener(new ImageCompression
                .ImageCompressionListener() {

            @Override
            public void onImageCompression(String compressionImagePath) {
                if (documentDialog != null && documentDialog.isShowing()) {
                    uploadImageFilePath = compressionImagePath;
                    if (documentDialog != null && documentDialog.isShowing()) {
//                        Glide.with(AddVehicleDetailActivity_Driver.this).load(imageUri).fallback(R
//                                .drawable.ellipse).override(200, 200)
//                                .into(ivDocumentImage);

                        PicassoTrustAll.getInstance(AddVehicleDetailActivity_Driver.this)
                                .load(imageUri)
                                .error(R.drawable.ellipse)
                                .resize(200,200)
                                .into(ivDocumentImage);
                    }
                }
            }
        }).execute(uploadImageFilePath);
    }

    /**
     * This method is used for handel result after select image from gallery .
     */

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            picUri = data.getData();
            // picUri = imageHelper.getPhotosUri(imageHelper.getRealPathFromURI(picUri));
            uploadImageFilePath = ImageHelper.getFromMediaUriPfd(this, getContentResolver
                    (), picUri).getPath();
            setDocumentImage(picUri);
        }
    }


    private void openDatePickerDialog() {


        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 0);
        final int currentYear = calendar.get(Calendar.YEAR);
        final int currentMonth = calendar.get(Calendar.MONTH);
        final int currentDate = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog
                .OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            }
        };
        final DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                onDateSetListener, currentYear,
                currentMonth,
                currentDate);
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, this
                .getResources()
                .getString(R.string.text_select), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (documentDialog != null && datePickerDialog.isShowing()) {

                    calendar.set(Calendar.YEAR, datePickerDialog.getDatePicker().getYear());
                    calendar.set(Calendar.MONTH, datePickerDialog.getDatePicker().getMonth());
                    calendar.set(Calendar.DAY_OF_MONTH, datePickerDialog.getDatePicker()
                            .getDayOfMonth());
                    etExpireDate.setText(parseContent.dateFormat.format(calendar.getTime()));
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Const
                            .DATE_TIME_FORMAT_WEB, Locale.US);
                    expireDate = simpleDateFormat.format(calendar.getTime());
                }
            }
        });
        long now = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMinDate(now + 86400000);
        datePickerDialog.show();
    }


/*
    private void uploadVehicleDocument(String expireDate, String uniqueCode, String documentId,
                                       final int position) {
        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                        .msg_waiting_for_upload_document), false,
                null);
        HashMap<String, RequestBody> map = new HashMap<>();

        try {
            map.put(Const.Params.DOCUMENT_ID, ApiClient.makeTextRequestBody(documentId));
            map.put(Const.Params.TOKEN, ApiClient.makeTextRequestBody(preferenceHelper
                    .getSessionToken()));
            map.put(Const.Params.UNIQUE_CODE, ApiClient.makeTextRequestBody(uniqueCode));
            map.put(Const.Params.EXPIRED_DATE, ApiClient.makeTextRequestBody(TextUtils.isEmpty
                    (expireDate) ? "" : expireDate));
            map.put(Const.Params.PROVIDER_ID, ApiClient.makeTextRequestBody(preferenceHelper
                    .getProviderId()));
            map.put(Const.Params.VEHICLE_ID, ApiClient.makeTextRequestBody(vehicleId));

            Call<VehicleDocumentResponse> userDataResponseCall;
            if (!TextUtils.isEmpty(uploadImageFilePath) || picUri != null) {
                userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                        (ApiInterface.class).uploadVehicleDocument(ApiClient
                                .makeMultipartRequestBody
                                        (this, (TextUtils.isEmpty
                                                (uploadImageFilePath) ?
                                                ImageHelper
                                                        .getFromMediaUriPfd(this,
                                                                getContentResolver(),
                                                                picUri).getPath() :
                                                uploadImageFilePath), Const.Params.PICTURE_DATA)
                        , map);
            } else {
                userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                        (ApiInterface.class).uploadVehicleDocument(null, map);
            }
            userDataResponseCall.enqueue(new Callback<VehicleDocumentResponse>() {
                @Override
                public void onResponse(Call<VehicleDocumentResponse> call,
                                       Response<VehicleDocumentResponse> response) {
                    if (parseContent.isSuccessful(response)) {
                        uploadImageFilePath = "";
                        picUri=null;
                        if (response.body().isSuccess()) {
                            Document document = docList.get(position);
                            Document documentResponse = response.body().getDocuments();
                            document.setDocumentPicture(documentResponse.getDocumentPicture());
                            document.setExpiredDate(documentResponse.getExpiredDate());
                            document.setUniqueCode(documentResponse.getUniqueCode());
                            document.setIsUploaded(documentResponse.getIsUploaded());
                            documentAdaptor.notifyDataSetChanged();
                            Utils.hideCustomProgressDialog();
                        } else {
                            Utils.hideCustomProgressDialog();
                            Utils.showErrorToast(response.body().getErrorCode(),
                                    AddVehicleDetailActivity_Driver
                                            .this);
                        }


                    }

                }

                @Override
                public void onFailure(Call<VehicleDocumentResponse> call, Throwable t) {
                    AppLog.handleThrowable(DocumentActivity_Driver.class.getSimpleName(), t);

                }
            });
        } catch (Exception e) {
            AppLog.handleException(Const.Tag.DOCUMENT_ACTIVITY, e);
        }

    }
*/


    private void updateEditIcon(boolean isEditable) {
        setEditable(isEditable);
        if (preferenceHelper.getProviderType() != Const.ProviderStatus
                .PROVIDER_TYPE_PARTNER) {
            setToolbarIcon(AppCompatResources.getDrawable(this, isEditable ? R.drawable
                    .ic_done_black_24dp : R.drawable.ic_mode_edit_black_24dp), this);
        }


    }

    private JSONArray getAccessibility() {
        JSONArray jsonArray = new JSONArray();
        if (cbHandicap.isChecked()) {
            jsonArray.put(Const.Accessibility.HANDICAP);
        }
        if (cbHotspot.isChecked()) {
            jsonArray.put(Const.Accessibility.HOTSPOT);
        }
        if (cbBabySeat.isChecked()) {
            jsonArray.put(Const.Accessibility.BABY_SEAT);
        }
        return jsonArray;
    }

    private void setAccessibility(List<String> stringList) {
//        if (TextUtils.equals(stringList, Const.Accessibility.HANDICAP)) {
//            cbHandicap.setChecked(true);
//        }
//        if (TextUtils.equals(stringList, Const.Accessibility.BABY_SEAT)) {
//            cbBabySeat.setChecked(true);
//        }
//        if (TextUtils.equals(stringList, Const.Accessibility.HOTSPOT)) {
//            cbHotspot.setChecked(true);
//        }


        if (stringList != null) {
            for (int i = 0; i < stringList.size(); i++) {
                if (TextUtils.equals(stringList.get(i), Const.Accessibility.HANDICAP)) {
                    cbHandicap.setChecked(true);
                }
                if (TextUtils.equals(stringList.get(i), Const.Accessibility.BABY_SEAT)) {
                    cbBabySeat.setChecked(true);
                }
                if (TextUtils.equals(stringList.get(i), Const.Accessibility.HOTSPOT)) {
                    cbHotspot.setChecked(true);
                }

            }
        }

    }

    private boolean isExpiredDate(Date date) {
        return date != null && date.before(new Date());


    }
}
