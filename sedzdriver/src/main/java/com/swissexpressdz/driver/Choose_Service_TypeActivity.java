package com.swissexpressdz.driver;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.swissexpressdz.driver.models.responsemodels.AddServiceResponse;
import com.swissexpressdz.driver.models.responsemodels.IsSuccessResponse;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextViewBold;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Choose_Service_TypeActivity extends BaseAppCompatActivity {

    String service_one,service_two = "",taxi = "",vtc="",rental = "",service_three = "",type = "",
            car = "", public_transport = "",bicycle = "",motorbike= "";
    boolean service1_selected= true,service2_selected=false;
    CustomTextViewBold txt_parcel,txt_taxi,txt_fooddelivery,txt_taxi_inside,txt_vtc_inside,txt_rentalprice_inside;
    RelativeLayout rr_check_parcel,rr_uncheck_parcel,rr_check_taxi,rr_uncheck_taxi,rr_check_fooddelivery,rr_uncheck_fooddelivery,
            rr_check_taxi_inside,rr_uncheck_taxi_inside,rr_check_vtc_inside,rr_uncheck_vtc_inside,rr_back,
            rr_check_rentalprice_inside,rr_uncheck_rentalprice_inside,rr_continue;
RadioGroup rdo_slctin;
Bundle b1;
    private boolean isAddVehicle;
    private String vehicleId = "",beforelogin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_servicetype);
        getBundleData();
        initUi();

    }

    private void getBundleData() {
        b1 = getIntent().getExtras();
        if(getIntent() != null)
        {
            System.out.println("Bundle is "+getIntent());


            isAddVehicle = getIntent().getExtras().getBoolean(Const.IS_ADD_VEHICLE);
            vehicleId = getIntent().getExtras().getString(Const.VEHICLE_ID);
            beforelogin = getIntent().getExtras().getString("beforelogin");
            System.out.println("Before Login "+beforelogin);
        }

    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    public void goWithBackArrow() {
        onBackPressed();

    }

    private void initUi() {
        txt_parcel = (CustomTextViewBold) findViewById(R.id.txt_parcel);
     //   service_one = txt_parcel.getText().toString();

        rdo_slctin = (RadioGroup) findViewById(R.id.rdo_slctin);


        txt_taxi = (CustomTextViewBold) findViewById(R.id.txt_taxi);
        service_two = txt_taxi.getText().toString();
        System.out.println("Service two "+service_two);

        txt_fooddelivery = (CustomTextViewBold) findViewById(R.id.txt_fooddelivery);
        txt_taxi_inside = (CustomTextViewBold) findViewById(R.id.txt_taxi_inside);
        txt_vtc_inside= (CustomTextViewBold) findViewById(R.id.txt_vtc_inside);
        txt_rentalprice_inside = (CustomTextViewBold) findViewById(R.id.txt_rentalprice_inside);

        rr_uncheck_parcel = (RelativeLayout) findViewById(R.id.rr_uncheck_parcel);
        rr_check_parcel = (RelativeLayout) findViewById(R.id.rr_check_parcel);

        rr_uncheck_taxi = (RelativeLayout) findViewById(R.id.rr_uncheck_taxi);
        rr_check_taxi = (RelativeLayout) findViewById(R.id.rr_check_taxi);

        rr_check_fooddelivery  = (RelativeLayout) findViewById(R.id.rr_check_fooddelivery);
        rr_uncheck_fooddelivery  = (RelativeLayout) findViewById(R.id.rr_uncheck_fooddelivery);

        rr_uncheck_taxi_inside = (RelativeLayout) findViewById(R.id.rr_uncheck_taxi_inside);
        rr_check_taxi_inside = (RelativeLayout) findViewById(R.id.rr_check_taxi_inside);

        rr_check_vtc_inside=(RelativeLayout) findViewById(R.id.rr_check_vtc_inside);
        rr_uncheck_vtc_inside = (RelativeLayout) findViewById(R.id.rr_uncheck_vtc_inside);

        rr_check_rentalprice_inside = (RelativeLayout) findViewById(R.id.rr_check_rentalprice_inside);
        rr_uncheck_rentalprice_inside = (RelativeLayout) findViewById(R.id.rr_uncheck_rentalprice_inside);

        rr_continue = (RelativeLayout) findViewById(R.id.rr_continue);
        rr_continue.setOnClickListener(this);

        rr_back = (RelativeLayout) findViewById(R.id.rr_back);
        rr_back.setOnClickListener(this);

//        rdo_slctin.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
//        {
//            public void onCheckedChanged(RadioGroup group, int checkedId)
//            {
//                if(!service_three.equalsIgnoreCase("")) {
//
//                    // This will get the radiobutton that has changed in its check state
//                    RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
//                    // This puts the value (true/false) into the variable
//                    boolean isChecked = checkedRadioButton.isChecked();
//                    // If the radiobutton that has changed in check state is now checked...
//                    if (isChecked) {
//                        // Changes the textview's text to "Checked: example radiobutton text"
//                        System.out.println("Checked Value " + checkedRadioButton.getText().toString());
//                        type = checkedRadioButton.getText().toString();
//
//                        if(type.equalsIgnoreCase(getResources().getString(R.string.text_car)))
//                        {
//                            car = type;
//                            public_transport = "";
//                                    bicycle = "";
//                                    motorbike= "";
//                        }
//                        else if(type.equalsIgnoreCase(getResources().getString(R.string.motobike)))
//                        {
//                            car = "";
//                            public_transport = "";
//                            bicycle = "";
//                            motorbike= type;
//                        }
//                        else if(type.equalsIgnoreCase(getResources().getString(R.string.bicycle)))
//                        {
//                            car = "";
//                            public_transport = "";
//                            bicycle = type;
//                            motorbike= "";
//                        }
//                        else if(type.equalsIgnoreCase(getResources().getString(R.string.text_publictransport)))
//                        {
//                            car = "";
//                            public_transport = type;
//                            bicycle = "";
//                            motorbike= "";
//                        }
//
//                    }
//                }
//                else
//                {
//                    RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
//
//                    checkedRadioButton.setChecked(false);
//                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.plz_slct_food),Toast.LENGTH_SHORT).show();
//                }
//
//
//            }
//        });





        rr_check_parcel.setOnClickListener(this);
        rr_uncheck_parcel.setOnClickListener(this);
        rr_check_taxi.setOnClickListener(this);
        rr_uncheck_taxi.setOnClickListener(this);
        rr_check_fooddelivery.setOnClickListener(this);
        rr_uncheck_fooddelivery.setOnClickListener(this);
        rr_uncheck_taxi_inside.setOnClickListener(this);
        rr_check_taxi_inside.setOnClickListener(this);
     rr_check_vtc_inside.setOnClickListener(this);
    rr_uncheck_vtc_inside.setOnClickListener(this);
        rr_check_rentalprice_inside.setOnClickListener(this);
        rr_uncheck_rentalprice_inside.setOnClickListener(this);

 }

    @Override
    public void onClick(View view) {

        if(view == rr_check_parcel)
        {
            rr_check_parcel.setVisibility(View.GONE);
            rr_uncheck_parcel.setVisibility(View.VISIBLE);
            service_one = "";

        }

        if(view == rr_uncheck_parcel)
        {
            rr_check_parcel.setVisibility(View.VISIBLE);
            rr_uncheck_parcel.setVisibility(View.GONE);
            service_one = txt_parcel.getText().toString();
        }
        if(view == rr_check_taxi)
        {
            rr_check_taxi.setVisibility(View.GONE);
            rr_uncheck_taxi.setVisibility(View.VISIBLE);

            service_two = "";
            taxi = "";
            vtc = "";
            rental = "";

            rr_check_taxi_inside.setVisibility(View.GONE);
            rr_uncheck_taxi_inside.setVisibility(View.VISIBLE);
            rr_check_vtc_inside.setVisibility(View.GONE);
            rr_uncheck_vtc_inside.setVisibility(View.VISIBLE);
            rr_check_rentalprice_inside.setVisibility(View.GONE);
            rr_uncheck_rentalprice_inside.setVisibility(View.VISIBLE);
        }

        if(view == rr_uncheck_taxi)
        {
            rr_check_taxi.setVisibility(View.VISIBLE);
            rr_uncheck_taxi.setVisibility(View.GONE);

            service_two = txt_taxi.getText().toString();

        }
        if(view == rr_check_fooddelivery)
        {
            rr_check_fooddelivery.setVisibility(View.GONE);
            rr_uncheck_fooddelivery.setVisibility(View.VISIBLE);
            service_three = "";

        }

        if(view == rr_uncheck_fooddelivery)
        {
            rr_check_fooddelivery.setVisibility(View.VISIBLE);
            rr_uncheck_fooddelivery.setVisibility(View.GONE);
            service_three = txt_fooddelivery.getText().toString();
        }
        if(view == rr_check_taxi_inside)
        {


                rr_check_taxi_inside.setVisibility(View.GONE);
                rr_uncheck_taxi_inside.setVisibility(View.VISIBLE);
                taxi = "";



        }

        if(view == rr_uncheck_taxi_inside)
        {
            if(!service_two.equalsIgnoreCase(""))
            {

                rr_check_taxi_inside.setVisibility(View.VISIBLE);
                rr_uncheck_taxi_inside.setVisibility(View.GONE);
                taxi = txt_taxi.getText().toString();
            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.plz_taxi_first),Toast.LENGTH_SHORT).show();
            }

        }

        if(view == rr_check_vtc_inside)
        {
            rr_check_vtc_inside.setVisibility(View.GONE);
            rr_uncheck_vtc_inside.setVisibility(View.VISIBLE);
            vtc = "";

        }

        if(view == rr_uncheck_vtc_inside)
        {
            if(!service_two.equalsIgnoreCase("")) {

                rr_check_vtc_inside.setVisibility(View.VISIBLE);
                rr_uncheck_vtc_inside.setVisibility(View.GONE);
                vtc = txt_vtc_inside.getText().toString();
                System.out.println("VTC is "+vtc);
            }
            else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.plz_taxi_first),Toast.LENGTH_SHORT).show();
            }

        }
        if(view == rr_check_rentalprice_inside)
        {
            rr_check_rentalprice_inside.setVisibility(View.GONE);
            rr_uncheck_rentalprice_inside.setVisibility(View.VISIBLE);
            rental = "";

        }

        if(view == rr_uncheck_rentalprice_inside)
        {
            if(!service_two.equalsIgnoreCase("")) {
                rr_check_rentalprice_inside.setVisibility(View.VISIBLE);
                rr_uncheck_rentalprice_inside.setVisibility(View.GONE);
                rental = txt_rentalprice_inside.getText().toString();

                System.out.println("Rental is "+rental);
            }

             else
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.plz_taxi_first),Toast.LENGTH_SHORT).show();
            }
        }

        if(view == rr_continue)
        {
            validate();
        }
        if(view == rr_back)
        {
            goWithBackArrow();
        }
    }

    public  void validate()
    {
        boolean isValidate = false;


//        if(service_one.equalsIgnoreCase("") && service_two.equalsIgnoreCase("") && service_three.equalsIgnoreCase(""))
//        {
//         isValidate = true;
//        }
//        else
//        {
//            Toast.makeText(getApplicationContext(),getResources().getString(R.string.txt_selcect_servicetype),Toast.LENGTH_SHORT).show();
//
//        }

        System.out.println("Service two validate "+service_two);

        if(service_two.equalsIgnoreCase("") || service_two.equalsIgnoreCase("null") || service_two == null)
        {
            isValidate = true;
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.txt_selcect_servicetype),Toast.LENGTH_SHORT).show();

        }

        else
        {
            isValidate = false;

        }

           if (!service_two.equalsIgnoreCase(""))
        {
            if(taxi.equalsIgnoreCase("")
                    && vtc.equalsIgnoreCase("")
                    && rental.equalsIgnoreCase(""))
            {
                isValidate = true;
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.plz_taxi_first),Toast.LENGTH_SHORT).show();


            }
            else
            {
                isValidate  = false;
            }
        }


         if (!service_three.equalsIgnoreCase(""))
     {
//            if(type.equalsIgnoreCase(""))
//            {
//                isValidate = false;
//
//            }
//            else
//            {
//                Toast.makeText(getApplicationContext(),getResources().getString(R.string.plz_slct_food),Toast.LENGTH_SHORT).show();
//
//            }
        }

        if(!isValidate)
        {
    addServiceType();
            System.out.println("Taxi inside "+vtc);
            System.out.println("VTC inside "+vtc);
            System.out.println("CarRental is "+rental);

        }
    }
    private void addServiceType() {
        Utils.showCustomProgressDialog(this, "", false, null);

        JSONObject jsonObject = new JSONObject();
        try {


            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.SERVICE1,service_one);
            jsonObject.put(Const.Params.SERVICE2, service_two);
            jsonObject.put(Const.Params.TAXI, taxi);
            jsonObject.put(Const.Params.VTC, vtc);
            jsonObject.put(Const.Params.CAR_RENT, rental);

            jsonObject.put(Const.Params.SERVICE3, service_three);
           // jsonObject.put(Const.Params.TYPE,type);
            jsonObject.put(Const.Params.CAR, car);
            jsonObject.put(Const.Params.MOTORBIKE, motorbike);
            jsonObject.put(Const.Params.BICYCLE, bicycle);
            jsonObject.put(Const.Params.PUBLICTRANSPORT, public_transport);


            System.out.println("JSOn AddService Object is###"+jsonObject);

            Call<AddServiceResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .providerAddservicetype(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<AddServiceResponse>() {
                @Override
                public void onResponse(Call<AddServiceResponse> call, Response<AddServiceResponse>
                        response) {
                    if (parseContent.isSuccessful(response)) {
                        if (response.body().getSuccess()) {
                            Utils.hideCustomProgressDialog();

                            if(b1!= null)
                            {

                                if(beforelogin!=null && !beforelogin.equalsIgnoreCase("") && !beforelogin.equalsIgnoreCase("null"))
                                {
                                    System.out.println("Before login inside "+beforelogin);

                                    Intent intent=new Intent(Choose_Service_TypeActivity.this, AddVehicleActivity.class);
                                    startActivity(intent);
                                }
                               else
                                {
                                    System.out.println("Is AddVehicle inside "+isAddVehicle);

                                    Intent intent = new Intent(Choose_Service_TypeActivity.this, AddVehicleAfterLoginActivity.class);

                                    intent.putExtra(Const.IS_ADD_VEHICLE, isAddVehicle);
                                    intent.putExtra(Const.VEHICLE_ID, vehicleId);
                                    startActivityForResult(intent, Const.REQUEST_ADD_VEHICLE);
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                                }

                            }
                            else
                            {
                                Intent intent=new Intent(Choose_Service_TypeActivity.this, AddVehicleActivity.class);
                                startActivity(intent);
                            }




                        } else {
                            Utils.hideCustomProgressDialog();

                                Utils.showErrorToast(response.body().getErrorCode(),
                                        Choose_Service_TypeActivity.this);


                        }
                    }

                }

                @Override
                public void onFailure(Call<AddServiceResponse> call, Throwable t) {
                    AppLog.handleThrowable(BankDetailActivity_Driver.class.getSimpleName(), t);

                }
            });

        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.BANK_DETAIL_ACTIVITY, e);
        }
    }

    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }
}
