package com.swissexpressdz.driver.utils;

import com.swissexpressdz.driver.parse.ApiClient;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.OkHttpClient;


public class SocketHelper {
    private static SocketHelper socketHelper;
    private Socket socket;
    public static String UPDATE_LOCATION = "update_location";
    public static String JOIN_TRIP = "join_trip";
    public static String TRIP_DETAIL_NOTIFY = "trip_detail_notify";
    private SocketListener socketListener;

    public boolean isConnected() {
        return socket.connected();
    }

    private SocketHelper() {
        try {

            HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {

                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {

                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }
            }};
            X509TrustManager trustManager = (X509TrustManager) trustAllCerts[0];

            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, null);
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .hostnameVerifier(hostnameVerifier)
                    .sslSocketFactory(sslSocketFactory, trustManager)
                    .build();

            IO.Options opts = new IO.Options();
            opts.callFactory = okHttpClient;
            opts.webSocketFactory = okHttpClient;
            socket = IO.socket(ApiClient.Base_URL, opts);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }

//            socket = IO.socket(ApiClient.Base_URL);



    }

    public static SocketHelper getInstance() {
        if (socketHelper == null) {
            socketHelper = new SocketHelper();
        }
        return socketHelper;
    }

    public void setSocketConnectionListener(SocketListener socketListener) {
        this.socketListener = socketListener;
    }

    public Socket getSocket() {
        return socket;
    }

    public void socketConnect() {
        System.out.println("Socket Connected   "+socket.connected());

        if (!socket.connected()) {
        socket.on(Socket.EVENT_CONNECT, onConnect);
        socket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        socket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        socket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        socket.connect();
        }
    }

    public void socketDisconnect() {
        if (socket.connected()) {
        socket.disconnect();
        socket.off(Socket.EVENT_CONNECT, onConnect);
        socket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        socket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        socket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        }
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            AppLog.Log(SocketHelper.class.getSimpleName(), "Socket Connected");
            if (socketListener != null) {
                socketListener.onSocketConnect();
            }
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            AppLog.Log(SocketHelper.class.getSimpleName(), "Socket Disconnected");
            if (socketListener != null) {
                socketListener.onSocketDisconnect();
            }
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            AppLog.Log(SocketHelper.class.getSimpleName(), "Socket ConnectError");
        }
    };


    public interface SocketListener {
        void onSocketConnect();

        void onSocketDisconnect();
    }


}

