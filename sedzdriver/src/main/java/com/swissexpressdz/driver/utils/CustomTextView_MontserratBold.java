package com.swissexpressdz.driver.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView_MontserratBold extends TextView {
    public CustomTextView_MontserratBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextView_MontserratBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextView_MontserratBold(Context context) {
        super(context);
        init();
    }


    public void init() {
        //  Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Bold.ttf");
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Montserrat-Bold.otf");
        setTypeface(tf);
    }
}