package com.swissexpressdz.driver;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.swissexpressdz.driver.activity.SplashActivity;
import com.swissexpressdz.driver.components.CustomDialogBigLabel;
import com.swissexpressdz.driver.components.CustomDialogEnable;
import com.swissexpressdz.driver.components.MyAppTitleFontTextView;
import com.swissexpressdz.driver.components.MyFontButton;
import com.swissexpressdz.driver.interfaces.AdminApprovedListener;
import com.swissexpressdz.driver.interfaces.ConnectivityReceiverListener;
import com.swissexpressdz.driver.models.responsemodels.IsSuccessResponse;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CurrencyHelper;
import com.swissexpressdz.driver.utils.LanguageHelper;
import com.swissexpressdz.driver.utils.NetworkHelper;
import com.swissexpressdz.driver.utils.PreferenceHelper;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.swissexpressdz.driver.utils.Utils.isScreenOn;

/**
 * Created by Groofl on 10-05-2016.
 */
public abstract class BaseAppCompatActivity extends AppCompatActivity implements View
        .OnClickListener, ConnectivityReceiverListener,
        AdminApprovedListener {

    public MyAppTitleFontTextView tvTitle;
    public MyAppTitleFontTextView tvTimeRemain;
    public PreferenceHelper preferenceHelper;
    public ParseContent parseContent;
    public String TAG = this.getClass().getSimpleName();
    protected Toolbar toolbar;
    protected ActionBar actionBar;
    private MyFontButton btnToolBar;
    private ImageView ivToolbarIcon;
    private CustomDialogEnable customDialogEnableGps, customDialogEnableInternet;
    private CustomDialogBigLabel customDialogExit, customDialogUnderReview;
    private ConnectivityReceiverListener connectivityReceiverListener;
    private AdminApprovedListener adminApprovedListener;
    private AppReceiver appReceiver = new AppReceiver();
    public CurrencyHelper currencyHelper;
    private NetworkHelper networkHelper;
    ImageView water_ball,water_ball_2,water_ball_3,water_ball_4,water_ball_5,water_ball_6,water_ball_7,water_ball_8,Water_ball_9,Water_ball_10;

BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarColor(ResourcesCompat.getColor(getResources(), R.color
                        .color_app_status_bar_green,
                null));
        preferenceHelper = PreferenceHelper.getInstance(this);
        parseContent = ParseContent.getInstance();
        currencyHelper = CurrencyHelper.getInstance(this);
        parseContent.getContext(this);
        adjustFontScale(getResources().getConfiguration());
        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(Const.GPS_ACTION);
        intentFilter.addAction(Const.ACTION_DECLINE_PROVIDER);
        intentFilter.addAction(Const.ACTION_APPROVED_PROVIDER);
        intentFilter.addAction(Const.ACTION_NEW_TRIP);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build
                .VERSION_CODES.LOLLIPOP) {
            networkHelper = NetworkHelper.getInstance();
            networkHelper.initConnectivityManager(this);
        } else {
            intentFilter.addAction(Const.NETWORK_ACTION);
        }
        registerReceiver(appReceiver, intentFilter);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    protected void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.myToolbar);

        tvTitle = toolbar.findViewById(R.id.tvTitle);
        ivToolbarIcon = (ImageView) toolbar.findViewById(R.id.ivToolbarIcon);
        tvTimeRemain = (MyAppTitleFontTextView) toolbar.findViewById(R.id.tvTimeRemain);
        btnToolBar = (MyFontButton) toolbar.findViewById(R.id.btnToolBar);

        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goWithBackArrow();
            }
        });

    }
    protected void initUI() {




        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        //  bottomNavigationView.setItemIconTintList(null);


//     bottomNavigationView.getMenu().getItem(0).setChecked(false);
//        bottomNavigationView.getMenu().getItem(0).setChecked(true);
//
//        bottomNavigationView.getMenu().getItem(2).setChecked(false);
//    bottomNavigationView.getMenu().getItem(2).setChecked(false);

//
//        bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.accueil_parcel_filled_icon);
//
//        bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.accueil_taxi_icon);
//        bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.accueil_profile_icon);

        bottomNavigationView.setSelectedItemId(R.id.action_taxi);



        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.action_parcelicon:



                        bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.accueil_parcel_filled_icon);
                        bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.accueil_taxi_icon);
                        bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.accueil_food_icon);



                        Uri myAction = Uri.parse(preferenceHelper.getSessionToken());

                        String url = "https://swissexpress.page.link/newuser/?"+ "&apn=com.swissexpressdz.user";;
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);


                        break;

                    case R.id.action_taxi:
                        bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.accueil_parcel_icon);
                        bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.accueil_taxi_icon_filled);
                        bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.accueil_food_icon);
                        // bottomNavigationView.getMenu().getItem(3).setIcon(R.drawable.accueil_profile_icon);
                        goToMainDrawerActivity();

                        break;

                    case R.id.action_food:

                        bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.accueil_parcel_icon);
                        bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.accueil_taxi_icon);
                        bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.accueil_food_icon_filled);
                        //     bottomNavigationView.getMenu().getItem(3).setIcon(R.drawable.accueil_profile_icon);



                        break;

//                    case R.id.action_profile:
//
//                        bottomNavigationView.getMenu().getItem(0).setIcon(R.drawable.accueil_parcel_icon);
//                        bottomNavigationView.getMenu().getItem(1).setIcon(R.drawable.accueil_taxi_icon);
//                  //      bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.accueil_food_icon);
//                        bottomNavigationView.getMenu().getItem(2).setIcon(R.drawable.accueil_profile_icon_filled);
//
//                        break;
                }
                return true;
            }
        });


    }

    public void setTitleOnToolbar(String title) {
        tvTitle.setText(title);
        tvTitle.setVisibility(View.VISIBLE);
        tvTimeRemain.setVisibility(View.GONE);
        ivToolbarIcon.setVisibility(View.GONE);
        btnToolBar.setVisibility(View.GONE);
    }

    public void setToolbarIcon(Drawable drawable, View.OnClickListener onClickListener) {
        ivToolbarIcon.setImageDrawable(drawable);
        ivToolbarIcon.setOnClickListener(onClickListener);
        ivToolbarIcon.setVisibility(View.VISIBLE);
    }

    public void setToolbarRightSideButton(String title, View.OnClickListener onClickListener) {
        ivToolbarIcon.setVisibility(View.GONE);
        tvTimeRemain.setVisibility(View.GONE);
        btnToolBar.setVisibility(View.VISIBLE);
        btnToolBar.setOnClickListener(onClickListener);
        btnToolBar.setText(title);
    }

    public void hideToolbarRightSideButton(boolean isHide) {
        if (ivToolbarIcon != null) {
            ivToolbarIcon.setVisibility(isHide ? View.GONE : View.VISIBLE);
        }

    }

    public void setToolbarBackgroundAndElevation(boolean isDrawable, int resId, int elevationId) {
        if (toolbar != null) {
            if (isDrawable) {
                toolbar.setBackground(AppCompatResources.getDrawable(this, resId));
            } else {
                toolbar.setBackgroundColor(ResourcesCompat.getColor(getResources(), resId, null));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (elevationId <= 0) {
                    toolbar.setElevation(0);
                } else {
                    toolbar.setElevation(getResources().getDimensionPixelOffset(elevationId));
                }


            }
        }
    }

    private void randomWaterBallAnimation(Toolbar toolbar){
        water_ball_3 = toolbar.findViewById(R.id.water_ball_3);
        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(9000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());
        water_ball_3.setAnimation(mAnimation3);
        water_ball_4 = toolbar.findViewById(R.id.water_ball_4);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());
        water_ball_4.setAnimation(mAnimation4);
        water_ball_5 = toolbar.findViewById(R.id.water_ball_5);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());
        water_ball_5.setAnimation(mAnimation5);
        water_ball_6 = toolbar.findViewById(R.id.water_ball_6);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF,0.0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());
        water_ball_6.setAnimation(mAnimation6);
        water_ball_7 = toolbar.findViewById(R.id.water_ball_7);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.1f);
        mAnimation7.setDuration(8000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());
        water_ball_7.setAnimation(mAnimation7);
        water_ball_8 = toolbar.findViewById(R.id.water_ball_8);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());
        water_ball_8.setAnimation(mAnimation8);
        Water_ball_10 = toolbar.findViewById(R.id.water_ball_10);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());
        Water_ball_10.setAnimation(mAnimation10);
        Water_ball_9 = toolbar.findViewById(R.id.water_ball_9);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(9000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_9.setAnimation(mAnimation9);
    }

    public void hideToolbarButton() {
        btnToolBar.setVisibility(View.GONE);
    }

    protected void openExitDialog(final Activity activity) {

        customDialogExit = new CustomDialogBigLabel(this, getString(R
                .string.text_exit_caps), getString(R.string.msg_are_you_sure), getString(R.string
                .text_yes), getString(R
                .string.text_no)) {
            @Override
            public void positiveButton() {
                customDialogExit.dismiss();
                activity.finishAffinity();
            }

            @Override
            public void negativeButton() {
                customDialogExit.dismiss();
            }
        };
        customDialogExit.show();
    }

    protected void openGpsDialog() {
        if (customDialogEnableGps != null && customDialogEnableGps.isShowing()) {
            return;
        }
        customDialogEnableGps = new CustomDialogEnable(this, getString(R.string.msg_gps_enable),
                getString(R.string.text_no), getString(R.string.text_yes)) {
            @Override
            public void doWithEnable() {
                startActivityForResult(new Intent(Settings
                        .ACTION_LOCATION_SOURCE_SETTINGS), Const.ACTION_SETTINGS);
            }

            @Override
            public void doWithDisable() {
                closedEnableDialogGps();
                finishAffinity();
            }
        };

       /* if(!this.isFinishing())
            customDialogEnableGps.show();*/
    }


    protected void openInternetDialog() {
        if (customDialogEnableInternet != null && customDialogEnableInternet.isShowing()) {
            return;
        }

        customDialogEnableInternet = new CustomDialogEnable(this, getString(R.string
                .msg_internet_enable), getString(R.string.text_no), getString(R.string.text_yes)) {
            @Override
            public void doWithEnable() {
                startActivityForResult(new Intent(Settings
                        .ACTION_SETTINGS), Const.ACTION_SETTINGS);
            }

            @Override
            public void doWithDisable() {
                closedEnableDialogInternet();
                finishAffinity();
            }
        };

        if (!this.isFinishing())
            customDialogEnableInternet.show();

    }

    protected void closedEnableDialogGps() {
        if (customDialogEnableGps != null && customDialogEnableGps.isShowing()) {
            customDialogEnableGps.dismiss();
            customDialogEnableGps = null;

        }
    }

    protected void closedEnableDialogInternet() {
        if (customDialogEnableInternet != null && customDialogEnableInternet.isShowing()) {
            customDialogEnableInternet.dismiss();
            customDialogEnableInternet = null;

        }
    }


    protected void goToDocumentActivity(boolean isClickInsideDrawer) {
        Intent docIntent = new Intent(this, DocumentActivity_Driver.class);
        docIntent.putExtra(Const.IS_CLICK_INSIDE_DRAWER, isClickInsideDrawer);
        startActivity(docIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    protected void goToMainDrawerActivity() {
        Intent mainDrawerIntent = new Intent(this, MainDrawerActivity
                .class);
        mainDrawerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mainDrawerIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainDrawerIntent);
        overridePendingTransition(R.anim.fade_in_fast, R.anim.fade_out_fast);
    }


    protected void goToChooseServiceActivity() {
        Intent mainDrawerIntent = new Intent(this, Choose_Service_TypeActivity
                .class);
        mainDrawerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mainDrawerIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mainDrawerIntent.putExtra("beforelogin","beforelogin");
        startActivity(mainDrawerIntent);
        overridePendingTransition(R.anim.fade_in_fast, R.anim.fade_out_fast);
    }

    protected void goToDcumentNewActivity() {
        Intent mainDrawerIntent = new Intent(this, DocumentNewActivity.class);
        mainDrawerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mainDrawerIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainDrawerIntent);
        overridePendingTransition(R.anim.fade_in_fast, R.anim.fade_out_fast);
    }
    protected void goToMainActivity() {
       // Intent sigInIntent = new Intent(this, MainActivity_Driver.class);
        Intent sigInIntent = new Intent(this, SignInActivity_Driver.class);

        sigInIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sigInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(sigInIntent);
        finishAffinity();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    protected void goToRegisterActivity() {
        Intent registerIntent = new Intent(this, RegisterActivity_Driver.class);
        startActivity(registerIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    protected void goToSignInActivity() {
        Intent intent = new Intent(this, SignInActivity_Driver.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    public void goToAddVehicleDetailActivity(boolean isAddVehicle, String vehicleId) {
        Intent intent = new Intent(this, AddVehicleDetailActivity_Driver.class);
        intent.putExtra(Const.IS_ADD_VEHICLE, isAddVehicle);
        intent.putExtra(Const.VEHICLE_ID, vehicleId);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    protected abstract boolean isValidate();


    /**
     * @param email
     * @param vehicleId
     * @deprecated
     */
    public void openUnderReviewDialog(final String email, final String vehicleId) {

        if (customDialogUnderReview != null && customDialogUnderReview.isShowing()) {
            return;
        }

        final String positiveButtonTitle;
        final String message;

        if (TextUtils.equals(email, Const.ADD_VEHICLE)) {
            if (TextUtils.isEmpty(vehicleId))
                positiveButtonTitle = getString(R.string.text_add_vehicle);
            else
                positiveButtonTitle = getString(R.string.text_edit_vehicle);

            message = getString(R.string.message_add_vehicle);
        } else {
            positiveButtonTitle = getString(R.string.text_email);
            message = getString(R.string.msg_under_review);
        }

        customDialogUnderReview = new CustomDialogBigLabel(this, getString(R.string
                .text_admin_alert), message, positiveButtonTitle, getString(R.string.text_logout)) {
            @Override
            public void positiveButton() {
                if (TextUtils.equals(positiveButtonTitle, getString(R.string.text_add_vehicle))
                        || TextUtils.equals(positiveButtonTitle, getString(R.string
                        .text_edit_vehicle))) {
                    closedUnderReviewDialog();
                    if (TextUtils.isEmpty(vehicleId)) {
                        goToAddVehicleDetailActivity(true, null);
                    } else {
                        goToAddVehicleDetailActivity(false, vehicleId);
                    }
                } else {
                    contactUsWithEmail(email);
                }
            }

            @Override
            public void negativeButton() {
                closedUnderReviewDialog();
                logOut();

            }
        };
        customDialogUnderReview.show();
    }

    public void closedUnderReviewDialog() {
        if (customDialogUnderReview != null && customDialogUnderReview.isShowing()) {
            customDialogUnderReview.dismiss();
            customDialogUnderReview = null;

        }
    }


    public void logOut() {
        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_log_out), false, null);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.TOKEN, preferenceHelper.getSessionToken());

            Call<IsSuccessResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .logout(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    if (parseContent.isSuccessful(response)) {
                        if (response.body().isSuccess()) {
                            Utils.hideCustomProgressDialog();
                            Utils.showMessageToast(response.body().getMessage(),
                                    BaseAppCompatActivity
                                            .this);
                            preferenceHelper.logout();// clear session token
                            goToMainActivity();
                        } else {
                            Utils.hideCustomProgressDialog();
                            Utils.showErrorToast(response.body()
                                    .getErrorCode(), BaseAppCompatActivity.this);
                        }
                    }


                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(BaseAppCompatActivity.class.getSimpleName(), t);
                }
            });

        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.MAIN_DRAWER_ACTIVITY, e);
        }
    }

    protected void contactUsWithEmail(String email) {
        Uri gmmIntentUri = Uri.parse("mailto:" + email +
                "?subject=" + getString(R.string.text_request_to_admin) +
                "&body=" + getString(R.string.text_hello_sir));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.gm");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        } else {
            Utils.showToast(getResources().getString(R.string
                    .msg_google_mail_app_not_installed), this);
        }
    }


    public abstract void goWithBackArrow();


    public void setConnectivityListener(ConnectivityReceiverListener
                                                listener) {
        connectivityReceiverListener = listener;
        if (networkHelper != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            networkHelper.setNetworkAvailableListener(connectivityReceiverListener);
        }
    }

    public void setAdminApprovedListener(AdminApprovedListener
                                                 listener) {
        adminApprovedListener = listener;
    }

    public void goWithAdminApproved() {
        preferenceHelper.putIsApproved(Const.ProviderStatus.IS_APPROVED);
        goToMainDrawerActivity();
    }

    public void goWithAdminDecline() {
        preferenceHelper.putIsProviderOnline(Const.ProviderStatus
                .PROVIDER_STATUS_OFFLINE);
        preferenceHelper.putIsApproved(Const.ProviderStatus.IS_DECLINED);
        goToMainDrawerActivity();
    }

    public String getAppVersion() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            AppLog.handleException(BaseAppCompatActivity.class.getName(), e);
        }
        return null;
    }

    protected Intent getIntentForPermission() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        return intent;
    }

    @Override
    protected void onResume() {
        super.onResume();
        /**
         * Remove  comment when you live eber in palyStore
         */
//        if (!TextUtils.equals(Locale.getDefault().getLanguage(), Const.EN)) {
//            openLanguageDialog();
//        }
    }

    private void openLanguageDialog() {

        final CustomDialogBigLabel customDialogBigLabel = new CustomDialogBigLabel(this,
                getResources()
                        .getString(R.string.text_attention), getResources()
                .getString(R.string.meg_language_not_an_english), getResources()
                .getString(R.string.text_settings),
                getResources()
                        .getString(R.string.text_exit_caps)) {
            @Override
            public void positiveButton() {
                startActivity(new Intent(Settings.ACTION_LOCALE_SETTINGS));
                dismiss();
            }

            @Override
            public void negativeButton() {
                dismiss();
                finishAffinity();

            }
        };
        customDialogBigLabel.show();
    }

    public void adjustFontScale(Configuration configuration) {
        if (configuration.fontScale > Const.DEFAULT_FONT_SCALE) {
            configuration.fontScale = Const.DEFAULT_FONT_SCALE;
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(metrics);
            metrics.scaledDensity = configuration.fontScale * metrics.density;
            getBaseContext().getResources().updateConfiguration(configuration, metrics);
        }
    }

    public void hideKeyBord() {
        InputMethodManager inm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public void restartApp() {
        startActivity(new Intent(this, SplashActivity.class));
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(LanguageHelper.wrapper(newBase, PreferenceHelper.getInstance
                (newBase).getLanguageCode()));
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(appReceiver);
        super.onDestroy();
    }

    /**
     * this method used to make decision after login or register for screen transaction with
     * specific preference
     */
    public void moveWithUserSpecificPreference() {
//        if (preferenceHelper.getAllDocUpload() == Const.FALSE) {
//            goToDocumentActivity(false);
//        } else {
//            goToMainDrawerActivity();
//        }


        goToDcumentNewActivity();
    }

    public void moveWithUserSpecificPreference2() {
//        if (preferenceHelper.getAllDocUpload() == Const.FALSE) {
//            goToDocumentActivity(false);
//        } else {
//            goToMainDrawerActivity();
//        }


        goToChooseServiceActivity();
    }



    public class AppReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent != null && intent.getAction() != null) {
                switch (intent.getAction()) {
                    case Const.NETWORK_ACTION:
                        if (connectivityReceiverListener != null) {
                            connectivityReceiverListener.onNetworkConnectionChanged(Utils
                                    .isInternetConnected(context));
                        }
                        break;
                    case Const.GPS_ACTION:
                        if (connectivityReceiverListener != null) {
                            connectivityReceiverListener.onGpsConnectionChanged(Utils.isGpsEnable
                                    (context));
                        }
                        break;
                    case Const.ACTION_APPROVED_PROVIDER:
                        if (adminApprovedListener != null) {
                            adminApprovedListener.onAdminApproved();
                        }
                        break;
                    case Const.ACTION_DECLINE_PROVIDER:
                        if (adminApprovedListener != null) {
                            adminApprovedListener.onAdminDeclined();
                        }
                        break;
                    case Const.ACTION_NEW_TRIP:
                        if (preferenceHelper.getIsMainScreenVisible()) {
                            return;
                        } else {
                            if (!isScreenOn(context)) {
                                preferenceHelper.putIsScreenLock(true);
                            } else {
                                preferenceHelper.putIsScreenLock(false);
                            }
                            Intent mapIntent = new Intent(context, MainDrawerActivity.class);
                            mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent
                                    .FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(mapIntent);
                        }
                        break;
                    default:
                        // do with default
                        break;
                }
            }
        }


    }

    public String validPhoneNumberMessage(int minDigit, int maxDigit) {
        if (maxDigit == minDigit) {
            return getResources().getString(R.string.msg_please_enter_valid_mobile_number,
                    maxDigit);
        } else {
            return getResources().getString(R.string.msg_please_enter_valid_mobile_number_between
                    , minDigit, maxDigit);
        }
    }
}
