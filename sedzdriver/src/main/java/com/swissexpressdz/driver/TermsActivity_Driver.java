package com.swissexpressdz.driver;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.swissexpressdz.driver.utils.Utils;

public class TermsActivity_Driver extends BaseAppCompatActivity {

    private WebView webViewTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_driver);
        initToolBar();
        setTitleOnToolbar(getResources().getString(R.string.title_terms_condition));
        webViewTerms = (WebView) findViewById(R.id.webViewTerms);
        initWebView();

    }

    private void initWebView() {
        webViewTerms.getSettings().setLoadsImagesAutomatically(true);
        webViewTerms.getSettings().setJavaScriptEnabled(true);
        webViewTerms.getSettings().setBuiltInZoomControls(true);
        webViewTerms.getSettings().setDisplayZoomControls(false);
        webViewTerms.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webViewTerms.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Utils.showCustomProgressDialog(TermsActivity_Driver.this, "", false, null);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Utils.hideCustomProgressDialog();
            }
        });
        webViewTerms.loadUrl(preferenceHelper.getTermsANdConditions());
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    public void goWithBackArrow() {
        onBackPressed();
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onAdminApproved() {
        goWithAdminApproved();
    }

    @Override
    public void onAdminDeclined() {
        goWithAdminDecline();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            closedEnableDialogInternet();
        } else {
            openInternetDialog();
        }
    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {
        if (isConnected) {
            closedEnableDialogGps();
        } else {
            openGpsDialog();

        }
    }
}
