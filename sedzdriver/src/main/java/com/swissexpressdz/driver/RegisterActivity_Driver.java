package com.swissexpressdz.driver;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.santalu.maskedittext.MaskEditText;
import com.swissexpressdz.driver.adapter.CityAdapter;
import com.swissexpressdz.driver.broadcast.OtpReader;
import com.swissexpressdz.driver.components.CustomCountryDialog;
import com.swissexpressdz.driver.components.CustomDialogBigLabel;
import com.swissexpressdz.driver.components.CustomDialogEnable;
import com.swissexpressdz.driver.components.CustomDialogVerifyDetail;
import com.swissexpressdz.driver.components.CustomPhotoDialog;
import com.swissexpressdz.driver.components.CustomPhotoDialog2;
import com.swissexpressdz.driver.components.MyAppTitleFontTextView;
import com.swissexpressdz.driver.components.MyFontButton;
import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.components.MyFontTextViewMedium;
import com.swissexpressdz.driver.interfaces.ClickListener;
import com.swissexpressdz.driver.interfaces.OTPListener;
import com.swissexpressdz.driver.interfaces.RecyclerTouchListener;
import com.swissexpressdz.driver.models.datamodels.City;
import com.swissexpressdz.driver.models.datamodels.CityType;
import com.swissexpressdz.driver.models.datamodels.Country;
import com.swissexpressdz.driver.models.responsemodels.CountriesResponse;
import com.swissexpressdz.driver.models.responsemodels.ProviderDataResponse;
import com.swissexpressdz.driver.models.responsemodels.TypesResponse;
import com.swissexpressdz.driver.models.responsemodels.VerificationResponse;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextViewRegular;
import com.swissexpressdz.driver.utils.GoogleClientHelper;
import com.swissexpressdz.driver.utils.ImageCompression;
import com.swissexpressdz.driver.utils.ImageHelper;
import com.swissexpressdz.driver.utils.LocationHelper;
import com.swissexpressdz.driver.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity_Driver extends BaseAppCompatActivity implements TextView
        .OnEditorActionListener,
        OTPListener, GoogleApiClient.OnConnectionFailedListener {


    private Uri picUri,picUri2;
    private ImageView ivProfilePicture;
    private String uploadImageFilePath = "";
    private String uploadImageFilePath2="";
    private String uploadImageFilePath3="";
    private String uploadImageFilePath4="";
    private String uploadImageFilePath5="";
    private String uploadImageFilePath6="";

    private MyFontEdittextView etFirstName, etLastName, etAddress, etZipCode
            , etEmail, etPassword,etRegisterconfrmPassword, etBio, tvRegisterCountryName, tvRegisterCityName;
    MaskEditText etContactNumber;
    private MyFontTextViewMedium tvCountryCode;
    private CustomTextViewRegular btnRegisterDone;
    private String loginType = Const.MANUAL;
    private ArrayList<Country> countryList;
    private Location lastLocation;
    private MyAppTitleFontTextView tvGoSignIn;
    private LinearLayout llPassword,ll_male,ll_female,llconfrmPassword;
    RelativeLayout rr_male_uncheck,rr_male_check,rr_female_uncheck,rr_female_check;
    private LocationHelper locationHelper;
    private CustomCountryDialog customCountryDialog;
    private CustomPhotoDialog customPhotoDialog;
    private CustomPhotoDialog2 customPhotoDialog2,customPhotoDialog3,customPhotoDialog4,customPhotoDialog5,customPhotoDialog6;

    private CustomDialogEnable customDialogEnable;
    private CustomDialogBigLabel customDialogBigLabel;
    private ArrayList<City> cityList;
    private CityAdapter cityAdapter;
    private boolean isEmailVerify, isSMSVerify, isBackPressedOnce;
    private String otpForSMS, otpForEmail;
    private CustomDialogVerifyDetail customDialogVerifyDetail;
    private ImageHelper imageHelper;
    private int phoneNumberLength, phoneNumberMinLength;
    private String selectedCountryPhoneCode = "";
    private MyFontTextView tvTerms;
    private ImageView ivRegisterFacebook, ivRegisterGoogle;
    LinearLayout ll_fb_login;
    private CallbackManager callbackManager;
    private String socialId, socialEmail, socialPhotoUrl, socialFirstName, socialLastName;
    private String msg, gender = "";
    private Spinner selectGender;
    private OtpReader otpReader;
    private Country country;
    private GoogleApiClient googleApiClient;
    private City city;
    private TextInputLayout tilPassword,tilcnfrmPassword;
    private RelativeLayout rr_register;
    ImageView water_ball,water_ball_2,water_ball_3,water_ball_4,water_ball_5,water_ball_6,water_ball_7,water_ball_8,Water_ball_9,Water_ball_10;
    ImageView water_ball1,water_ball_21,water_ball_31,water_ball_41,water_ball_51,water_ball_61,water_ball_71,water_ball_81,Water_ball_91,Water_ball_101;
    private ArrayList<CityType> vehicleTypeList;
    private ArrayList<String> vehicleTypeList2;
ImageView tvFlag;
    MyFontEdittextView tvselect_document,tvselect_document2,tvselect_document3,tvselect_document4,tvselect_document5;
Spinner tvselect_document_type,tvselect_vehicle_type,tvselect_document_type2,tvselect_document_type3,tvselect_document_type4,tvselect_document_type5;

String items_vehicle_id="",items_vehicle_name="",file_ext,file_ext2,file_ext3,file_ext4,file_ext5,file_ext6;

LinearLayout llselect_document2,llselect_document_type2,llselect_document3,llselect_document_type3,llselect_document4,llselect_document_type4,llselect_document5,llselect_document_type5;

ArrayList<String> arrayList2=new ArrayList<>();
    ArrayList<String> arrayList3=new ArrayList<>();
    ArrayList<String> arrayList4=new ArrayList<>();
    ArrayList<String> arrayList5=new ArrayList<>();
    ArrayList<String> arrayList6=new ArrayList<>();
String path,str_lang_code;
File file_image;

    ArrayList<String> arrayList_files_type=new ArrayList<>();
    ArrayList<String> arrayList_files=new ArrayList<>();
    ArrayList<String> arrayList_files_name=new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_driver);

        FacebookSdk.sdkInitialize(getApplicationContext());

        CurrentTrip.getInstance().clear();
        System.out.println("Language Code is###"+preferenceHelper.getLanguageCode());
         str_lang_code=preferenceHelper.getLanguageCode();

        googleApiClient = new GoogleClientHelper(this).build();
        vehicleTypeList = new ArrayList<>();
        vehicleTypeList2 = new ArrayList<>();

        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList2.add("Sélectionnez le type de document");
            arrayList2.add("Permis De Conduire");
            arrayList2.add("licence VTC");
            arrayList2.add("Assurance Société");
            arrayList2.add("Assurance Transport De Personnes");
            arrayList2.add("Carte Grise Du Véhicule");

        }
        else if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList2.add("Select document type");
            arrayList2.add("Drivers License");
            arrayList2.add("VTC license");
            arrayList2.add("Company Insurance");
            arrayList2.add("Passenger Transport Insurance");
            arrayList2.add("Vehicle Registration Certificate");
        }

        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList3.add("Sélectionnez le type de document");
            arrayList3.add("Permis De Conduire");
            arrayList3.add("licence VTC");
            arrayList3.add("Assurance Société");
            arrayList3.add("Assurance Transport De Personnes");
            arrayList3.add("Carte Grise Du Véhicule");

        }
        else
        {
            arrayList3.add("Select document type");
            arrayList3.add("Drivers License");
            arrayList3.add("VTC license");
            arrayList3.add("Company Insurance");
            arrayList3.add("Passenger Transport Insurance");
            arrayList3.add("Vehicle Registration Certificate");
        }
        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList4.add("Sélectionnez le type de document");
            arrayList4.add("Permis De Conduire");
            arrayList4.add("licence VTC");
            arrayList4.add("Assurance Société");
            arrayList4.add("Assurance Transport De Personnes");
            arrayList4.add("Carte Grise Du Véhicule");

        }
        else
        {
            arrayList4.add("Select document type");
            arrayList4.add("Drivers License");
            arrayList4.add("VTC license");
            arrayList4.add("Company Insurance");
            arrayList4.add("Passenger Transport Insurance");
            arrayList4.add("Vehicle Registration Certificate");
        }


        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList5.add("Sélectionnez le type de document");
            arrayList5.add("Permis De Conduire");
            arrayList5.add("licence VTC");
            arrayList5.add("Assurance Société");
            arrayList5.add("Assurance Transport De Personnes");
            arrayList5.add("Carte Grise Du Véhicule");

        }
        else
        {
            arrayList5.add("Select document type");
            arrayList5.add("Drivers License");
            arrayList5.add("VTC license");
            arrayList5.add("Company Insurance");
            arrayList5.add("Passenger Transport Insurance");
            arrayList5.add("Vehicle Registration Certificate");
        }
        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList6.add("Sélectionnez le type de document");
            arrayList6.add("Permis De Conduire");
            arrayList6.add("licence VTC");
            arrayList6.add("Assurance Société");
            arrayList6.add("Assurance Transport De Personnes");
            arrayList6.add("Carte Grise Du Véhicule");

        }
        else
        {
            arrayList6.add("Select document type");
            arrayList6.add("Drivers License");
            arrayList6.add("VTC license");
            arrayList6.add("Company Insurance");
            arrayList6.add("Passenger Transport Insurance");
            arrayList6.add("Vehicle Registration Certificate");
        }
        imageHelper = new ImageHelper(this);
        isEmailVerify = preferenceHelper.getIsProviderEmailVerification();
        isSMSVerify = preferenceHelper.getIsProviderSMSVerification();
        locationHelper = new LocationHelper(this);
        locationHelper.checkLocationSetting(this);
        checkPlayServices();
        ivProfilePicture = (ImageView) findViewById(R.id.ivProfilePicture);
        ivProfilePicture.setOnClickListener(this);
        rr_register=(RelativeLayout)findViewById(R.id.rr_register);

        llselect_document2=(LinearLayout)findViewById(R.id.llselect_document2);
        llselect_document_type2=(LinearLayout)findViewById(R.id.llselect_document_type2);
        llselect_document3=(LinearLayout)findViewById(R.id.llselect_document3);
        llselect_document_type3=(LinearLayout)findViewById(R.id.llselect_document_type3);
        llselect_document4=(LinearLayout)findViewById(R.id.llselect_document4);
        llselect_document_type4=(LinearLayout)findViewById(R.id.llselect_document_type4);
        llselect_document5=(LinearLayout)findViewById(R.id.llselect_document5);
        llselect_document_type5=(LinearLayout)findViewById(R.id.llselect_document_type5);


        btnRegisterDone = (CustomTextViewRegular) findViewById(R.id.btnRegisterDone);

        gender=Const.Gender.MALE;

       // btnRegisterDone.setOnClickListener(this);
        rr_register.setOnClickListener(this);
//        ll_male.setOnClickListener(this);
//        ll_female.setOnClickListener(this);


        etFirstName = (MyFontEdittextView) findViewById(R.id.etFirstName);
        etLastName = (MyFontEdittextView) findViewById(R.id.etLastName);
        etAddress = (MyFontEdittextView) findViewById(R.id.etAddress);
        etZipCode = (MyFontEdittextView) findViewById(R.id.etZipCode);
        etPassword = (MyFontEdittextView) findViewById(R.id.etRegisterPassword);
        etRegisterconfrmPassword = (MyFontEdittextView) findViewById(R.id.etRegisterconfrmPassword);

        etEmail = (MyFontEdittextView) findViewById(R.id.etRegisterEmailId);
        etBio = (MyFontEdittextView) findViewById(R.id.etBio);
        tvselect_document_type=(Spinner)findViewById(R.id.tvselect_document_type);
        tvselect_vehicle_type=(Spinner)findViewById(R.id.tvselect_vehicle_type);

        tvselect_document_type2=(Spinner)findViewById(R.id.tvselect_document_type2);
        tvselect_document2=(MyFontEdittextView) findViewById(R.id.tvselect_document2);
        tvselect_document2.setOnClickListener(this);


        tvselect_document_type3=(Spinner)findViewById(R.id.tvselect_document_type3);
        tvselect_document3=(MyFontEdittextView) findViewById(R.id.tvselect_document3);
        tvselect_document3.setOnClickListener(this);

        tvselect_document_type4=(Spinner)findViewById(R.id.tvselect_document_type4);
        tvselect_document4=(MyFontEdittextView) findViewById(R.id.tvselect_document4);
        tvselect_document4.setOnClickListener(this);

        tvselect_document_type5=(Spinner)findViewById(R.id.tvselect_document_type5);
        tvselect_document5=(MyFontEdittextView) findViewById(R.id.tvselect_document5);
        tvselect_document5.setOnClickListener(this);

        etContactNumber = (MaskEditText) findViewById(R.id.etContactNumber);
        tvCountryCode = (MyFontTextViewMedium) findViewById(R.id.tvCountryCode);
        tvRegisterCountryName = findViewById(R.id.tvRegisterCountryName);

        tvRegisterCountryName.setOnClickListener(this);
        tvFlag=findViewById(R.id.tvFlag);
        tvFlag.setOnClickListener(this);
        tvCountryCode.setOnClickListener(this);

        tvRegisterCityName = findViewById(R.id.tvRegisterCityName);
        tvselect_document=(MyFontEdittextView)findViewById(R.id.tvselect_document);
        tvselect_document.setOnClickListener(this);
        tvRegisterCityName.setOnClickListener(this);
        tvCountryCode.setOnClickListener(this);
        llPassword = (LinearLayout) findViewById(R.id.llPassword);
        tvGoSignIn = (MyAppTitleFontTextView) findViewById(R.id.tvGoSignIn);
        tilPassword=findViewById(R.id.tilPassword);

        tilcnfrmPassword = findViewById(R.id.tilcnfrmPassword);
        llconfrmPassword = (LinearLayout) findViewById(R.id.llconfrmPassword);

        tvGoSignIn.setOnClickListener(this);

        etAddress.setOnEditorActionListener(this);
        etBio.setOnEditorActionListener(this);
        etZipCode.setOnEditorActionListener(this);
        tvTerms = (MyFontTextView) findViewById(R.id.tvTerms);
        String link = getResources().getString(R.string.text_trems_and_condition_main,
                preferenceHelper.getTermsANdConditions(), preferenceHelper.getPolicy());
        System.out.println("Link::::::::"+preferenceHelper.getTermsANdConditions());
        tvTerms.setText(Utils.fromHtml(link));
        tvTerms.setMovementMethod(LinkMovementMethod.getInstance());
        callbackManager = CallbackManager.Factory.create();

        ivRegisterFacebook = (ImageView) findViewById(R.id.ivRegisterFacebook);
        ivRegisterFacebook.setOnClickListener(this);



        ivRegisterGoogle = (ImageView) findViewById(R.id.ivRegisterGoogle);
        ivRegisterGoogle.setOnClickListener(this);
        countryList = new ArrayList<>();
        cityList = new ArrayList<>();
        //enableRegisterButton();
        updateUi(false);
        checkPermission();
        initGenderSelection();
//        randomWaterBallAnimation();
//        randomWaterBallAnimation1();
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tilPassword.setPasswordVisibilityToggleTintMode(PorterDuff.Mode.MULTIPLY);
            }
        });

        etRegisterconfrmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tilcnfrmPassword.setPasswordVisibilityToggleTintMode(PorterDuff.Mode.MULTIPLY);
            }
        });


        tvselect_vehicle_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                if(vehicleTypeList2!=null && vehicleTypeList2.size()>0) {

                    items_vehicle_id= vehicleTypeList.get(arg2).getId();
                    items_vehicle_name= vehicleTypeList.get(arg2).getTypename();

                    System.out.println("Selected Item%%%" + items_vehicle_id);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }

        });


//        otpReader = new OtpReader(this, preferenceHelper.getTwilioNumber());
//        registerReceiver(otpReader, new IntentFilter(Const.ACTION_OTP_SMS));
    }
    private boolean checkPlayServices() {

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 12).show();
            } else {
                AppLog.Log("Google Play Service", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setConnectivityListener(this);
        setAdminApprovedListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Const.PIC_URI, picUri);
        outState.putParcelable(Const.PIC_URI2, picUri2);

        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        picUri = savedInstanceState.getParcelable(Const.PIC_URI);
        picUri2 = savedInstanceState.getParcelable(Const.PIC_URI2);

    }


    /**
     * Facebook login part
     */
    private void registerCallForFacebook() {
        LoginManager.getInstance().registerCallback(callbackManager, new
                FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Utils.showCustomProgressDialog(RegisterActivity_Driver.this, "", false, null);
                        getFacebookProfileDetail(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        AppLog.Log("Facebook :", "" + error);
                        Utils.showToast(getString(R.string.message_can_not_register_facebook),
                                RegisterActivity_Driver.this);
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(this, Collections.singletonList
                ("email"));
    }


    private void getFacebookProfileDetail(AccessToken accessToken) {

        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest
                .GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Utils.hideCustomProgressDialog();
                try {
                    socialId = object.getString("id");
                    socialPhotoUrl = new URL("https://graph.facebook.com/" + socialId +
                            "/picture?width=250&height=250").toString();
                    storeSocialImageFile(socialPhotoUrl);
                    if (object.has("email")) {
                        socialEmail = object.getString("email");
                    }
                    socialFirstName = object.getString("first_name");
                    socialLastName = object.getString("last_name");
                    loginType = Const.SOCIAL_FACEBOOK;
                    setSocialData();

                } catch (Exception e) {
                    socialPhotoUrl = "";
                    AppLog.handleException(Const.Tag.SIGN_IN_ACTIVITY, e);
                }
                AppLog.Log("fb response", object.toString());
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();

    }


    /**
     * Google signIn part...
     */
    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, Const.google.RC_SIGN_IN);
    }

    private void handleGoogleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            try {
                GoogleSignInAccount account = result.getSignInAccount();
                loginType = Const.SOCIAL_GOOGLE;
                socialId = account.getId();
                socialEmail = account.getEmail();
                String str = account.getDisplayName();
                if (str.trim().contains(" ")) {
                    String[] name = str.split("\\s+");
                    socialFirstName = name[0].trim();
                    socialLastName = name[1].trim();
                } else {
                    socialFirstName = str.trim();
                    socialLastName = "";
                }
                socialPhotoUrl = account.getPhotoUrl().toString();
                storeSocialImageFile(socialPhotoUrl);
                setSocialData();
            } catch (Exception e) {
                setSocialData();
                AppLog.handleException(Const.Tag.SIGN_IN_ACTIVITY, e);
            }
        } else {
            AppLog.Log("Error", result.getStatus().toString());
            Utils.showToast(getString(R.string.message_can_not_register_google), RegisterActivity_Driver
                    .this);
        }
    }

    private void signOutFromGoogle() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        AppLog.Log("Google SignOut", "" + status);
                    }
                });
    }


    /**
     * Use for download profile image from facebook and google profile picture url...
     *
     * @param url
     */
    private void storeSocialImageFile(String url) {
        PicassoTrustAll.getInstance(getApplicationContext())
                .load(url)
                .error(R.drawable.driver_car)
                .into(ivProfilePicture);
//        Glide.with(getApplicationContext()).asBitmap()
//                .load(url)
//
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .listener(new RequestListener<Bitmap>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
//                                                Target<Bitmap> target, boolean isFirstResource) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(Bitmap resource, Object model,
//                                                   Target<Bitmap> target,
//                                                   DataSource dataSource, boolean isFirstResource) {
//                        uploadImageFilePath = createProfilePhoto(resource).getPath();
//                        ivProfilePicture.setImageBitmap(resource);
//                        return true;
//                    }
//                }).into(ivProfilePicture);
    }


    private File createProfilePhoto(Bitmap bitmap) {
        File imgaeFile = new File(this.getFilesDir(), "image.jpg");
        FileOutputStream os;
        try {
            os = new FileOutputStream(imgaeFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imgaeFile;
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null && !googleApiClient.isConnected()) {
            googleApiClient.connect(GoogleApiClient.SIGN_IN_MODE_OPTIONAL);
        }

    }

    @Override
    protected void onStop() {
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
        super.onStop();

        locationHelper.onStop();
        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "GoogleApiClient is Disconnected");
    }

    @Override
    protected void onDestroy() {
        //locationHelper.onStop();
        //unregisterReceiver(otpReader);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Const.ServiceCode.TAKE_PHOTO:
                if (resultCode == RESULT_OK) {
                    onCaptureImageResult();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO:
                onSelectFromGalleryResult(data);
                break;

            case Const.ServiceCode.TAKE_PHOTO2:
                if (resultCode == RESULT_OK) {
                  //  onCaptureImageResult2();
                    try {
                        Uri selectedImage = data.getData();
                        try {
                            System.out.println("Selected Image Extention Mime###"+getContentResolver().getType(selectedImage));

                           /* ContentResolver cR = getContentResolver();
                            MimeTypeMap mime = MimeTypeMap.getSingleton();


                            file_ext = mime.getExtensionFromMimeType(cR.getType(selectedImage));*/

                            System.out.println("Selected Image Extention###"+selectedImage.toString());

                            uploadImageFilePath2=selectedImage.toString()+"."+file_ext;
                            tvselect_document.setText(uploadImageFilePath2+"");

                            System.out.println("Selected Image Extention###"+file_ext);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO2:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        file_ext2 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + file_ext2);

                        uploadImageFilePath2 = selectedImage.toString() + "." + file_ext2;

                        tvselect_document.setText(uploadImageFilePath2 + "");

                        llselect_document2.setVisibility(View.VISIBLE);
                        llselect_document_type2.setVisibility(View.VISIBLE);


                        for(int i=0;i<arrayList2.size();i++) {
                            if(arrayList2.get(i).equalsIgnoreCase(tvselect_document_type.getSelectedItem().toString()))

                                arrayList2.remove(i);
                            ArrayAdapter<String> dealAdapter = new ArrayAdapter<String>(
                                    RegisterActivity_Driver.this,
                                    R.layout.detailspinnertext, arrayList2);
                            dealAdapter
                                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            tvselect_document_type2.setAdapter(dealAdapter);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                // onSelectFromGalleryResult2(data);
                break;

            case Const.ServiceCode.CHOOSE_PHOTO3:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        file_ext3 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention third###" + selectedImage.toString());
                        System.out.println("Selected Image Extention third###" + file_ext3);

                        uploadImageFilePath3 = selectedImage.toString() + "." + file_ext3;

                        tvselect_document2.setText(uploadImageFilePath3 + "");

                        llselect_document3.setVisibility(View.VISIBLE);
                        llselect_document_type3.setVisibility(View.VISIBLE);

                        for(int i=0;i<arrayList3.size();i++) {
                            if(arrayList3.get(i).equalsIgnoreCase(tvselect_document_type.getSelectedItem().toString()))

                                arrayList3.remove(i);

                            for(int k=0;k<arrayList3.size();k++) {
                                if (arrayList3.get(k).equalsIgnoreCase(tvselect_document_type2.getSelectedItem().toString()))

                                    arrayList3.remove(k);


                                ArrayAdapter<String> dealAdapter2 = new ArrayAdapter<String>(
                                        RegisterActivity_Driver.this,
                                        R.layout.detailspinnertext, arrayList3);
                                dealAdapter2
                                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                tvselect_document_type3.setAdapter(dealAdapter2);

                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                // onSelectFromGalleryResult2(data);
                break;
            case Const.ServiceCode.CHOOSE_PHOTO4:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        file_ext4 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention fourth###" + selectedImage.toString());
                        System.out.println("Selected Image Extention fourth###" +file_ext4);

                        uploadImageFilePath4 = selectedImage.toString() + "." + file_ext4;

                        tvselect_document3.setText(uploadImageFilePath4 + "");

                        llselect_document4.setVisibility(View.VISIBLE);
                        llselect_document_type4.setVisibility(View.VISIBLE);

                        for(int i=0;i<arrayList4.size();i++) {
                            if(arrayList4.get(i).equalsIgnoreCase(tvselect_document_type.getSelectedItem().toString()))

                                arrayList4.remove(i);

                            for(int k=0;k<arrayList4.size();k++) {
                                if (arrayList4.get(k).equalsIgnoreCase(tvselect_document_type2.getSelectedItem().toString()))

                                    arrayList4.remove(k);

                                for(int l=0;l<arrayList4.size();l++) {
                                    if (arrayList4.get(k).equalsIgnoreCase(tvselect_document_type3.getSelectedItem().toString()))

                                        arrayList4.remove(k);

                                    ArrayAdapter<String> dealAdapter2 = new ArrayAdapter<String>(
                                            RegisterActivity_Driver.this,
                                            R.layout.detailspinnertext, arrayList4);
                                    dealAdapter2
                                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    tvselect_document_type4.setAdapter(dealAdapter2);
                                }
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                // onSelectFromGalleryResult2(data);
                break;



            case Const.ServiceCode.CHOOSE_PHOTO5:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        file_ext5 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention fifth###" + selectedImage.toString());
                        System.out.println("Selected Image Extention fifth###" +file_ext5);

                        uploadImageFilePath5 = selectedImage.toString() + "." + file_ext5;

                        tvselect_document4.setText(uploadImageFilePath5 + "");

                        llselect_document5.setVisibility(View.VISIBLE);
                        llselect_document_type5.setVisibility(View.VISIBLE);


                                for (int i = 0; i < arrayList5.size(); i++) {

                                    if (arrayList5.get(i).equalsIgnoreCase(tvselect_document_type.getSelectedItem().toString()))

                                        arrayList5.remove(i);

                                    for (int k = 0; k < arrayList5.size(); k++) {
                                        if (arrayList5.get(k).equalsIgnoreCase(tvselect_document_type2.getSelectedItem().toString()))


                                            arrayList5.remove(k);

                                        for (int l = 0; l < arrayList5.size(); l++) {

                                            if (arrayList5.get(l).equalsIgnoreCase(tvselect_document_type3.getSelectedItem().toString()))
                                                arrayList5.remove(l);

                                            for(int j=0;j<arrayList5.size();j++) {

                                                if (arrayList5.get(j).equalsIgnoreCase(tvselect_document_type4.getSelectedItem().toString()))

                                                    arrayList5.remove(j);

                                                ArrayAdapter<String> dealAdapter2 = new ArrayAdapter<String>(
                                                            RegisterActivity_Driver.this,
                                                            R.layout.detailspinnertext, arrayList5);
                                                dealAdapter2
                                                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                tvselect_document_type5.setAdapter(dealAdapter2);
                                            }
                                        }
                                    }

                        }

                        System.out.println("Selected Image Extention###" + file_ext5);

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                // onSelectFromGalleryResult2(data);
                break;

            case Const.ServiceCode.CHOOSE_PHOTO6:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        file_ext6 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention sixth###" + selectedImage.toString());

                        uploadImageFilePath6 = selectedImage.toString() + "." + file_ext6;

                        tvselect_document5.setText(uploadImageFilePath6 + "");


                        System.out.println("Selected Image Extention sixth###" + file_ext6);

                    } catch (Exception e) {
                        System.out.println("Exception Five$$$"+e);
                        e.printStackTrace();

                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                // onSelectFromGalleryResult2(data);
                break;

            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                handleCrop(resultCode, data);
                break;

            case Const.PERMISSION_FOR_LOCATION:
                checkPermission();
                break;
            case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                openPhotoDialog();
                break;
            case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE2:
                openPhotoDialog2();
                break;
            case Const.google.RC_SIGN_IN:
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleGoogleSignInResult(result);
                break;
            case Const.LOCATION_SETTING_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    getServicesCountry();
                }
                break;


            default:
                callbackManager.onActivityResult(requestCode, resultCode, data);
                break;
        }

    }
    private void gender_m_click() {
        Log.e("Gender Male Click", "Gender Male Click");
        rr_male_check.setVisibility(View.VISIBLE);
        rr_male_uncheck.setVisibility(View.GONE);
        rr_female_check.setVisibility(View.GONE);
        rr_female_uncheck.setVisibility(View.VISIBLE);
        gender=Const.Gender.MALE;

//        gender="Male";
//                selectedGender.clear();
//        selectedGender.add(Const.Gender.MALE);

    }
    private void gender_f_click() {
        Log.e("Gender Male Click", "Gender Male Click");
        rr_male_check.setVisibility(View.GONE);
        rr_male_uncheck.setVisibility(View.VISIBLE);
        rr_female_check.setVisibility(View.VISIBLE);
        rr_female_uncheck.setVisibility(View.GONE);
        gender=Const.Gender.FEMALE;

//        gender="Female";
//        selectedGender.clear();
//        selectedGender.add(Const.Gender.FEMALE);
    }
    /**
     * This method is used for handel result after select image from gallery .
     */

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            picUri = data.getData();
            beginCrop(picUri);
        }
    }

    /**
     * This method is used for handel result after captured image from camera .
     */
    private void onCaptureImageResult() {
        beginCrop(picUri);
    }


    private void onSelectFromGalleryResult2(Intent data) {
        if (data != null) {

          picUri2 = data.getData();

         tvselect_document.setText(picUri2+"");
            File myFile = new File(picUri2.getPath());
           uploadImageFilePath2 = myFile.getAbsolutePath();

           // uploadImageFilePath2=imageHelper.getRealPathFromURI(picUri2);
            System.out.println("File Name 1st$$$"+imageHelper.getRealPathFromURI(picUri2));

            String filenameArray[] = uploadImageFilePath2.split("\\.");
             file_ext = filenameArray[filenameArray.length-1];

            System.out.println("File Name 2nd$$$"+file_ext);

     /*       picUri2 = data.getData();
            beginCrop2(picUri2);*/

        }
    }

    /**
     * This method is used for handel result after captured image from camera .
     */
    private void onCaptureImageResult2() {

        tvselect_document.setText(picUri2+"");

        File myFile = new File(picUri2.getPath());
        uploadImageFilePath2 = myFile.getAbsolutePath();

        String filenameArray[] = uploadImageFilePath2.split("\\.");
        file_ext = filenameArray[filenameArray.length-1];
        System.out.println("File Name 2nd$$$"+file_ext);

        //   beginCrop2(picUri2);

    }
    /**
     * This method is used for crop the image which selected or captured by currentTrip.
     */
    private void beginCrop(Uri sourceUri) {
        CropImage.activity(sourceUri).setGuidelines(com.theartofdev.edmodo.cropper.CropImageView
                .Guidelines.ON).start(this);
    }
    private void beginCrop2(Uri sourceUri) {

        CropImage.activity(sourceUri).setGuidelines(com.theartofdev.edmodo.cropper.CropImageView
                .Guidelines.ON).start(this);
    }
    private void setProfileImage(Uri imageUri) {



        PicassoTrustAll.getInstance(getApplicationContext())
                .load(imageUri)
                .error(R.drawable.ellipse)
                .into(ivProfilePicture);
    }

    /**
     * This method is used for  handel crop result after crop the image.
     */
    private void handleCrop(int resultCode, Intent result) {
        final CropImage.ActivityResult activityResult = CropImage.getActivityResult(result);
        if (resultCode == RESULT_OK) {
         //   uploadImageFilePath = imageHelper.getRealPathFromURI(activityResult.getUri());
            uploadImageFilePath = imageHelper.getFromMediaUriPfd(this, getContentResolver(), activityResult.getUri()).getPath();
            System.out.println("File Name 1st@@@"+uploadImageFilePath);

            new ImageCompression(this).setImageCompressionListener(new ImageCompression
                    .ImageCompressionListener() {
                @Override
                public void onImageCompression(String compressionImagePath) {

                    setProfileImage(activityResult.getUri());
                    System.out.println("File Name 1st!!!"+compressionImagePath);

                    uploadImageFilePath = compressionImagePath;
                    uploadImageFilePath = imageHelper.getFromMediaUriPfd(RegisterActivity_Driver.this, getContentResolver(), activityResult.getUri()).getPath();

                    System.out.println("File Name 1st$$$"+uploadImageFilePath);


                }
            }).execute(uploadImageFilePath);
        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Utils.showToast(activityResult.getError().getMessage(), this);
        }
    }
    private void handleCrop2(int resultCode, Intent result) {
        final CropImage.ActivityResult activityResult = CropImage.getActivityResult(result);
        if (resultCode == RESULT_OK) {
            uploadImageFilePath2 = imageHelper.getRealPathFromURI(activityResult.getUri());
            new ImageCompression(this).setImageCompressionListener(new ImageCompression
                    .ImageCompressionListener() {
                @Override
                public void onImageCompression(String compressionImagePath) {
                   // setProfileImage(activityResult.getUri());
                    tvselect_document.setText(uploadImageFilePath2+"");

                    uploadImageFilePath2 = compressionImagePath;
                    System.out.println("File Name 1st$$$"+uploadImageFilePath2);


                }
            }).execute(uploadImageFilePath);
        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Utils.showToast(activityResult.getError().getMessage(), this);
        }
    }
    private void choosePhotoFromGallery() {

        /*Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);*/
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Utils.isNougat()) {
            picUri = FileProvider.getUriForFile(RegisterActivity_Driver.this, this.getApplicationContext
                    ().getPackageName(), imageHelper.createImageFile());
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            picUri = Uri.fromFile(imageHelper.createImageFile());
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, Const.ServiceCode.TAKE_PHOTO);
    }
    private void choosePhotoFromGallery2() {

        /*Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);*/
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO2);
    }
    private void choosePhotoFromGallery3() {

        /*Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);*/
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO3);
    }


    private void choosePhotoFromGallery4() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO4);
    }

    private void choosePhotoFromGallery5() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO5);
    }
    private void choosePhotoFromGallery6() {

        System.out.println("Sixth Gallery is@@@"+"Sixth Gallery is@@@");

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO6);
    }

    private void takePhotoFromCamera2() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Utils.isNougat()) {
            picUri2 = FileProvider.getUriForFile(RegisterActivity_Driver.this, this.getApplicationContext
                    ().getPackageName(), imageHelper.createImageFile());
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            picUri2 = Uri.fromFile(imageHelper.createImageFile());
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri2);
        startActivityForResult(intent, Const.ServiceCode.TAKE_PHOTO2);
    }
    protected void openPhotoDialog() {
        if (ContextCompat.checkSelfPermission(RegisterActivity_Driver.this, Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission
                (RegisterActivity_Driver.this, Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegisterActivity_Driver.this, new String[]{Manifest
                    .permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
        }
        else {

            customPhotoDialog = new CustomPhotoDialog(this) {
                @Override
                public void clickedOnCamera() {
                    customPhotoDialog.dismiss();
                    takePhotoFromCamera();
                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog.dismiss();
                    choosePhotoFromGallery();
                }
            };
            customPhotoDialog.show();
        }


    }

    protected void openPhotoDialog2() {

      if (ContextCompat.checkSelfPermission(RegisterActivity_Driver.this, Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission
                (RegisterActivity_Driver.this, Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegisterActivity_Driver.this, new String[]{Manifest
                    .permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE2);
        }
        else {

            customPhotoDialog2 = new CustomPhotoDialog2(this) {

                @Override
                public void clickedOnCamera() {
                    customPhotoDialog2.dismiss();
                    takePhotoFromCamera2();
                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog2.dismiss();
                    choosePhotoFromGallery2();
                }
            };
            customPhotoDialog2.show();
        }

    }


    protected void openPhotoDialog3() {


            customPhotoDialog3 = new CustomPhotoDialog2(this) {

                @Override
                public void clickedOnCamera() {
                    customPhotoDialog3.dismiss();
                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog3.dismiss();
                    choosePhotoFromGallery3();
                }
            };
            customPhotoDialog3.show();


    }
    protected void openPhotoDialog4() {


        customPhotoDialog4 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                customPhotoDialog4.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog4.dismiss();
                choosePhotoFromGallery4();
            }
        };
        customPhotoDialog4.show();


    }

    protected void openPhotoDialog5() {


        customPhotoDialog5 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                customPhotoDialog5.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog5.dismiss();
                choosePhotoFromGallery5();
            }
        };
        customPhotoDialog5.show();


    }

    protected void openPhotoDialog6() {


        customPhotoDialog6 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                System.out.println("Sixth Camera is###"+"Sixth Gallery is###");

                customPhotoDialog6.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog6.dismiss();
                System.out.println("Sixth Gallery is###"+"Sixth Gallery is###");
                choosePhotoFromGallery6();
            }
        };
        customPhotoDialog6.show();


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rr_register:
//                Intent ii   =new Intent(this,RegisterNextPageActivity.class);
//                startActivity(ii);
                checkValidationForRegister();
                break;
            case R.id.ivProfilePicture:
                openPhotoDialog();
                break;
            case R.id.tvselect_document:

                String txt_spin = tvselect_document_type.getSelectedItem().toString();
                System.out.println("Spinner Text is####"+txt_spin);

                if(txt_spin==null || txt_spin.equalsIgnoreCase("") || txt_spin.equalsIgnoreCase("null") ||
                txt_spin.equalsIgnoreCase("Select document type") || txt_spin.equalsIgnoreCase("Sélectionnez le type de document"))
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.pls_select_document_type), Toast.LENGTH_SHORT).show();
                }
                else {
                    openPhotoDialog2();
                }
                break;

            case R.id.tvselect_document2:

                String txt_spin2 = tvselect_document_type2.getSelectedItem().toString();
                System.out.println("Spinner Text is####"+txt_spin2);

                if(txt_spin2==null || txt_spin2.equalsIgnoreCase("") || txt_spin2.equalsIgnoreCase("null") ||
                        txt_spin2.equalsIgnoreCase("Select Document Second") || txt_spin2.equalsIgnoreCase("Sélectionner le document en second"))
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.pls_select_document_type), Toast.LENGTH_SHORT).show();
                }
                else {
                    openPhotoDialog3();
                }
                break;

            case R.id.tvselect_document3:

                String txt_spin3 = tvselect_document_type3.getSelectedItem().toString();
                System.out.println("Spinner Text is####"+txt_spin3);

                if(txt_spin3==null || txt_spin3.equalsIgnoreCase("") || txt_spin3.equalsIgnoreCase("null") ||
                        txt_spin3.equalsIgnoreCase("Select Document Third") || txt_spin3.equalsIgnoreCase("Sélectionner le troisième documen"))
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.pls_select_document_type), Toast.LENGTH_SHORT).show();
                }
                else {
                    openPhotoDialog4();
                }
                break;

            case R.id.tvselect_document4:

                String txt_spin4 = tvselect_document_type4.getSelectedItem().toString();
                System.out.println("Spinner Text is fourth####"+txt_spin4);

                if(txt_spin4==null || txt_spin4.equalsIgnoreCase("") || txt_spin4.equalsIgnoreCase("null") ||
                        txt_spin4.equalsIgnoreCase("Select Document Fourth") || txt_spin4.equalsIgnoreCase("Sélectionner le quatrième document"))
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.pls_select_document_type), Toast.LENGTH_SHORT).show();
                }
                else {
                    openPhotoDialog5();
                }
                break;


            case R.id.tvselect_document5:

                String txt_spin5 = tvselect_document_type5.getSelectedItem().toString();
                System.out.println("Spinner Text is fifth####"+txt_spin5);

                if(txt_spin5==null || txt_spin5.equalsIgnoreCase("") || txt_spin5.equalsIgnoreCase("null") ||
                        txt_spin5.equalsIgnoreCase("Select Document Fifth") || txt_spin5.equalsIgnoreCase("Sélectionnez le cinquième document"))
                {
                    System.out.println("Spinner Text is fifth@@@"+txt_spin5);

                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.pls_select_document_type), Toast.LENGTH_SHORT).show();
                }
                else {
                    System.out.println("Spinner Text is fifth!!!"+txt_spin5);

                    openPhotoDialog6();
                }
                break;

            case R.id.tvRegisterCountryName:
                openCountryCodeDialog();
                break;

            case R.id.tvFlag:
                openCountryCodeDialog();
                break;


            case R.id.tvCountryCode:
                openCountryCodeDialog();
                break;

            case R.id.tvRegisterCityName:
                if (cityList.size() == 0) {
                    openRequestRegisterTypeDialog(getResources().getString(R.string
                            .msg_type_not_available_current_city));
                } else {
                    openCityNameDialog();
                }
                break;
            case R.id.tvGoSignIn:
                goToSignInActivity();
                break;
            case R.id.tvTerms:
                goToTermsAndConditionPage();
                break;
            case R.id.ivRegisterFacebook:
                LoginManager.getInstance().logOut();
                registerCallForFacebook();
                break;


            case R.id.ivRegisterGoogle:
                signOutFromGoogle();
                googleSignIn();
                break;
            default:
                //Do something with default
                break;
        }

    }


    private void checkValidationForRegister() {



        if (isValidate()) {
            if (isEmailVerify || isSMSVerify) {
                //Changed Code 02-Feb-2020
                OTPVerify();

            } else {


              //  setData(loginType);
register(loginType);


            }
        }
    }

    private void   setData(String loginType){

        if (etPassword.getVisibility() == View.VISIBLE) {

            preferenceHelper.putUserPassword(etPassword.getText().toString());
        }

        else {
            preferenceHelper.putUserPassword("");
            preferenceHelper.putSocialId(socialId);


        }
        preferenceHelper.putFirstName(etFirstName.getText().toString());
        preferenceHelper.putLastName(etLastName.getText().toString());
        preferenceHelper.putEmail(etEmail.getText().toString());
        preferenceHelper.putContact(etContactNumber.getText().toString());
        preferenceHelper.putBio(etBio.getText().toString());
        preferenceHelper.putAddress(etAddress.getText().toString());
        preferenceHelper.putZipCode(etZipCode.getText().toString());
        preferenceHelper.putLoginBy(loginType);
        preferenceHelper.putGender(gender);


        preferenceHelper.putCountryPhoneCode(tvCountryCode.getText().toString());
        preferenceHelper.putCountryName(country.getCountryname());
        preferenceHelper.putCountryId(country.getId());
        preferenceHelper.putCity(city.getFullCityName());
        preferenceHelper.putCityId(city.getId());

        preferenceHelper.putProfilePic(uploadImageFilePath);

//
//        Intent ii   =new Intent(this,RegisterNextPageActivity.class);
//        startActivity(ii);
    }




    private void updateUi(boolean update) {
        if (update) {
            llPassword.setVisibility(View.GONE);
            etPassword.setVisibility(View.GONE);

            llconfrmPassword.setVisibility(View.GONE);
            etRegisterconfrmPassword.setVisibility(View.GONE);

        } else {
            llPassword.setVisibility(View.VISIBLE);
            etPassword.setVisibility(View.VISIBLE);

            llconfrmPassword.setVisibility(View.VISIBLE);
            etRegisterconfrmPassword.setVisibility(View.VISIBLE);
        }
    }

    private void setSocialData() {
        updateUi(true);
        if (!TextUtils.isEmpty(socialEmail))
            etEmail.setEnabled(false);
        etEmail.setText(socialEmail);
        etFirstName.setText(socialFirstName);
        etLastName.setText(socialLastName);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "GoogleApiClient is Failed to Connect ");
    }


    @Override
    protected boolean isValidate() {

        msg = null;

        if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
            msg = getString(R.string.msg_enter_name);
            etFirstName.requestFocus();
            etFirstName.setError(msg);
        } else if (TextUtils.isEmpty(etLastName.getText().toString().trim())) {
            msg = getString(R.string.msg_enter_lname);
            etLastName.requestFocus();
            etLastName.setError(msg);
        }
        else if (!Utils.eMailValidation((etEmail.getText().toString().trim()))) {
            msg = getString(R.string.msg_enter_valid_email);
            etEmail.requestFocus();
            etEmail.setError(msg);
        }
        else if (etPassword.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(etPassword.getText().toString().trim())) {
                msg = getString(R.string.msg_enter_password);
                etPassword.requestFocus();
                etPassword.setError(msg);
                tilPassword.setPasswordVisibilityToggleTintMode(PorterDuff.Mode.CLEAR);
            }
            else if (etPassword.getText().toString().trim().length() < 6) {
                msg = getString(R.string.msg_enter_valid_password);
                etPassword.requestFocus();
                etPassword.setError(msg);
                tilPassword.setPasswordVisibilityToggleTintMode(PorterDuff.Mode.CLEAR);
            } else {
               // validateOtherInformation();
            }
        } else if (etPassword.getVisibility() == View.GONE) {
            //validateOtherInformation();
        }

         if (etRegisterconfrmPassword.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(etRegisterconfrmPassword.getText().toString().trim())) {
                msg = getString(R.string.msg_enter_password);
                etRegisterconfrmPassword.requestFocus();
                etRegisterconfrmPassword.setError(msg);
                tilcnfrmPassword.setPasswordVisibilityToggleTintMode(PorterDuff.Mode.CLEAR);
            }
            else if (etRegisterconfrmPassword.getText().toString().trim().length() < 6) {
                msg = getString(R.string.msg_enter_valid_password);
                etRegisterconfrmPassword.requestFocus();
                etRegisterconfrmPassword.setError(msg);
                tilcnfrmPassword.setPasswordVisibilityToggleTintMode(PorterDuff.Mode.CLEAR);
            }
            else if (!etRegisterconfrmPassword.getText().toString().equalsIgnoreCase(etPassword.getText().toString())) {
                msg = getString(R.string.msg_confrmpass_sme_pass);
                etRegisterconfrmPassword.requestFocus();
                etRegisterconfrmPassword.setError(msg);
                tilcnfrmPassword.setPasswordVisibilityToggleTintMode(PorterDuff.Mode.CLEAR);
            }
            else {
                // validateOtherInformation();
            }
        } else if (etRegisterconfrmPassword.getVisibility() == View.GONE) {
            //validateOtherInformation();
        }
if(tvRegisterCountryName.getText().toString() == null || tvRegisterCountryName.getText().toString().equalsIgnoreCase("")
|| tvRegisterCountryName.getText().toString().equalsIgnoreCase(getResources().getString(R.string.text_hint_select_country)))
{
    msg = getString(R.string.msg_plz_select_country);
    tvRegisterCountryName.requestFocus();
    tvRegisterCountryName.setError(msg);
}

        if (city == null) {
            msg = getString(R.string.msg_enter_lname);
            tvRegisterCityName.requestFocus();
            tvRegisterCityName.setError(msg);
        }
        else{
           /* msg = getString(R.string.msg_enter_lname);
            tvRegisterCityName.requestFocus();
            tvRegisterCityName.setError(msg);*/

        }

        return TextUtils.isEmpty(msg);

    }


    private void validateOtherInformation() {
        String txt_spin2 = null,txt_spin3 = null,txt_spin4 = null,txt_spin5 = null;
        int phoneLength = etContactNumber.getText().toString().length();

        String txt_spin = tvselect_document_type.getSelectedItem().toString();

        if(tvselect_document_type2.getSelectedItem()!=null) {
             txt_spin2 = tvselect_document_type2.getSelectedItem().toString();
        }

        if(tvselect_document_type3.getSelectedItem()!=null) {
            txt_spin3 = tvselect_document_type3.getSelectedItem().toString();
        }

        if(tvselect_document_type4.getSelectedItem()!=null) {
            txt_spin4 = tvselect_document_type4.getSelectedItem().toString();
        }
        if(tvselect_document_type5.getSelectedItem()!=null) {
            txt_spin5 = tvselect_document_type5.getSelectedItem().toString();
        }


        System.out.println("Spinner Text is####" + txt_spin2);

        if (TextUtils.equals(tvRegisterCountryName.getText().toString(), getResources()
                .getString(R.string.text_hint_select_country)
        ) || TextUtils.isEmpty(tvRegisterCountryName.getText().toString())) {
            msg = getString(R.string.msg_plz_select_country);
            tvRegisterCountryName.requestFocus();
            tvRegisterCountryName.setError(msg);
        } else if (TextUtils.equals(tvRegisterCityName.getText().toString(), getResources()
                .getString(R.string.text_hint_select_city)) || TextUtils.isEmpty
                (tvRegisterCityName.getText().toString())) {
            msg = getString(R.string.msg_plz_select_city);
            tvRegisterCityName.requestFocus();
            tvRegisterCityName.setError(msg);
        } else if (TextUtils.isEmpty(etContactNumber.getText().toString())) {
            msg = getString(R.string.msg_enter_number);
            etContactNumber.requestFocus();
            etContactNumber.setError(msg);
        } else if (phoneLength > phoneNumberLength || phoneLength < phoneNumberMinLength) {
            msg = validPhoneNumberMessage(phoneNumberMinLength, phoneNumberLength);
            etContactNumber.requestFocus();
            etContactNumber.setError(msg);
        }

        else  if (txt_spin == null || txt_spin.equalsIgnoreCase("") || txt_spin.equalsIgnoreCase("null") ||

                txt_spin.equalsIgnoreCase("Select document type") || txt_spin.equalsIgnoreCase("Sélectionnez le type de document")) {

            msg = getString(R.string.this_field_required);

            TextView errorText = (TextView)tvselect_document_type.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);
        }

        else if(TextUtils.isEmpty(tvselect_document.getText().toString()) || tvselect_document.getText().toString().equalsIgnoreCase("Select Document")
        || tvselect_document.getText().toString().equalsIgnoreCase("Sélectionner un document"))
        {
            msg = getString(R.string.this_field_requiredd);
            tvselect_document.requestFocus();
            tvselect_document.setError(msg);
        }

        else  if (txt_spin2 == null || txt_spin2.equalsIgnoreCase("") || txt_spin2.equalsIgnoreCase("null") ||

                txt_spin2.equalsIgnoreCase("Select document type") || txt_spin2.equalsIgnoreCase("Sélectionnez le type de document")) {

            llselect_document_type2.setVisibility(View.VISIBLE);
            llselect_document2.setVisibility(View.VISIBLE);

            msg = getString(R.string.this_field_required);

            TextView errorText = (TextView)tvselect_document_type2.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);
        }

        else if(TextUtils.isEmpty(tvselect_document2.getText().toString()) || tvselect_document2.getText().toString().equalsIgnoreCase("Select Document Second")
                || tvselect_document2.getText().toString().equalsIgnoreCase("Sélectionner le document en second"))
        {
            msg = getString(R.string.this_field_requiredd);
            tvselect_document2.requestFocus();
            tvselect_document2.setError(msg);
        }


        else  if (txt_spin3 == null || txt_spin3.equalsIgnoreCase("") || txt_spin3.equalsIgnoreCase("null") ||

                txt_spin3.equalsIgnoreCase("Select document type") || txt_spin3.equalsIgnoreCase("Sélectionnez le type de document")) {

            llselect_document_type3.setVisibility(View.VISIBLE);
            llselect_document3.setVisibility(View.VISIBLE);

            msg = getString(R.string.this_field_required);

            TextView errorText = (TextView)tvselect_document_type3.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);
        }

        else if(TextUtils.isEmpty(tvselect_document3.getText().toString()) || tvselect_document3.getText().toString().equalsIgnoreCase("Select Document Third")
                || tvselect_document3.getText().toString().equalsIgnoreCase("Sélectionner le troisième document"))
        {
            msg = getString(R.string.this_field_requiredd);
            tvselect_document3.requestFocus();
            tvselect_document3.setError(msg);
        }

        else  if (txt_spin4 == null || txt_spin4.equalsIgnoreCase("") || txt_spin4.equalsIgnoreCase("null") ||

                txt_spin4.equalsIgnoreCase("Select document type") || txt_spin4.equalsIgnoreCase("Sélectionnez le type de document")) {

            llselect_document_type4.setVisibility(View.VISIBLE);
            llselect_document4.setVisibility(View.VISIBLE);

            msg = getString(R.string.this_field_required);

            TextView errorText = (TextView)tvselect_document_type4.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);
        }

        else if(TextUtils.isEmpty(tvselect_document4.getText().toString()) || tvselect_document4.getText().toString().equalsIgnoreCase("Select Document Fourth")
                || tvselect_document4.getText().toString().equalsIgnoreCase("Sélectionner le quatrième document"))
        {
            msg = getString(R.string.this_field_requiredd);
            tvselect_document4.requestFocus();
            tvselect_document4.setError(msg);
        }
        else  if (txt_spin5 == null || txt_spin5.equalsIgnoreCase("") || txt_spin5.equalsIgnoreCase("null") ||

                txt_spin5.equalsIgnoreCase("Select document type") || txt_spin5.equalsIgnoreCase("Sélectionnez le type de document")) {

            llselect_document_type5.setVisibility(View.VISIBLE);
            llselect_document5.setVisibility(View.VISIBLE);

            msg = getString(R.string.this_field_required);

            TextView errorText = (TextView)tvselect_document_type5.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);
        }

        else if(TextUtils.isEmpty(tvselect_document5.getText().toString()) || tvselect_document5.getText().toString().equalsIgnoreCase("Select Document Fifth")
                || tvselect_document5.getText().toString().equalsIgnoreCase("Sélectionnez le cinquième document"))
        {
            msg = getString(R.string.this_field_requiredd);
            tvselect_document5.requestFocus();
            tvselect_document5.setError(msg);
        }
    }
    private void register(final String loginType) {




        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register valid");

        HashMap<String, RequestBody> map = new HashMap<>();




        map.put(Const.Params.FIRST_NAME, ApiClient.makeTextRequestBody(etFirstName.getText().toString()));
        map.put(Const.Params.LAST_NAME, ApiClient.makeTextRequestBody(etLastName.getText().toString()));
        map.put(Const.Params.PASSWORD, ApiClient.makeTextRequestBody(etPassword.getText().toString()));

        map.put(Const.Params.EMAIL, ApiClient.makeTextRequestBody(etEmail.getText().toString()));
        map.put(Const.Params.DEVICE_TYPE, ApiClient.makeTextRequestBody(Const
                .DEVICE_TYPE_ANDROID));
        map.put(Const.Params.DEVICE_TOKEN, ApiClient.makeTextRequestBody(preferenceHelper
                .getDeviceToken()));

        map.put(Const.Params.PHONE, ApiClient.makeTextRequestBody(etContactNumber.getText().toString()));
        map.put(Const.Params.BIO, ApiClient.makeTextRequestBody(etBio.getText().toString()));


        map.put(Const.Params.SERVICE_TYPE, ApiClient.makeTextRequestBody(""));
        map.put(Const.Params.DEVICE_TIMEZONE, ApiClient.makeTextRequestBody(Utils
                .getTimeZoneName()));
        map.put(Const.Params.ADDRESS, ApiClient.makeTextRequestBody(etAddress.getText().toString()));

        map.put(Const.Params.ZIPCODE, ApiClient.makeTextRequestBody(etZipCode.getText().toString()));
        map.put(Const.Params.LOGIN_BY, ApiClient.makeTextRequestBody(loginType));
        map.put(Const.Params.APP_VERSION, ApiClient.makeTextRequestBody(getAppVersion()));


        map.put(Const.Params.COUNTRY, ApiClient.makeTextRequestBody(country.getCountryname()));
        map.put(Const.Params.COUNTRY_ID, ApiClient.makeTextRequestBody(country.getId()));
        map.put(Const.Params.COUNTRY_PHONE_CODE, ApiClient.makeTextRequestBody(tvCountryCode.getText().toString()));
        map.put(Const.Params.CITY_ID, ApiClient.makeTextRequestBody(city.getId()));
        map.put(Const.Params.CITY, ApiClient.makeTextRequestBody(city.getFullCityName()));



        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_registering), false, null);

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[arrayList_files.size()];




        //For First Document
        Call<ProviderDataResponse> userDataResponseCall;
        userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                (ApiInterface.class).register(
                surveyImagesParts
                , map);

        System.out.println("Map is###"+map.toString());


        userDataResponseCall.enqueue(new Callback<ProviderDataResponse>() {
            @Override
            public void onResponse(Call<ProviderDataResponse> call,
                                   Response<ProviderDataResponse> response) {
                if (parseContent.isSuccessful(response)) {
                    if (parseContent.saveProviderData(response.body(), true)) {

                        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register Success");

                        CurrentTrip.getInstance().clear();
                        Utils.hideCustomProgressDialog();
                        preferenceHelper.putCityId(city.getId());
                        preferenceHelper.putUserPassword(etPassword.getText().toString());

                        moveWithUserSpecificPreference2();


                    } else {
                        Utils.hideCustomProgressDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderDataResponse> call, Throwable t) {
                AppLog.handleThrowable(RegisterActivity_Driver.class.getSimpleName(), t);
            }
        });


    }


    @Override
    public void goWithBackArrow() {
        // Do with back arrow
    }


    private void getCountryCodeList(String country) {
        int countryListSize = countryList.size();

        for (int i = 0; i < countryListSize; i++) {

            System.out.println("countryList :::::"+countryList.size());
            if (countryList.get(i).getCountryname().toUpperCase().startsWith(country.toUpperCase
                    ())) {
                setCountry(i);
                return;
            }
        }
        setCountry(0);
        Utils.hideCustomProgressDialog();
    }

    private void openCountryCodeDialog() {
        tvRegisterCountryName.setError(null);
        customCountryDialog = null;
        customCountryDialog = new CustomCountryDialog(this, countryList) {
            @Override
            public void onSelect(int position, ArrayList<Country> filterList) {
                if (!selectedCountryPhoneCode.equalsIgnoreCase(filterList.get(position)
                        .getCountryphonecode())) {

                    etContactNumber.getText().clear();
                    selectedCountryPhoneCode = filterList.get(position).getCountryphonecode();
                }
                phoneNumberLength = filterList.get(position).getPhoneNumberLength();
                phoneNumberMinLength = filterList.get(position).getPhoneNumberMinLength();
                tvCountryCode.setText(filterList.get(position).getCountryphonecode());
                country = filterList.get(position);
                setContactNoLength(phoneNumberLength);
                vehicleTypeList.clear();
                vehicleTypeList2.clear();
                System.out.println("Flag URL  "+filterList.get(position).getCountryphonecode());
                tvRegisterCountryName.setText(filterList.get(position).getCountryname());
                tvRegisterCountryName.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                        .color_app_text, null));
             //   tvFlag.setText(filterList.get(position).getFlagUrl());
                PicassoTrustAll.getInstance(RegisterActivity_Driver.this)
                        .load(ApiClient.Base_URL+countryList.get(position).getFlagUrl())
                      //  .error(R.drawable.ellipse)
                        .into(tvFlag);

                if(filterList.get(position).getCountryphonecode().equalsIgnoreCase("+33"))
                {
                    etContactNumber.setMask("# ## ## ## ##");

                }

                else  if(filterList.get(position).getCountryphonecode().equalsIgnoreCase("+91"))
                {
                    etContactNumber.setMask("## #### ####");

                }

                else if(filterList.get(position).getCountryphonecode().equalsIgnoreCase("+971"))
                {
                    etContactNumber.setMask("## ### ####");

                }

                else
                {
                    etContactNumber.setMask("# ## ## ## ##");

                }


                getCityListSelectedCountry(filterList.get(position).getCities());
                InputMethodManager inm = (InputMethodManager) getSystemService(Context
                        .INPUT_METHOD_SERVICE);
                inm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                customCountryDialog.dismiss();
            }
        };
        customCountryDialog.show();

    }

    private void openCityNameDialog() {
        tvRegisterCityName.setError(null);
        RecyclerView rcvCountryCode;
        MyFontEdittextView etCountrySearch;
        TextView tvDialogTitle;
        MyFontButton btnAddCity;

        final Dialog cityDialog = new Dialog(this);

        cityDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cityDialog.setContentView(R.layout.dialog_country_code);
        btnAddCity = (MyFontButton) cityDialog.findViewById(R.id.btnAddCity);
        btnAddCity.setVisibility(View.VISIBLE);
        tvDialogTitle = cityDialog.findViewById(R.id.tvDialogTitle);
        tvDialogTitle.setText(getResources().getString(R.string.text_city_name));
        rcvCountryCode = (RecyclerView) cityDialog.findViewById(R.id.rcvCountryCode);
        etCountrySearch = (MyFontEdittextView) cityDialog.findViewById(R.id.etCountrySearch);
        etCountrySearch.setHint(getResources().getString(R.string.text_search_city_name));
        rcvCountryCode.setLayoutManager(new LinearLayoutManager(this));
        cityAdapter = new CityAdapter(cityList);
        rcvCountryCode.setAdapter(cityAdapter);
        WindowManager.LayoutParams params = cityDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        cityDialog.getWindow().setAttributes(params);
        cityDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        btnAddCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cityDialog.dismiss();
                openRequestRegisterTypeDialog(getResources().getString(R.string
                        .msg_type_not_available_current_city));

            }
        });
        etCountrySearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (cityAdapter != null) {
                    cityAdapter.getFilter().filter(s);
                } else {
                    Log.d("filter", "no filter availible");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                // do with text
            }
        });

        rcvCountryCode.addOnItemTouchListener(new RecyclerTouchListener(RegisterActivity_Driver.this,
                rcvCountryCode,
                new ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        tvRegisterCityName.setText(cityAdapter.getFilterResult().get(position)
                                .getCityName());
                        city = cityAdapter.getFilterResult().get(position);
                        tvRegisterCityName.setTextColor(ResourcesCompat.getColor(getResources(), R
                                .color.color_app_text, null));
                        InputMethodManager inm = (InputMethodManager) getSystemService(Context
                                .INPUT_METHOD_SERVICE);
                        inm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);


                        cityDialog.dismiss();

            getFromLocation(cityAdapter.getFilterResult().get(position).getCityName());

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

        cityDialog.show();
    }


    private void getFromLocation(String address)
    {
        double dub_latitude= 0.0, dub_longtitude= 0.0;

        Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
        try
        {
            List<Address> addresses = geoCoder.getFromLocationName(address , 1);
            if (addresses.size() > 0)
            {

                for(int i = 0; i < addresses.size(); i++) { // MULTIPLE MATCHES

                    Address addr = addresses.get(i);

                    dub_latitude = addr.getLatitude();
                    dub_longtitude = addr.getLongitude(); // DO SOMETHING WITH VALUES

                    System.out.println("Location Latitude is###"+dub_latitude);
                    System.out.println("Location Longitude is###"+dub_longtitude);

                    getVehicleType(tvRegisterCountryName.getText().toString(),dub_latitude,dub_longtitude);

                }


            }
        }
        catch(Exception ee)
        {

        }

}

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        switch (v.getId()) {
            case R.id.etAddress:
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //checkValidationForRegister();
                    hideKeyBord();
                    return true;
                }
                break;
            default:
                //Do something
                break;
        }
        return false;
    }

    @Override
    public void otpReceived(String otp) {
        if (customDialogVerifyDetail != null && customDialogVerifyDetail.isShowing()) {
            customDialogVerifyDetail.notifyDataSetChange(otp);
        }
    }


    private void enableRegisterButton() {
        btnRegisterDone.setClickable(true);
        btnRegisterDone.setBackground(AppCompatResources.getDrawable(this, R.drawable
                .selector_round_rect_shape_blue));
    }

    private void disableRegisterButton() {
        btnRegisterDone.setClickable(false);
        btnRegisterDone.setBackground(AppCompatResources.getDrawable(this, R.drawable
                .selector_round_rect_shape_blue_disable));

    }

    @Override
    public void onBackPressed() {
        if (isBackPressedOnce) {
            super.onBackPressed();
            backToMainActivity();
            return;
        } else {
            openDetailNotSaveDialog();
        }


    }

    private void backToMainActivity() {
        Intent sigInIntent = new Intent(this, SignInActivity_Driver.class);
        sigInIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sigInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(sigInIntent);
        finishAffinity();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

    }

    private void openCameraPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogEnable(this, getResources().getString(R.string
                .msg_reason_for_camera_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void doWithEnable() {
                ActivityCompat.requestPermissions(RegisterActivity_Driver.this, new String[]{Manifest
                        .permission
                        .CAMERA, Manifest
                        .permission
                        .READ_EXTERNAL_STORAGE}, Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
                closedPermissionDialog();
            }

            @Override
            public void doWithDisable() {
                closedPermissionDialog();
            }
        };
        customDialogEnable.show();
    }

    private void openPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogEnable(this, getResources().getString(R.string
                .msg_reason_for_permission_location), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void doWithEnable() {
                ActivityCompat.requestPermissions(RegisterActivity_Driver.this, new String[]{Manifest
                        .permission
                        .ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const
                        .PERMISSION_FOR_LOCATION);
                closedPermissionDialog();
            }

            @Override
            public void doWithDisable() {
                closedPermissionDialog();
                finishAffinity();
            }
        };
        customDialogEnable.show();
    }

    private void openPermissionNotifyDialog(final int code) {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogEnable(this, getResources()
                .getString(R.string
                        .msg_permission_notification), getResources()
                .getString(R
                        .string.text_exit_caps), getResources().getString(R
                .string
                .text_settings)) {
            @Override
            public void doWithEnable() {
                closedPermissionDialog();
                startActivityForResult(getIntentForPermission(), code);

            }

            @Override
            public void doWithDisable() {
                closedPermissionDialog();
                finishAffinity();
            }
        };
        customDialogEnable.show();

    }

    private void closedPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            customDialogEnable.dismiss();
            customDialogEnable = null;

        }
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                    .ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const
                    .PERMISSION_FOR_LOCATION);
        } else {
            getServicesCountry();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        openPhotoDialog();
                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest
                                .permission.CAMERA)) {
                            openCameraPermissionDialog();
                        } else {
                            closedPermissionDialog();
                            openPermissionNotifyDialog(Const
                                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
                        }
                    } else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest
                                .permission.READ_EXTERNAL_STORAGE)) {
                            openCameraPermissionDialog();
                        } else {
                            closedPermissionDialog();
                            openPermissionNotifyDialog(Const
                                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
                        }
                    }
                    break;
                }
            case Const.PERMISSION_FOR_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        getServicesCountry();
                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest
                                .permission.ACCESS_COARSE_LOCATION) && ActivityCompat
                                .shouldShowRequestPermissionRationale(this, Manifest
                                        .permission.ACCESS_FINE_LOCATION)) {
                            openPermissionDialog();
                        } else {
                            openPermissionNotifyDialog(Const.PERMISSION_FOR_LOCATION);
                        }
                    }
                    break;
                }
            default:
                //Do something
                break;

        }
    }

    private void getCityListSelectedCountry(List<City> cities) {
        tvRegisterCityName.setText(getResources().getString(R.string
                .text_hint_select_city));
        tvRegisterCityName.setTextColor(ResourcesCompat.getColor(getResources(),
                R.color
                        .color_app_label, null));
        cityList.clear();
        if (cities != null && !cities.isEmpty()) {
            cityList.addAll(cities);
        }
        Utils.hideCustomProgressDialog();
        if (cityList.isEmpty()) {
            if (Utils.isGpsEnable(RegisterActivity_Driver.this)) {
                openRequestRegisterTypeDialog(getResources().getString(R.string
                        .msg_type_not_available_current_country));
            }
        }

    }

    private void setCountry(int position) {
        tvCountryCode.setText((countryList.get(position).getCountryphonecode()));
        phoneNumberLength = countryList.get(position).getPhoneNumberLength();
        phoneNumberMinLength = countryList.get(position).getPhoneNumberMinLength();
        if (!selectedCountryPhoneCode.equalsIgnoreCase(countryList.get(position)
                .getCountryphonecode())) {
            etContactNumber.getText().clear();
            selectedCountryPhoneCode = countryList.get(position).getCountryphonecode();
        }
        setContactNoLength(phoneNumberLength);
        country = countryList.get(position);
        tvRegisterCountryName.setText(countryList.get(position).getCountryname());
        tvRegisterCountryName.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                .color_app_text, null));

       // tvFlag.setText("https://www.alberdriver.com/flags/united_arab_emirates.gif");
        PicassoTrustAll.getInstance(RegisterActivity_Driver.this)
                .load(ApiClient.Base_URL+countryList.get(position).getFlagUrl())
               // .error(R.drawable.ellipse)
                .into(tvFlag);
        if(countryList.get(position).getCountryphonecode().equalsIgnoreCase("+33"))
        {
            etContactNumber.setMask("# ## ## ## ##");

        }

        else  if(countryList.get(position).getCountryphonecode().equalsIgnoreCase("+91"))
        {
            etContactNumber.setMask("## #### ####");

        }

        else if(countryList.get(position).getCountryphonecode().equalsIgnoreCase("+971"))
        {
            etContactNumber.setMask("## ### ####");

        }

        else
        {
            etContactNumber.setMask("# ## ## ## ##");

        }

        getCityListSelectedCountry(countryList.get(position).getCities());
    }

    private void setContactNoLength(int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        etContactNumber.setFilters(FilterArray);
    }

    private void openRequestRegisterTypeDialog(String message) {
        customDialogBigLabel = new CustomDialogBigLabel(this, getResources().getString(R.string
                .text_register_type), message, getResources().getString(R.string
                .text_email), getResources().getString(R.string.text_cancel)) {
            @Override
            public void positiveButton() {
                dismiss();
                contactUsWithEmail(preferenceHelper.getContactUsEmail());

            }

            @Override
            public void negativeButton() {
                dismiss();
            }
        };
        customDialogBigLabel.show();
    }

    private void OTPVerify() {

        Utils.showCustomProgressDialog(this, getResources().getString(R.string.msg_loading),
                false, null);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.EMAIL, etEmail.getText());
            jsonObject.put(Const.Params.PHONE, etContactNumber.getText());
            jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, tvCountryCode.getText()
                    .toString());
            jsonObject.put(Const.Params.TYPE, Const.PROVIDER);
            Call<VerificationResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .verification(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<VerificationResponse>() {
                @Override
                public void onResponse(Call<VerificationResponse> call,
                                       Response<VerificationResponse> response) {
                    if (parseContent.isSuccessful(response)) {
                        if (response.body().isSuccess()) {
                            Utils.hideCustomProgressDialog();
                            otpForEmail = response.body().getOtpForEmail();
                            otpForSMS = response.body().getOtpForSMS();
                            openOTPVerifyDialog();

                        } else {
                            Utils.hideCustomProgressDialog();
                            Utils.showErrorToast(response.body().getErrorCode(), RegisterActivity_Driver
                                    .this);
                        }
                    }


                }

                @Override
                public void onFailure(Call<VerificationResponse> call, Throwable t) {
                    AppLog.handleThrowable(ProfileActivity_Driver.class.getSimpleName(), t);
                }
            });

        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.REGISTER_ACTIVITY, e);
        }
    }

    private void openOTPVerifyDialog() {
        //Code add 02-FEB-2020

        if (customDialogVerifyDetail != null && customDialogVerifyDetail.isShowing()) {
            return;
        }
        customDialogVerifyDetail = new CustomDialogVerifyDetail(this, isEmailVerify, isSMSVerify,otpForEmail,otpForSMS) {
            @Override
            public void doWithSubmit(EditText etEmailVerify, EditText etSMSVerify) {

                if (isEmailVerify && isSMSVerify) {


                    if (otpForEmail.equals(etEmailVerify.getText().toString()) && otpForSMS.equals
                            (etSMSVerify.getText().toString())) {
                        dismiss();
                      register(loginType);
                        //setData(loginType);

                    } else {
                        Utils.showToast(getResources().getString(R.string.msg_otp_wrong),
                                RegisterActivity_Driver.this);
                    }


                } else if (isEmailVerify) {
                    if (otpForEmail.equals(etEmailVerify.getText().toString())) {
                        dismiss();
                     register(loginType);
                      //  setData(loginType);

                    } else {
                        Utils.showToast(getResources().getString(R.string.msg_otp_wrong),
                                RegisterActivity_Driver.this);
                    }


                } else {
                    if (otpForSMS.equals(etSMSVerify.getText().toString())) {
                        dismiss();
                        register(loginType);

                    } else {
                        Utils.showToast(getResources().getString(R.string.msg_otp_wrong),
                                RegisterActivity_Driver.this);
                    }
                }


            }

            @Override
            public void doCancel() {
                customDialogVerifyDetail.dismiss();
            }
        };
        customDialogVerifyDetail.show();
    }

    private void openDetailNotSaveDialog() {
        CustomDialogBigLabel detailNotSaveDialog = new CustomDialogBigLabel(this, getResources()
                .getString(R.string.msg_are_you_sure),
                getResources().getString(R.string.msg_not_save)
                , getResources().getString(R.string.text_yes), getResources().getString(R.string
                .text_no)) {
            @Override
            public void positiveButton() {
                this.dismiss();
                isBackPressedOnce = true;
                RegisterActivity_Driver.this.onBackPressed();

            }

            @Override
            public void negativeButton() {
                this.dismiss();
            }
        };
        detailNotSaveDialog.show();
    }

    private void getServicesCountry() {
        Utils.showCustomProgressDialog(this, "", false, null);
        Call<CountriesResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                .getCountries();
        call.enqueue(new Callback<CountriesResponse>() {
            @Override
            public void onResponse(Call<CountriesResponse> call, Response<CountriesResponse>
                    response) {
                if (parseContent.isSuccessful(response)) {
                    Utils.hideCustomProgressDialog();
                    System.out.println("Country List  "+response.body());


                    countryList.addAll(response.body().getCountry());
                 //   Collections.reverse(countryList);


                    locationHelper.getLastLocation(RegisterActivity_Driver.this, new
                            OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    lastLocation = location;
                                    AppLog.Log("GET LOCATION IS :", "" + lastLocation);
                                    if (!countryList.isEmpty()) {
                                        if (lastLocation != null) {
                                           // new GetCityAndCountryTask().execute();
                                        } else {
                                            //setCountry(0);
                                        }
                                    }
                                }
                            });


                }
            }

            @Override
            public void onFailure(Call<CountriesResponse> call, Throwable t) {
                AppLog.handleThrowable(ProfileActivity_Driver.class.getSimpleName(), t);
            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            closedEnableDialogInternet();
        } else {
            openInternetDialog();
        }
    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {
        if (isConnected) {
            closedEnableDialogGps();
        } else {
            openGpsDialog();

        }
    }

    @Override
    public void onAdminApproved() {
        goWithAdminApproved();
    }

    @Override
    public void onAdminDeclined() {
        goWithAdminDecline();
    }


    private void goToTermsAndConditionPage() {
        Intent termsActivity = new Intent(RegisterActivity_Driver.this,
                TermsActivity_Driver.class);
        startActivity(termsActivity);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void initGenderSelection() {
        selectGender = findViewById(R.id.selectGender);
        String[] typedArray = getResources().getStringArray(R.array.gender);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.item_spinner,
                typedArray);
        selectGender.setAdapter(arrayAdapter);
        selectGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String[] typedArray = getResources().getStringArray(R.array.gender_english);
                gender = typedArray[i].toString();
                AppLog.Log("GENDER", gender);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    protected class GetCityAndCountryTask extends AsyncTask<String, Void, Address> {

        @Override
        protected Address doInBackground(String... params) {

            Geocoder geocoder = new Geocoder(RegisterActivity_Driver.this, new Locale("en_US"));
            try {
                List<Address> addressList = geocoder.getFromLocation(lastLocation.getLatitude(),
                        lastLocation.getLongitude(), 1);
                if (addressList != null && !addressList.isEmpty()) {

                    Address address = addressList.get(0);
                    return address;
                }

            } catch (IOException e) {
                e.printStackTrace();
                AppLog.handleException(Const.Tag.REGISTER_ACTIVITY, e);
                publishProgress();

            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            getCountryCodeList("India");
        }

        @Override
        protected void onPostExecute(Address address) {

//            System.out.println("address:::::"+address.toString());
            if (address != null) {
                String countryName = address.getCountryName();
                getCountryCodeList(countryName);
                String currentCityName;
                if (address.getLocality() != null) {
                    currentCityName = address.getLocality();
                } else if (address.getSubAdminArea() != null) {
                    currentCityName = address.getSubAdminArea();
                } else {
                    currentCityName = address.getAdminArea();
                }
                AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "countryName= " + countryName);
                AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "currentCityName= " + currentCityName);
            } else {
                getCountryCodeList("United States");
            }
        }
    }
    private void randomWaterBallAnimation(){

      /*  water_ball1 =  findViewById(R.id.water_ball);


        // For First First

        RotateAnimation anim = new RotateAnimation(0f, 350f, 15f, 15f);
        //   RotateAnimation anim = new RotateAnimation(0f, 0.5f, 1.1f, -0.3f);

        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(9000);

        TranslateAnimation mAnimation ;
        mAnimation = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.3f);
        mAnimation.setDuration(25000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());

        water_ball1.setAnimation(mAnimation);
        water_ball1.startAnimation(anim);


        // For Second Second  water_ball_2 =  findViewById(R.id.water_ball_2);
        water_ball_21 =  findViewById(R.id.water_ball_2);
        TranslateAnimation mAnimation2 ;
        mAnimation2 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.7f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation2.setDuration(30000);
        mAnimation2.setRepeatCount(-1);
        mAnimation2.setRepeatMode(Animation.REVERSE);
        mAnimation2.setInterpolator(new LinearInterpolator());

        water_ball_21.setAnimation(mAnimation2);

        water_ball_21.animate().rotation(1800f).setDuration(25000).start();
        water_ball_21.getAnimation().setRepeatCount(-1);

        water_ball_21.getAnimation().setRepeatMode(Animation.REVERSE);
        water_ball_21.getAnimation().setInterpolator(new LinearInterpolator());*/




        water_ball_3 = findViewById(R.id.water_ball_3);


        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(19000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());
        water_ball_3.setAnimation(mAnimation3);

        water_ball_4 = findViewById(R.id.water_ball_4);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());
        water_ball_4.setAnimation(mAnimation4);

        water_ball_5 = findViewById(R.id.water_ball_5);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());
        water_ball_5.setAnimation(mAnimation5);

        water_ball_6 = findViewById(R.id.water_ball_6);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF,0.0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());
        water_ball_6.setAnimation(mAnimation6);

        water_ball_7 = findViewById(R.id.water_ball_7);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.1f);
        mAnimation7.setDuration(8000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());
        water_ball_7.setAnimation(mAnimation7);

        water_ball_8 = findViewById(R.id.water_ball_8);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());
        water_ball_8.setAnimation(mAnimation8);


        Water_ball_10 = findViewById(R.id.water_ball_10);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());
        Water_ball_10.setAnimation(mAnimation10);

        Water_ball_9 = findViewById(R.id.water_ball_9);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(10000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_9.setAnimation(mAnimation9);
    }

    private void getVehicleType(String countryName, double cityLatitude, double
            cityLongitude) {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put(Const.Params.COUNTRY, countryName);
            jsonObject.put(Const.Params.LATITUDE, cityLatitude);
            jsonObject.put(Const.Params.LONGITUDE, cityLongitude);

            Call<TypesResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .withoutlogin_getVehicleTypes(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<TypesResponse>() {
                @Override
                public void onResponse(@NonNull Call<TypesResponse> call,
                                       @NonNull Response<TypesResponse> response) {
                    if (ParseContent.getInstance().isSuccessful(response) && response.body() != null) {
                        vehicleTypeList.clear();
                        vehicleTypeList2.clear();

                        if (parseContent.parseTypes(response.body())) {
                            vehicleTypeList.addAll(response.body().getCityTypes());

    for(int k=0;k<vehicleTypeList.size();k++)
    {
    vehicleTypeList2.add(vehicleTypeList.get(k).getTypename());
    }
                            ArrayAdapter<String> dealAdapter = new ArrayAdapter<String>(
                                    RegisterActivity_Driver.this,
                                    R.layout.detailspinnertext, vehicleTypeList2);
                            dealAdapter
                                    .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            tvselect_vehicle_type.setAdapter(dealAdapter);



                        } else {
                            try {
                                String msg;
                                String errorCode =
                                        Const.ERROR_CODE_PREFIX + response.body().getErrorCode();
                                msg =
                                        getResources().getString(getResources().getIdentifier
                                                (errorCode, Const.STRING,
                                                        getPackageName()));


                            } catch (Resources.NotFoundException e) {
                            }
                        }
                    }

                }

                @Override
                public void onFailure(Call<TypesResponse> call, Throwable t) {
                    AppLog.handleThrowable(RegisterActivity_Driver.class.getSimpleName(), t);
                }
            });
        } catch (
                JSONException e) {
            AppLog.handleException(Const.Tag.MAP_FRAGMENT, e);
        }


    }

    private void randomWaterBallAnimation1(){




        water_ball_31 = findViewById(R.id.water_ball_31);


        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(9000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());

        water_ball_31.setAnimation(mAnimation3);



        water_ball_41 = findViewById(R.id.water_ball_41);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());

        water_ball_41.setAnimation(mAnimation4);


        water_ball_51 = findViewById(R.id.water_ball_51);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());

        water_ball_51.setAnimation(mAnimation5);


        water_ball_61= findViewById(R.id.water_ball_61);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());

        water_ball_61.setAnimation(mAnimation6);


        water_ball_71 = findViewById(R.id.water_ball_71);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation7.setDuration(9000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());

        water_ball_71.setAnimation(mAnimation7);


        water_ball_81 = findViewById(R.id.water_ball_81);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());

        water_ball_81.setAnimation(mAnimation8);



        Water_ball_101 = findViewById(R.id.water_ball_101);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());

        Water_ball_101.setAnimation(mAnimation10);


        Water_ball_91 = findViewById(R.id.water_ball_91);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(9000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_91.setAnimation(mAnimation9);
    }

}