package com.swissexpressdz.driver.activity.contacts;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.feedback.FeedbackActivity;


public class Emergency_ContactsActivity extends AppCompatActivity implements View.OnClickListener {

    TextView title,add_contacts;
    RelativeLayout back_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency__contacts);
        inti();
    }
    public  void inti(){
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.contact_emergency_contacts);
        add_contacts=(TextView)findViewById(R.id.add_contacts);
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);


        add_contacts.setOnClickListener(this);
        back_icon.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v==add_contacts){
            Intent ii= new Intent(this, FeedbackActivity.class);
            startActivity(ii);
        }
        if(v==back_icon){
            finish();
        }

    }
}
