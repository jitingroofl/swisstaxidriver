package com.swissexpressdz.driver.activity.discuss;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.setting.SettingActivity;


public class DiscussActivity extends AppCompatActivity implements View.OnClickListener {
    TextView title;
    RelativeLayout back_icon,rr_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discuss);
        inti();
    }

    public  void inti(){
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.discuss);
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        rr_send=(RelativeLayout)findViewById(R.id.rr_send);
        back_icon.setOnClickListener(this);
        rr_send.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(back_icon==v){
            finish();
        }
        if(v==rr_send){
            Intent intent=new Intent(this, SettingActivity.class);
            startActivity(intent);
        }

    }
}
