package com.swissexpressdz.driver.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.support.SupportActivity;


public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    TextView title,txt_sauvegaeder;
    RelativeLayout back_icon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        inti();
    }
    public  void inti(){
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.account_setting);
        back_icon=(RelativeLayout) findViewById(R.id.back_icon);
        txt_sauvegaeder=(TextView)findViewById(R.id.txt_sauvegaeder);

        back_icon.setOnClickListener(this);
        txt_sauvegaeder.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        if(v==back_icon){
            finish();
        }
        if(v==txt_sauvegaeder){
            Intent intent=new Intent(this, SupportActivity.class);
            startActivity(intent);

        }

    }
}
