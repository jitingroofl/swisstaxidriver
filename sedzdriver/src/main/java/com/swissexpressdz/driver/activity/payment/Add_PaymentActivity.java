package com.swissexpressdz.driver.activity.payment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.contacts.Emergency_ContactsActivity;


public class Add_PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    TextView title ,add_new_card;
    RelativeLayout back_icon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__payment);
        inti();
    }

    public  void  inti(){
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.payment_status);
        add_new_card=(TextView)findViewById(R.id.add_new_card);
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        back_icon.setOnClickListener(this);
        add_new_card.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==add_new_card){
            Intent ii=new Intent(this, Emergency_ContactsActivity.class);
            startActivity(ii);
        }
        if(v==back_icon){
            finish();
        }

    }
}
