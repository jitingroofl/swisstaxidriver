package com.swissexpressdz.driver.activity.history;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.adapter.DetailsAdapter;
import com.swissexpressdz.driver.activity.adapter.TodayDetailAdapter;

import java.util.ArrayList;

public class Detail_History_RaceActivity extends AppCompatActivity implements View.OnClickListener {

    TextView title;
    RecyclerView recyclerView,yesterday_list_item;
    final ThreadLocal<TodayDetailAdapter> todayDetailAdapter = new ThreadLocal<TodayDetailAdapter>();
    LinearLayoutManager linearLayoutManager,linearLayoutManager1;
    DetailsAdapter detailsAdapter;
    ArrayList list=new ArrayList();
    TextView today;
    RelativeLayout back_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail__history__race);
        inti();
    }

    public  void  inti(){

        list.add("Ram");
        list.add("Shyam");


        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.detail_of_the_race_history);
        today=(TextView)findViewById(R.id.today) ;
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);


        recyclerView=(RecyclerView)findViewById(R.id.today_list_item);
        linearLayoutManager = new LinearLayoutManager(Detail_History_RaceActivity.this);
        todayDetailAdapter.set(new TodayDetailAdapter(Detail_History_RaceActivity.this, list));
        recyclerView.setAdapter(todayDetailAdapter.get());
        recyclerView.setLayoutManager(linearLayoutManager);


        yesterday_list_item=(RecyclerView)findViewById(R.id.yesterday_list_item);
        linearLayoutManager1= new LinearLayoutManager(Detail_History_RaceActivity.this);
        detailsAdapter=new DetailsAdapter(Detail_History_RaceActivity.this,list);
        yesterday_list_item.setLayoutManager(linearLayoutManager1);
        yesterday_list_item.setAdapter(detailsAdapter);

        today.setOnClickListener(this);
        back_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(v==today){
            Intent ii=new Intent(this,Details_History_Rase_TripActivity.class);
            startActivity(ii);
        }
        if(v==back_icon){
            finish();
        }

    }
}
