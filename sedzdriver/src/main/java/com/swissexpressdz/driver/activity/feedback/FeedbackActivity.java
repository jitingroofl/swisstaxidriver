package com.swissexpressdz.driver.activity.feedback;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.discuss.DiscussActivity;


public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener {

    TextView title;
    Button submit;
    RelativeLayout back_icon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);


        inti();


    }

    public  void  inti(){
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.feedback);
        submit=(Button)findViewById(R.id.submit);
        back_icon=(RelativeLayout) findViewById(R.id.back_icon);

        submit.setOnClickListener(this);
        back_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==submit){
            Intent ii =new Intent(this, DiscussActivity.class);
            startActivity(ii);
        }
        if(v==back_icon){
            finish();
        }

    }
}
