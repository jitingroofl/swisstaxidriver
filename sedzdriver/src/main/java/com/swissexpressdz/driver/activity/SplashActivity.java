package com.swissexpressdz.driver.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.swissexpressdz.driver.BaseAppCompatActivity;
import com.swissexpressdz.driver.MainDrawerActivity;
import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.SelectLanguageActivity;
import com.swissexpressdz.driver.SignInActivity_Driver;
import com.swissexpressdz.driver.SplashScreenActivity_Driver;
import com.swissexpressdz.driver.components.CustomDialogBigLabel;
import com.swissexpressdz.driver.models.datamodels.AdminSettings;
import com.swissexpressdz.driver.models.responsemodels.SettingsDetailsResponse;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.PreferenceHelper;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends BaseAppCompatActivity {

    ImageView water_ball1,water_ball_21,water_ball_31,water_ball_41,water_ball_51,water_ball_61,water_ball_71,water_ball_81,Water_ball_91,Water_ball_101;

    ProgressBar loader;
    public PreferenceHelper preferenceHelper;
    int oneTimeCall;
    SplashScreenActivity_Driver splashScreenActivity_driver;
    Uri uri_data;
    Uri deepLink = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_splash);


        splashScreenActivity_driver=new SplashScreenActivity_Driver();

        preferenceHelper = PreferenceHelper.getInstance(this);

        loader=(ProgressBar)findViewById(R.id.loader);
        loader.setVisibility(View.VISIBLE);

        Intent in = getIntent();
        uri_data = in.getData();

        System.out.println("Data is###"+uri_data);

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {

                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            System.out.println("getDynamicLink:onSucess "+deepLink);



//                            String user_id = deepLink.getQueryParameter("user_id");
//                            System.out.println("User id Dynamic "+user_id);


                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("", "getDynamicLink:onFailure", e);
                    }
                });

        checkIfGpsOrInternetIsEnable();

//        Handler handler=new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                checkIfGpsOrInternetIsEnable();
//            }
//        },2000);
    }

    @Override
    protected boolean isValidate() {
        return false;
    }
    @Override
    protected void onResume() {
        super.onResume();
        setConnectivityListener(this);
        setAdminApprovedListener(this);
    }

    @Override
    public void goWithBackArrow() {

    }


    private void checkIfGpsOrInternetIsEnable() {
        if (!Utils.isInternetConnected(this)) {
            loader.setVisibility(View.GONE);

            openInternetDialog();
        } else {
            closedEnableDialogGps();
            closedEnableDialogInternet();
    System.out.println("Device Token  Splash before "+preferenceHelper.getDeviceToken());

            if(deepLink!=null)
            {
                System.out.println("Device Token  Splash "+preferenceHelper.getDeviceToken());

            }
            else
            {
                getAPIKeys();
            }
          //  getAPIKeys();
        }

    }
    public  void  main(){

        if(preferenceHelper.getSessionToken()!=null && !preferenceHelper.getSessionToken().equalsIgnoreCase("") && !preferenceHelper.getSessionToken().equalsIgnoreCase("null"))
        {

            Intent intent=new Intent(this, MainDrawerActivity.class);
            startActivity(intent);

        }

        else
        {
            // finish();
//         preferenceHelper.putLanguageCode(language);
            Intent intent=new Intent(this, SignInActivity_Driver.class);
            startActivity(intent);

            // loader.setVisibility(View.GONE);
        }


    }

//    public void getDriverDetails() {
//
//        if (oneTimeCall == 0) {
//            oneTimeCall++;
//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put(Const.Params.PROVIDER_ID, !TextUtils.isEmpty(preferenceHelper
//                        .getSessionToken()) ? preferenceHelper.getProviderId() : null);
//                jsonObject.put(Const.Params.TOKEN, preferenceHelper.getSessionToken());
//                jsonObject.put(Const.Params.APP_VERSION, getAppVersion());
//                jsonObject.put(Const.Params.DEVICE_TYPE, Const.DEVICE_TYPE_ANDROID);
//                Call<SettingsDetailsResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface
//                        .class)
//                        .getProviderSettingDetail
//                                (ApiClient.makeJSONRequestBody(jsonObject));
//                call.enqueue(new Callback<SettingsDetailsResponse>() {
//                    @Override
//                    public void onResponse(Call<SettingsDetailsResponse> call,
//                                           Response<SettingsDetailsResponse>
//                                                   response) {
//
//                        loader.setVisibility(View.GONE);
//                        if (parseContent.isSuccessful(response)) {
//
//                            parseContent.parseProviderSettingDetail(response.body());
//
//                            System.out.println("email admin::::"+response.body().getAdminSettings().getAdminPhone());
//                            System.out.println("contect::::"+response.body().getAdminSettings().getContactUsEmail());
//
//                            preferenceHelper.putAdminPhone(response.body().getAdminSettings().getAdminPhone());
//                            preferenceHelper.putContactUsEmail(response.body().getAdminSettings().getContactUsEmail());
//
//
//                            /***
//                             * check if user login session is not expired
//                             */
//                            AdminSettings adminSettings = response.body().getAdminSettings();
////                            if (checkVersionCode(adminSettings.getAndroidProviderAppVersionCode
////                                    ())) {
////                                openUpdateAppDialog(adminSettings
////                                        .isAndroidProviderAppForceUpdate());
////                            } else {
//                            if (TextUtils.isEmpty(preferenceHelper.getSessionToken())) {
////                                    goToMainActivity();
//                                Intent intent=new Intent(SplashActivity.this, SelectLanguageActivity.class);
//                                startActivity(intent);
//
//
//                            } else {
//
//                                try {
//                                    preferenceHelper.putProviderRating(response.body().getProviderData().getRate());
//
//                                    preferenceHelper.putIsRegisterStatus(response.body().getProviderData().isregisterSuccess());
//                                    preferenceHelper.putIsServiceStatus(response.body().getProviderData().isserviceSuccess());
//                                    preferenceHelper.putIsVehiclerStatus(response.body().getProviderData().isvehicleSuccess());
//                                    preferenceHelper.putIsDocumentStatus(response.body().getProviderData().isdocumentSuccess());
//
//                                    Intent intent=new Intent(SplashActivity.this, SelectLanguageActivity.class);
//                                    startActivity(intent);
//
//
//
//
//
//                                }
//                                catch (Exception e)
//                                {
//
//                                }
//
//
////                                Intent intent=new Intent(SplashActivity.this, MainDrawerActivity.class);
////                                startActivity(intent);
//                            }
//                        }
//
//                    }
//                    //    }
//
//                    private boolean checkVersionCode(String code) {
//
//                        String[] appVersionWeb = code
//                                .split("\\.");
//                        String[] appVersion = getAppVersion().split("\\.");
//
//                        int sizeWeb = appVersionWeb.length;
//                        int sizeApp = appVersion.length;
//                        if (sizeWeb == sizeApp) {
//                            for (int i = 0; i < sizeWeb; i++) {
//                                if (Double.valueOf(appVersionWeb[i]) > Double
//                                        .valueOf(appVersion[i])) {
//                                    return true;
//                                } else if ((Integer.valueOf(appVersionWeb[i]) == Integer
//                                        .valueOf(appVersion[i]))) {
//                                } else {
//                                    return false;
//                                }
//
//                            }
//                            return false;
//                        }
//                        return true;
//
//                    }
//                    @Override
//                    public void onFailure(Call<SettingsDetailsResponse> call, Throwable t) {
//                        AppLog.handleThrowable(BaseAppCompatActivity.class.getSimpleName(), t);
//                    }
//                });
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    public void getAPIKeys() {

        if (oneTimeCall == 0) {
            oneTimeCall++;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put(Const.Params.PROVIDER_ID, !TextUtils.isEmpty(preferenceHelper
                        .getSessionToken()) ? preferenceHelper.getProviderId() : null);
                jsonObject.put(Const.Params.TOKEN, preferenceHelper.getSessionToken());
                jsonObject.put(Const.Params.APP_VERSION, getAppVersion());
                jsonObject.put(Const.Params.DEVICE_TYPE, Const.DEVICE_TYPE_ANDROID);
                Call<SettingsDetailsResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface
                        .class)
                        .getProviderSettingDetail
                                (ApiClient.makeJSONRequestBody(jsonObject));
                call.enqueue(new Callback<SettingsDetailsResponse>() {
                    @Override
                    public void onResponse(Call<SettingsDetailsResponse> call,
                                           Response<SettingsDetailsResponse>
                                                   response) {

                        loader.setVisibility(View.GONE);
                        if (parseContent.isSuccessful(response)) {

                            parseContent.parseProviderSettingDetail(response.body());

                            System.out.println("email admin::::"+response.body().getAdminSettings().getAdminPhone());
                            System.out.println("contect::::"+response.body().getAdminSettings().getContactUsEmail());

                            preferenceHelper.putAdminPhone(response.body().getAdminSettings().getAdminPhone());
                            preferenceHelper.putContactUsEmail(response.body().getAdminSettings().getContactUsEmail());


                            /***
                             * check if user login session is not expired
                             */
                            AdminSettings adminSettings = response.body().getAdminSettings();

                            if (TextUtils.isEmpty(preferenceHelper.getSessionToken())) {
//                                    goToMainActivity();
                                Intent intent=new Intent(SplashActivity.this, SelectLanguageActivity.class);
                                startActivity(intent);


                            } else {

                                try {
                                    preferenceHelper.putProviderRating(response.body().getProviderData().getRate());

                                    preferenceHelper.putIsRegisterStatus(response.body().getProviderData().isregisterSuccess());
                                    preferenceHelper.putIsServiceStatus(response.body().getProviderData().isserviceSuccess());
                                    preferenceHelper.putIsVehiclerStatus(response.body().getProviderData().isvehicleSuccess());
                                    preferenceHelper.putIsDocumentStatus(response.body().getProviderData().isdocumentSuccess());

                                    if(response.body().getProviderData().isregisterSuccess() == true && response.body().getProviderData().isserviceSuccess() == true
                                    && response.body().getProviderData().isvehicleSuccess() == true && response.body().getProviderData().isdocumentSuccess() == true)


                                    {
                                   Intent intent=new Intent(SplashActivity.this, MainDrawerActivity.class);
                                        startActivity(intent);
                                    }
                                    else
                                    {
                                        Intent intent=new Intent(SplashActivity.this, SelectLanguageActivity.class);
                                        startActivity(intent);
                                    }






                                }
                                catch (Exception e)
                                {
                                    Intent intent=new Intent(SplashActivity.this, SelectLanguageActivity.class);
                                    startActivity(intent);
                                }


                            }
                        }

                    }
                    //    }

                    private boolean checkVersionCode(String code) {

                        String[] appVersionWeb = code
                                .split("\\.");
                        String[] appVersion = getAppVersion().split("\\.");

                        int sizeWeb = appVersionWeb.length;
                        int sizeApp = appVersion.length;
                        if (sizeWeb == sizeApp) {
                            for (int i = 0; i < sizeWeb; i++) {
                                if (Double.valueOf(appVersionWeb[i]) > Double
                                        .valueOf(appVersion[i])) {
                                    return true;
                                } else if ((Integer.valueOf(appVersionWeb[i]) == Integer
                                        .valueOf(appVersion[i]))) {
                                } else {
                                    return false;
                                }

                            }
                            return false;
                        }
                        return true;

                    }
                    @Override
                    public void onFailure(Call<SettingsDetailsResponse> call, Throwable t) {
                        AppLog.handleThrowable(BaseAppCompatActivity.class.getSimpleName(), t);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void openUpdateAppDialog(final boolean isForceUpdate) {
        String btnNegative;
        if (isForceUpdate) {
            btnNegative = getResources()
                    .getString(R.string.text_exit_caps);
        } else {
            btnNegative =
                    getResources()
                            .getString(R.string.text_not_now);
        }

        final CustomDialogBigLabel customDialogBigLabel = new CustomDialogBigLabel(this,
                getResources()
                        .getString(R.string.text_update_app), getResources()
                .getString(R.string.meg_update_app), getResources()
                .getString(R.string.text_update), btnNegative) {
            @Override
            public void positiveButton() {
                final String appPackageName = getPackageName(); // getPackageName() from Context
                // or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
                            + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google" +
                            ".com/store/apps/details?id=" + appPackageName)));
                }
                dismiss();
                finishAffinity();
            }

            @Override
            public void negativeButton() {
                dismiss();
                if (isForceUpdate) {
                    finishAffinity();
                } else {
                    if (!TextUtils.isEmpty(preferenceHelper.getSessionToken())) {
//                        goToMainDrawerActivity();
                        Intent intent=new Intent(SplashActivity.this, MainDrawerActivity.class);
                        startActivity(intent);
                    } else {
                        // goToMainActivity();
                        Intent intent=new Intent(SplashActivity.this, SignInActivity_Driver.class);
                        startActivity(intent);
                    }
                }


            }
        };
        if (!isFinishing()) {
            customDialogBigLabel.show();
        }


    }


    private void randomWaterBallAnimation(){
        water_ball1 =  findViewById(R.id.water_ball1);        // For First First
        RotateAnimation anim = new RotateAnimation(0f, 350f, 15f, 15f);
        //  RotateAnimation anim = new RotateAnimation(0f, 0.5f, 1.1f, -0.3f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(9000);        TranslateAnimation mAnimation ;
        mAnimation = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.3f);
        mAnimation.setDuration(25000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        water_ball1.setAnimation(mAnimation);
        water_ball1.startAnimation(anim);
        // For Second Second
        // water_ball_2 =  findViewById(R.id.water_ball_2);
        water_ball_21 =  findViewById(R.id.water_ball_21);
        TranslateAnimation mAnimation2 ;
        mAnimation2 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.7f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation2.setDuration(30000);
        mAnimation2.setRepeatCount(-1);
        mAnimation2.setRepeatMode(Animation.REVERSE);
        mAnimation2.setInterpolator(new LinearInterpolator());
        water_ball_21.setAnimation(mAnimation2);
        water_ball_21.animate().rotation(1800f).setDuration(25000).start();
        water_ball_21.getAnimation().setRepeatCount(-1);
        water_ball_21.getAnimation().setRepeatMode(Animation.REVERSE);
        water_ball_21.getAnimation().setInterpolator(new LinearInterpolator());
        water_ball_31 = findViewById(R.id.water_ball_31);        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(9000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());
        water_ball_31.setAnimation(mAnimation3);
        water_ball_41 = findViewById(R.id.water_ball_41);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());
        water_ball_41.setAnimation(mAnimation4);
        water_ball_51 = findViewById(R.id.water_ball_51);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());
        water_ball_51.setAnimation(mAnimation5);
        water_ball_61= findViewById(R.id.water_ball_61);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());
        water_ball_61.setAnimation(mAnimation6);
        water_ball_71 = findViewById(R.id.water_ball_71);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation7.setDuration(9000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());
        water_ball_71.setAnimation(mAnimation7);
        water_ball_81 = findViewById(R.id.water_ball_81);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());
        water_ball_81.setAnimation(mAnimation8);        Water_ball_101 = findViewById(R.id.water_ball_101);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());
        Water_ball_101.setAnimation(mAnimation10);
        Water_ball_91 = findViewById(R.id.water_ball_91);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(10000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_91.setAnimation(mAnimation9);
    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }


}
