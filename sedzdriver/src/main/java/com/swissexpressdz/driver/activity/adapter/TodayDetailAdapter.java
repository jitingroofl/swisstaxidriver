package com.swissexpressdz.driver.activity.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.history.Detail_History_RaceActivity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class TodayDetailAdapter extends RecyclerView.Adapter<TodayDetailAdapter.ViewHolder> {
    Context context;
    ArrayList list;

    public TodayDetailAdapter(Detail_History_RaceActivity detail_history_raceActivity, ArrayList list) {

        this.context=detail_history_raceActivity;
        this.list=list;
    }


    @Override
    public TodayDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.today_detail_race, parent, false);

        TodayDetailAdapter.ViewHolder viewHolder = new TodayDetailAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        // viewHolder.UsernameInfoTV.setText(personUtils.get(i).getPatient());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout rr_contact;
        ImageView read_tv;
        CircleImageView propic_circular;
        public ViewHolder(View itemView) {
            super(itemView);

            //  rr_contact=(RelativeLayout) itemView.findViewById(R.id.rr_contact);

        }
    }
}
