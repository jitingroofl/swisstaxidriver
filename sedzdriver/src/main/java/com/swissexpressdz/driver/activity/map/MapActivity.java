package com.swissexpressdz.driver.activity.map;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.bill.Bill_FactureActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;


public class MapActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {


    LinearLayout conduire_layout,by_card_layout,driver_profile_layout,temp_layout ,click_start_layout,accept_layout,
            pikup_point_layout,location_header1;
    Button cancel_action,start_race_driver,btn_accept,go_online;
    RelativeLayout back_icon,car_image_layout;
    TextView text_des_frias;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 111;
    private GoogleMap mMap;
    TextView title;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment);
        mapFragment.getMapAsync(this);
        inti();
    }

    public  void inti(){
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.Destination);
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        conduire_layout=(LinearLayout)findViewById(R.id.conduire_layout);
        start_race_driver=(Button)findViewById(R.id.start_race_driver);
        cancel_action=(Button)findViewById(R.id.cancel_action);
        by_card_layout=(LinearLayout)findViewById(R.id.by_card_layout);
        driver_profile_layout=(LinearLayout)findViewById(R.id.driver_profile_layout);
        btn_accept=(Button)findViewById(R.id.btn_accept) ;
        text_des_frias=(TextView)findViewById(R.id.text_des_frias);
        temp_layout=(LinearLayout)findViewById(R.id.temp_layout);
        click_start_layout=(LinearLayout)findViewById(R.id.click_start_layout);
        accept_layout=(LinearLayout)findViewById(R.id.accept_layout);
        pikup_point_layout=(LinearLayout)findViewById(R.id.pikup_point_layout);
        location_header1=(LinearLayout)findViewById(R.id.location_header1);
        car_image_layout=(RelativeLayout)findViewById(R.id.car_image_layout);
        go_online=(Button)findViewById(R.id.go_online);




        conduire_layout.setVisibility(View.VISIBLE);
        by_card_layout.setVisibility(View.GONE);
        driver_profile_layout.setVisibility(View.GONE);
        text_des_frias.setVisibility(View.GONE);
        temp_layout.setVisibility(View.VISIBLE);
        click_start_layout.setVisibility(View.GONE);
        accept_layout.setVisibility(View.VISIBLE);
        pikup_point_layout.setVisibility(View.VISIBLE);
        location_header1.setVisibility(View.VISIBLE);
        car_image_layout.setVisibility(View.GONE);
        go_online.setVisibility(View.GONE);

        back_icon.setOnClickListener(this);
        start_race_driver.setOnClickListener(this);
        cancel_action.setOnClickListener(this);
        btn_accept.setOnClickListener(this);
        click_start_layout.setOnClickListener(this);
        go_online.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v==back_icon){
            finish();
        }
        if(v==start_race_driver){
            conduire_layout.setVisibility(View.GONE);
            by_card_layout.setVisibility(View.VISIBLE);
            driver_profile_layout.setVisibility(View.VISIBLE);

        }
        if (v==cancel_action){
            cancelDialog();
        }
        if(v==btn_accept){

            text_des_frias.setVisibility(View.VISIBLE);
            temp_layout.setVisibility(View.GONE);
            click_start_layout.setVisibility(View.VISIBLE);
            accept_layout.setVisibility(View.GONE);
            pikup_point_layout.setVisibility(View.GONE);
            by_card_layout.setVisibility(View.GONE);

        }
        if(v==click_start_layout){

            driver_profile_layout.setVisibility(View.GONE);
            location_header1.setVisibility(View.GONE);

            car_image_layout.setVisibility(View.VISIBLE);
            go_online.setVisibility(View.VISIBLE);

        }
        if (v==go_online){
            Intent ii=new Intent(this, Bill_FactureActivity.class);
            startActivity(ii);
        }


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener);
        mMap.setOnMyLocationClickListener(onMyLocationClickListener);
        enableMyLocationIfPermitted();

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMinZoomPreference(11);

    }

    private GoogleMap.OnMyLocationButtonClickListener onMyLocationButtonClickListener =
            new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    mMap.setMinZoomPreference(15);
                    return false;
                }
            };

    private GoogleMap.OnMyLocationClickListener onMyLocationClickListener =
            new GoogleMap.OnMyLocationClickListener() {
                @Override
                public void onMyLocationClick(@NonNull Location location) {

                    mMap.setMinZoomPreference(12);

                    CircleOptions circleOptions = new CircleOptions();
                    circleOptions.center(new LatLng(location.getLatitude(),
                            location.getLongitude()));

                    circleOptions.radius(200);
                    circleOptions.fillColor(Color.RED);
                    circleOptions.strokeWidth(6);

                    mMap.addCircle(circleOptions);
                }
            };


    private void enableMyLocationIfPermitted() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enableMyLocationIfPermitted();
                } else {
                    //showDefaultLocation();
                }
                return;
            }

        }
    }

    public void  cancelDialog(){
        dialog=new Dialog(this);
        dialog.setContentView(R.layout.cancel_dialog);
        dialog.show();
    }
}
