package com.swissexpressdz.driver.activity.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.swissexpressdz.driver.MainActivity;
import com.swissexpressdz.driver.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecentTransationAdapter extends RecyclerView.Adapter<RecentTransationAdapter.ViewHolder> {
    MainActivity mainActivity;
    ArrayList list;


    public RecentTransationAdapter(MainActivity mainActivity, ArrayList list) {
        this.list=list;
        this.mainActivity=mainActivity;
    }

    @Override
    public RecentTransationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recent_tran_list_item, parent, false);

        RecentTransationAdapter.ViewHolder viewHolder = new RecentTransationAdapter.ViewHolder(v);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

       // viewHolder.UsernameInfoTV.setText(personUtils.get(i).getPatient());

    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout rr_contact;
        ImageView read_tv;
        CircleImageView propic_circular;
        public ViewHolder(View itemView) {
            super(itemView);

          //  rr_contact=(RelativeLayout) itemView.findViewById(R.id.rr_contact);

        }
    }
}