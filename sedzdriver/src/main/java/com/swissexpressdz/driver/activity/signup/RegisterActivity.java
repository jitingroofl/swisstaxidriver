package com.swissexpressdz.driver.activity.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.components.CustomCountryDialog;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.models.datamodels.Country;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.utils.CustomEditTextRegular;
import com.swissexpressdz.driver.utils.CustomTextViewRegular;

import java.util.ArrayList;


public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    //TextView login_account;
    RelativeLayout rr_register,rr_layout;
    CardView register_crd;
    Animation slide_bottom_to_top;
    CustomTextViewRegular login_account;
    ImageView water_ball,water_ball_2,water_ball_3,water_ball_4,water_ball_5,water_ball_6,water_ball_7,water_ball_8,Water_ball_9,Water_ball_10;
    ImageView water_ball1,water_ball_21,water_ball_31,water_ball_41,water_ball_51,water_ball_61,water_ball_71,water_ball_81,Water_ball_91,Water_ball_101;
    private String contactNumber, countryCode, countryName;

    CustomEditTextRegular ext_email,ext_password,ext_conf_password,ext_first_name,ext_last_name,ext_country_name,ext_phone;
    MyFontTextView ext_country_code;
    LinearLayout ll_ext_country_code;
    private ArrayList<Country> codeArrayList;
    private int phoneNumberLength, phoneNumberMinLength;

    ParseContent parseContent;
    private CustomCountryDialog customCountryDialog;
    private Country country;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        parseContent = ParseContent.getInstance();
        parseContent.getContext(this);
        inti();
    }

    public  void inti(){
        rr_register=(RelativeLayout) findViewById(R.id.rr_register);
        rr_register.setOnClickListener(this);
        rr_layout=(RelativeLayout)findViewById(R.id.rr_layout);
        login_account=(CustomTextViewRegular) findViewById(R.id.login_account);
        register_crd=(CardView)findViewById(R.id.register_crd);
        login_account.setOnClickListener(this);

        ext_conf_password=(CustomEditTextRegular)findViewById(R.id.ext_conf_password);
        ext_email=(CustomEditTextRegular)findViewById(R.id.ext_email);
        ext_password=(CustomEditTextRegular)findViewById(R.id.ext_password);

        ext_first_name=(CustomEditTextRegular)findViewById(R.id.ext_first_name);
        ext_last_name=(CustomEditTextRegular)findViewById(R.id.ext_last_name);
        ext_country_name=(CustomEditTextRegular)findViewById(R.id.ext_country_name);
        ext_country_code=(MyFontTextView)findViewById(R.id.ext_country_code);
        ll_ext_country_code=(LinearLayout)findViewById(R.id.ll_ext_country_code) ;
        ext_phone=(CustomEditTextRegular)findViewById(R.id.ext_phone);
        ext_phone.setEnabled(true);
        //ext_country_code.setEnabled(false);

        slide_bottom_to_top = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bottom_up);

        slide_bottom_to_top.setFillAfter(true);

        register_crd.setVisibility(View.VISIBLE);
        rr_layout.setVisibility(View.VISIBLE);
        //  register_crd.startAnimation(slide_bottom_to_top);
        rr_layout.setAnimation(slide_bottom_to_top);

        randomWaterBallAnimation();
        randomWaterBallAnimation1();

        codeArrayList = parseContent.getRawCountryCodeList();
        CurrentTrip.getInstance().setCountryCodes(codeArrayList);
        codeArrayList = CurrentTrip.getInstance().getCountryCodes();

        setCountry(0);
        ext_country_code.setOnClickListener(this);
        ll_ext_country_code.setOnClickListener(this);
    }


    private void openCountryCodeDialog() {

        customCountryDialog = null;
        customCountryDialog = new CustomCountryDialog(this, codeArrayList) {
            @Override
            public void onSelect(int position, ArrayList<Country> filterList) {
                codeArrayList = filterList;
                setCountry(position);
                customCountryDialog.dismiss();

            }
        };
        customCountryDialog.show();

    }


    private void setCountry(int position) {
        country = codeArrayList.get(position);
        ext_country_code.setText(country.getCountryphonecode());
        countryName = country.getCountryname();
        phoneNumberLength = country.getPhoneNumberLength();
        phoneNumberMinLength = country.getPhoneNumberMinLength();
        setContactNoLength(phoneNumberLength);
    }

    private void setContactNoLength(int length) {
        InputFilter[] FilterArray = new InputFilter[1];
        FilterArray[0] = new InputFilter.LengthFilter(length);
        ext_phone.setFilters(FilterArray);
    }
    private void validate() {
        boolean isError=false;
        String user_email=ext_email.getText().toString();
        String user_pass=ext_password.getText().toString();
        String user_conf_pass=ext_conf_password.getText().toString();
        String str_user_name=ext_first_name.getText().toString();
        String str_user_last_name=ext_last_name.getText().toString();
        String str_user_country=ext_country_name.getText().toString();
        String str_user_country_code=ext_country_code.getText().toString();
        String  str_user_phone=ext_phone.getText().toString();

        if(str_user_name==null || str_user_name.equalsIgnoreCase("") || str_user_name.equalsIgnoreCase("null")) {
            isError=true;
            ext_first_name.setError(getResources().getString(R.string.ce_champ_est_requis));


        } else {
            ext_first_name.setError(null);
        }
        if(str_user_last_name==null || str_user_last_name.equalsIgnoreCase("") || str_user_last_name.equalsIgnoreCase("null")) {
            isError=true;
            ext_last_name.setError(getResources().getString(R.string.ce_champ_est_requis));


        } else {
            ext_last_name.setError(null);
        }
        if(str_user_country==null || str_user_country.equalsIgnoreCase("") || str_user_country.equalsIgnoreCase("null")) {
            isError=true;
            ext_country_name.setError(getResources().getString(R.string.ce_champ_est_requis));


        } else {
            ext_country_name.setError(null);
        }
        if(str_user_country_code==null || str_user_country_code.equalsIgnoreCase("") || str_user_country_code.equalsIgnoreCase("null")) {
            isError=true;
            ext_country_code.setError(getResources().getString(R.string.ce_champ_est_requis));


        } else {
            ext_country_code.setError(null);
        }
        if(str_user_phone==null || str_user_phone.equalsIgnoreCase("") || str_user_phone.equalsIgnoreCase("null")) {
            isError=true;
            ext_phone.setError(getResources().getString(R.string.ce_champ_est_requis));


        } else {
            ext_phone.setError(null);
        }


        if(user_email==null || user_email.equalsIgnoreCase("") || user_email.equalsIgnoreCase("null"))
        {
            isError=true;
            ext_email.setError(getResources().getString(R.string.ce_champ_est_requis));

        }


        else if(!isValidEmail(user_email)){

            isError=true;
            ext_email.setError(getResources().getString(R.string.sil_vous_plaite_entr_eml_valid));

        }
        else
        {
            ext_email.setError(null);
        }



        if(user_pass==null || user_pass.equalsIgnoreCase("") || user_pass.equalsIgnoreCase("null")) {
            isError=true;
            ext_password.setError(getResources().getString(R.string.ce_champ_est_requis));


        } else {
            ext_password.setError(null);
        }



        if(user_conf_pass==null || user_conf_pass.equalsIgnoreCase("") || user_conf_pass.equalsIgnoreCase("null")) {
            isError = true;
            ext_conf_password.setError(getResources().getString(R.string.ce_champ_est_requis));


        } else if(!user_conf_pass.equalsIgnoreCase(user_pass)) {
            isError=true;
            ext_conf_password.setError(getResources().getString(R.string.confrm_que_lemot_aumot_de_passe));

        }

        else {
            ext_conf_password.setError(null);
        }

        if(!isError) {

            // callWebService();

            String loginType="manual";
            //register(loginType);

        }

    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    public void onClick(View v) {
        if(v==login_account){

            Intent intent=new Intent(this,LoginActivity.class);
            startActivity(intent);
        }
        if(v==rr_register){
            validate();
        }

        if(v==ext_country_code)
        {
            openCountryCodeDialog();

        }

        if(v==ll_ext_country_code)
        {
            openCountryCodeDialog();

        }
    }




    private void randomWaterBallAnimation(){

        water_ball_3 = findViewById(R.id.water_ball_3);


        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(9000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());
        water_ball_3.setAnimation(mAnimation3);

        water_ball_4 = findViewById(R.id.water_ball_4);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());
        water_ball_4.setAnimation(mAnimation4);

        water_ball_5 = findViewById(R.id.water_ball_5);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());
        water_ball_5.setAnimation(mAnimation5);

        water_ball_6 = findViewById(R.id.water_ball_6);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF,0.0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());
        water_ball_6.setAnimation(mAnimation6);

        water_ball_7 = findViewById(R.id.water_ball_7);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.1f);
        mAnimation7.setDuration(8000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());
        water_ball_7.setAnimation(mAnimation7);

        water_ball_8 = findViewById(R.id.water_ball_8);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());
        water_ball_8.setAnimation(mAnimation8);


        Water_ball_10 = findViewById(R.id.water_ball_10);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());
        Water_ball_10.setAnimation(mAnimation10);

        Water_ball_9 = findViewById(R.id.water_ball_9);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(1000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_9.setAnimation(mAnimation9);
    }

    private void randomWaterBallAnimation1(){




        water_ball_31 = findViewById(R.id.water_ball_31);


        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(9000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());

        water_ball_31.setAnimation(mAnimation3);



        water_ball_41 = findViewById(R.id.water_ball_41);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());

        water_ball_41.setAnimation(mAnimation4);


        water_ball_51 = findViewById(R.id.water_ball_51);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());

        water_ball_51.setAnimation(mAnimation5);


        water_ball_61= findViewById(R.id.water_ball_61);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());

        water_ball_61.setAnimation(mAnimation6);


        water_ball_71 = findViewById(R.id.water_ball_71);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation7.setDuration(9000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());

        water_ball_71.setAnimation(mAnimation7);


        water_ball_81 = findViewById(R.id.water_ball_81);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());

        water_ball_81.setAnimation(mAnimation8);



        Water_ball_101 = findViewById(R.id.water_ball_101);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());

        Water_ball_101.setAnimation(mAnimation10);


        Water_ball_91 = findViewById(R.id.water_ball_91);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(9000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_91.setAnimation(mAnimation9);
    }

    /*private void register(String loginType) {
        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register valid");

        HashMap<String, RequestBody> map = new HashMap<>();



     *//*   if (etPassword.getVisibility() == View.VISIBLE) {
            map.put(Const.Params.PASSWORD, ApiClient.makeTextRequestBody(etPassword.getText()
                    .toString()));
            FirebaseAuth.getInstance().signOut();
        } else {
            map.put(Const.Params.PASSWORD, ApiClient.makeTextRequestBody(""));
            map.put(Const.Params.SOCIAL_UNIQUE_ID, ApiClient.makeTextRequestBody(socialId));
        }*//*
        map.put(Const.Params.PASSWORD, ApiClient.makeTextRequestBody(ext_password.getText().toString()));
        map.put(Const.Params.FIRST_NAME, ApiClient.makeTextRequestBody(ext_first_name.getText()
                .toString()));
        map.put(Const.Params.LAST_NAME, ApiClient.makeTextRequestBody(ext_last_name.getText()
                .toString()));
        map.put(Const.Params.EMAIL, ApiClient.makeTextRequestBody(ext_email.getText().toString
                ()));
        map.put(Const.Params.DEVICE_TYPE, ApiClient.makeTextRequestBody(Const
                .DEVICE_TYPE_ANDROID));
        map.put(Const.Params.DEVICE_TOKEN, ApiClient.makeTextRequestBody(preferenceHelper
                .getDeviceToken()));

        map.put(Const.Params.PHONE, ApiClient.makeTextRequestBody(ext_phone.getText()
                .toString()));
        map.put(Const.Params.BIO, ApiClient.makeTextRequestBody(etBio.getText().toString()));


        map.put(Const.Params.SERVICE_TYPE, ApiClient.makeTextRequestBody(""));
        map.put(Const.Params.DEVICE_TIMEZONE, ApiClient.makeTextRequestBody(Utils
                .getTimeZoneName()));
        map.put(Const.Params.ADDRESS, ApiClient.makeTextRequestBody(etAddress.getText()
                .toString()));

        map.put(Const.Params.ZIPCODE, ApiClient.makeTextRequestBody(etZipCode.getText()
                .toString()));
        map.put(Const.Params.LOGIN_BY, ApiClient.makeTextRequestBody(loginType));
        map.put(Const.Params.APP_VERSION, ApiClient.makeTextRequestBody(getAppVersion()));

        if (country != null) {
            map.put(Const.Params.COUNTRY, ApiClient.makeTextRequestBody(country.getCountryname()));
            map.put(Const.Params.COUNTRY_ID, ApiClient.makeTextRequestBody(country.getId()));
            map.put(Const.Params.COUNTRY_PHONE_CODE, ApiClient.makeTextRequestBody(tvCountryCode
                    .getText()
                    .toString()));
        }
        if (city != null) {
            map.put(Const.Params.CITY_ID, ApiClient.makeTextRequestBody(city.getId()));
            map.put(Const.Params.CITY, ApiClient.makeTextRequestBody(city.getFullCityName()));
        }


        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_registering), false, null);
        Call<ProviderDataResponse> userDataResponseCall;
        if (!TextUtils.isEmpty(uploadImageFilePath)) {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(ApiClient.makeMultipartRequestBody
                            (this, uploadImageFilePath, Const.Params.PICTURE_DATA)
                    , map);
        } else {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(null, map);
        }
        userDataResponseCall.enqueue(new Callback<ProviderDataResponse>() {
            @Override
            public void onResponse(Call<ProviderDataResponse> call,
                                   Response<ProviderDataResponse> response) {
                if (parseContent.isSuccessful(response)) {
                    if (parseContent.saveProviderData(response.body(), true)) {
                        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register Success");
                        CurrentTrip.getInstance().clear();
                        Utils.hideCustomProgressDialog();
                        moveWithUserSpecificPreference();
                    } else {
                        Utils.hideCustomProgressDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderDataResponse> call, Throwable t) {
                AppLog.handleThrowable(RegisterActivity_Driver.class.getSimpleName(), t);
            }
        });


    }*/
}
