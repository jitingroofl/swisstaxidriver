package com.swissexpressdz.driver.activity.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.swissexpressdz.driver.NewPasswordActivity;
import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.editprofile.Edit_ProfileActivity;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextViewBold;
import com.swissexpressdz.driver.utils.CustomTextViewRegular;
import com.swissexpressdz.driver.utils.PreferenceHelper;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout back_icon,edit_icon;
    ImageView water_ball1,water_ball_21,water_ball_3,water_ball_4,water_ball_5,water_ball_6,water_ball_7,water_ball_8,Water_ball_9,Water_ball_10;
    CustomTextViewBold change_pass,user_name;
    CustomTextViewRegular user_mobile_no,txt_gt_name,txt_gt_email,txt_gt_address,txt_gt_mobilenumber;
    public PreferenceHelper preferenceHelper;
    CircleImageView profile_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        preferenceHelper = PreferenceHelper.getInstance(this);
        inti();

    }
    public  void  inti(){
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        edit_icon=(RelativeLayout)findViewById(R.id.edit_icon);
        change_pass=(CustomTextViewBold)findViewById(R.id.change_pass);

        user_name=(CustomTextViewBold)findViewById(R.id.user_name);
        user_mobile_no=(CustomTextViewRegular)findViewById(R.id.user_mobile_no);

        txt_gt_name=(CustomTextViewRegular)findViewById(R.id.txt_gt_name);
        txt_gt_email=(CustomTextViewRegular)findViewById(R.id.txt_gt_email);
        txt_gt_address=(CustomTextViewRegular)findViewById(R.id.txt_gt_address);
        txt_gt_mobilenumber=(CustomTextViewRegular)findViewById(R.id.txt_gt_mobilenumber);
        profile_icon=(CircleImageView)findViewById(R.id.profile_icon);

        back_icon.setOnClickListener(this);
        edit_icon.setOnClickListener(this);
        change_pass.setOnClickListener(this);

       // randomWaterBallAnimation();
        setProfileData();
    }


    @Override
    public void onClick(View v) {
        if(v==back_icon){
            finish();
        }
        if(v==change_pass){
            Intent intent = new Intent(this, NewPasswordActivity.class);
            intent.putExtra(Const.Params.PHONE, preferenceHelper.getContact());
            intent.putExtra(Const.Params.COUNTRY_PHONE_CODE, preferenceHelper.getCountryPhoneCode());
            startActivity(intent);
        }
        if (v==edit_icon){
            Intent intent = new Intent(this, Edit_ProfileActivity.class);
            intent.putExtra(Const.Params.PHONE, preferenceHelper.getContact());
            intent.putExtra(Const.Params.COUNTRY_PHONE_CODE, preferenceHelper.getCountryPhoneCode());
            startActivity(intent);
        }

    }

    private void setProfileData() {
        user_name.setText(preferenceHelper.getFirstName()+" "+preferenceHelper.getLastName());
        txt_gt_name.setText(preferenceHelper.getFirstName()+" "+preferenceHelper.getLastName());
        System.out.println("Profile Number###"+preferenceHelper.getCountryPhoneCode()+"-"+preferenceHelper.getContact());
        txt_gt_address.setText(preferenceHelper.getAddress());
        txt_gt_email.setText(preferenceHelper.getEmail());
        user_mobile_no.setText(preferenceHelper.getCountryPhoneCode()+"-"+preferenceHelper.getContact());
        txt_gt_address.setText(preferenceHelper.getAddress());
        txt_gt_mobilenumber.setText(preferenceHelper.getCountryPhoneCode()+"-"+preferenceHelper.getContact());

//        Glide.with(this).load(preferenceHelper.getProfilePic())
//                .dontAnimate().placeholder(R.drawable.ellipse).override(80, 80).into
//                (profile_icon);
        PicassoTrustAll.getInstance(this)
                .load(preferenceHelper.getProfilePic())
                .error(R.drawable.ellipse)
                .resize(80,80)
                .into(profile_icon);

    }
    private void randomWaterBallAnimation(){


        water_ball1 =  findViewById(R.id.water_ball1);


        // For First First

        RotateAnimation anim = new RotateAnimation(0f, 350f, 15f, 15f);
        //   RotateAnimation anim = new RotateAnimation(0f, 0.5f, 1.1f, -0.3f);

        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(9000);

        TranslateAnimation mAnimation ;
        mAnimation = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.3f);
        mAnimation.setDuration(25000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());

        water_ball1.setAnimation(mAnimation);
        water_ball1.startAnimation(anim);


        // For Second Second  water_ball_2 =  findViewById(R.id.water_ball_2);
        water_ball_21 =  findViewById(R.id.water_ball_21);
        TranslateAnimation mAnimation2 ;
        mAnimation2 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.7f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation2.setDuration(30000);
        mAnimation2.setRepeatCount(-1);
        mAnimation2.setRepeatMode(Animation.REVERSE);
        mAnimation2.setInterpolator(new LinearInterpolator());

        water_ball_21.setAnimation(mAnimation2);

        water_ball_21.animate().rotation(1800f).setDuration(25000).start();
        water_ball_21.getAnimation().setRepeatCount(-1);

        water_ball_21.getAnimation().setRepeatMode(Animation.REVERSE);
        water_ball_21.getAnimation().setInterpolator(new LinearInterpolator());

        water_ball_3 = findViewById(R.id.water_ball_31);


        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(9000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());
        water_ball_3.setAnimation(mAnimation3);

        water_ball_4 = findViewById(R.id.water_ball_41);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());
        water_ball_4.setAnimation(mAnimation4);

        water_ball_5 = findViewById(R.id.water_ball_51);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());
        water_ball_5.setAnimation(mAnimation5);

        water_ball_6 = findViewById(R.id.water_ball_61);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF,0.0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());
        water_ball_6.setAnimation(mAnimation6);

        water_ball_7 = findViewById(R.id.water_ball_71);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.1f);
        mAnimation7.setDuration(8000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());
        water_ball_7.setAnimation(mAnimation7);

        water_ball_8 = findViewById(R.id.water_ball_81);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());
        water_ball_8.setAnimation(mAnimation8);


        Water_ball_10 = findViewById(R.id.water_ball_101);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());
        Water_ball_10.setAnimation(mAnimation10);

        Water_ball_9 = findViewById(R.id.water_ball_91);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(9000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_9.setAnimation(mAnimation9);
    }
}
