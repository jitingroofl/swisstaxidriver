package com.swissexpressdz.driver.activity.support;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;


public class SupportActivity extends AppCompatActivity implements View.OnClickListener {
    TextView title;
    RelativeLayout back_icon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        inti();
    }
    public  void inti(){
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.support);
        back_icon=(RelativeLayout) findViewById(R.id.back_icon);

        back_icon.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        if(v==back_icon){
            finish();
        }
    }
}
