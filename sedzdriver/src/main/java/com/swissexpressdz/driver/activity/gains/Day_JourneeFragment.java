package com.swissexpressdz.driver.activity.gains;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.documents.DocumentsActivity;


public class Day_JourneeFragment extends Fragment {

    TextView paint;
    @Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.day_journee, container, false);

        paint=(TextView)v.findViewById(R.id.paint);
        paint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii=new Intent(getActivity(), DocumentsActivity.class);
                startActivity(ii);
            }
        });
    return v;
}
}
