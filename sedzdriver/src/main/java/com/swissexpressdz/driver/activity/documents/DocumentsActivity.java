package com.swissexpressdz.driver.activity.documents;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.manageVehicles.Manage_VehiclesActivity;


public class DocumentsActivity extends AppCompatActivity implements View.OnClickListener {

    TextView title;
    RelativeLayout back_icon;
    Button btn_download;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documents);
        inti();
    }

    public  void inti(){
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.télécharger);
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        btn_download=(Button)findViewById(R.id.btn_download);


        back_icon.setOnClickListener(this);
        btn_download.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v==back_icon){
            finish();
        }
        if(v==btn_download){
            Intent ii=new Intent(this, Manage_VehiclesActivity.class);
            startActivity(ii);
        }

    }
}
