package com.swissexpressdz.driver.activity.manageVehicles;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.history.Detail_History_RaceActivity;


public class Manage_VehiclesActivity extends AppCompatActivity implements View.OnClickListener {

    TextView title;
    RelativeLayout back_icon,save_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage__vehicles);
        inti();
    }

    public  void inti(){
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.gerer_les_vehicules);
        save_icon=(RelativeLayout)findViewById(R.id.save_icon);
        save_icon.setVisibility(View.VISIBLE);

        back_icon.setOnClickListener(this);
        save_icon.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v==back_icon){
           finish();
        }
        if(v==save_icon){
            Toast.makeText(this,"Enregistrer terminé", Toast.LENGTH_SHORT).show();
            Intent ii=new Intent(this, Detail_History_RaceActivity.class);
            startActivity(ii);
        }

    }
}
