package com.swissexpressdz.driver.activity.dashboard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.swissexpressdz.driver.EarningActivity_Driver;
import com.swissexpressdz.driver.PaymentActivity_Driver;
import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.TripHistoryActivity_Driver;
import com.swissexpressdz.driver.utils.CustomTextViewRegular;
import com.swissexpressdz.driver.utils.PreferenceHelper;

public class Dash_BoardActivity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout back_icon;
    LinearLayout ll_payment,ll_earning,ll_see_status;
    ImageView water_ball,water_ball_2,water_ball_3,water_ball_4,water_ball_5,water_ball_6,water_ball_7,water_ball_8,Water_ball_9,Water_ball_10;
    public PreferenceHelper preferenceHelper;
    CustomTextViewRegular total_wallet,txt_rating;
    String str_wallet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash__board);
        preferenceHelper = PreferenceHelper.getInstance(this);
        inti();
    }

    public  void  inti(){
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        ll_earning=(LinearLayout)findViewById(R.id.ll_earning) ;
        ll_payment=(LinearLayout)findViewById(R.id.ll_payment);
        ll_see_status=(LinearLayout)findViewById(R.id.ll_see_status);
        total_wallet=(CustomTextViewRegular)findViewById(R.id.total_wallet);
        txt_rating=(CustomTextViewRegular)findViewById(R.id.txt_rating);

        randomWaterBallAnimation();
        str_wallet=preferenceHelper.getWallet();
        if(str_wallet!=null){
            total_wallet.setText(preferenceHelper.getWallet());
        }else {
            total_wallet.setText("00.00");
        }

        txt_rating.setText(preferenceHelper.getRating());

        back_icon.setOnClickListener(this);
        ll_payment.setOnClickListener(this);
        ll_earning.setOnClickListener(this);
        ll_see_status.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v==back_icon){
            onBackPressed();
        }
        if(v==ll_payment){
            goToPaymentActivity();
        }
        if(v==ll_earning){
            goToEarningActivity();
        }
        if(v==ll_see_status){
            goToHistoryActivity();
        }

    }

    private void goToEarningActivity() {
        Intent earning = new Intent(this, EarningActivity_Driver.class);
        startActivity(earning);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    private void goToPaymentActivity() {
        Intent paymentIntent = new Intent(this,
                PaymentActivity_Driver
                        .class);
        startActivity(paymentIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    private void goToHistoryActivity() {
        Intent historyIntent = new Intent(this,
                TripHistoryActivity_Driver.class);
        startActivity(historyIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private void randomWaterBallAnimation(){
        water_ball_3 = findViewById(R.id.water_ball_3);
        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(9000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());
        water_ball_3.setAnimation(mAnimation3);

        water_ball_4 = findViewById(R.id.water_ball_4);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());
        water_ball_4.setAnimation(mAnimation4);
        water_ball_5 = findViewById(R.id.water_ball_5);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());
        water_ball_5.setAnimation(mAnimation5);
        water_ball_6 = findViewById(R.id.water_ball_6);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF,0.0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());
        water_ball_6.setAnimation(mAnimation6);
        water_ball_7 = findViewById(R.id.water_ball_7);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.1f);
        mAnimation7.setDuration(8000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());
        water_ball_7.setAnimation(mAnimation7);        water_ball_8 = findViewById(R.id.water_ball_8);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());
        water_ball_8.setAnimation(mAnimation8);        Water_ball_10 = findViewById(R.id.water_ball_10);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());
        Water_ball_10.setAnimation(mAnimation10);        Water_ball_9 = findViewById(R.id.water_ball_9);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(9000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_9.setAnimation(mAnimation9);
    }
}
