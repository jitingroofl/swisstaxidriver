package com.swissexpressdz.driver.activity.applyfordamage;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.swissexpressdz.driver.AddVehicleActivity;
import com.swissexpressdz.driver.BaseAppCompatActivity;
import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.RegisterActivity_Driver;
import com.swissexpressdz.driver.TripHistoryActivity_Driver;
import com.swissexpressdz.driver.TripHistoryDetailActivity_Driver;
import com.swissexpressdz.driver.adapter.TripHistoryAdaptor;
import com.swissexpressdz.driver.components.CustomPhotoDialog;
import com.swissexpressdz.driver.components.MyAppTitleFontTextView;
import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.components.MyTitleFontTextView;
import com.swissexpressdz.driver.models.datamodels.TripHistory;
import com.swissexpressdz.driver.models.responsemodels.AddVTCResponse;
import com.swissexpressdz.driver.models.responsemodels.CarrentalResponse;
import com.swissexpressdz.driver.models.responsemodels.DepositeResponseModel;
import com.swissexpressdz.driver.models.responsemodels.TripHistoryResponse;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CurrencyHelper;
import com.swissexpressdz.driver.utils.ImageCompression;
import com.swissexpressdz.driver.utils.ImageHelper;
import com.swissexpressdz.driver.utils.PreferenceHelper;
import com.swissexpressdz.driver.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeSet;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplyForDamageActivity extends BaseAppCompatActivity {


    private ArrayList<CarrentalResponse.Trip> tripHistoryList;
    RecyclerView history_recycler_view;
    TripHistoryAdaptor tripHistoryAdaptor;
    MyTitleFontTextView tvNoItemHistory;
    Dialog selct_vehicle_dlg;
LinearLayout ll_if_damage,ll_ifnot_damage;
ImageView img_crd;
MyFontEdittextView ext_depositeamount;
RelativeLayout rr_apply,rr_notapply;
RadioGroup rdo_slctin_hr_dy;
boolean is_damaege = true;
    private String msg,trip_id = "";
    CustomPhotoDialog customPhotoDialog;
    private ImageHelper imageHelper;

    private String uploadImageFilePath = "";
    private Uri picUri;
    String file_ext2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apply_for_damage);
        initToolBar();
        setTitleOnToolbar(getResources().getString(R.string.text_applyfordamage));
        initUII();
    }

    private void initUII() {
        imageHelper = new ImageHelper(this);

        tripHistoryList = new ArrayList<>();

        history_recycler_view = (RecyclerView) findViewById(R.id.history_recycler_view);
        tvNoItemHistory = (MyTitleFontTextView) findViewById(R.id.tvNoItemHistory);

        getTripHistory("","");
    }

    @Override
    protected void initToolBar() {
        super.initToolBar();

    }

    private void getTripHistory(String startDate, String endDate) {
        JSONObject jsonObject = new JSONObject();
        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_history), false, null);

        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.TOKEN, preferenceHelper.getSessionToken());


            Call<CarrentalResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .trips_car_rent(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<CarrentalResponse>() {
                @Override
                public void onResponse(Call<CarrentalResponse> call,
                                       Response<CarrentalResponse> response) {

                    if (parseContent.isSuccessful(response)) {
                        tripHistoryList.clear();
                        if (response.body().getSuccess()) {
                            tripHistoryList.clear();
                            tripHistoryList.addAll(response.body().getTrips());
                            if (tripHistoryAdaptor == null) {
                                tripHistoryAdaptor =
                                        new TripHistoryAdaptor(ApplyForDamageActivity.this, tripHistoryList);

                                LinearLayoutManager vehicleLinearLayoutManager = new LinearLayoutManager(ApplyForDamageActivity.this);
                                vehicleLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

                                history_recycler_view.setLayoutManager(vehicleLinearLayoutManager);
                                history_recycler_view.setAdapter(tripHistoryAdaptor);
                                history_recycler_view.setNestedScrollingEnabled(true);

                            } else {
                                tripHistoryAdaptor.notifyDataSetChanged();
                            }
                            updateUi(tripHistoryList.size() > 0);
                            Utils.hideCustomProgressDialog();
                        } else {
                            updateUi(false);
                            Utils.hideCustomProgressDialog();
                        }
                    }


                }

                @Override
                public void onFailure(Call<CarrentalResponse> call, Throwable t) {
                    AppLog.handleThrowable(TripHistoryActivity_Driver.class.getSimpleName(), t);

                }
            });
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.TRIP_HISTORY_ACTIVITY, e);
        }

    }
    private void updateUi(boolean isUpdate) {
        if (isUpdate) {
            tvNoItemHistory.setVisibility(View.GONE);
            history_recycler_view.setVisibility(View.VISIBLE);
        } else {
            tvNoItemHistory.setVisibility(View.VISIBLE);
            history_recycler_view.setVisibility(View.GONE);
        }


    }
    @Override
    protected boolean isValidate() {
        msg = null;
        if (TextUtils.isEmpty(ext_depositeamount.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            ext_depositeamount.requestFocus();
            ext_depositeamount.setError(msg);
        }


        else if (uploadImageFilePath.equalsIgnoreCase("")) {
            msg = getString(R.string.this_field_requiredd);
            img_crd.requestFocus();
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.plz_slt_photo),Toast.LENGTH_SHORT).show();
        }


           return TextUtils.isEmpty(msg);
    }

    @Override
    public void goWithBackArrow() {
onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }
    private void vehicleSelectDialog()
    {
        selct_vehicle_dlg = new Dialog(ApplyForDamageActivity.this);
        selct_vehicle_dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        selct_vehicle_dlg.setCancelable(true);
        selct_vehicle_dlg.setContentView(R.layout.dlg_is_damage_or_not);
     //   selct_vehicle_dlg.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.w));

        ll_if_damage = (LinearLayout) selct_vehicle_dlg.findViewById(R.id.ll_if_damage);
        ll_ifnot_damage = (LinearLayout) selct_vehicle_dlg.findViewById(R.id.ll_ifnot_damage);
        img_crd = (ImageView) selct_vehicle_dlg.findViewById(R.id.img_crd);
        ext_depositeamount = (MyFontEdittextView) selct_vehicle_dlg.findViewById(R.id.ext_depositeamount);
        rr_apply = (RelativeLayout) selct_vehicle_dlg.findViewById(R.id.rr_apply);
                rr_notapply = (RelativeLayout) selct_vehicle_dlg.findViewById(R.id.rr_notapply);
        rdo_slctin_hr_dy = (RadioGroup) selct_vehicle_dlg.findViewById(R.id.rdo_slctin_hr_dy);

        rdo_slctin_hr_dy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked) {
                  String  isDamage_selcetion =  checkedRadioButton.getText().toString();

                    if(isDamage_selcetion.equalsIgnoreCase(getResources().getString(R.string.text_yes)))
                    {
                        ll_if_damage.setVisibility(View.VISIBLE);
                        ll_ifnot_damage.setVisibility(View.GONE);

                        is_damaege = true;
                    }

                    else if(isDamage_selcetion.equalsIgnoreCase(getResources().getString(R.string.text_no)))
                    {
                        ll_if_damage.setVisibility(View.GONE);
                        ll_ifnot_damage.setVisibility(View.VISIBLE);

                        is_damaege = false;
                    }
                }
            }
        });

        img_crd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openPhotoDialog();

            }
        });

        rr_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValidate()) {

                    applyforDamage(true);
                }

            }
        });

        rr_notapply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                applyforDamage(false);

            }
        });
        selct_vehicle_dlg.show();

    }

    protected void openPhotoDialog() {
        if (ContextCompat.checkSelfPermission(ApplyForDamageActivity.this, Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission
                (ApplyForDamageActivity.this, Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ApplyForDamageActivity.this, new String[]{Manifest
                    .permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
        }
        else {

            customPhotoDialog = new CustomPhotoDialog(this) {
                @Override
                public void clickedOnCamera() {
                    customPhotoDialog.dismiss();
                    takePhotoFromCamera();
                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog.dismiss();
                    choosePhotoFromGallery();
                }
            };
            customPhotoDialog.show();
        }


    }

    private void choosePhotoFromGallery() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Utils.isNougat()) {
            picUri = FileProvider.getUriForFile(ApplyForDamageActivity.this, getApplicationContext().getPackageName(), imageHelper.createImageFile());
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            picUri = Uri.fromFile(imageHelper.createImageFile());
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, Const.ServiceCode.TAKE_PHOTO);
    }
    private void onCaptureImageResult() {
        beginCrop(picUri);
    }
    private void beginCrop(Uri sourceUri) {
        CropImage.activity(sourceUri).setGuidelines(com.theartofdev.edmodo.cropper.CropImageView
                .Guidelines.ON).start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Const.ServiceCode.TAKE_PHOTO:

                if (resultCode == RESULT_OK) {
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        file_ext2 = mime.getExtensionFromMimeType(cR.getType(picUri));

                    }
                    catch (Exception e)
                    {

                    }
                    onCaptureImageResult();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO:
                Uri selectedImage = data.getData();
                try {
                    ContentResolver cR = getContentResolver();
                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                    file_ext2 = mime.getExtensionFromMimeType(cR.getType(selectedImage));

                }
                catch (Exception e)
                {

                }
                onSelectFromGalleryResult(data);
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                handleCrop(resultCode, data);
                break;


        }
    }

    private void handleCrop(int resultCode, Intent result) {
        final CropImage.ActivityResult activityResult = CropImage.getActivityResult(result);
        if (resultCode == RESULT_OK) {
            uploadImageFilePath = imageHelper.getFromMediaUriPfd(this, getContentResolver(), activityResult.getUri()).getPath();
            System.out.println("File Name 1st@@@"+uploadImageFilePath);

            new ImageCompression(this).setImageCompressionListener(new ImageCompression
                    .ImageCompressionListener() {
                @Override
                public void onImageCompression(String compressionImagePath) {

                    setProfileImage(activityResult.getUri());
                    System.out.println("File Name 1st!!!"+compressionImagePath);

                    uploadImageFilePath = compressionImagePath;
                    uploadImageFilePath = imageHelper.getFromMediaUriPfd(ApplyForDamageActivity.this, getContentResolver(), activityResult.getUri()).getPath();

                    System.out.println("File Name 1st$$$"+uploadImageFilePath);

                    System.out.println("File Ext 1st$$$"+file_ext2);


                }
            }).execute(uploadImageFilePath);
        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Utils.showToast(activityResult.getError().getMessage(), this);
        }
    }

    private void setProfileImage(Uri imageUri) {


        PicassoTrustAll.getInstance(getApplicationContext())
                .load(imageUri)
                .error(R.drawable.ellipse)
                .into(img_crd);
    }


    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            picUri = data.getData();
            beginCrop(picUri);
        }
    }
    private void applyforDamage(boolean bol_val) {

        ArrayList<String> arrayList_files=new ArrayList<>();



        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register valid");

        HashMap<String, RequestBody> map = new HashMap<>();
        MultipartBody.Part[] surveyImagesParts = null;


        map.put(Const.Params.TRIP_ID, ApiClient.makeTextRequestBody(trip_id));
        System.out.println("Is damage value "+bol_val);

        map.put(Const.Params.IS_DAMAGE, ApiClient.makeTextRequestBody(bol_val));



        if(bol_val == true)
        {
            arrayList_files.add(uploadImageFilePath);
            surveyImagesParts = new MultipartBody.Part[arrayList_files.size()];

            File file = new File(uploadImageFilePath);
            RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
            surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.vehicle_damage_image, file.getName(), surveyBody);

            map.put(Const.Params.DAMAGE_AMOUNT, ApiClient.makeTextRequestBody(ext_depositeamount.getText().toString()));
            map.put(Const.Params.Ext, ApiClient.makeTextRequestBody(file_ext2));

        }



        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_registering), false, null);






        //For First Document
        Call<DepositeResponseModel> userDataResponseCall;
        userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                (ApiInterface.class).deposit(surveyImagesParts
                , map);

        System.out.println("Map is###"+map.toString());


        userDataResponseCall.enqueue(new Callback<DepositeResponseModel>() {
            @Override
            public void onResponse(Call<DepositeResponseModel> call,
                                   Response<DepositeResponseModel> response) {
                if (parseContent.isSuccessful(response)) {

                    if(response.body().getSuccess())
                    {
                        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register Success");

                        Utils.hideCustomProgressDialog();
                       // getTripHistory("","");
                        finish();
                        Intent ii =new Intent(ApplyForDamageActivity.this,ApplyForDamageActivity.class);
                        startActivity(ii);

                    }
                    else {
                        Utils.hideCustomProgressDialog();
                  Utils.showErrorToast(response.body().getErrorCode(), ApplyForDamageActivity.this);

                    }





                }

                else
                {

                }
            }

            @Override
            public void onFailure(Call<DepositeResponseModel> call, Throwable t) {
                AppLog.handleThrowable(RegisterActivity_Driver.class.getSimpleName(), t);
            }
        });


    }



    public class TripHistoryAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        public static final String TAG = "TripHistoryAdaptor";
        private static final int TYPE_ITEM = 0;
        private static final int TYPE_SEPARATOR = 1;
        private ApplyForDamageActivity historyActivity;
        private ArrayList<CarrentalResponse.Trip> tripHistoryList;
        private ParseContent parseContent;
        private TreeSet<Integer> separatorsSet;
        private SimpleDateFormat dateFormat;
        private NumberFormat currencyFormat;



        public TripHistoryAdaptor(ApplyForDamageActivity applyForDamageActivity, ArrayList<CarrentalResponse.Trip> tripHistoryList) {

            this.historyActivity = applyForDamageActivity;
            this.tripHistoryList = tripHistoryList;
            this.separatorsSet = separatorsSet;
            parseContent = ParseContent.getInstance();
            parseContent.getContext(this.historyActivity);
            dateFormat = parseContent.dateFormat;
            currencyFormat =
                    CurrencyHelper.getInstance(historyActivity).getCurrencyFormat(PreferenceHelper.getInstance(historyActivity).getCurrencyCode());

        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            if (viewType == TYPE_ITEM) {
                View v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.apply_for_damage_item, parent, false);
                return new ViewHolderHistory(v);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            // set data here
            final CarrentalResponse.Trip history = tripHistoryList.get(position);
            if (holder instanceof ViewHolderHistory) {
            ViewHolderHistory viewHolder = (ViewHolderHistory) holder;

                viewHolder.tvHistoryClientName.setText(history.getFirstName() + " " + history
                        .getLastName());



                viewHolder.tvHistoryTotalCost.setText(currencyFormat.format(history.getTotal()));
                //  String.format("%.2f", provider.getRate())

                String dub_value=   String.format("%.2f",  history.getProviderServiceFees());
                // viewHolder.tvProviderEarning.setText(history.getCurrency()+""+ dub_value);
                viewHolder.tvProviderEarning.setText(currencyFormat.format(history.getProviderServiceFees()));

                try {
                    Date date = ParseContent.getInstance().webFormat.parse(history.getUserCreateTime());
                    viewHolder.tvHistoryTripTime.setText(ParseContent.getInstance().timeFormat
                            .format(date));
                } catch (ParseException e) {
                    AppLog.handleException(ApplyForDamageActivity.class.getSimpleName(), e);
                }
                if (tripHistoryList.get(position).getIsTripCancelledByProvider() == Const.TRUE) {
                    viewHolder.tvCanceledBy.setText(historyActivity.getResources().getString(R.string
                            .text_you_canceled_a_trip));
                    viewHolder.tvCanceledBy.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.tvCanceledBy.setText("");
                    viewHolder.tvCanceledBy.setVisibility(View.GONE);
                }

                System.out.println("profile pic:::::::"+history.getPicture());



                PicassoTrustAll.getInstance(historyActivity)
                        .load(ApiClient.Base_URL + history.getPicture())
                        .error(R.drawable.ellipse)
                        .resize(200,200)
                        .into(viewHolder.ivClientPhotoDialog);
                if(history.isIs_damage_apply() == true)
                {
                    viewHolder.rr_applyordamage.setVisibility(View.GONE);
                }
                else if(history.isIs_damage_apply() == false)
                {
                    viewHolder.rr_applyordamage.setVisibility(View.VISIBLE);

                }

                viewHolder.rr_applyordamage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                      trip_id =  tripHistoryList.get(position).getTripId();
                        vehicleSelectDialog();
                    }
                });

            }


        }


        @Override
        public int getItemCount() {
            return tripHistoryList.size();
        }


        private String getYesterdayDateString() {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            return dateFormat.format(cal.getTime());
        }

        private void goToTripHistoryDetail(String tripId, int unit, String currency) {

            Intent tripDetailIntent = new Intent(historyActivity, TripHistoryDetailActivity_Driver
                    .class);
            tripDetailIntent.putExtra(Const.Params.TRIP_ID, tripId);
            tripDetailIntent.putExtra(Const.Params.UNIT, unit);
            tripDetailIntent.putExtra(Const.Params.CURRENCY, currency);
            historyActivity.startActivity(tripDetailIntent);
            historyActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

        protected class ViewHolderHistory extends RecyclerView.ViewHolder implements View
                .OnClickListener {

            TextView tvHistoryClientName, tvHistoryTripTime, tvHistoryTotalCost, tvCanceledBy,
                    tvProviderEarning;
            LinearLayout llHistory;
            ImageView ivClientPhotoDialog;
            RelativeLayout rr_applyordamage;

            public ViewHolderHistory(View itemView) {
                super(itemView);
                tvHistoryClientName = itemView.findViewById(R.id.tvHistoryClientName);
                tvHistoryTotalCost = itemView.findViewById(R.id.tvHistoryTripCost);
                tvHistoryTripTime = itemView.findViewById(R.id.tvHistoryTripTime);
                ivClientPhotoDialog = (ImageView) itemView.findViewById(R.id.ivClientPhotoDialog);
                tvCanceledBy = itemView.findViewById(R.id.tvCanceledBy);
                llHistory = (LinearLayout) itemView.findViewById(R.id.llHistory);
                llHistory.setOnClickListener(this);
                tvProviderEarning = itemView.findViewById(R.id.tvProviderEarning);
                rr_applyordamage = itemView.findViewById(R.id.rr_applyordamage);

            }

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.llHistory:
                        int position = getAdapterPosition();
                        if (tripHistoryList.get(position).getIsTripCompleted() == Const.TRUE) {
                            goToTripHistoryDetail(tripHistoryList.get(position).getTripId(),
                                    (int) tripHistoryList.get(position).getUnit(),
                                    tripHistoryList.get(position).getCurrency());
                        }
                        break;
                    default:
                        // do with default
                        break;
                }

            }

        }

        protected class ViewHolderSeparator extends RecyclerView.ViewHolder {

            MyFontTextView tvDateSeparator;

            public ViewHolderSeparator(View itemView) {
                super(itemView);
                tvDateSeparator = (MyFontTextView) itemView.findViewById(R.id.tvDateSeparator);
            }
        }

    }

}
