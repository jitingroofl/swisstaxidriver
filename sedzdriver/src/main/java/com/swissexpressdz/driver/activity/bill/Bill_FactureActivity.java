package com.swissexpressdz.driver.activity.bill;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.gains.GainsActivity;


public class Bill_FactureActivity extends AppCompatActivity implements View.OnClickListener {

    TextView title;
    RelativeLayout rr_payment_layout;
    RelativeLayout back_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill__facture);
        inti();
    }

    public  void  inti(){
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.bill);
        rr_payment_layout=(RelativeLayout)findViewById(R.id.rr_payment_layout);
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);

        rr_payment_layout.setOnClickListener(this);
        back_icon.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v==rr_payment_layout){
           Intent ii=new Intent(this, GainsActivity.class);
           startActivity(ii);
        }
        if(v==back_icon){
            finish();
        }


    }
}
