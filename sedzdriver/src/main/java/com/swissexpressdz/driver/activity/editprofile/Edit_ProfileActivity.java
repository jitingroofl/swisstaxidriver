package com.swissexpressdz.driver.activity.editprofile;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.swissexpressdz.driver.ProfileActivity_Driver;
import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.font.AppLog;
import com.swissexpressdz.driver.activity.profile.ProfileActivity;
import com.swissexpressdz.driver.components.CustomDialogEnable;
import com.swissexpressdz.driver.components.CustomDialogVerifyAccount;
import com.swissexpressdz.driver.components.CustomPhotoDialog;
import com.swissexpressdz.driver.components.CustomPhotoDialog2;
import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.models.responsemodels.ProviderDataResponse;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomEditTextRegular;
import com.swissexpressdz.driver.utils.CustomTextViewRegular;
import com.swissexpressdz.driver.utils.ImageCompression;
import com.swissexpressdz.driver.utils.ImageHelper;
import com.swissexpressdz.driver.utils.PreferenceHelper;
import com.swissexpressdz.driver.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Edit_ProfileActivity extends AppCompatActivity  implements View.OnClickListener {
    ImageView water_ball1,water_ball_21,water_ball_31,water_ball_41,water_ball_51,water_ball_61,water_ball_71,water_ball_81,Water_ball_91,Water_ball_101;
    RelativeLayout back_icon,save_icon;
CircleImageView profile_icon;
CustomEditTextRegular user_name,ext_last_name,user_email,user_address;
    CustomTextViewRegular tvProfileCountryCode,etProfileContactNumber;
    public PreferenceHelper preferenceHelper;
    public ParseContent parseContent;

    private ImageHelper imageHelper;
    private CustomPhotoDialog customPhotoDialog;
    private Uri picUri;
    private String uploadImageFilePath = "";
    private CustomDialogEnable customDialogEnable;
    private CustomDialogVerifyAccount verifyDialog;
    private String currentPassword,str_lang_code;

    MyFontEdittextView tvselect_document,tvselect_document2,tvselect_document3,tvselect_document4,tvselect_document5;
    CustomTextViewRegular tvselect_document_type,tvselect_document_type2,tvselect_document_type3,tvselect_document_type4,tvselect_document_type5;
    LinearLayout llselect_document2,llselect_document_type2,llselect_document3,llselect_document_type3,llselect_document4,llselect_document_type4,llselect_document5,llselect_document_type5;
        String file_ext,file_ext2,file_ext3,file_ext4,file_ext5;

    private CustomPhotoDialog2 customPhotoDialog2,customPhotoDialog3,customPhotoDialog4,customPhotoDialog5,customPhotoDialog6;

CustomTextViewRegular etVehicleType;

    ArrayList<String> arrayList2=new ArrayList<>();
    ArrayList<String> arrayList3=new ArrayList<>();
    ArrayList<String> arrayList4=new ArrayList<>();
    ArrayList<String> arrayList5=new ArrayList<>();
    ArrayList<String> arrayList6=new ArrayList<>();

String str_document_type1,str_document_file1,str_document_ext1,str_document_type2,str_document_file2,str_document_ext2,str_document_type3,str_document_file3,str_document_ext3,str_document_type4,str_document_file4,str_document_ext4,str_document_type5,str_document_file5,str_document_ext5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__profile);
        inti();
    }
    public  void inti(){

        parseContent = ParseContent.getInstance();
        parseContent.getContext(this);

        imageHelper = new ImageHelper(this);
        preferenceHelper = PreferenceHelper.getInstance(this);


        str_lang_code=preferenceHelper.getLanguageCode();


        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList2.add("Sélectionnez le type de document");
            arrayList2.add("Permis De Conduire");
            arrayList2.add("licence VTC");
            arrayList2.add("Assurance Société");
            arrayList2.add("Assurance Transport De Personnes");
            arrayList2.add("Carte Grise Du Véhicule");

        }
        else
        {
            arrayList2.add("Select document type");
            arrayList2.add("Drivers License");
            arrayList2.add("VTC license");
            arrayList2.add("Company Insurance");
            arrayList2.add("Passenger Transport Insurance");
            arrayList2.add("Vehicle Registration Certificate");
        }

        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList3.add("Sélectionnez le type de document");
            arrayList3.add("Permis De Conduire");
            arrayList3.add("licence VTC");
            arrayList3.add("Assurance Société");
            arrayList3.add("Assurance Transport De Personnes");
            arrayList3.add("Carte Grise Du Véhicule");

        }
        else
        {
            arrayList3.add("Select document type");
            arrayList3.add("Drivers License");
            arrayList3.add("VTC license");
            arrayList3.add("Company Insurance");
            arrayList3.add("Passenger Transport Insurance");
            arrayList3.add("Vehicle Registration Certificate");
        }
        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList4.add("Sélectionnez le type de document");
            arrayList4.add("Permis De Conduire");
            arrayList4.add("licence VTC");
            arrayList4.add("Assurance Société");
            arrayList4.add("Assurance Transport De Personnes");
            arrayList4.add("Carte Grise Du Véhicule");

        }
        else
        {
            arrayList4.add("Select document type");
            arrayList4.add("Drivers License");
            arrayList4.add("VTC license");
            arrayList4.add("Company Insurance");
            arrayList4.add("Passenger Transport Insurance");
            arrayList4.add("Vehicle Registration Certificate");
        }


        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList5.add("Sélectionnez le type de document");
            arrayList5.add("Permis De Conduire");
            arrayList5.add("licence VTC");
            arrayList5.add("Assurance Société");
            arrayList5.add("Assurance Transport De Personnes");
            arrayList5.add("Carte Grise Du Véhicule");

        }
        else
        {
            arrayList5.add("Select document type");
            arrayList5.add("Drivers License");
            arrayList5.add("VTC license");
            arrayList5.add("Company Insurance");
            arrayList5.add("Passenger Transport Insurance");
            arrayList5.add("Vehicle Registration Certificate");
        }
        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList6.add("Sélectionnez le type de document");
            arrayList6.add("Permis De Conduire");
            arrayList6.add("licence VTC");
            arrayList6.add("Assurance Société");
            arrayList6.add("Assurance Transport De Personnes");
            arrayList6.add("Carte Grise Du Véhicule");

        }
        else
        {
            arrayList6.add("Select document type");
            arrayList6.add("Drivers License");
            arrayList6.add("VTC license");
            arrayList6.add("Company Insurance");
            arrayList6.add("Passenger Transport Insurance");
            arrayList6.add("Vehicle Registration Certificate");
        }

        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        save_icon=(RelativeLayout)findViewById(R.id.save_icon);

        back_icon.setOnClickListener(this);



        profile_icon=(CircleImageView)findViewById(R.id.profile_icon);
        user_name=(CustomEditTextRegular)findViewById(R.id.user_name);
        user_email=(CustomEditTextRegular)findViewById(R.id.user_email);
        ext_last_name=(CustomEditTextRegular)findViewById(R.id.ext_last_name);
        user_address=(CustomEditTextRegular)findViewById(R.id.user_address);
        etVehicleType=(CustomTextViewRegular)findViewById(R.id.etVehicleType);

        //tvProfileCountryCode=(CustomTextViewRegular)findViewById(R.id.tvProfileCountryCode);

        etProfileContactNumber=(CustomTextViewRegular)findViewById(R.id.etProfileContactNumber);
profile_icon.setOnClickListener(this);

      //  randomWaterBallAnimation();

        //For First Document
        tvselect_document_type=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type);
        tvselect_document=(MyFontEdittextView)findViewById(R.id.tvselect_document);

        //For Second Document
        tvselect_document_type2=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type2);
        tvselect_document2=(MyFontEdittextView)findViewById(R.id.tvselect_document2);

        //For Third Document
        tvselect_document_type3=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type3);
        tvselect_document3=(MyFontEdittextView)findViewById(R.id.tvselect_document3);

        //For Fourth Document
        tvselect_document_type4=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type4);
        tvselect_document4=(MyFontEdittextView)findViewById(R.id.tvselect_document4);

        //For Fifth Document

        tvselect_document_type5=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type5);
        tvselect_document5=(MyFontEdittextView)findViewById(R.id.tvselect_document5);


        setProfileData();

        save_icon.setOnClickListener(this);



    }


    private void setProfileData() {

        user_name.setText(preferenceHelper.getFirstName());
        ext_last_name.setText(preferenceHelper.getLastName());
       // tvProfileCountryCode.setText(preferenceHelper.getCountryPhoneCode()+"-");

        //tvProfileCountryCode.setText(preferenceHelper.getCountryPhoneCode());
        etProfileContactNumber.setText(preferenceHelper.getCountryPhoneCode()+"-"+ preferenceHelper.getContact());
        user_email.setText(preferenceHelper.getEmail());
//        Glide.with(this).load(preferenceHelper.getProfilePic())
//                .dontAnimate().placeholder(R.drawable.ellipse).override(80, 80).into
//                (profile_icon);


        PicassoTrustAll.getInstance(this)
                .load(preferenceHelper.getProfilePic())
                .error(R.drawable.ellipse)
                .resize(80,80)
                .into(profile_icon);

        currentPassword=preferenceHelper.getUserPassword();
        etVehicleType.setText(preferenceHelper.getVehicleType());
        System.out.println("Password is###"+currentPassword);


        //For First Document
        str_document_type1=preferenceHelper.getDRIVER_DOCUMENT_TYPE1();
        str_document_file1=preferenceHelper.getDRIVER_DOCUMENT_File1();
        str_document_ext1=preferenceHelper.getDRIVER_DOCUMENT_Extension1();
        tvselect_document_type.setText(str_document_type1+"");
        tvselect_document.setText(str_document_file1);

        tvselect_document.setOnClickListener(this);

        //For Second Document
        str_document_type2=preferenceHelper.getDRIVER_DOCUMENT_TYPE2();
        str_document_file2=preferenceHelper.getDRIVER_DOCUMENT_File2();
        str_document_ext2=preferenceHelper.getDRIVER_DOCUMENT_Extension2();
        tvselect_document_type2.setText(str_document_type2+"");
        tvselect_document2.setText(str_document_file2);
        tvselect_document2.setOnClickListener(this);



        //For Third Document
        str_document_type3=preferenceHelper.getDRIVER_DOCUMENT_TYPE3();
        str_document_file3=preferenceHelper.getDRIVER_DOCUMENT_File3();
        str_document_ext4=preferenceHelper.getDRIVER_DOCUMENT_Extension3();
        tvselect_document_type3.setText(str_document_type3+"");
        tvselect_document3.setText(str_document_file3);
        tvselect_document3.setOnClickListener(this);

        //For Fourth Document
        str_document_type4=preferenceHelper.getDRIVER_DOCUMENT_TYPE4();
        str_document_file4=preferenceHelper.getDRIVER_DOCUMENT_File4();
        str_document_ext4=preferenceHelper.getDRIVER_DOCUMENT_Extension4();
        tvselect_document_type4.setText(str_document_type4+"");
        tvselect_document4.setText(str_document_file4);
        tvselect_document4.setOnClickListener(this);

        //For Fifth Document
        str_document_type5=preferenceHelper.getDRIVER_DOCUMENT_TYPE5();
        str_document_file5=preferenceHelper.getDRIVER_DOCUMENT_File5();
        str_document_ext5=preferenceHelper.getDRIVER_DOCUMENT_Extension5();
        tvselect_document_type5.setText(str_document_type5+"");
        tvselect_document5.setText(str_document_file5);
        tvselect_document5.setOnClickListener(this);

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Const.PIC_URI, picUri);
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onClick(View v) {
        if(v==back_icon){
            finish();
        }
        if(v==save_icon){
           // Toast.makeText(this,"click done",Toast.LENGTH_SHORT).show();
     validate();

        }

        if(v==profile_icon)
        {
        openPhotoDialog();
        }

        if(v==tvselect_document)
        {
    openPhotoDialog2();
        }

        if(v==tvselect_document2)
        {
            openPhotoDialog3();
        }


        if(v==tvselect_document3)
        {
            openPhotoDialog4();
        }

        if(v==tvselect_document4)
        {
            openPhotoDialog5();
        }
        if(v==tvselect_document5)
        {
            openPhotoDialog6();
        }
    }


    protected void openPhotoDialog2() {

        if (ContextCompat.checkSelfPermission(Edit_ProfileActivity.this, Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission
                (Edit_ProfileActivity.this, Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Edit_ProfileActivity.this, new String[]{Manifest
                    .permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE2);
        }
        else {

            customPhotoDialog2 = new CustomPhotoDialog2(this) {

                @Override
                public void clickedOnCamera() {
                    customPhotoDialog2.dismiss();
                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog2.dismiss();
                    choosePhotoFromGallery2();
                }
            };
            customPhotoDialog2.show();
        }

    }

    protected void openPhotoDialog3() {


        customPhotoDialog3 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                customPhotoDialog3.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog3.dismiss();
                choosePhotoFromGallery3();
            }
        };
        customPhotoDialog3.show();


    }
    protected void openPhotoDialog4() {


        customPhotoDialog4 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                customPhotoDialog4.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog4.dismiss();
                choosePhotoFromGallery4();
            }
        };
        customPhotoDialog4.show();


    }

    protected void openPhotoDialog5() {


        customPhotoDialog5 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                customPhotoDialog5.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog5.dismiss();
                choosePhotoFromGallery5();
            }
        };
        customPhotoDialog5.show();


    }

    protected void openPhotoDialog6() {


        customPhotoDialog6 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                System.out.println("Sixth Camera is###"+"Sixth Gallery is###");

                customPhotoDialog6.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog6.dismiss();
                System.out.println("Sixth Gallery is###"+"Sixth Gallery is###");
                choosePhotoFromGallery6();
            }
        };
        customPhotoDialog6.show();


    }
    private void choosePhotoFromGallery2() {

        /*Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);*/
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO2);
    }
    private void choosePhotoFromGallery3() {

        /*Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);*/
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO3);
    }


    private void choosePhotoFromGallery4() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO4);
    }

    private void choosePhotoFromGallery5() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO5);
    }
    private void choosePhotoFromGallery6() {

        System.out.println("Sixth Gallery is@@@"+"Sixth Gallery is@@@");

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO6);
    }

    private void validate() {
        boolean isError=false;
        String str_user_email=user_email.getText().toString();
        String str_user_name=user_name.getText().toString();
        String str_user_last_name=ext_last_name.getText().toString();

        if(str_user_name==null || str_user_name.equalsIgnoreCase("") || str_user_name.equalsIgnoreCase("null")) {
            isError=true;
            user_name.setError(getResources().getString(R.string.ce_champ_est_requis));


        } else {
            user_name.setError(null);
        }
        if(str_user_last_name==null || str_user_last_name.equalsIgnoreCase("") || str_user_last_name.equalsIgnoreCase("null")) {
            isError=true;
            ext_last_name.setError(getResources().getString(R.string.ce_champ_est_requis));


        } else {
            ext_last_name.setError(null);
        }



        if(str_user_email==null || str_user_email.equalsIgnoreCase("") || str_user_email.equalsIgnoreCase("null"))
        {
            isError=true;
            user_email.setError(getResources().getString(R.string.ce_champ_est_requis));

        }


        else if(!isValidEmail(str_user_email)){

            isError=true;
            user_email.setError(getResources().getString(R.string.sil_vous_plaite_entr_eml_valid));

        }
        else
        {
            user_email.setError(null);
        }


        if(!isError) {

            upDateProfile();

        }

    }
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    protected void openPhotoDialog() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager
                .PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest
                .permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
        } else {
            customPhotoDialog = new CustomPhotoDialog(this) {
                @Override
                public void clickedOnCamera() {
                    customPhotoDialog.dismiss();
                    takePhotoFromCamera();
                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog.dismiss();
                    choosePhotoFromGallery();
                }
            };
            customPhotoDialog.show();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                    goWithCameraAndStoragePermission(grantResults);
                    break;
                default:
                    // do default
                    break;

            }
        }
    }

    private void closedPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            customDialogEnable.dismiss();
            customDialogEnable = null;

        }
    }


    private void goWithCameraAndStoragePermission(int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            openPhotoDialog();
        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission
                    .CAMERA)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
                openPermissionNotifyDialog(Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
            }
        } else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission
                    .READ_EXTERNAL_STORAGE)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
                openPermissionNotifyDialog(Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.ServiceCode.TAKE_PHOTO:
                if (resultCode == RESULT_OK) {
                    onCaptureImageResult();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO:
                onSelectFromGalleryResult(data);
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                handleCrop(resultCode, data);
                break;
            case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                openPhotoDialog();
                break;

            case Const.ServiceCode.CHOOSE_PHOTO2:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext1 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext1);

                        str_document_file1 = selectedImage.toString() + "." + str_document_ext1;
                        tvselect_document.setText(str_document_file1+ "");
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                    break;

            case Const.ServiceCode.CHOOSE_PHOTO3:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext2 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext2);

                        str_document_file2 = selectedImage.toString() + "." + str_document_ext2;

                        tvselect_document2.setText(str_document_file2 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case Const.ServiceCode.CHOOSE_PHOTO4:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext3 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext3);

                        str_document_file3 = selectedImage.toString() + "." + str_document_ext3;
                        tvselect_document3.setText(str_document_file3 + "");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO5:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext4 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext4);

                        str_document_file4 = selectedImage.toString() + "." + str_document_ext4;

                        tvselect_document4.setText(str_document_file4 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case Const.ServiceCode.CHOOSE_PHOTO6:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext5 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext5);

                        str_document_file5 = selectedImage.toString() + "." + str_document_ext5;

                        tvselect_document5.setText(str_document_file5 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            default:
                //do default
                break;

        }
    }



    private void upDateProfile() {

        HashMap<String, RequestBody> map = new HashMap<>();
        hideKeyPad();
        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_update_profile), false, null);

        if (TextUtils.equals(Const.MANUAL, preferenceHelper
                .getLoginBy())) {
            map.put(Const.Params.OLD_PASSWORD, ApiClient.makeTextRequestBody(currentPassword));
        } else {
            map.put(Const.Params.OLD_PASSWORD, ApiClient.makeTextRequestBody(""));
        }
        map.put(Const.Params.FIRST_NAME, ApiClient.makeTextRequestBody(user_name.getText
                ().toString()));
        map.put(Const.Params.LAST_NAME, ApiClient.makeTextRequestBody(ext_last_name.getText()
                .toString()));
        if (TextUtils.equals(Const.MANUAL, preferenceHelper
                .getLoginBy())) {
            map.put(Const.Params.NEW_PASSWORD, ApiClient.makeTextRequestBody(currentPassword));
        } else {
            map.put(Const.Params.NEW_PASSWORD, ApiClient.makeTextRequestBody(""));
        }
        map.put(Const.Params.PHONE, ApiClient.makeTextRequestBody( preferenceHelper.getContact()));
        map.put(Const.Params.BIO, ApiClient.makeTextRequestBody(""));
        map.put(Const.Params.PROVIDER_ID, ApiClient.makeTextRequestBody(preferenceHelper
                .getProviderId()));
        map.put(Const.Params.ADDRESS, ApiClient.makeTextRequestBody(user_address.getText()
                .toString()));
        map.put(Const.Params.ZIPCODE, ApiClient.makeTextRequestBody(""));
        map.put(Const.Params.COUNTRY_PHONE_CODE, ApiClient.makeTextRequestBody(preferenceHelper.getCountryPhoneCode()));
        map.put(Const.Params.TOKEN, ApiClient.makeTextRequestBody(preferenceHelper
                .getSessionToken()));


        Call<ProviderDataResponse> userDataResponseCall;
        if (!TextUtils.isEmpty(uploadImageFilePath)) {
            userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                    (ApiInterface.class).updateProfile(ApiClient.makeMultipartRequestBody
                            (this, uploadImageFilePath, Const.Params.PICTURE_DATA)
                    , map);
        } else {
            userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                    (ApiInterface.class).updateProfile(null, map);
        }

/*
        //For First Document

        if (!TextUtils.isEmpty(str_document_file1)) {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(ApiClient.makeMultipartRequestBody
                            (this, str_document_file1, Const.Params.DOCUMENT_DATA)
                    , map);
        } else {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(null, map);
        }
        map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(str_document_ext1));
        map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(str_document_type1));



        //For Second Document
        if (!TextUtils.isEmpty(str_document_file2)) {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(ApiClient.makeMultipartRequestBody
                            (this, str_document_file2, Const.Params.DOCUMENT_DATA)
                    , map);
        } else {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(null, map);
        }
        map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(str_document_ext2));
        map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(str_document_type2));


        //For Third Document
        if (!TextUtils.isEmpty(str_document_file3)) {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(ApiClient.makeMultipartRequestBody
                            (this, str_document_file3, Const.Params.DOCUMENT_DATA)
                    , map);
        } else {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(null, map);
        }
        map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(str_document_ext3));
        map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(str_document_type3));

        //For Fourth Document
        if (!TextUtils.isEmpty(str_document_file4)) {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(ApiClient.makeMultipartRequestBody
                            (this, str_document_file4, Const.Params.DOCUMENT_DATA)
                    , map);
        } else {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(null, map);
        }
        map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(str_document_ext4));
        map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(str_document_type4));

        //For Fifth Document
        if (!TextUtils.isEmpty(str_document_file5)) {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(ApiClient.makeMultipartRequestBody
                            (this, str_document_file5, Const.Params.DOCUMENT_DATA)
                    , map);
        } else {
            userDataResponseCall = ApiClient.getClient().create
                    (ApiInterface.class).register(null, map);
        }
        map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(str_document_ext5));
        map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(str_document_type5));*/
        userDataResponseCall.enqueue(new Callback<ProviderDataResponse>() {
            @Override
            public void onResponse(Call<ProviderDataResponse> call, Response<ProviderDataResponse
                    > response) {
                if (parseContent.isSuccessful(response)) {
                    if (parseContent.saveProviderData(response.body(), false)) {
                        Utils.hideCustomProgressDialog();
                        onBackPressed();
                    } else {
                        Utils.hideCustomProgressDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderDataResponse> call, Throwable t) {
                com.swissexpressdz.driver.utils.AppLog.handleThrowable(ProfileActivity_Driver.class.getSimpleName(), t);
            }
        });

    }
@Override
public void onBackPressed()
{
    Intent ii=new Intent(Edit_ProfileActivity.this, ProfileActivity.class);
    startActivity(ii);
    finish();
}
    public void hideKeyPad() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService
                    (INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    private void openPermissionNotifyDialog(final int code) {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogEnable(this, getResources().getString(R.string
                .msg_permission_notification), getResources().getString(R.string.text_exit_caps),
                getResources().getString(R.string.text_settings)) {

            @Override
            public void doWithEnable() {

                closedPermissionDialog();
                startActivityForResult(getIntentForPermission(), code);
            }

            @Override
            public void doWithDisable() {

                closedPermissionDialog();
                finishAffinity();
            }
        };
        customDialogEnable.show();

    }
    protected Intent getIntentForPermission() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        return intent;
    }

    private void openCameraPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogEnable(this, getResources().getString(R.string
                .msg_reason_for_camera_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void doWithEnable() {
                ActivityCompat.requestPermissions(Edit_ProfileActivity.this, new String[]{Manifest
                        .permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
                closedPermissionDialog();
            }

            @Override
            public void doWithDisable() {
                closedPermissionDialog();
            }
        };
        customDialogEnable.show();
    }
    private void choosePhotoFromGallery() {

     /*   Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media
     .EXTERNAL_CONTENT_URI);

        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);*/

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        picUri = savedInstanceState.getParcelable(Const.PIC_URI);
    }


    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            picUri = data.getData();
            beginCrop(picUri);
        }
    }


    private void handleCrop(int resultCode, Intent result) {
        final CropImage.ActivityResult activityResult = CropImage.getActivityResult(result);
        if (resultCode == RESULT_OK) {
            uploadImageFilePath = imageHelper.getRealPathFromURI(activityResult.getUri());
            new ImageCompression(this).setImageCompressionListener(new ImageCompression
                    .ImageCompressionListener() {
                @Override
                public void onImageCompression(String compressionImagePath) {
                    setProfileImage(activityResult.getUri());
                    uploadImageFilePath = compressionImagePath;

                }
            }).execute(uploadImageFilePath);

        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Utils.showToast(activityResult.getError().getMessage(), this);
        }
    }

    /**
     * This method is used for handel result after captured image from camera .
     */
    private void onCaptureImageResult() {
        beginCrop(picUri);
    }
    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Utils.isNougat()) {
            picUri = FileProvider.getUriForFile(Edit_ProfileActivity.this, getApplicationContext().getPackageName(),
                    imageHelper.createImageFile());
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
        else {
            picUri = Uri.fromFile(imageHelper.createImageFile().getAbsoluteFile());
        }
        AppLog.Log("URI", picUri.toString());

        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, Const.ServiceCode.TAKE_PHOTO);
    }
    private void beginCrop(Uri sourceUri) {
        CropImage.activity(sourceUri).setGuidelines(com.theartofdev.edmodo.cropper.CropImageView
                .Guidelines.ON).start(this);
    }

    private void setProfileImage(Uri imageUri) {
//        Glide.with(this).load(imageUri).fallback(R
//                .drawable.ellipse).into(profile_icon);
        PicassoTrustAll.getInstance(this)
                .load(imageUri)
                .error(R.drawable.ellipse)
                .into(profile_icon);
    }
    private void randomWaterBallAnimation(){


        water_ball1 =  findViewById(R.id.water_ball1);


        // For First First

        RotateAnimation anim = new RotateAnimation(0f, 350f, 15f, 15f);
        //   RotateAnimation anim = new RotateAnimation(0f, 0.5f, 1.1f, -0.3f);

        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(9000);

        TranslateAnimation mAnimation ;
        mAnimation = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.3f);
        mAnimation.setDuration(25000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());

        water_ball1.setAnimation(mAnimation);
        water_ball1.startAnimation(anim);


        // For Second Second  water_ball_2 =  findViewById(R.id.water_ball_2);
        water_ball_21 =  findViewById(R.id.water_ball_21);
        TranslateAnimation mAnimation2 ;
        mAnimation2 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.7f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation2.setDuration(30000);
        mAnimation2.setRepeatCount(-1);
        mAnimation2.setRepeatMode(Animation.REVERSE);
        mAnimation2.setInterpolator(new LinearInterpolator());

        water_ball_21.setAnimation(mAnimation2);

        water_ball_21.animate().rotation(1800f).setDuration(25000).start();
        water_ball_21.getAnimation().setRepeatCount(-1);

        water_ball_21.getAnimation().setRepeatMode(Animation.REVERSE);
        water_ball_21.getAnimation().setInterpolator(new LinearInterpolator());


        water_ball_31 = findViewById(R.id.water_ball_31);


        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(9000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());

        water_ball_31.setAnimation(mAnimation3);



        water_ball_41 = findViewById(R.id.water_ball_41);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());

        water_ball_41.setAnimation(mAnimation4);


        water_ball_51 = findViewById(R.id.water_ball_51);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());

        water_ball_51.setAnimation(mAnimation5);


        water_ball_61= findViewById(R.id.water_ball_61);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());

        water_ball_61.setAnimation(mAnimation6);


        water_ball_71 = findViewById(R.id.water_ball_71);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation7.setDuration(9000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());

        water_ball_71.setAnimation(mAnimation7);


        water_ball_81 = findViewById(R.id.water_ball_81);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());

        water_ball_81.setAnimation(mAnimation8);



        Water_ball_101 = findViewById(R.id.water_ball_101);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());

        Water_ball_101.setAnimation(mAnimation10);


        Water_ball_91 = findViewById(R.id.water_ball_91);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(9000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_91.setAnimation(mAnimation9);
    }
}
