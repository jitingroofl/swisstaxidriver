package com.swissexpressdz.driver.activity.checkamagestatu;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.BaseAppCompatActivity;
import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.TripHistoryActivity_Driver;
import com.swissexpressdz.driver.TripHistoryDetailActivity_Driver;
import com.swissexpressdz.driver.activity.applyfordamage.ApplyForDamageActivity;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.components.MyTitleFontTextView;
import com.swissexpressdz.driver.models.datamodels.TripHistory;
import com.swissexpressdz.driver.models.responsemodels.DepositListModel;
import com.swissexpressdz.driver.models.responsemodels.TripHistoryResponse;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CurrencyHelper;
import com.swissexpressdz.driver.utils.ImageHelper;
import com.swissexpressdz.driver.utils.PreferenceHelper;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TreeSet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckDamageStatusActiity extends BaseAppCompatActivity {
    ImageHelper imageHelper;

    private ArrayList<DepositListModel.DepositList> depositeList;
    RecyclerView history_recycler_view;
    MyTitleFontTextView tvNoItemHistory;
    TripHistoryAdaptor tripHistoryAdaptor;

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apply_for_damage);
        initToolBar();
        setTitleOnToolbar(getResources().getString(R.string.text_check_status_damage));



        initUII();
    }

    private void initUII() {
        imageHelper = new ImageHelper(this);

        depositeList = new ArrayList<>();

        history_recycler_view = (RecyclerView) findViewById(R.id.history_recycler_view);
        tvNoItemHistory = (MyTitleFontTextView) findViewById(R.id.tvNoItemHistory);

        getTripHistory("","");

    }
    @Override
    protected void initToolBar() {
        super.initToolBar();

    }



    private void getTripHistory(String startDate, String endDate) {
        JSONObject jsonObject = new JSONObject();
        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_history), false, null);

        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.TOKEN, preferenceHelper.getSessionToken());


            Call<DepositListModel> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .getdeposit_list(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<DepositListModel>() {
                @Override
                public void onResponse(Call<DepositListModel> call,
                                       Response<DepositListModel> response) {

                    if (parseContent.isSuccessful(response)) {
                        depositeList.clear();
                        if (response.body().getSuccess()) {
                            depositeList.clear();
                            depositeList.addAll(response.body().getDepositList());
                            if (tripHistoryAdaptor == null) {
                                tripHistoryAdaptor =
                                        new TripHistoryAdaptor(CheckDamageStatusActiity.this, depositeList);

                                LinearLayoutManager vehicleLinearLayoutManager = new LinearLayoutManager(CheckDamageStatusActiity.this);
                                vehicleLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

                                history_recycler_view.setLayoutManager(vehicleLinearLayoutManager);
                                history_recycler_view.setAdapter(tripHistoryAdaptor);
                                history_recycler_view.setNestedScrollingEnabled(true);

                            } else {
                                tripHistoryAdaptor.notifyDataSetChanged();
                            }
                            updateUi(depositeList.size() > 0);
                            Utils.hideCustomProgressDialog();
                        } else {
                            updateUi(false);
                            Utils.hideCustomProgressDialog();
                        }
                    }


                }

                @Override
                public void onFailure(Call<DepositListModel> call, Throwable t) {
                    AppLog.handleThrowable(TripHistoryActivity_Driver.class.getSimpleName(), t);

                }
            });
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.TRIP_HISTORY_ACTIVITY, e);
        }

    }
    private void updateUi(boolean isUpdate) {
        if (isUpdate) {
            tvNoItemHistory.setVisibility(View.GONE);
            history_recycler_view.setVisibility(View.VISIBLE);
        } else {
            tvNoItemHistory.setVisibility(View.VISIBLE);
            history_recycler_view.setVisibility(View.GONE);
        }


    }
    @Override
    public void goWithBackArrow() {

    onBackPressed();
    }

    @Override
    public void onBackPressed() {
     super.onBackPressed();
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }

    public class TripHistoryAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        public static final String TAG = "TripHistoryAdaptor";
        private static final int TYPE_ITEM = 0;
        private static final int TYPE_SEPARATOR = 1;
        private CheckDamageStatusActiity historyActivity;
        private ArrayList<DepositListModel.DepositList> tripHistoryList;
        private ParseContent parseContent;
        private TreeSet<Integer> separatorsSet;
        private SimpleDateFormat dateFormat;
        private NumberFormat currencyFormat;



        public TripHistoryAdaptor(CheckDamageStatusActiity applyForDamageActivity, ArrayList<DepositListModel.DepositList> tripHistoryList) {

            this.historyActivity = applyForDamageActivity;
            this.tripHistoryList = tripHistoryList;
            this.separatorsSet = separatorsSet;
            parseContent = ParseContent.getInstance();
            parseContent.getContext(this.historyActivity);
            dateFormat = parseContent.dateFormat;
            currencyFormat =
                    CurrencyHelper.getInstance(historyActivity).getCurrencyFormat(PreferenceHelper.getInstance(historyActivity).getCurrencyCode());

        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            if (viewType == TYPE_ITEM) {
                View v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.check_deposite_status_item, parent, false);
                return new ViewHolderHistory(v);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            // set data here
            final DepositListModel.DepositList history = tripHistoryList.get(position);
            if (holder instanceof ViewHolderHistory) {
                ViewHolderHistory viewHolder = (ViewHolderHistory) holder;

                viewHolder.tvHistoryClientName.setText(history.getUserName() + " ");



                //  String.format("%.2f", provider.getRate())

                viewHolder.tvHistoryTripTime.setText(getResources().getString(R.string.is_damage)+": "+history.getIsDamage()+"");

                System.out.println("Vehicle image:::::::"+history.getVehicleDamageImage());

        if(history.getIsDamage() == true && history.getPaymentStatus().equalsIgnoreCase("UNPAID"))
        {
            viewHolder.tvCanceledBy.setText(getResources().getString(R.string.status)+": "+history.getPaymentStatus()+"");
            viewHolder.tvCanceledBy.setTextColor(Color.parseColor("#e33c3c"));
            viewHolder.tvpayamounttouser.setText(getResources().getString(R.string.pay_to_user)+": "+history.getDepositAmountPayToUser()+"");

            viewHolder.tvdepositmout.setText(getResources().getString(R.string.depoit_amount)+": "+history.getDepositAmount()+"");
            viewHolder.tvpdamageamount.setText(getResources().getString(R.string.text_damage_amount)+": "+history.getDamageAmount()+"");


        }
               else if(history.getIsDamage() == true && history.getPaymentStatus().equalsIgnoreCase("PAID"))
        {
            viewHolder.tvCanceledBy.setText(history.getPaymentStatus()+": "+history.getPaymentStatus()+"");
            viewHolder.tvCanceledBy.setTextColor(Color.parseColor("#30AD23"));
            viewHolder.tvpayamounttouser.setText(getResources().getString(R.string.pay_to_user)+": "+history.getDepositAmountPayToUser()+"");

            viewHolder.tvdepositmout.setText(getResources().getString(R.string.depoit_amount)+": "+history.getDepositAmount()+"");
            viewHolder.tvpdamageamount.setText(getResources().getString(R.string.text_damage_amount)+": "+history.getDamageAmount()+"");



        }
               else
        {
            viewHolder.tvCanceledBy.setVisibility(View.GONE);
            viewHolder.tvpayamounttouser.setVisibility(View.GONE);
            viewHolder.tvdepositmout.setVisibility(View.GONE);
            viewHolder.tvpdamageamount.setVisibility(View.GONE);

        }




                    PicassoTrustAll.getInstance(historyActivity)
                        .load(ApiClient.Base_URL + history.getVehicleDamageImage())
                        .error(R.drawable.ellipse)
                        .resize(60,60)
                        .into(viewHolder.ivClientPhotoDialog);






            }


        }


        @Override
        public int getItemCount() {
            return tripHistoryList.size();
        }


        private String getYesterdayDateString() {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            return dateFormat.format(cal.getTime());
        }

        private void goToTripHistoryDetail(String tripId, int unit, String currency) {

            Intent tripDetailIntent = new Intent(historyActivity, TripHistoryDetailActivity_Driver
                    .class);
            tripDetailIntent.putExtra(Const.Params.TRIP_ID, tripId);
            tripDetailIntent.putExtra(Const.Params.UNIT, unit);
            tripDetailIntent.putExtra(Const.Params.CURRENCY, currency);
            historyActivity.startActivity(tripDetailIntent);
            historyActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

        protected class ViewHolderHistory extends RecyclerView.ViewHolder implements View
                .OnClickListener {

            TextView tvHistoryClientName, tvHistoryTripTime, tvCanceledBy;
            LinearLayout llHistory;
            ImageView ivClientPhotoDialog;
            MyFontTextView tvpayamounttouser,tvdepositmout,tvpdamageamount;


            public ViewHolderHistory(View itemView) {
                super(itemView);
                tvHistoryClientName = itemView.findViewById(R.id.tvHistoryClientName);
                tvHistoryTripTime = itemView.findViewById(R.id.tvHistoryTripTime);
                ivClientPhotoDialog = (ImageView) itemView.findViewById(R.id.ivClientPhotoDialog);
                tvCanceledBy = itemView.findViewById(R.id.tvCanceledBy);
                tvpayamounttouser = itemView.findViewById(R.id.tvpayamounttouser);
                llHistory = (LinearLayout) itemView.findViewById(R.id.llHistory);
                tvdepositmout = (MyFontTextView)  itemView.findViewById(R.id.tvdepositmout);
                tvpdamageamount = (MyFontTextView) itemView.findViewById(R.id.tvpdamageamount);
                llHistory.setOnClickListener(this);

            }

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.llHistory:
                        int position = getAdapterPosition();

                        break;
                    default:
                        // do with default
                        break;
                }

            }

        }

        protected class ViewHolderSeparator extends RecyclerView.ViewHolder {

            MyFontTextView tvDateSeparator;

            public ViewHolderSeparator(View itemView) {
                super(itemView);
                tvDateSeparator = (MyFontTextView) itemView.findViewById(R.id.tvDateSeparator);
            }
        }

    }

}
