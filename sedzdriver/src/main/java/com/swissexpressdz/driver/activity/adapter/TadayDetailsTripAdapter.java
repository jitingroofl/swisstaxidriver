package com.swissexpressdz.driver.activity.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.history.Details_History_Rase_TripActivity;

import java.util.ArrayList;

public class TadayDetailsTripAdapter extends RecyclerView.Adapter<TadayDetailsTripAdapter.ViewHolder> {

    Context context;
    ArrayList list;


    public TadayDetailsTripAdapter(Details_History_Rase_TripActivity details_history_rase_tripActivity, ArrayList list) {
        this.context=details_history_rase_tripActivity;
        this.list=list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v= LayoutInflater.from(context).inflate(R.layout.today_detail_trip_race_item,viewGroup,false);

        TadayDetailsTripAdapter.ViewHolder viewHolder=new TadayDetailsTripAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
