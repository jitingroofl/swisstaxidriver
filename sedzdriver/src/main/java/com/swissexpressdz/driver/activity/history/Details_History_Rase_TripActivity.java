package com.swissexpressdz.driver.activity.history;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.adapter.TadayDetailsTripAdapter;
import com.swissexpressdz.driver.activity.payment.Add_PaymentActivity;

import java.util.ArrayList;

public class Details_History_Rase_TripActivity extends AppCompatActivity implements View.OnClickListener {
    TextView title,see_more;
    TadayDetailsTripAdapter todayDetailsTripAdapter;
    RecyclerView today_list_item_trip;
    LinearLayoutManager linearLayoutManager1;
    ArrayList list=new ArrayList();
    RelativeLayout back_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details__history__rase__trip);
        inti();
    }

    public  void  inti(){
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.detail_of_the_race_trip_history);
        list.add("ram");
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        see_more=(TextView)findViewById(R.id.see_more);

        see_more.setOnClickListener(this);

        today_list_item_trip=(RecyclerView)findViewById(R.id.today_list_item_trip);
        linearLayoutManager1= new LinearLayoutManager(Details_History_Rase_TripActivity.this);
        todayDetailsTripAdapter=new TadayDetailsTripAdapter(Details_History_Rase_TripActivity.this,list);
        today_list_item_trip.setLayoutManager(linearLayoutManager1);
        today_list_item_trip.setAdapter(todayDetailsTripAdapter);

        back_icon.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v==back_icon){
            finish();
        }

        if(v==see_more){
            Intent intent=new Intent(this, Add_PaymentActivity.class);
            startActivity(intent);
        }

    }
}
