package com.swissexpressdz.driver.activity.gains;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.activity.adapter.DayAdapter;


public class GainsActivity extends AppCompatActivity implements View.OnClickListener {

    TextView title;
    RelativeLayout back_icon;

    private DayAdapter dayAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gains);
        inti();


    }

    public void  inti(){
        title=(TextView)findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(R.string.gains);
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        dayAdapter = new DayAdapter(getSupportFragmentManager());
        dayAdapter.addFragment(new Day_JourneeFragment(), "Journée");
        dayAdapter.addFragment(new Week_SemaineFragment(), "Semaine");
        // dayAdapter.addFragment(new Tab3Fragment(), "Tab 3");
        viewPager.setAdapter(dayAdapter);
        tabLayout.setupWithViewPager(viewPager);
        back_icon.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(back_icon==v){
            finish();
        }

    }
}
