package com.swissexpressdz.driver.activity.menu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.swissexpressdz.driver.BankDetailActivity_Driver;
import com.swissexpressdz.driver.BaseAppCompatActivity;
import com.swissexpressdz.driver.ContactUsActivity_Driver;
import com.swissexpressdz.driver.EarningActivity_Driver;
import com.swissexpressdz.driver.GetVTCActivity;
import com.swissexpressdz.driver.MainDrawerActivity;
import com.swissexpressdz.driver.PaymentActivity_Driver;
import com.swissexpressdz.driver.ProfileActivity_Driver;
import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.SettingActivity_Driver;
import com.swissexpressdz.driver.TripHistoryActivity_Driver;
import com.swissexpressdz.driver.activity.applyfordamage.ApplyForDamageActivity;
import com.swissexpressdz.driver.activity.checkamagestatu.CheckDamageStatusActiity;
import com.swissexpressdz.driver.activity.dashboard.Dash_BoardActivity;
import com.swissexpressdz.driver.activity.profile.ProfileActivity;
import com.swissexpressdz.driver.components.CustomDialogBigLabel;
import com.swissexpressdz.driver.models.responsemodels.ProviderSeriveSelctionModel;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextViewBold;
import com.swissexpressdz.driver.utils.CustomTextViewRegular;
import com.swissexpressdz.driver.utils.LatLngInterpolator;
import com.swissexpressdz.driver.utils.PreferenceHelper;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MenuActivity extends  BaseAppCompatActivity implements View.OnClickListener {

    RelativeLayout back_icon;
    ImageView water_ball,water_ball_2,water_ball_3,water_ball_4,water_ball_5,water_ball_6,water_ball_7,water_ball_8,Water_ball_9,Water_ball_10;
    private CustomDialogBigLabel customDialogBigLabel;
    LinearLayout layout_logout,layout_addvtc,profil_layout,layout_setting,layout_document,layout_payment,
            layout_history,layout_earning, layout_contact_as,layout_bank_details,dashboard_layout,layout_checkfordamage;
    CustomTextViewBold user_name;
    CustomTextViewRegular user_mobile_no;
    public PreferenceHelper preferenceHelper;
    CircleImageView profile_icon;
    LinearLayout layout_applyfordamage;
    boolean bol_carrental = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        preferenceHelper = PreferenceHelper.getInstance(this);
        inti();
        getProviderServiceStatus();
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    public void goWithBackArrow() {

    }
    private void getProviderServiceStatus() {
        Utils.showCustomProgressDialog(this, "", false, null);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());

            Call<ProviderSeriveSelctionModel> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .provider_vehicle_status(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<ProviderSeriveSelctionModel>() {
                @Override
                public void onResponse(Call<ProviderSeriveSelctionModel> call, Response<ProviderSeriveSelctionModel>
                        response) {

                    if (parseContent.isSuccessful(response)) {
                        Utils.hideCustomProgressDialog();
                        System.out.println("ProviderVehicle List  " + response.body().getSuccess());
                        if (response.body().getSuccess()) {


                            bol_carrental = response.body().getCarRent();

                            if (bol_carrental == true) {
                                layout_applyfordamage.setVisibility(View.VISIBLE);
                                layout_checkfordamage.setVisibility(View.VISIBLE);

                            }
                            else if(bol_carrental == false)
                            {
                                layout_checkfordamage.setVisibility(View.GONE);
                                layout_applyfordamage.setVisibility(View.GONE);

                            }



                        } else {

                        }


                    }
                }

                @Override
                public void onFailure(Call<ProviderSeriveSelctionModel> call, Throwable t) {
                    AppLog.handleThrowable(ProfileActivity_Driver.class.getSimpleName(), t);
                }


            });
        }
        catch (JSONException e) {
            AppLog.handleException(Const.Tag.BANK_DETAIL_ACTIVITY, e);
        }

    }
    public  void  inti(){
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        layout_logout=(LinearLayout)findViewById(R.id.layout_logout);
        profil_layout=(LinearLayout)findViewById(R.id.profil_layout);
        layout_addvtc=(LinearLayout)findViewById(R.id.layout_addvtc);
        if(preferenceHelper.getIsApproved() == 1)
        {

            layout_addvtc.setVisibility(View.GONE);
        }
        else
        {
            layout_addvtc.setVisibility(View.GONE);

        }
        layout_setting=(LinearLayout)findViewById(R.id.layout_setting);
        layout_document=(LinearLayout)findViewById(R.id.layout_document);
        layout_payment=(LinearLayout)findViewById(R.id.layout_payment);
        layout_history=(LinearLayout)findViewById(R.id.layout_history);
        layout_contact_as=(LinearLayout)findViewById(R.id.layout_contact_as);
        layout_bank_details=(LinearLayout)findViewById(R.id.layout_bank_details);
        dashboard_layout=(LinearLayout)findViewById(R.id.dashboard_layout);
        layout_earning=(LinearLayout)findViewById(R.id.layout_earning);

        layout_checkfordamage = (LinearLayout) findViewById(R.id.layout_checkfordamage);
        user_name=(CustomTextViewBold)findViewById(R.id.user_name);
        user_mobile_no=(CustomTextViewRegular)findViewById(R.id.user_mobile_no);
        profile_icon=(CircleImageView)findViewById(R.id.profile_icon);
        layout_applyfordamage = (LinearLayout) findViewById(R.id.layout_applyfordamage);



        back_icon.setOnClickListener(this);
        layout_logout.setOnClickListener(this);
        profil_layout.setOnClickListener(this);
        layout_addvtc.setOnClickListener(this);
        layout_setting.setOnClickListener(this);
        layout_document.setOnClickListener(this);
        layout_payment.setOnClickListener(this);
        layout_history.setOnClickListener(this);
        layout_bank_details.setOnClickListener(this);
        layout_earning.setOnClickListener(this);
        layout_contact_as.setOnClickListener(this);
        dashboard_layout.setOnClickListener(this);
        layout_applyfordamage.setOnClickListener(this);
        layout_checkfordamage.setOnClickListener(this);
      //  randomWaterBallAnimation();
        setProfileData();
    }

    @Override
    public void onClick(View v) {
        if(v==back_icon){
            finish();
            onBackPressed();

        }
        if(v==layout_logout){
            openLogoutDialog();
        }
        if(v==profil_layout){
            goToProfileActivity();
        }

        if(v==layout_addvtc)
        {
            goToAddVTCActivity();
        }
        if (v== layout_setting){
            goToSettingsActivity();
        }
        if(v==layout_document){
            goToDocumentActivity(true);
        }
        if(v==layout_payment){
            goToPaymentActivity();
        }
        if(v==layout_history){
            goToHistoryActivity();
        }
        if(v==layout_earning){
            goToEarningActivity();
        }
        if(v==layout_contact_as){
            goToContactUsActivity();
        }
        if(v==layout_bank_details){
            gotoBankDetailActivity();
        }

        if(v==dashboard_layout){
            gotoDashBoardActivity();
        }
        if(v==layout_applyfordamage){
            gotoApplyforDamageActivity();
        }

        if(v== layout_checkfordamage)
        {
            gotoCheckDamageActivity();

        }

    }

    @Override
    public void onBackPressed() {
    //    super.onBackPressed();
        Intent intent = new Intent(this, MainDrawerActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    private  void  gotoDashBoardActivity(){
        Intent bankInfo = new Intent(this, Dash_BoardActivity.class);
        startActivity(bankInfo);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    private void gotoBankDetailActivity() {
        Intent bankInfo = new Intent(this, BankDetailActivity_Driver.class);
        startActivity(bankInfo);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToContactUsActivity() {
        Intent intent = new Intent(MenuActivity.this, ContactUsActivity_Driver.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToEarningActivity() {
        Intent earning = new Intent(this, EarningActivity_Driver.class);
        startActivity(earning);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    private void goToSettingsActivity() {
        Intent settings = new Intent(this, SettingActivity_Driver.class);
        startActivity(settings);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void goToProfileActivity() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    private void goToAddVTCActivity() {
        Intent intent = new Intent(this, GetVTCActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private  void  gotoApplyforDamageActivity(){
        Intent applynfo = new Intent(this, ApplyForDamageActivity.class);
        startActivity(applynfo);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    private  void  gotoCheckDamageActivity(){
        Intent checkdamageInfo = new Intent(this, CheckDamageStatusActiity.class);
        startActivity(checkdamageInfo);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
    private void goToPaymentActivity() {
        Intent paymentIntent = new Intent(this,
                PaymentActivity_Driver
                        .class);
        startActivity(paymentIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    private void goToHistoryActivity() {
        Intent historyIntent = new Intent(MenuActivity.this,
                TripHistoryActivity_Driver.class);
        startActivity(historyIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void setProfileData() {
        user_name.setText(preferenceHelper.getFirstName()+" "+preferenceHelper.getLastName());
        System.out.println("Profile Number###"+preferenceHelper.getCountryPhoneCode()+"-"+preferenceHelper.getContact());
        user_mobile_no.setText(preferenceHelper.getCountryPhoneCode()+"-"+preferenceHelper.getContact());
//        Glide.with(this).load(preferenceHelper.getProfilePic())
//                .dontAnimate().placeholder(R.drawable.ellipse).override(80, 80).into
//                (profile_icon);

        PicassoTrustAll.getInstance(this)
                .load(preferenceHelper.getProfilePic())
                .error(R.drawable.ellipse)
                .resize(80,80)
                .into(profile_icon);

    }

    protected void openLogoutDialog() {

        customDialogBigLabel = new CustomDialogBigLabel(this, getString(R.string.text_logout),
                getString(R.string.msg_are_you_sure), getString(R.string.text_yes), getString(R
                .string.text_no)) {
            @Override
            public void positiveButton() {
                customDialogBigLabel.dismiss();
                logOut();
            }

            @Override
            public void negativeButton() {
                customDialogBigLabel.dismiss();
            }
        };
        customDialogBigLabel.show();
    }

    private void randomWaterBallAnimation(){
        water_ball_3 = findViewById(R.id.water_ball_3);
        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(9000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());
        water_ball_3.setAnimation(mAnimation3);

        water_ball_4 = findViewById(R.id.water_ball_4);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());
        water_ball_4.setAnimation(mAnimation4);
        water_ball_5 = findViewById(R.id.water_ball_5);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());
        water_ball_5.setAnimation(mAnimation5);
        water_ball_6 = findViewById(R.id.water_ball_6);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF,0.0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());
        water_ball_6.setAnimation(mAnimation6);
        water_ball_7 = findViewById(R.id.water_ball_7);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.1f);
        mAnimation7.setDuration(8000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());
        water_ball_7.setAnimation(mAnimation7);        water_ball_8 = findViewById(R.id.water_ball_8);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());
        water_ball_8.setAnimation(mAnimation8);        Water_ball_10 = findViewById(R.id.water_ball_10);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());
        Water_ball_10.setAnimation(mAnimation10);        Water_ball_9 = findViewById(R.id.water_ball_9);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(9000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_9.setAnimation(mAnimation9);
    }


    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }
}
