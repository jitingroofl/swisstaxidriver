package com.swissexpressdz.driver;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.swissexpressdz.driver.activity.menu.MenuActivity;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.models.responsemodels.AddVTCResponse;
import com.swissexpressdz.driver.models.responsemodels.GetVtcResponse;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextView_MontserratBold;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetVTCActivity extends BaseAppCompatActivity {
    CustomTextView_MontserratBold btnaddvtc;
    RecyclerView rcvgetVtc;
    List<GetVtcResponse.ProviderCityTypeVtc> providerCityTypeVtcs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_vtc);
//        initToolBar();
//        setTitleOnToolbar(getResources().getString(R.string.text_bank_detail));

        initToolBar();
        setTitleOnToolbar(getResources().getString(R.string.text_add_vehicle));
        initUI2();

    }

    private void initUI2() {
        rcvgetVtc = (RecyclerView) findViewById(R.id.rcvgetVtc);
        btnaddvtc = (CustomTextView_MontserratBold)findViewById(R.id.btnaddvtc);
        btnaddvtc.setOnClickListener(this);
    getVTC();
    }



    private void getVTC() {
        Utils.showCustomProgressDialog(GetVTCActivity.this, getResources().getString(R.string
                .msg_waiting_for_getting_credit_cards), false, null);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());



            Call<GetVtcResponse> call = ApiClient.getClient(GetVTCActivity.this).create(ApiInterface.class).provider_get_city_type_vtc_value
                    (ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<GetVtcResponse>() {
                @Override
                public void onResponse(Call<GetVtcResponse> call, Response<GetVtcResponse> response) {
                    if (parseContent.isSuccessful(response)) {

                        if (response.body().getSuccess()) {
                            System.out.println("Provider CityType  "+response.body().getProviderCityTypeVtc());
                            if(response.body().getProviderCityTypeVtc()!= null) {

                                providerCityTypeVtcs = new ArrayList<>();
                                providerCityTypeVtcs.clear();
                                //   providerCityTypeVtcs.add(response.body().getProviderCityTypeVtc());
                                LinearLayoutManager llm = new LinearLayoutManager(GetVTCActivity.this);
                                llm.setOrientation(LinearLayoutManager.VERTICAL);

                                GETVTC_Adapter getvtc_adapter = new GETVTC_Adapter(GetVTCActivity.this, response.body().getProviderCityTypeVtc());
                                rcvgetVtc.setLayoutManager(llm);

                                rcvgetVtc.setAdapter(getvtc_adapter);
                                rcvgetVtc.setNestedScrollingEnabled(false);

                                Utils.hideCustomProgressDialog();
                            }
                        }    else {
                            Utils.hideCustomProgressDialog();
                            Utils.showErrorToast(response.body().getErrorCode(), GetVTCActivity.this);
                        }

                    }

                }

                @Override
                public void onFailure(Call<GetVtcResponse> call, Throwable t) {
                    AppLog.handleThrowable(GetVTCActivity.class.getSimpleName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.VIEW_AND_ADD_PAYMENT_ACTIVITY, e);
        }
    }
    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    public void goWithBackArrow() {
        onBackPressed();
    }

    @Override
    public void onClick(View view) {
        if(view == btnaddvtc)
        {
            System.out.println("Type id "+preferenceHelper.getTypeid());
            finish();
            Intent intent = new Intent(this, AddVTCActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

    }

    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public class GETVTC_Adapter extends RecyclerView.Adapter<GETVTC_Adapter.GETVTC_AdapterViewHolder>{


        private GetVTCActivity drawerActivity;

        private List<GetVtcResponse.ProviderCityTypeVtc> cardList;
        private boolean showRentalTypeOnly;

        public GETVTC_Adapter(GetVTCActivity drawerActivity, List<GetVtcResponse.ProviderCityTypeVtc>
                vehicleTypeList) {

            this.drawerActivity = drawerActivity;
            this.cardList = vehicleTypeList;
            Log.e("Dialog List Size",this.cardList.size()+"");
        }



        public void setShowRentalTypeOnly(boolean showRentalTypeOnly) {
            this.showRentalTypeOnly = showRentalTypeOnly;
        }

        @Override
        public GETVTC_AdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.getvtcadapter, parent,
                    false);
            GETVTC_AdapterViewHolder vehicleViewHolder = new GETVTC_AdapterViewHolder(view);
            return vehicleViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull GETVTC_AdapterViewHolder holder, final int position) {

            holder.txt_countryname.setText(drawerActivity.getResources().getString(R.string.country_name)+": "+cardList.get(position).getCountryname());
            holder.txt_cityname.setText(drawerActivity.getResources().getString(R.string.city_name)+": "+cardList.get(position).getCityname());

            holder.cvProductFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                    Intent ii =new Intent(drawerActivity,EditVTCActivity.class);
                    ii.putExtra("type_id",cardList.get(position).getTypeid());
                    ii.putExtra("provider_city_type_vtc_id",cardList.get(position).getId());

                    startActivity(ii);
                }
            });
        }



        @Override
        public int getItemCount() {
            return cardList.size();
        }

        public class GETVTC_AdapterViewHolder extends RecyclerView.ViewHolder {

            MyFontTextView txt_countryname,txt_cityname;
            CardView cvProductFilter;
            // View viewSelectDiv;

            public GETVTC_AdapterViewHolder(View itemView) {
                super(itemView);

                txt_countryname = (MyFontTextView) itemView.findViewById(R.id.txt_countryname);
                txt_cityname = (MyFontTextView) itemView.findViewById(R.id.txt_cityname);
                cvProductFilter = (CardView) itemView.findViewById(R.id.cvProductFilter);
            }

        }


    }
}
