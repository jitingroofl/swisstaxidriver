package com.swissexpressdz.driver;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.swissexpressdz.driver.activity.adapter.RecentTransationAdapter;
import com.swissexpressdz.driver.activity.menu.MenuActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recent_tran_list;
    RecentTransationAdapter recentTransationAdapter;
    LinearLayoutManager linearLayoutManager;
    RelativeLayout layout_menu;
    ArrayList list=new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public  void inti(){

        layout_menu=(RelativeLayout) findViewById(R.id.layout_menu);
        recent_tran_list=(RecyclerView)findViewById(R.id.recent_tran_list);

        linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recentTransationAdapter = new RecentTransationAdapter(MainActivity.this,list);
        recent_tran_list.setAdapter(recentTransationAdapter);
        recent_tran_list.setLayoutManager(linearLayoutManager);
        layout_menu.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v==layout_menu){
            Intent ii  =new Intent(this, MenuActivity.class);
            startActivity(ii);
        }

    }
}
