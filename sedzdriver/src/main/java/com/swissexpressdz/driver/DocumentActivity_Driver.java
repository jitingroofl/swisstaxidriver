package com.swissexpressdz.driver;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.swissexpressdz.driver.adapter.DocumentAdaptor;
import com.swissexpressdz.driver.components.CustomDialogBigLabel;
import com.swissexpressdz.driver.components.CustomDialogEnable;
import com.swissexpressdz.driver.components.CustomPhotoDialog;
import com.swissexpressdz.driver.components.CustomPhotoDialog2;
import com.swissexpressdz.driver.components.MyAppTitleFontTextView;
import com.swissexpressdz.driver.components.MyFontButton;
import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.interfaces.ClickListener;
import com.swissexpressdz.driver.interfaces.RecyclerTouchListener;
import com.swissexpressdz.driver.models.datamodels.Document;
import com.swissexpressdz.driver.models.responsemodels.DocumentResponse;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextViewRegular;
import com.swissexpressdz.driver.utils.ImageCompression;
import com.swissexpressdz.driver.utils.ImageHelper;
import com.swissexpressdz.driver.utils.PreferenceHelper;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DocumentActivity_Driver extends BaseAppCompatActivity {

    public ImageHelper imageHelper;
    private String expireDate;
    private Uri picUri,picUri2,picUri3,picUri4,picUri5,picUri6,picUri7,picUri8,picUri9,picUri10,picUri11;
    private CustomPhotoDialog customPhotoDialog;
    private RecyclerView rcvDocumentList;
    private ArrayList<Document> docList;
    private DocumentAdaptor documentAdaptor;
    private MyFontButton btnSubmitDocument;
    private CustomDialogBigLabel customDialogBigLabel;
    private int docListPosition = 0;
    private boolean isClickedOnDrawer;
    private CustomDialogEnable customDialogEnable;
    private Dialog documentDialog;
    private ImageView ivDocumentImage;
    private MyFontEdittextView etDocumentNumber, etExpireDate;
    private MyAppTitleFontTextView tvDocumentTitle;
    private TextInputLayout tilDocumentNumber;
    private String uploadImageFilePath = "";
    private LinearLayout llDocumentList;
    private FrameLayout flNoDocumentMsg;
    String str_document_type1,str_document_file1,str_document_ext1,str_document_type2,str_document_file2,
            str_document_ext2,str_document_type3,str_document_file3,str_document_ext3,
            str_document_type4,str_document_file4,str_document_ext4,str_document_type5,str_document_file5,str_document_ext5,
            str_document_type6,str_document_file6,str_document_ext6,
            str_document_type7,str_document_file7,str_document_ext7,
    str_document_type8,str_document_file8,str_document_ext8,
    str_document_type9,str_document_file9,str_document_ext9,
    str_document_type10,str_document_file10,str_document_ext10;
    CustomTextViewRegular tvselect_document_type,tvselect_document_type2,tvselect_document_type3,tvselect_document_type4,
            tvselect_document_type5,tvselect_document_type6,tvselect_document_type7,
            tvselect_document_type8,tvselect_document_type9,tvselect_document_type10;

    private  String str_lang_code;
    ArrayList<String> arrayList2=new ArrayList<>();
    ArrayList<String> arrayList3=new ArrayList<>();
    ArrayList<String> arrayList4=new ArrayList<>();
    ArrayList<String> arrayList5=new ArrayList<>();
    ArrayList<String> arrayList6=new ArrayList<>();
    ArrayList<String> arrayList7=new ArrayList<>();
    ArrayList<String> arrayList8=new ArrayList<>();

    MyFontEdittextView tvselect_document,tvselect_document2,tvselect_document3,tvselect_document4,tvselect_document5,tvselect_document6,
            tvselect_document7,tvselect_document8,tvselect_document9,tvselect_document10;

    private CustomPhotoDialog2 customPhotoDialog2,customPhotoDialog3,customPhotoDialog4,customPhotoDialog5
            ,customPhotoDialog6,customPhotoDialog7,customPhotoDialog8,customPhotoDialog9,customPhotoDialog10,customPhotoDialog11;

    ImageView water_ball1,water_ball_21,water_ball_31,water_ball_41,water_ball_51,water_ball_61,water_ball_71,water_ball_81,Water_ball_91,Water_ball_101;
LinearLayout llselect_document;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_driver);
        preferenceHelper = PreferenceHelper.getInstance(this);

        str_lang_code=preferenceHelper.getLanguageCode();

        setArr_List();

        initToolBar();
        setTitleOnToolbar(getResources().getString(R.string.text_Document));


        btnSubmitDocument = (MyFontButton) findViewById(R.id.btnSubmitDocument);
        llDocumentList = (LinearLayout) findViewById(R.id.llDocumentList);
        flNoDocumentMsg = (FrameLayout) findViewById(R.id.flNoDocumentMsg);
        btnSubmitDocument.setOnClickListener(this);
        imageHelper = new ImageHelper(this);
        initDocumentRcv();
        getProviderDocument();

        if (getIntent() != null) {
            isClickedOnDrawer = getIntent().getExtras().getBoolean(Const.IS_CLICK_INSIDE_DRAWER);
        }

        //For First Document
        llselect_document=(LinearLayout)findViewById(R.id.llselect_document);
        tvselect_document_type=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type);
        tvselect_document=(MyFontEdittextView)findViewById(R.id.tvselect_document);

        //For Second Document
        tvselect_document_type2=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type2);
        tvselect_document2=(MyFontEdittextView)findViewById(R.id.tvselect_document2);

        //For Third Document
        tvselect_document_type3=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type3);
        tvselect_document3=(MyFontEdittextView)findViewById(R.id.tvselect_document3);

        //For Fourth Document
        tvselect_document_type4=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type4);
        tvselect_document4=(MyFontEdittextView)findViewById(R.id.tvselect_document4);

        //For Fifth Document

        tvselect_document_type5=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type5);
        tvselect_document5=(MyFontEdittextView)findViewById(R.id.tvselect_document5);

        //For Sixth Document

        tvselect_document_type6=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type6);
        tvselect_document6=(MyFontEdittextView)findViewById(R.id.tvselect_document6);

        //For Seventh Document

        tvselect_document_type7=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type7);
        tvselect_document7=(MyFontEdittextView)findViewById(R.id.tvselect_document7);

        //For Eighth Document
        tvselect_document_type8=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type8);
        tvselect_document8=(MyFontEdittextView)findViewById(R.id.tvselect_document8);

        //For Ninth Document
        tvselect_document_type9=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type9);
        tvselect_document9=(MyFontEdittextView)findViewById(R.id.tvselect_document9);

        //For Tenth Document
        tvselect_document_type10=(CustomTextViewRegular)findViewById(R.id.tvselect_document_type10);
        tvselect_document10=(MyFontEdittextView)findViewById(R.id.tvselect_document10);



      //  randomWaterBallAnimation();



    }

    private void setDocData() {

        //For First Document
        str_document_type1=docList.get(0).getDocument_type();
        str_document_file1=docList.get(0).getDocumentData();
        str_document_ext1=docList.get(0).getDocumentExt();
        tvselect_document_type.setText(str_document_type1+"");
      //  String replace_str_one=docList.get(0).getDocumentData().replace(ApiClient.Base_URL+"provider_document/","");
        String replace_str_one=docList.get(0).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");

        System.out.println("Replace String is###"+replace_str_one);
        tvselect_document.setText(replace_str_one);
        tvselect_document.setOnClickListener(this);

        //For Second Document
        str_document_type2=docList.get(1).getDocument_type();
        str_document_file2=docList.get(1).getDocumentData();
        str_document_ext2=docList.get(1).getDocumentExt();
        tvselect_document_type2.setText(str_document_type2+"");
        String replace_str_two=docList.get(1).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");

        tvselect_document2.setText(replace_str_two);
        tvselect_document2.setOnClickListener(this);



        //For Third Document
        str_document_type3=docList.get(2).getDocument_type();
        str_document_file3=docList.get(2).getDocumentData();
        str_document_ext4=docList.get(2).getDocumentExt();
        tvselect_document_type3.setText(str_document_type3+"");

        String replace_str_three=docList.get(2).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");

        tvselect_document3.setText(replace_str_three);
        tvselect_document3.setOnClickListener(this);

        //For Fourth Document
        str_document_type4=docList.get(3).getDocument_type();
        str_document_file4=docList.get(3).getDocumentData();
        str_document_ext4=docList.get(3).getDocumentExt();
        tvselect_document_type4.setText(str_document_type4+"");

        String replace_str_four=docList.get(3).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");

        tvselect_document4.setText(replace_str_four);
        tvselect_document4.setOnClickListener(this);

        //For Fifth Document
        str_document_type5=docList.get(4).getDocument_type();
        str_document_file5=docList.get(4).getDocumentData();
        str_document_ext5=docList.get(4).getDocumentExt();

        tvselect_document_type5.setText(str_document_type5+"");
        String replace_str_fifth=docList.get(4).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");

        tvselect_document5.setText(replace_str_fifth);
        tvselect_document5.setOnClickListener(this);


        //For Sixth Document
        try {
            str_document_type6=docList.get(5).getDocument_type();
            str_document_file6=docList.get(5).getDocumentData();
            str_document_ext6=docList.get(5).getDocumentExt();

            tvselect_document_type6.setText(str_document_type6+"");
            String replace_str_sixth=docList.get(5).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");

            tvselect_document6.setText(replace_str_sixth);
            tvselect_document6.setOnClickListener(this);
        }
        catch (Exception e)
        {
            System.out.println("Exception "+e);
        }

        //For Seventh Document
        try {
            str_document_type7=docList.get(6).getDocument_type();
            str_document_file7=docList.get(6).getDocumentData();
            str_document_ext7=docList.get(6).getDocumentExt();

            tvselect_document_type7.setText(str_document_type7+"");
            String replace_str_seventh=docList.get(6).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");

            tvselect_document7.setText(replace_str_seventh);
            tvselect_document7.setOnClickListener(this);
        }
        catch (Exception e)
        {
            System.out.println("Exception "+e);
        }

        //For Eighth Document
        try {
            str_document_type8=docList.get(7).getDocument_type();
            str_document_file8=docList.get(7).getDocumentData();
            str_document_ext8=docList.get(7).getDocumentExt();

            tvselect_document_type8.setText(str_document_type8+"");
            String replace_str_eightth=docList.get(7).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");

            tvselect_document8.setText(replace_str_eightth);
            tvselect_document8.setOnClickListener(this);
        }
        catch (Exception e)
        {
            System.out.println("Exception "+e);
        }
        //For Night Document
        try {
            str_document_type9=docList.get(8).getDocument_type();
            str_document_file9=docList.get(8).getDocumentData();
            str_document_ext9=docList.get(8).getDocumentExt();

            tvselect_document_type9.setText(str_document_type9+"");
            String replace_str_nineth=docList.get(8).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");

            tvselect_document9.setText(replace_str_nineth);
            tvselect_document9.setOnClickListener(this);
        }
        catch (Exception e)
        {
            System.out.println("Exception "+e);
        }

        //For Tenth Document
        try {
            str_document_type10=docList.get(9).getDocument_type();
            str_document_file10=docList.get(9).getDocumentData();
            str_document_ext10=docList.get(9).getDocumentExt();

            tvselect_document_type10.setText(str_document_type10+"");
            String replace_str_nineth=docList.get(9).getDocumentData().replace("http://103.231.44.74:5000/provider_document/","");

            tvselect_document10.setText(replace_str_nineth);
            tvselect_document10.setOnClickListener(this);
        }
        catch (Exception e)
        {
            System.out.println("Exception "+e);
        }
    }


    public void setArr_List()
    {
        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList2.add("Sélectionnez le type de document");
            arrayList2.add("Permis de conduire recto-verso");
            arrayList2.add("Carte d’identité recto-verso");
            arrayList2.add("Carte VTC recto-verso");
            arrayList2.add("Attestation assurance RC Pro");
            arrayList2.add("KBis ou attestation auto-entrepreneur");

        }
        else
        {
            arrayList2.add("Select document type");
            arrayList2.add("Permis de conduire recto-verso");
            arrayList2.add("VTC license");
            arrayList2.add("Company Insurance");
            arrayList2.add("Passenger Transport Insurance");
            arrayList2.add("Vehicle Registration Certificate");
        }

        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList3.add("Sélectionnez le type de document");
            arrayList3.add("Permis De Conduire");
            arrayList3.add("Carte d’identité recto-verso");
            arrayList3.add("Carte VTC recto-verso");
            arrayList3.add("Attestation assurance RC Pro");
            arrayList3.add("KBis ou attestation auto-entrepreneur");

        }
        else
        {
            arrayList3.add("Select document type");
            arrayList3.add("Drivers License");
            arrayList3.add("VTC license");
            arrayList3.add("Company Insurance");
            arrayList3.add("Passenger Transport Insurance");
            arrayList3.add("Vehicle Registration Certificate");
        }
        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList4.add("Sélectionnez le type de document");
            arrayList4.add("Permis de conduire recto-verso");
            arrayList4.add("Carte d’identité recto-verso");
            arrayList4.add("Carte VTC recto-verso");
            arrayList4.add("Attestation assurance RC Pro");
            arrayList4.add("KBis ou attestation auto-entrepreneur");

        }
        else
        {
            arrayList4.add("Select document type");
            arrayList4.add("Drivers License");
            arrayList4.add("VTC license");
            arrayList4.add("Company Insurance");
            arrayList4.add("Passenger Transport Insurance");
            arrayList4.add("Vehicle Registration Certificate");
        }


        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList5.add("Sélectionnez le type de document");
            arrayList5.add("Permis de conduire recto-verso");
            arrayList5.add("Carte d’identité recto-verso");
            arrayList5.add("Carte VTC recto-verso");
            arrayList5.add("Attestation assurance RC Pro");
            arrayList5.add("KBis ou attestation auto-entrepreneur");

        }
        else
        {
            arrayList5.add("Select document type");
            arrayList5.add("Drivers License");
            arrayList5.add("VTC license");
            arrayList5.add("Company Insurance");
            arrayList5.add("Passenger Transport Insurance");
            arrayList5.add("Vehicle Registration Certificate");
        }
        if(str_lang_code.equalsIgnoreCase("fr"))
        {
            arrayList6.add("Sélectionnez le type de document");
            arrayList6.add("Permis de conduire recto-verso");
            arrayList6.add("Carte d’identité recto-verso");
            arrayList6.add("Carte VTC recto-verso");
            arrayList6.add("Attestation assurance RC Pro");
            arrayList6.add("KBis ou attestation auto-entrepreneur");

        }
        else
        {
            arrayList6.add("Select document type");
            arrayList6.add("Drivers License");
            arrayList6.add("VTC license");
            arrayList6.add("Company Insurance");
            arrayList6.add("Passenger Transport Insurance");
            arrayList6.add("Vehicle Registration Certificate");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        setConnectivityListener(this);
        setAdminApprovedListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Const.PIC_URI, picUri);
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        picUri = savedInstanceState.getParcelable(Const.PIC_URI);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.ServiceCode.TAKE_PHOTO:
                if (resultCode == RESULT_OK) {
                    onCaptureImageResult();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO:
                onSelectFromGalleryResult(data);
                break;
            case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                openPhotoDialog();
                break;
            case Const.ServiceCode.CHOOSE_PHOTO2:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext1 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        //str_document_ext1 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        str_document_file1 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri2=selectedImage;
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext1);

                      //  tvselect_document.setText(str_document_file1+ "");

                        uploadDocument("","",docList.get(0).getDocumentId(),0,
                                Const.Params.DOCUMENT_EXT,str_document_ext1,Const.Params.DOCUMENT_TYPE,str_document_type1,Const.Params.DOCUMENT_DATA,str_document_file1,picUri2);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                break;

            case Const.ServiceCode.CHOOSE_PHOTO3:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext2 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext2);

                        str_document_file2 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri3=selectedImage;

                        uploadDocument("","",docList.get(1).getDocumentId(),0,
                                Const.Params.DOCUMENT_EXT,str_document_ext2,Const.Params.DOCUMENT_TYPE,str_document_type2,Const.Params.DOCUMENT_DATA,str_document_file2,picUri3);

                      //  tvselect_document2.setText(str_document_file2 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case Const.ServiceCode.CHOOSE_PHOTO4:
                try {
                    Uri selectedImage = data.getData();
                    try {

                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext3 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext3);

                       // str_document_file3 = selectedImage.toString() + "." + str_document_ext3;
                        str_document_file3 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri4=selectedImage;
                        uploadDocument("","",docList.get(2).getDocumentId(),0,
                                Const.Params.DOCUMENT_EXT,str_document_ext3,Const.Params.DOCUMENT_TYPE,str_document_type3,Const.Params.DOCUMENT_DATA,str_document_file3,picUri4);
                        //tvselect_document3.setText(str_document_file3 + "");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO5:
                try {
                    Uri selectedImage = data.getData();
                    try {

                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext4 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext4);
                      //  str_document_file4 = selectedImage.toString() + "." + str_document_ext4;
                        str_document_file4 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri5=selectedImage;
                        uploadDocument("","",docList.get(3).getDocumentId(),0,
                                Const.Params.DOCUMENT_EXT,str_document_ext4,Const.Params.DOCUMENT_TYPE,str_document_type4,Const.Params.DOCUMENT_DATA,str_document_file4,picUri5);

                   //     tvselect_document4.setText(str_document_file4 + "");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case Const.ServiceCode.CHOOSE_PHOTO6:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext5 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext5);

                     //   str_document_file5 = selectedImage.toString() + "." + str_document_ext5;
                        str_document_file5 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri6=selectedImage;
                        uploadDocument("","",docList.get(4).getDocumentId(),0,
                                Const.Params.DOCUMENT_EXT,str_document_ext5,Const.Params.DOCUMENT_TYPE,
                                str_document_type5,Const.Params.DOCUMENT_DATA,str_document_file5,picUri6);

                      //  tvselect_document5.setText(str_document_file5 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case Const.ServiceCode.CHOOSE_PHOTO7:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext6 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext6);

                        //   str_document_file5 = selectedImage.toString() + "." + str_document_ext5;
                        str_document_file6 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri7=selectedImage;
                        uploadDocument("","",docList.get(5).getDocumentId(),0,
                                Const.Params.DOCUMENT_EXT,str_document_ext6,Const.Params.DOCUMENT_TYPE,str_document_type6,
                                Const.Params.DOCUMENT_DATA,str_document_file6,picUri7);

                        //  tvselect_document5.setText(str_document_file5 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO8:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext7 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext7);

                        //   str_document_file5 = selectedImage.toString() + "." + str_document_ext5;
                        str_document_file7 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri8=selectedImage;
                        uploadDocument("","",docList.get(6).getDocumentId(),0,
                                Const.Params.DOCUMENT_EXT,str_document_ext7,Const.Params.DOCUMENT_TYPE,str_document_type7,
                                Const.Params.DOCUMENT_DATA,str_document_file7,picUri8);

                        //  tvselect_document5.setText(str_document_file5 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO9:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext8 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext8);

                        //   str_document_file5 = selectedImage.toString() + "." + str_document_ext5;
                        str_document_file8 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri9=selectedImage;
                        uploadDocument("","",docList.get(7).getDocumentId(),0,
                                Const.Params.DOCUMENT_EXT,str_document_ext8,Const.Params.DOCUMENT_TYPE,str_document_type8,
                                Const.Params.DOCUMENT_DATA,str_document_file8,picUri9);

                        //  tvselect_document5.setText(str_document_file5 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO10:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext9 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext9);

                        //   str_document_file5 = selectedImage.toString() + "." + str_document_ext5;
                        str_document_file9 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri10=selectedImage;
                        uploadDocument("","",docList.get(8).getDocumentId(),0,
                                Const.Params.DOCUMENT_EXT,str_document_ext9,Const.Params.DOCUMENT_TYPE,str_document_type9,
                                Const.Params.DOCUMENT_DATA,str_document_file9,picUri10);

                        //  tvselect_document5.setText(str_document_file5 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CHOOSE_PHOTO11:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        str_document_ext10 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext10);

                        //   str_document_file5 = selectedImage.toString() + "." + str_document_ext5;
                        str_document_file10 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                        picUri11=selectedImage;
                        uploadDocument("","",docList.get(9).getDocumentId(),0,
                                Const.Params.DOCUMENT_EXT,str_document_ext10,Const.Params.DOCUMENT_TYPE,str_document_type10,
                                Const.Params.DOCUMENT_DATA,str_document_file10,picUri11);

                        //  tvselect_document5.setText(str_document_file5 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            default:

                // do with default
                break;

        }
    }

    @Override
    protected boolean isValidate() {
        return false;
    }

    @Override
    public void goWithBackArrow() {
        onBackPressed();
    }

    protected void openPhotoDialog() {

        if (ContextCompat.checkSelfPermission(DocumentActivity_Driver.this, Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission
                (DocumentActivity_Driver.this, Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DocumentActivity_Driver.this, new String[]{Manifest
                    .permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
        } else {
            customPhotoDialog = new CustomPhotoDialog(this) {
                @Override
                public void clickedOnCamera() {
                    customPhotoDialog.dismiss();
                    takePhotoFromCamera();
                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog.dismiss();
                    choosePhotoFromGallery();
                }
            };
            customPhotoDialog.show();
        }

    }

    private void choosePhotoFromGallery() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Utils.isNougat()) {
            picUri = FileProvider.getUriForFile(this, getPackageName(), imageHelper
                    .createImageFile());
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            picUri = Uri.fromFile(imageHelper.createImageFile());
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, Const.ServiceCode.TAKE_PHOTO);
    }

    /**
     * This method is used for handel result after select image from gallery .
     */

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            picUri = data.getData();
            uploadImageFilePath = ImageHelper.getFromMediaUriPfd(this, getContentResolver
                    (), picUri).getPath();
            setDocumentImage(picUri);
        }
    }

    /**
     * This method is used for handel result after captured image from camera .
     */
    private void onCaptureImageResult() {
        uploadImageFilePath = ImageHelper.getFromMediaUriPfd(this, getContentResolver
                (), picUri).getPath();
        setDocumentImage(picUri);
    }


    private void setDocumentImage(final Uri imageUri) {
        new ImageCompression(this).setImageCompressionListener(new ImageCompression
                .ImageCompressionListener() {

            @Override
            public void onImageCompression(String compressionImagePath) {
                if (documentDialog != null && documentDialog.isShowing()) {
                    uploadImageFilePath = compressionImagePath;
//                    Glide.with(DocumentActivity_Driver.this).load(imageUri).fallback(R
//                            .drawable.ellipse).override(200, 200)
//                            .into(ivDocumentImage);

                    PicassoTrustAll.getInstance(DocumentActivity_Driver.this)
                            .load(imageUri)
                            .error(R.drawable.ellipse)
                            .resize(200,200)
                            .into(ivDocumentImage);
                }
            }
        }).execute(uploadImageFilePath);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmitDocument:
               if (preferenceHelper.getAllDocUpload() == Const.ProviderStatus.IS_UPLOADED) {
                    goToMainDrawerActivity();
                } else {
                    Utils.showToast(getResources().getString(R.string.msg_upload_all_document),
                            this);
                }

                break;


            case R.id.tvselect_document:

                openPhotoDialog2();
                break;

                case R.id.tvselect_document2:
                openPhotoDialog3();
                break;

            case R.id.tvselect_document3:
                openPhotoDialog4();
                break;

            case R.id.tvselect_document4:
                openPhotoDialog5();
                break;

            case R.id.tvselect_document5:

                openPhotoDialog6();
                break;
            case R.id.tvselect_document6:

                openPhotoDialog7();
                break;

                case R.id.tvselect_document7:

                openPhotoDialog8();
                break;
            case R.id.tvselect_document8:

                openPhotoDialog9();
                break;
            case R.id.tvselect_document9:

                openPhotoDialog10();
                break;

            case R.id.tvselect_document10:

                openPhotoDialog11();
                break;
            default:
                // do with default
                break;
        }

    }
    protected void openPhotoDialog2() {

        if (ContextCompat.checkSelfPermission(DocumentActivity_Driver.this, Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission
                (DocumentActivity_Driver.this, Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DocumentActivity_Driver.this, new String[]{Manifest
                    .permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE2);
        }
        else {

            customPhotoDialog2 = new CustomPhotoDialog2(this) {

                @Override
                public void clickedOnCamera() {
                    customPhotoDialog2.dismiss();
                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog2.dismiss();
                    choosePhotoFromGallery2();
                }
            };
            customPhotoDialog2.show();
        }

    }
    protected void openPhotoDialog3() {


        customPhotoDialog3 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                customPhotoDialog3.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog3.dismiss();
                choosePhotoFromGallery3();
            }
        };
        customPhotoDialog3.show();


    }
    protected void openPhotoDialog4() {


        customPhotoDialog4 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                customPhotoDialog4.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog4.dismiss();
                choosePhotoFromGallery4();
            }
        };
        customPhotoDialog4.show();


    }

    protected void openPhotoDialog5() {


        customPhotoDialog5 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                customPhotoDialog5.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog5.dismiss();
                choosePhotoFromGallery5();
            }
        };
        customPhotoDialog5.show();


    }

    protected void openPhotoDialog6() {


        customPhotoDialog6 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                System.out.println("Sixth Camera is###"+"Sixth Gallery is###");

                customPhotoDialog6.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog6.dismiss();
                System.out.println("Sixth Gallery is###"+"Sixth Gallery is###");
                choosePhotoFromGallery6();
            }
        };
        customPhotoDialog6.show();


    }
    protected void openPhotoDialog7() {


        customPhotoDialog7 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                System.out.println("Seventh Camera is###"+"Seventh Gallery is###");

                customPhotoDialog7.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog7.dismiss();
                System.out.println("Seventh Gallery is###"+"Seventh Gallery is###");
                choosePhotoFromGallery7();
            }
        };
        customPhotoDialog7.show();


    }
    protected void openPhotoDialog8() {


        customPhotoDialog8 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                System.out.println("Eigthth Camera is###"+"Eigthth Gallery is###");

                customPhotoDialog8.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog8.dismiss();
                System.out.println("Eigthth Gallery is###"+"Eigthth Gallery is###");
                choosePhotoFromGallery8();
            }
        };
        customPhotoDialog8.show();


    }

    protected void openPhotoDialog9() {


        customPhotoDialog9 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                System.out.println("Ninth Camera is###"+"Ninth Gallery is###");

                customPhotoDialog9.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog9.dismiss();
                System.out.println("Eigthth Gallery is###"+"Eigthth Gallery is###");
                choosePhotoFromGallery9();
            }
        };
        customPhotoDialog9.show();


    }
    protected void openPhotoDialog10() {


        customPhotoDialog10 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                System.out.println("Ninth Camera is###"+"Ninth Gallery is###");

                customPhotoDialog10.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog10.dismiss();
                System.out.println("Ninth Gallery is###"+"Ninth Gallery is###");
                choosePhotoFromGallery10();
            }
        };
        customPhotoDialog10.show();


    }
    protected void openPhotoDialog11() {


        customPhotoDialog11 = new CustomPhotoDialog2(this) {

            @Override
            public void clickedOnCamera() {
                System.out.println("Tenth Camera is###"+"Tenth Gallery is###");

                customPhotoDialog11.dismiss();
            }

            @Override
            public void clickedOnGallery() {
                customPhotoDialog11.dismiss();
                System.out.println("Tenth Gallery is###"+"Tenth Gallery is###");
                choosePhotoFromGallery11();
            }
        };
        customPhotoDialog11.show();


    }


    private void choosePhotoFromGallery2() {

        /*Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO);*/
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO2);
    }
    private void choosePhotoFromGallery3() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO3);
    }


    private void choosePhotoFromGallery4() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO4);
    }

    private void choosePhotoFromGallery5() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO5);
    }
    private void choosePhotoFromGallery6() {

        System.out.println("Sixth Gallery is@@@"+"Sixth Gallery is@@@");

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO6);
    }
    private void choosePhotoFromGallery7() {

        System.out.println("Seventh Gallery is@@@"+"Seventh Gallery is@@@");

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO7);
    }
    private void choosePhotoFromGallery8() {

        System.out.println("Eighth Gallery is@@@"+"Eighth Gallery is@@@");

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO8);
    }
    private void choosePhotoFromGallery9() {

        System.out.println("Ninth Gallery is@@@"+"Ninth Gallery is@@@");

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO9);
    }
    private void choosePhotoFromGallery10() {

        System.out.println("Tenth Gallery is@@@"+"Tenth Gallery is@@@");

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO10);
    }
    private void choosePhotoFromGallery11() {

        System.out.println("Tenth Gallery is@@@"+"Tenth Gallery is@@@");

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO11);
    }

    private void getProviderDocument() {
        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                        .msg_waiting_for_get_documents),
                false, null);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.TOKEN, preferenceHelper.getSessionToken());
            Call<DocumentResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .getDocuments(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<DocumentResponse>() {
                @Override
                public void onResponse(Call<DocumentResponse> call, Response<DocumentResponse>
                        response) {

                    if (parseContent.isSuccessful(response)) {
                        System.out.println("Response get is###"+response);
                        if (response.body().isSuccess()) {
                            docList.addAll(response.body().getDocuments());
                            if (docList.size() == 0) {
                                preferenceHelper.putAllDocUpload(Const.ProviderStatus.IS_UPLOADED);
                                goToMainDrawerActivity();
                            } else {
                                setDocData();

                                documentAdaptor.notifyDataSetChanged();


                            }
                            Utils.hideCustomProgressDialog();
                        } else {
                            Utils.hideCustomProgressDialog();
                        }
                    }
                }

                @Override
                public void onFailure(Call<DocumentResponse> call, Throwable t) {
                    AppLog.handleThrowable(DocumentActivity_Driver.class.getSimpleName(), t);
                }
            });


        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.DOCUMENT_ACTIVITY, e);
        }

    }

    private void uploadDocument(String expireDate, String uniqueCode, String documentId, final int position,
                                String document_ext,String document_ext_value,String document_type,String doc_type_value,
                                String str_doc_data,String str_doc_file,Uri picUrii) {


        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                        .msg_waiting_for_upload_document), false,
                null);
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put(Const.Params.DOCUMENT_ID, ApiClient.makeTextRequestBody(documentId));
        map.put(Const.Params.TOKEN, ApiClient.makeTextRequestBody(preferenceHelper
                .getSessionToken()));

        map.put(document_ext, ApiClient.makeTextRequestBody(document_ext_value));
        map.put(document_type, ApiClient.makeTextRequestBody(doc_type_value));

        map.put(Const.Params.TOKEN, ApiClient.makeTextRequestBody(preferenceHelper
                .getSessionToken()));
        map.put(Const.Params.PROVIDER_ID, ApiClient.makeTextRequestBody(preferenceHelper
                .getProviderId()));
        map.put(Const.Params.UNIQUE_CODE, ApiClient.makeTextRequestBody(uniqueCode));
        if (!TextUtils.isEmpty(expireDate)) {
            map.put(Const.Params.EXPIRED_DATE, ApiClient.makeTextRequestBody(expireDate));
        }
        System.out.println("Document File is###"+str_doc_file);
        System.out.println("Document File is @@@###"+picUrii);

        Call<Document> userDataResponseCall;
        if (!TextUtils.isEmpty(str_doc_file)) {
            userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                    (ApiInterface.class).updateDocument(ApiClient.makeMultipartRequestBody
                            (this, (TextUtils.isEmpty
                                    (str_doc_file) ?
                                    ImageHelper
                                            .getFromMediaUriPfd(this, getContentResolver(),
                                                    picUrii).getPath() :
                                    str_doc_file), str_doc_data)
                    , map);
        } else {
            userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                    (ApiInterface.class).updateDocument(null, map);
        }
        userDataResponseCall.enqueue(new Callback<Document>() {
            @Override
            public void onResponse(Call<Document> call,
                                   Response<Document> response) {
                if (parseContent.isSuccessful(response)) {

                    try {
                        str_document_file1 = "";
                        picUri2=null;

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    try {
                        str_document_file2 = "";
                        picUri3=null;

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    try {
                        str_document_file3 = "";
                        picUri4=null;

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    try {
                        str_document_file4 = "";
                        picUri5=null;

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    try {
                        str_document_file5 = "";
                        picUri6=null;

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    try {
                        str_document_file6 = "";
                        picUri7=null;

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    try {
                        str_document_file7 = "";
                        picUri8=null;

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    try {
                        str_document_file8 = "";
                        picUri9=null;

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    try {
                        str_document_file9 = "";
                        picUri10=null;

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    try {
                        str_document_file10 = "";
                        picUri11=null;

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                    //getProviderDocument();

                    finish();
                    Intent ii=new Intent(DocumentActivity_Driver.this,DocumentActivity_Driver.class);
                    startActivity(ii);

                /*    if (response.body().isSuccess()) {
                        Document document = docList.get(position);
                        document.setDocumentPicture(response.body().getDocumentPicture());
                        document.setExpiredDate(response.body().getExpiredDate());
                        document.setUniqueCode(response.body().getUniqueCode());
                        document.setIsUploaded(response.body().getIsUploaded());
                        documentAdaptor.notifyDataSetChanged();
                        *//*preferenceHelper.putAllDocUpload(response.body()
                                .getIsDocumentUploaded());*//*
                        Utils.hideCustomProgressDialog();
                    } else {
                        Utils.hideCustomProgressDialog();
                        Utils.showErrorToast(response.body().get(), DocumentActivity_Driver
                                .this);
                    }*/




                }

            }

            @Override
            public void onFailure(Call<Document> call, Throwable t) {
                AppLog.handleThrowable(DocumentActivity_Driver.class.getSimpleName(), t);

            }
        });

    }

    @Override
    public void onBackPressed() {
        if (isClickedOnDrawer) {
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
          //  openLogoutDialog();
            super.onBackPressed();
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }

    }

    protected void openLogoutDialog() {

        customDialogBigLabel = new CustomDialogBigLabel(this, getString(R.string.text_logout),
                getString(R.string.msg_are_you_sure), getString(R.string.text_yes), getString(R
                .string.text_no)) {
            @Override
            public void positiveButton() {
                customDialogBigLabel.dismiss();
                logOut();
            }

            @Override
            public void negativeButton() {
                customDialogBigLabel.dismiss();
            }
        };
        customDialogBigLabel.show();
    }


    private void openCameraPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogEnable(this, getResources().getString(R.string
                .msg_reason_for_camera_permission), getString(R.string.text_i_am_sure), getString
                (R.string.text_re_try)) {
            @Override
            public void doWithEnable() {
                ActivityCompat.requestPermissions(DocumentActivity_Driver.this, new String[]{Manifest
                        .permission
                        .CAMERA, Manifest
                        .permission
                        .READ_EXTERNAL_STORAGE}, Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
                closedPermissionDialog();
            }

            @Override
            public void doWithDisable() {
                closedPermissionDialog();
            }
        };
        customDialogEnable.show();
    }

    private void closedPermissionDialog() {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            customDialogEnable.dismiss();
            customDialogEnable = null;

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Const.PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE:
                    goWithCameraAndStoragePermission(grantResults);
                    break;
                default:
                    // do with default
                    break;
            }
        }
    }

    private void goWithCameraAndStoragePermission(int[] grantResults) {
        if (grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            //Do the stuff that requires permission...
            openPhotoDialog();
        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest
                    .permission.CAMERA)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
                openPermissionNotifyDialog(Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
            }
        } else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest
                    .permission.READ_EXTERNAL_STORAGE)) {
                openCameraPermissionDialog();
            } else {
                closedPermissionDialog();
                openPermissionNotifyDialog(Const
                        .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE);
            }
        }
    }



    private void initDocumentRcv() {

        docList = new ArrayList<>();
        rcvDocumentList = (RecyclerView) findViewById(R.id.rcvDocumentList);
        rcvDocumentList.setLayoutManager(new LinearLayoutManager(this));
        documentAdaptor = new DocumentAdaptor(this, docList);
        rcvDocumentList.setAdapter(documentAdaptor);
        rcvDocumentList.addOnItemTouchListener(new RecyclerTouchListener(this, rcvDocumentList, new
                ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        docListPosition = position;
                        openDocumentUploadDialog(position);

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            closedEnableDialogInternet();
        } else {
            openInternetDialog();
        }
    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {
        if (isConnected) {
            closedEnableDialogGps();
        } else {
            openGpsDialog();

        }
    }



    @Override
    public void onAdminApproved() {
        goWithAdminApproved();
    }

    @Override
    public void onAdminDeclined() {
        goWithAdminDecline();
    }

    private void openDocumentUploadDialog(final int position) {


        if (documentDialog != null && documentDialog.isShowing()) {
            return;
        }

        final Document document = docList.get(position);
        expireDate = "";
        documentDialog = new Dialog(this);
        documentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        documentDialog.setContentView(R.layout.dialog_document_upload);
        ivDocumentImage = (ImageView) documentDialog.findViewById(R.id.ivDocumentImage);
        etDocumentNumber = (MyFontEdittextView) documentDialog.findViewById(R.id.etDocumentNumber);
        etExpireDate = (MyFontEdittextView) documentDialog.findViewById(R.id.etExpireDate);
        tilDocumentNumber = (TextInputLayout) documentDialog.findViewById(R.id.tilDocumentNumber);
        tvDocumentTitle = (MyAppTitleFontTextView) documentDialog.findViewById(R.id
                .tvDocumentTitle);
        tvDocumentTitle.setText(document.getName());
//        Glide.with(this).load(ApiClient.Base_URL + document.getDocumentPicture()).dontAnimate().fallback(R
//                .drawable.ellipse).override(200, 200).placeholder(R
//                .drawable.ellipse)
//                .into(ivDocumentImage);
        PicassoTrustAll.getInstance(this)
                .load(ApiClient.Base_URL + document.getDocumentPicture())
                .error(R.drawable.ellipse)
                .resize(200,200)
                .into(ivDocumentImage);


        if (document.isIsExpiredDate()) {
            etExpireDate.setVisibility(View.VISIBLE);


            try {
                etExpireDate.setText(ParseContent.getInstance().dateFormat.format(ParseContent
                        .getInstance()
                        .webFormatWithLocalTimeZone
                        .parse(document.getExpiredDate())));
                Date date = parseContent.dateFormat.parse(etExpireDate.getText().toString());
                date.setTime(date.getTime() + 86399000);// 86399000 add 24 hour
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Const
                        .DATE_TIME_FORMAT_WEB, Locale.US);
                expireDate = simpleDateFormat.format(date);
            } catch (ParseException e) {
                AppLog.handleException(TAG, e);
            }
        }
        if (document.isIsUniqueCode()) {
            tilDocumentNumber.setVisibility(View.VISIBLE);
            etDocumentNumber.setText(document.getUniqueCode());
        }


        documentDialog.findViewById(R.id.btnDialogDocumentSubmit).setOnClickListener(new View
                .OnClickListener() {


            @Override
            public void onClick(View view) {
                Date date = null;
                if (!TextUtils.isEmpty(expireDate)) {
                    try {
                        date = parseContent.dateFormat.parse(expireDate);
                    } catch (ParseException e) {
                        AppLog.handleException(TAG, e);
                    }
                }
                if ((TextUtils.isEmpty(expireDate) && document
                        .isIsExpiredDate()) || isExpiredDate(date)) {
                    Utils.showToast(getResources().getString(R.string
                            .msg_plz_enter_document_expire_date), DocumentActivity_Driver.this);

                } else if (TextUtils.isEmpty(etDocumentNumber.getText().toString().trim()) &&
                        document.isIsUniqueCode()) {
                    Utils.showToast(getResources().getString(R.string
                            .msg_plz_enter_document_unique_code), DocumentActivity_Driver.this);

                } else if (TextUtils.isEmpty(uploadImageFilePath) && TextUtils.isEmpty(document
                        .getDocumentPicture())) {
                    Utils.showToast(getResources().getString(R.string
                            .msg_plz_select_document_image), DocumentActivity_Driver.this);
                } else {
                    documentDialog.dismiss();
               /*     uploadDocument(expireDate, etDocumentNumber
                            .getText().toString(), document.getId(), position);*/
                    expireDate = "";
                }
            }
        });

        documentDialog.findViewById(R.id.btnDialogDocumentCancel).setOnClickListener(new View
                .OnClickListener() {


            @Override
            public void onClick(View view) {
                documentDialog.dismiss();
            }
        });

        ivDocumentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPhotoDialog();
            }
        });
        etExpireDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePickerDialog();
            }
        });
        WindowManager.LayoutParams params = documentDialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        documentDialog.getWindow().setAttributes(params);
        documentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        documentDialog.setCancelable(false);
        documentDialog.show();

    }

    private void openDatePickerDialog() {


        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 0);
        final int currentYear = calendar.get(Calendar.YEAR);
        final int currentMonth = calendar.get(Calendar.MONTH);
        final int currentDate = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog
                .OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            }
        };
        final DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                onDateSetListener, currentYear,
                currentMonth,
                currentDate);
        datePickerDialog.setButton(DialogInterface.BUTTON_POSITIVE, this
                .getResources()
                .getString(R.string.text_select), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (documentDialog != null && datePickerDialog.isShowing()) {
                    calendar.set(Calendar.YEAR, datePickerDialog.getDatePicker().getYear());
                    calendar.set(Calendar.MONTH, datePickerDialog.getDatePicker().getMonth());
                    calendar.set(Calendar.DAY_OF_MONTH, datePickerDialog.getDatePicker()
                            .getDayOfMonth());
                    etExpireDate.setText(parseContent.dateFormat.format(calendar.getTime()));
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Const
                            .DATE_TIME_FORMAT_WEB, Locale.US);
                    expireDate = simpleDateFormat.format(calendar.getTime());
                }
            }
        });
        long now = System.currentTimeMillis();
        datePickerDialog.getDatePicker().setMinDate(now + 86400000);
        datePickerDialog.show();

    }

    private void openPermissionNotifyDialog(final int code) {
        if (customDialogEnable != null && customDialogEnable.isShowing()) {
            return;
        }
        customDialogEnable = new CustomDialogEnable(this, getResources()
                .getString(R.string
                        .msg_permission_notification), getResources()
                .getString(R
                        .string.text_exit_caps), getResources().getString(R
                .string
                .text_settings)) {
            @Override
            public void doWithEnable() {
                closedPermissionDialog();
                startActivityForResult(getIntentForPermission(), code);

            }

            @Override
            public void doWithDisable() {
                closedPermissionDialog();
                finishAffinity();
            }
        };
        customDialogEnable.show();

    }

    private boolean isExpiredDate(Date date) {
        return date != null && date.before(new Date());


    }
    public void hideKeyPad() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService
                    (INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

}
