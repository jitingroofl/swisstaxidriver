package com.swissexpressdz.driver.components;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.swissexpressdz.driver.R;


/**
 * Created by elluminati on 08-08-2016.
 */
public abstract class CustomPhotoDialog2 extends Dialog implements View.OnClickListener {

    private ImageView ivCamera, ivGallery;

    public CustomPhotoDialog2(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_choose_picture2);
        ivCamera = (ImageView) findViewById(R.id.ivCamera);
        ivGallery = (ImageView) findViewById(R.id.ivGallery);
        ivGallery.setOnClickListener(this);
        ivCamera.setOnClickListener(this);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setDimAmount(0);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivCamera:
                clickedOnCamera();
                break;
            case R.id.ivGallery:
                clickedOnGallery();
                break;

            default:
                break;
        }
    }

    public abstract void clickedOnCamera();

    public abstract void clickedOnGallery();
}
