package com.swissexpressdz.driver;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.swissexpressdz.driver.adapter.DocumentAdaptor;
import com.swissexpressdz.driver.components.CustomPhotoDialog2;
import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.models.datamodels.DocumentModel;
import com.swissexpressdz.driver.models.responsemodels.DocumentNew;
import com.swissexpressdz.driver.models.responsemodels.DocumentNewResponse;
import com.swissexpressdz.driver.models.responsemodels.DocumentSeparateModel;
import com.swissexpressdz.driver.models.responsemodels.NewDocumentResponse;
import com.swissexpressdz.driver.models.responsemodels.ProviderDataResponse;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextViewBold;
import com.swissexpressdz.driver.utils.CustomTextViewRegular;
import com.swissexpressdz.driver.utils.CustomTextView_MontserratBold;
import com.swissexpressdz.driver.utils.ImageHelper;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DocumentNewActivity extends BaseAppCompatActivity {
            List<DocumentSeparateModel> docList = new ArrayList<>();
            RecyclerView rcvDocumentList;
            DocumentInsideAdaptor documentInsideAdaptor;
    int list_positin;
CustomPhotoDialog2 customPhotoDialog2;
ArrayList<DocumentModel> documentModels = new ArrayList<>();
    private String msg;

CustomTextView_MontserratBold btnSubmitDocument;

@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adddocument_driver);
    //    initToolBar();
       // setTitleOnToolbar(getResources().getString(R.string.txt_add_document));
        rcvDocumentList = (RecyclerView) findViewById(R.id.rcvDocumentList);
        btnSubmitDocument = (CustomTextView_MontserratBold) findViewById(R.id.btnSubmitDocument);
        rcvDocumentList.setLayoutManager(new LinearLayoutManager(DocumentNewActivity.this));

    btnSubmitDocument = (CustomTextView_MontserratBold) findViewById(R.id.btnSubmitDocument);
    btnSubmitDocument.setOnClickListener(this);
        getServicetypeDocuments();

    }

    private void getServicetypeDocuments() {
        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                        .msg_waiting_for_get_documents),
                false, null);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PROVIDER_ID, preferenceHelper.getProviderId());
            Call<NewDocumentResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .providerGetdocument(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<NewDocumentResponse>() {
                @Override
                public void onResponse(Call<NewDocumentResponse> call, Response<NewDocumentResponse>
                        response) {

                    if (parseContent.isSuccessful(response)) {
                        System.out.println("Response get is###"+response);
                        if (response.body().getSuccess()) {
                   //     docList.addAll(response.body().getDocuments());
                            List<NewDocumentResponse.Document> documentList = response.body().getDocuments();
                            for(int k= 0; k<documentList.size(); k++)
                            {
                                DocumentSeparateModel documentSeparateModel = new DocumentSeparateModel();
                                documentSeparateModel.setDocument_name(documentList.get(k).getName());
                                documentSeparateModel.setDocument_type("");
                                documentSeparateModel.setStr_document_ext1("");
                                documentSeparateModel.setStr_document_file1("");
                                documentSeparateModel.setPicuri(null);
                                docList.add(documentSeparateModel);

                            }

                            documentInsideAdaptor = new DocumentInsideAdaptor(DocumentNewActivity.this, docList);
                            rcvDocumentList.setAdapter(documentInsideAdaptor);
                            rcvDocumentList.setNestedScrollingEnabled(true);


                            Utils.hideCustomProgressDialog();
                        } else {
                            Utils.hideCustomProgressDialog();
                        }
                    }
                }

                @Override
                public void onFailure(Call<NewDocumentResponse> call, Throwable t) {
                    AppLog.handleThrowable(DocumentActivity_Driver.class.getSimpleName(), t);
                }
            });


        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.DOCUMENT_ACTIVITY, e);
        }

    }


    public  void addProviderDocuments()
    {

    }

    protected void openPhotoDialog_fromlist2() {

        if (ContextCompat.checkSelfPermission(DocumentNewActivity.this, Manifest.permission
                .CAMERA) !=
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission
                (DocumentNewActivity.this, Manifest.permission
                        .READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DocumentNewActivity.this, new String[]{Manifest
                    .permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, Const
                    .PERMISSION_FOR_CAMERA_AND_EXTERNAL_STORAGE2);
        }
        else {

            customPhotoDialog2 = new CustomPhotoDialog2(DocumentNewActivity.this) {



                @Override
                public void clickedOnCamera() {

                }

                @Override
                public void clickedOnGallery() {
                    customPhotoDialog2.dismiss();
                    choosePhotoFromGallery2();
                }
            };
            customPhotoDialog2.show();
        }

    }

    private void choosePhotoFromGallery2() {


        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Const.ServiceCode.CHOOSE_PHOTO2);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case Const.ServiceCode.CHOOSE_PHOTO2:
                try {
                    Uri selectedImage = data.getData();
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                   String  str_document_ext1 = mime.getExtensionFromMimeType(cR.getType(selectedImage));
                      String  str_document_file1 = ImageHelper.getFromMediaUriPfd(this, getContentResolver(), selectedImage).getPath();
                       Uri picUri2 = selectedImage;
                        System.out.println("Selected Image Extention second###" + selectedImage.toString());
                        System.out.println("Selected Image Extention second###" + str_document_ext1);

                        if(docList !=null)
                        {
                            if(docList.size() > 0)
                            {
                                    for(int i=0; i<docList.size(); i++)
                                    {
                                        if(docList.get(i).getDocument_name().equalsIgnoreCase(docList.get(list_positin).getDocument_name()))
                                        {


                                            DocumentSeparateModel documentSeparateModel = new DocumentSeparateModel();
                                            documentSeparateModel.setDocument_name(docList.get(i).getDocument_name());
                                            documentSeparateModel.setDocument_type(str_document_file1);
                                            documentSeparateModel.setStr_document_ext1(str_document_ext1);
                                            documentSeparateModel.setStr_document_file1(str_document_file1);
                                            documentSeparateModel.setPicuri(picUri2);
                                            docList.set(i,documentSeparateModel);
                                            documentInsideAdaptor.notifyItemChanged(list_positin);

                                        }

                                        else
                                        {

                                        }
                                    }
                            }
                        }

                        else
                        {

                        }



                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }




    @Override
    protected boolean isValidate() {

        msg = null;
        try {

            if(docList!=null && docList.size()>0)
            {
                for(int i= 0; i<docList.size(); i++)
                {
                    if(docList.get(i).getStr_document_ext1().equalsIgnoreCase(""))
                    {
                        msg = getString(R.string.plz_slct_all_documents);
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                    }

                }

            }


        }
        catch (Exception e)
        {
            msg = getString(R.string.plz_slct_all_documents);
            Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
        }



        return TextUtils.isEmpty(msg);
    }

    @Override
    public void goWithBackArrow() {

    }

    @Override
    public void onClick(View view) {
     if(view == btnSubmitDocument)
    {
        if (isValidate()) {

            addDocument();
        }
     }
    }

    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }

    private void addDocument() {




        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register valid");

        HashMap<String, RequestBody> map = new HashMap<>();


        map.put(Const.Params.PROVIDER_ID, ApiClient.makeTextRequestBody(preferenceHelper.getProviderId()));
        map.put(Const.Params.TOTAL_DOCUMENT,ApiClient.makeTextRequestBody(docList.size()));

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[docList.size()];

        for(int j =0; j<docList.size(); j++)
      {
          if(docList.size() == 1)
          {
              map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(docList.get(0).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(docList.get(0).getDocument_name()));

              File file1 = new File(docList.get(0).getDocument_type());
              RequestBody surveyBody1 = RequestBody.create(MediaType.parse("image/*"), file1);
              surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA, file1.getName(), surveyBody1);
          }
          else if(docList.size() == 2)
          {
              map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(docList.get(0).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(docList.get(0).getDocument_name()));

              File file1 = new File(docList.get(0).getDocument_type());
              RequestBody surveyBody1 = RequestBody.create(MediaType.parse("image/*"), file1);
              surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA, file1.getName(), surveyBody1);


              map.put(Const.Params.DOCUMENT_EXT2, ApiClient.makeTextRequestBody(docList.get(1).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE2, ApiClient.makeTextRequestBody(docList.get(1).getDocument_name()));

              File file2 = new File(docList.get(1).getDocument_type());
              RequestBody surveyBody2 = RequestBody.create(MediaType.parse("image/*"), file2);
              surveyImagesParts[1] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA2, file2.getName(), surveyBody2);
          }

          else if(docList.size() == 3)
          {
              map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(docList.get(0).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(docList.get(0).getDocument_name()));

              File file1 = new File(docList.get(0).getDocument_type());
              RequestBody surveyBody1 = RequestBody.create(MediaType.parse("image/*"), file1);
              surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA, file1.getName(), surveyBody1);


              map.put(Const.Params.DOCUMENT_EXT2, ApiClient.makeTextRequestBody(docList.get(1).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE2, ApiClient.makeTextRequestBody(docList.get(1).getDocument_name()));

              File file2 = new File(docList.get(1).getDocument_type());
              RequestBody surveyBody2 = RequestBody.create(MediaType.parse("image/*"), file2);
              surveyImagesParts[1] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA2, file2.getName(), surveyBody2);


              map.put(Const.Params.DOCUMENT_EXT3, ApiClient.makeTextRequestBody(docList.get(2).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE3, ApiClient.makeTextRequestBody(docList.get(2).getDocument_name()));

              File file3 = new File(docList.get(2).getDocument_type());
              RequestBody surveyBody3 = RequestBody.create(MediaType.parse("image/*"), file3);
              surveyImagesParts[2] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA3, file3.getName(), surveyBody3);

          }

          else if(docList.size() == 4)
          {
              map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(docList.get(0).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(docList.get(0).getDocument_name()));

              File file1 = new File(docList.get(0).getDocument_type());
              RequestBody surveyBody1 = RequestBody.create(MediaType.parse("image/*"), file1);
              surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA, file1.getName(), surveyBody1);


              map.put(Const.Params.DOCUMENT_EXT2, ApiClient.makeTextRequestBody(docList.get(1).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE2, ApiClient.makeTextRequestBody(docList.get(1).getDocument_name()));

              File file2 = new File(docList.get(1).getDocument_type());
              RequestBody surveyBody2 = RequestBody.create(MediaType.parse("image/*"), file2);
              surveyImagesParts[1] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA2, file2.getName(), surveyBody2);


              map.put(Const.Params.DOCUMENT_EXT3, ApiClient.makeTextRequestBody(docList.get(2).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE3, ApiClient.makeTextRequestBody(docList.get(2).getDocument_name()));

              File file3 = new File(docList.get(2).getDocument_type());
              RequestBody surveyBody3 = RequestBody.create(MediaType.parse("image/*"), file3);
              surveyImagesParts[2] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA3, file3.getName(), surveyBody3);

              map.put(Const.Params.DOCUMENT_EXT4, ApiClient.makeTextRequestBody(docList.get(3).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE4, ApiClient.makeTextRequestBody(docList.get(3).getDocument_name()));

              File file4 = new File(docList.get(3).getDocument_type());
              RequestBody surveyBody4 = RequestBody.create(MediaType.parse("image/*"), file4);
              surveyImagesParts[2] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA4, file4.getName(), surveyBody4);



          }
          else if(docList.size() == 5)
          {
              map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(docList.get(0).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(docList.get(0).getDocument_name()));

              File file1 = new File(docList.get(0).getDocument_type());
              RequestBody surveyBody1 = RequestBody.create(MediaType.parse("image/*"), file1);
              surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA, file1.getName(), surveyBody1);


              map.put(Const.Params.DOCUMENT_EXT2, ApiClient.makeTextRequestBody(docList.get(1).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE2, ApiClient.makeTextRequestBody(docList.get(1).getDocument_name()));

              File file2 = new File(docList.get(1).getDocument_type());
              RequestBody surveyBody2 = RequestBody.create(MediaType.parse("image/*"), file2);
              surveyImagesParts[1] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA2, file2.getName(), surveyBody2);


              map.put(Const.Params.DOCUMENT_EXT3, ApiClient.makeTextRequestBody(docList.get(2).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE3, ApiClient.makeTextRequestBody(docList.get(2).getDocument_name()));

              File file3 = new File(docList.get(2).getDocument_type());
              RequestBody surveyBody3 = RequestBody.create(MediaType.parse("image/*"), file3);
              surveyImagesParts[2] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA3, file3.getName(), surveyBody3);

              map.put(Const.Params.DOCUMENT_EXT4, ApiClient.makeTextRequestBody(docList.get(3).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE4, ApiClient.makeTextRequestBody(docList.get(3).getDocument_name()));

              File file4 = new File(docList.get(3).getDocument_type());
              RequestBody surveyBody4 = RequestBody.create(MediaType.parse("image/*"), file4);
              surveyImagesParts[3] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA4, file4.getName(), surveyBody4);

              map.put(Const.Params.DOCUMENT_EXT5, ApiClient.makeTextRequestBody(docList.get(4).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE5, ApiClient.makeTextRequestBody(docList.get(4).getDocument_name()));

              File file5 = new File(docList.get(4).getDocument_type());
              RequestBody surveyBody5 = RequestBody.create(MediaType.parse("image/*"), file5);
              surveyImagesParts[4] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA4, file5.getName(), surveyBody5);

          }

          else if(docList.size() == 6)
          {
              map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(docList.get(0).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(docList.get(0).getDocument_name()));

              File file1 = new File(docList.get(0).getDocument_type());
              RequestBody surveyBody1 = RequestBody.create(MediaType.parse("image/*"), file1);
              surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA, file1.getName(), surveyBody1);


              map.put(Const.Params.DOCUMENT_EXT2, ApiClient.makeTextRequestBody(docList.get(1).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE2, ApiClient.makeTextRequestBody(docList.get(1).getDocument_name()));

              File file2 = new File(docList.get(1).getDocument_type());
              RequestBody surveyBody2 = RequestBody.create(MediaType.parse("image/*"), file2);
              surveyImagesParts[1] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA2, file2.getName(), surveyBody2);


              map.put(Const.Params.DOCUMENT_EXT3, ApiClient.makeTextRequestBody(docList.get(2).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE3, ApiClient.makeTextRequestBody(docList.get(2).getDocument_name()));

              File file3 = new File(docList.get(2).getDocument_type());
              RequestBody surveyBody3 = RequestBody.create(MediaType.parse("image/*"), file3);
              surveyImagesParts[2] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA3, file3.getName(), surveyBody3);

              map.put(Const.Params.DOCUMENT_EXT4, ApiClient.makeTextRequestBody(docList.get(3).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE4, ApiClient.makeTextRequestBody(docList.get(3).getDocument_name()));

              File file4 = new File(docList.get(3).getDocument_type());
              RequestBody surveyBody4 = RequestBody.create(MediaType.parse("image/*"), file4);
              surveyImagesParts[3] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA4, file4.getName(), surveyBody4);


              map.put(Const.Params.DOCUMENT_EXT5, ApiClient.makeTextRequestBody(docList.get(4).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE5, ApiClient.makeTextRequestBody(docList.get(4).getDocument_name()));

              File file5 = new File(docList.get(4).getDocument_type());
              RequestBody surveyBody5 = RequestBody.create(MediaType.parse("image/*"), file5);
              surveyImagesParts[4] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA5, file5.getName(), surveyBody5);

              map.put(Const.Params.DOCUMENT_EXT6, ApiClient.makeTextRequestBody(docList.get(5).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE6, ApiClient.makeTextRequestBody(docList.get(5).getDocument_name()));

              File file6 = new File(docList.get(5).getDocument_type());
              RequestBody surveyBody6 = RequestBody.create(MediaType.parse("image/*"), file6);
              surveyImagesParts[5] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA6, file6.getName(), surveyBody6);
          }
          else if(docList.size() == 7)
          {
              map.put(Const.Params.DOCUMENT_EXT, ApiClient.makeTextRequestBody(docList.get(0).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE, ApiClient.makeTextRequestBody(docList.get(0).getDocument_name()));

              File file1 = new File(docList.get(0).getDocument_type());
              RequestBody surveyBody1 = RequestBody.create(MediaType.parse("image/*"), file1);
              surveyImagesParts[0] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA, file1.getName(), surveyBody1);


              map.put(Const.Params.DOCUMENT_EXT2, ApiClient.makeTextRequestBody(docList.get(1).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE2, ApiClient.makeTextRequestBody(docList.get(1).getDocument_name()));

              File file2 = new File(docList.get(1).getDocument_type());
              RequestBody surveyBody2 = RequestBody.create(MediaType.parse("image/*"), file2);
              surveyImagesParts[1] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA2, file2.getName(), surveyBody2);


              map.put(Const.Params.DOCUMENT_EXT3, ApiClient.makeTextRequestBody(docList.get(2).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE3, ApiClient.makeTextRequestBody(docList.get(2).getDocument_name()));

              File file3 = new File(docList.get(2).getDocument_type());
              RequestBody surveyBody3 = RequestBody.create(MediaType.parse("image/*"), file3);
              surveyImagesParts[2] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA3, file3.getName(), surveyBody3);

              map.put(Const.Params.DOCUMENT_EXT4, ApiClient.makeTextRequestBody(docList.get(3).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE4, ApiClient.makeTextRequestBody(docList.get(3).getDocument_name()));

              File file4 = new File(docList.get(3).getDocument_type());
              RequestBody surveyBody4 = RequestBody.create(MediaType.parse("image/*"), file4);
              surveyImagesParts[3] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA4, file4.getName(), surveyBody4);


              map.put(Const.Params.DOCUMENT_EXT5, ApiClient.makeTextRequestBody(docList.get(4).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE5, ApiClient.makeTextRequestBody(docList.get(4).getDocument_name()));

              File file5 = new File(docList.get(4).getDocument_type());
              RequestBody surveyBody5 = RequestBody.create(MediaType.parse("image/*"), file5);
              surveyImagesParts[4] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA5, file5.getName(), surveyBody5);

              map.put(Const.Params.DOCUMENT_EXT6, ApiClient.makeTextRequestBody(docList.get(5).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE6, ApiClient.makeTextRequestBody(docList.get(5).getDocument_name()));

              File file6 = new File(docList.get(5).getDocument_type());
              RequestBody surveyBody6 = RequestBody.create(MediaType.parse("image/*"), file6);
              surveyImagesParts[5] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA6, file6.getName(), surveyBody6);

              map.put(Const.Params.DOCUMENT_EXT7, ApiClient.makeTextRequestBody(docList.get(6).getStr_document_ext1()));
              map.put(Const.Params.DOCUMENT_TYPE7, ApiClient.makeTextRequestBody(docList.get(6).getDocument_name()));

              File file7 = new File(docList.get(6).getDocument_type());
              RequestBody surveyBody7 = RequestBody.create(MediaType.parse("image/*"), file7);
              surveyImagesParts[6] = MultipartBody.Part.createFormData(Const.Params.DOCUMENT_DATA7, file7.getName(), surveyBody7);
          }


      }




        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_registering), false, null);
               Call<DocumentNewResponse> userDataResponseCall;
        userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                (ApiInterface.class).provider_add_document(
                surveyImagesParts
                , map);

        System.out.println("Map is###"+map.toString());


        userDataResponseCall.enqueue(new Callback<DocumentNewResponse>() {
            @Override
            public void onResponse(Call<DocumentNewResponse> call,
                                   Response<DocumentNewResponse> response) {
                if (parseContent.isSuccessful(response)) {
                    if (parseContent.saveProviderDatafromDocument(response.body(), true)) {

                        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register Success");

                        CurrentTrip.getInstance().clear();
                        Utils.hideCustomProgressDialog();
                        //moveWithUserSpecificPreference2();
                        Intent ii = new Intent(DocumentNewActivity.this,MainDrawerActivity.class);
                        startActivity(ii);
                                


                    } else {
                        Utils.hideCustomProgressDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<DocumentNewResponse> call, Throwable t) {
                AppLog.handleThrowable(RegisterActivity_Driver.class.getSimpleName(), t);
            }
        });


    }
    public class DocumentInsideAdaptor extends RecyclerView.Adapter<DocumentInsideAdaptor.DocumentInsideViewHolder> {

        private List<DocumentSeparateModel> docList;
        private Context context;

        public DocumentInsideAdaptor(Context context, List<DocumentSeparateModel> docList) {
            this.docList = docList;
            this.context = context;
        }


        @Override
        public DocumentInsideViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .edit_document_list, parent, false);
            return new DocumentInsideViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final DocumentInsideViewHolder holder, final int position) {

                holder.tvselect_document_type.setText(docList.get(position).getDocument_name());
                try {
                    holder.tvselect_document.setText(docList.get(position).getDocument_type());
                }
                catch (Exception e)
                {

                }
                holder.tvselect_document.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        list_positin = position;
                        openPhotoDialog_fromlist2();

                    }
                });
            holder.llselect_document.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    list_positin = position;
                    openPhotoDialog_fromlist2();

                }
            });




        }



        @Override
        public int getItemCount() {
            return docList.size();
        }


        class DocumentInsideViewHolder extends RecyclerView.ViewHolder {
            CustomTextViewBold tvselect_document_type;
            MyFontEdittextView tvselect_document;
CardView crd_view;
RelativeLayout llselect_document;

            public DocumentInsideViewHolder(View itemView) {
                super(itemView);
                tvselect_document_type = (CustomTextViewBold) itemView.findViewById(R.id.tvselect_document_type);
                llselect_document = (RelativeLayout) itemView.findViewById(R.id.llselect_document);
                tvselect_document = (MyFontEdittextView) itemView.findViewById(R.id.tvselect_document);
                crd_view = (CardView)itemView.findViewById(R.id.crd_view);



            }


        }
    }
}
