package com.swissexpressdz.driver;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.forminimummaximumvalue.InputFilterMinMax;
import com.swissexpressdz.driver.models.responsemodels.AddVTCResponse;
import com.swissexpressdz.driver.models.responsemodels.AddVehicleResponse;
import com.swissexpressdz.driver.models.responsemodels.EditVTCResponse;
import com.swissexpressdz.driver.models.responsemodels.GetVtcResponse;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextView_MontserratBold;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVTCActivity extends BaseAppCompatActivity {
    private String msg;
CustomTextView_MontserratBold btnaddvtc;
MyFontEdittextView etdistanceBasePrice,etBaseprice,etPriceperdistance,etWaitingtime,etWaitingtime_price,
        etHourly_price,etCancellation_fees,etProvider_profit,etcarrent_hourlyprice,etcarrent_dayprice,etcarrent_depositprice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addvtc);
        initToolBar();
        setTitleOnToolbar(getResources().getString(R.string.text_add_vehicle));
        //setTitleOnToolbar(getResources().getString(R.string.text_add_vehicle));

        etdistanceBasePrice = (MyFontEdittextView)findViewById(R.id.etdistanceBasePrice);
        etBaseprice = (MyFontEdittextView)findViewById(R.id.etBaseprice);
        etPriceperdistance = (MyFontEdittextView)findViewById(R.id.etPriceperdistance);
        etWaitingtime = (MyFontEdittextView)findViewById(R.id.etWaitingtime);
        etWaitingtime_price = (MyFontEdittextView)findViewById(R.id.etWaitingtime_price);
        etHourly_price = (MyFontEdittextView)findViewById(R.id.etHourly_price);
        etCancellation_fees = (MyFontEdittextView)findViewById(R.id.etCancellation_fees);
        etProvider_profit = (MyFontEdittextView)findViewById(R.id.etProvider_profit);
        etcarrent_hourlyprice = (MyFontEdittextView)findViewById(R.id.etcarrent_hourlyprice);
        etcarrent_dayprice = (MyFontEdittextView)findViewById(R.id.etcarrent_dayprice);
        etcarrent_depositprice = (MyFontEdittextView)findViewById(R.id.etcarrent_depositprice);

        btnaddvtc = (CustomTextView_MontserratBold) findViewById(R.id.btnaddvtc);
        btnaddvtc.setOnClickListener(this);
        getVTCType();


    }
    @Override
    protected void initToolBar() {
        super.initToolBar();

    }


    private void getVTCType() {
        Utils.showCustomProgressDialog(AddVTCActivity.this, getResources().getString(R.string
                .msg_waiting_for_getting_credit_cards), false, null);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.TYPE_ID, preferenceHelper.getTypeid());



            Call<EditVTCResponse> call = ApiClient.getClient(AddVTCActivity.this)
                    .create(ApiInterface.class).provider_get_vtc_city_type(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<EditVTCResponse>() {
                @Override
                public void onResponse(Call<EditVTCResponse> call, Response<EditVTCResponse> response) {
                    if (parseContent.isSuccessful(response)) {

                        if (response.body().getSuccess()) {
                            System.out.println("Provider CityType  "+response.body().getVtcType().getDeposit());

                                etcarrent_depositprice.setText(response.body().getVtcType().getDeposit()+"");




               //  etBaseprice.setFilters(new InputFilter[]{ new InputFilterMinMax("10", "15")});
                       //     etPriceperdistance.setFilters(new InputFilter[]{ new InputFilterMinMax(response.body().getVtcType().getPricePerUnitDistanceMin(), response.body().getVtcType().getPricePerUnitDistanceMin())});


                                Utils.hideCustomProgressDialog();

                        }
                        else {
                            Utils.hideCustomProgressDialog();
                            Utils.showErrorToast(response.body().getErrorCode(), AddVTCActivity.this);
                        }

                    }

                }

                @Override
                public void onFailure(Call<EditVTCResponse> call, Throwable t) {
                    AppLog.handleThrowable(GetVTCActivity.class.getSimpleName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.VIEW_AND_ADD_PAYMENT_ACTIVITY, e);
        }
    }
    @Override
    protected boolean isValidate() {
        msg = null;

        if (TextUtils.isEmpty(etdistanceBasePrice.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etdistanceBasePrice.requestFocus();
            etdistanceBasePrice.setError(msg);
        } else if (TextUtils.isEmpty(etBaseprice.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etBaseprice.requestFocus();
            etBaseprice.setError(msg);
        }
        else if (TextUtils.isEmpty(etPriceperdistance.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etPriceperdistance.requestFocus();
            etPriceperdistance.setError(msg);
        }
        else if (TextUtils.isEmpty(etWaitingtime.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etWaitingtime.requestFocus();
            etWaitingtime.setError(msg);
        }
        else if (TextUtils.isEmpty(etWaitingtime_price.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etWaitingtime_price.requestFocus();
            etWaitingtime_price.setError(msg);
        }


        else if (TextUtils.isEmpty(etHourly_price.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etHourly_price.requestFocus();
            etHourly_price.setError(msg);
        }
        else if (TextUtils.isEmpty(etCancellation_fees.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etCancellation_fees.requestFocus();
            etCancellation_fees.setError(msg);
        }
        else if (TextUtils.isEmpty(etProvider_profit.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etProvider_profit.requestFocus();
            etProvider_profit.setError(msg);
        }
        else if (TextUtils.isEmpty(etcarrent_hourlyprice.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etcarrent_hourlyprice.requestFocus();
            etcarrent_hourlyprice.setError(msg);
        }
        else if (TextUtils.isEmpty(etcarrent_dayprice.getText().toString().trim())) {
            msg = getString(R.string.this_field_requiredd);
            etcarrent_dayprice.requestFocus();
            etcarrent_dayprice.setError(msg);
        }

        return TextUtils.isEmpty(msg);
    }


    private void addVTC() {




        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register valid");

        HashMap<String, RequestBody> map = new HashMap<>();

        map.put(Const.Params.PROVIDER_ID, ApiClient.makeTextRequestBody(preferenceHelper.getProviderId()));
        map.put(Const.Params.COUNTRY_ID, ApiClient.makeTextRequestBody(preferenceHelper.getCountryId()));
        map.put(Const.Params.CITY_ID, ApiClient.makeTextRequestBody(preferenceHelper.getCityId()));
        map.put(Const.Params.TYPEID , ApiClient.makeTextRequestBody(preferenceHelper.getTypeid()));


        map.put(Const.Params.BASE_PRICE_DISTANCE, ApiClient.makeTextRequestBody(etdistanceBasePrice.getText().toString()));
        map.put(Const.Params.BASE_PRICE, ApiClient.makeTextRequestBody(etBaseprice.getText().toString()));
        map.put(Const.Params.PRICE_PER_UNIT_DISTANCE, ApiClient.makeTextRequestBody(etPriceperdistance.getText().toString()));

        map.put(Const.Params.WAITING_TIME_MINUTES, ApiClient.makeTextRequestBody(etWaitingtime.getText().toString()));
        map.put(Const.Params.WAITING_TIME_PRICE, ApiClient.makeTextRequestBody(etWaitingtime_price.getText().toString()));
        map.put(Const.Params.HOURLY_PRICE, ApiClient.makeTextRequestBody(etHourly_price.getText().toString()));
        map.put(Const.Params.CACELLATION_FEES, ApiClient.makeTextRequestBody(etCancellation_fees.getText().toString()));
        map.put(Const.Params.PROVIDER_PROFIT, ApiClient.makeTextRequestBody(etProvider_profit.getText().toString()));
        map.put(Const.Params.HOURLY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(etcarrent_hourlyprice.getText().toString()));
        map.put(Const.Params.DAY_PRICE_CAR_RENT, ApiClient.makeTextRequestBody(etcarrent_dayprice.getText().toString()));
        map.put(Const.Params.DEPOSIT, ApiClient.makeTextRequestBody(etcarrent_depositprice.getText().toString()));


        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_registering), false, null);

        ArrayList<String> arrayList_files=new ArrayList<>();

        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[arrayList_files.size()];



        //For First Document
        Call<AddVTCResponse> userDataResponseCall;
        userDataResponseCall = ApiClient.getClient(getApplicationContext()).create
                (ApiInterface.class).provider_add_city_type_vtc_value(
                surveyImagesParts
                , map);

        System.out.println("Map is###"+map.toString());


        userDataResponseCall.enqueue(new Callback<AddVTCResponse>() {
            @Override
            public void onResponse(Call<AddVTCResponse> call,
                                   Response<AddVTCResponse> response) {
                if (parseContent.isSuccessful(response)) {

                    if(response.body().getSuccess())
                    {
                        AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "Register Success");

                        CurrentTrip.getInstance().clear();
                        Utils.hideCustomProgressDialog();
                            onBackPressed();

                                }
                    else {
                        Utils.hideCustomProgressDialog();
                     //   Utils.showErrorToast(response.body().getErrorCode(), AddVTCActivity.this);
                        if(response.body().getErrorCode() == 998)
                        {
                         //   Utils.showErrorToast(getResources().getString(R.string.msg_alrdy_adde_vtc), AddVTCActivity.this);
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.msg_alrdy_adde_vtc),Toast.LENGTH_SHORT).show();
                        }
                    }





                }

                else
                {

                }
            }

            @Override
            public void onFailure(Call<AddVTCResponse> call, Throwable t) {
                AppLog.handleThrowable(RegisterActivity_Driver.class.getSimpleName(), t);
            }
        });


    }
    @Override
    public void goWithBackArrow() {
        onBackPressed();

    }
    @Override
    public void onBackPressed() {
      //  super.onBackPressed();
        Intent intent = new Intent(this, GetVTCActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onClick(View view) {
        if(view == btnaddvtc)
        {

            if(isValidate())
            {


                addVTC();
            }
        }

    }

    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }
}
