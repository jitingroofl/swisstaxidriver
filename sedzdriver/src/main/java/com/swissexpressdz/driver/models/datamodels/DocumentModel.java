package com.swissexpressdz.driver.models.datamodels;

import android.net.Uri;

public class DocumentModel {
        String str_document_ext1;
    String str_document_file1;
    String str_doc_name;



    String str_document_name;
    Uri picuri;

    public String getStr_doc_name() {
        return str_doc_name;
    }

    public void setStr_doc_name(String str_doc_name) {
        this.str_doc_name = str_doc_name;
    }




    public String getStr_document_ext1() {
        return str_document_ext1;
    }

    public void setStr_document_ext1(String str_document_ext1) {
        this.str_document_ext1 = str_document_ext1;
    }

    public String getStr_document_file1() {
        return str_document_file1;
    }

    public void setStr_document_file1(String str_document_file1) {
        this.str_document_file1 = str_document_file1;
    }

    public String getStr_document_name() {
        return str_document_name;
    }

    public void setStr_document_name(String str_document_name) {
        this.str_document_name = str_document_name;
    }

    public Uri getPicuri() {
        return picuri;
    }

    public void setPicuri(Uri picuri) {
        this.picuri = picuri;
    }



}
