package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Service3 {

    @SerializedName("public_transport")
    @Expose
    private String publicTransport;
    @SerializedName("bicycle")
    @Expose
    private String bicycle;
    @SerializedName("motorbike")
    @Expose
    private String motorbike;
    @SerializedName("car")
    @Expose
    private String car;
    @SerializedName("name")
    @Expose
    private String name;

    public String getPublicTransport() {
        return publicTransport;
    }

    public void setPublicTransport(String publicTransport) {
        this.publicTransport = publicTransport;
    }

    public String getBicycle() {
        return bicycle;
    }

    public void setBicycle(String bicycle) {
        this.bicycle = bicycle;
    }

    public String getMotorbike() {
        return motorbike;
    }

    public void setMotorbike(String motorbike) {
        this.motorbike = motorbike;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
