package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DepositListModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("deposit_list")
    private ArrayList<DepositList> depositList = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<DepositList> getDepositList() {
        return depositList;
    }

    public void setDepositList(ArrayList<DepositList> depositList) {
        this.depositList = depositList;
    }

    public class DepositList {

        @SerializedName("_id")
        private String id;
        @SerializedName("user_id")
        private String userId;
        @SerializedName("provider_id")
        private String providerId;
        @SerializedName("__v")
        private double v;
        @SerializedName("payment_status")
        private String paymentStatus;
        @SerializedName("is_damage")
        private Boolean isDamage;
        @SerializedName("apply_date")
        private String applyDate;
        @SerializedName("deposit_amount_pay_to_user")
        private double depositAmountPayToUser;
        @SerializedName("damage_amount")
        private double damageAmount;
        @SerializedName("deposit_amount")
        private double depositAmount;
        @SerializedName("user_name")
        private String userName;
        @SerializedName("provider_name")
        private String providerName;
        @SerializedName("vehicle_damage_image")
        private String vehicleDamageImage;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getProviderId() {
            return providerId;
        }

        public void setProviderId(String providerId) {
            this.providerId = providerId;
        }

        public double getV() {
            return v;
        }

        public void setV(double v) {
            this.v = v;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public Boolean getIsDamage() {
            return isDamage;
        }

        public void setIsDamage(Boolean isDamage) {
            this.isDamage = isDamage;
        }

        public String getApplyDate() {
            return applyDate;
        }

        public void setApplyDate(String applyDate) {
            this.applyDate = applyDate;
        }

        public double getDepositAmountPayToUser() {
            return depositAmountPayToUser;
        }

        public void setDepositAmountPayToUser(double depositAmountPayToUser) {
            this.depositAmountPayToUser = depositAmountPayToUser;
        }

        public double getDamageAmount() {
            return damageAmount;
        }

        public void setDamageAmount(double damageAmount) {
            this.damageAmount = damageAmount;
        }

        public double getDepositAmount() {
            return depositAmount;
        }

        public void setDepositAmount(double depositAmount) {
            this.depositAmount = depositAmount;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getProviderName() {
            return providerName;
        }

        public void setProviderName(String providerName) {
            this.providerName = providerName;
        }

        public String getVehicleDamageImage() {
            return vehicleDamageImage;
        }

        public void setVehicleDamageImage(String vehicleDamageImage) {
            this.vehicleDamageImage = vehicleDamageImage;
        }

    }
}
