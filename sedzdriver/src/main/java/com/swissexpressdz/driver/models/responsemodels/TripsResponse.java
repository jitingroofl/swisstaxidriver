package com.swissexpressdz.driver.models.responsemodels;

import com.swissexpressdz.driver.models.datamodels.User;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TripsResponse{

	@SerializedName("trip_id")
	private String tripId;

	@SerializedName("destination_address")
	private String destinationAddress;

	@SerializedName("trip")
	private Trip trip;

	@SerializedName("is_trip_end")
	private int isTripEnd;

	@SerializedName("success")
	private boolean success;

	@SerializedName("time_left_to_responds_trip")
	private int timeLeftToRespondsTrip;

	@SerializedName("source_address")
	private String sourceAddress;

	@SerializedName("destinationLocation")
	private List<Double> destinationLocation;

	@SerializedName("sourceLocation")
	private List<Double> sourceLocation;

	@SerializedName("message")
	private String message;

	@SerializedName("user")
	private User user;

	public void setTripId(String tripId){
		this.tripId = tripId;
	}

	public String getTripId(){
		return tripId;
	}

	public void setDestinationAddress(String destinationAddress){
		this.destinationAddress = destinationAddress;
	}

	public String getDestinationAddress(){
		return destinationAddress;
	}

	public void setIsTripEnd(int isTripEnd){
		this.isTripEnd = isTripEnd;
	}

	public int getIsTripEnd(){
		return isTripEnd;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setTimeLeftToRespondsTrip(int timeLeftToRespondsTrip){
		this.timeLeftToRespondsTrip = timeLeftToRespondsTrip;
	}

	public int getTimeLeftToRespondsTrip(){
		return timeLeftToRespondsTrip;
	}

	public void setSourceAddress(String sourceAddress){
		this.sourceAddress = sourceAddress;
	}

	public String getSourceAddress(){
		return sourceAddress;
	}

	public void setDestinationLocation(List<Double> destinationLocation){
		this.destinationLocation = destinationLocation;
	}

	public List<Double> getDestinationLocation(){
		return destinationLocation;
	}

	public void setSourceLocation(List<Double> sourceLocation){
		this.sourceLocation = sourceLocation;
	}

	public List<Double> getSourceLocation(){
		return sourceLocation;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	public Trip getTrip() {
		return trip;
	}

	public void setTrip(Trip trip) {
		this.trip = trip;
	}

	@Override
 	public String toString(){
		return 
			"TripsResponse{" + 
			"trip_id = '" + tripId + '\'' + 
			",destination_address = '" + destinationAddress + '\'' + 
			",is_trip_end = '" + isTripEnd + '\'' + 
			",success = '" + success + '\'' + 
			",time_left_to_responds_trip = '" + timeLeftToRespondsTrip + '\'' + 
			",source_address = '" + sourceAddress + '\'' + 
			",destinationLocation = '" + destinationLocation + '\'' + 
			",sourceLocation = '" + sourceLocation + '\'' + 
			",message = '" + message + '\'' + 
			",user = '" + user + '\'' + 
			"}";
		}
	public class Trip{
		@SerializedName("paypal_payment")
		private boolean paypal_payment;

		@SerializedName("payment_mode")
		private int payment_mode;


		@SerializedName("service_selection")
		private String serviceSelection;


		@SerializedName("total_distance")
		private double totalDistance;

		@SerializedName("unit")
		private int unit;


		@SerializedName("total_time")
		private double totalTime;

		@SerializedName("per_day")
		private Integer perDay;

		@SerializedName("per_hour")
		private Integer perHour;

		@SerializedName("pick_up_date")
		private String pickUpDate;

		@SerializedName("pick_up_time")
		private String pickUpTime;

		public Integer getPerDay() {
			return perDay;
		}

		public void setPerDay(Integer perDay) {
			this.perDay = perDay;
		}

		public Integer getPerHour() {
			return perHour;
		}

		public void setPerHour(Integer perHour) {
			this.perHour = perHour;
		}

		public String getPickUpDate() {
			return pickUpDate;
		}

		public void setPickUpDate(String pickUpDate) {
			this.pickUpDate = pickUpDate;
		}

		public String getPickUpTime() {
			return pickUpTime;
		}

		public void setPickUpTime(String pickUpTime) {
			this.pickUpTime = pickUpTime;
		}




		public double getTotalTime() {
			return totalTime;
		}

		public void setTotalTime(double totalTime) {
			this.totalTime = totalTime;
		}

		public int getUnit() {
			return unit;
		}

		public void setUnit(int unit) {
			this.unit = unit;
		}


		public void setTotalDistance(double totalDistance) {
			this.totalDistance = totalDistance;
		}

		public double getTotalDistance() {
			return totalDistance;
		}
		public String getServiceSelection() {
			return serviceSelection;
		}

		public void setServiceSelection(String serviceSelection) {
			this.serviceSelection = serviceSelection;
		}
		public boolean isPaypal_payment() {
			return paypal_payment;
		}

		public void setPaypal_payment(boolean paypal_payment) {
			this.paypal_payment = paypal_payment;
		}

		public int getPayment_mode() {
			return payment_mode;
		}

		public void setPayment_mode(int payment_mode) {
			this.payment_mode = payment_mode;
		}



	}
}