package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DocumentNewResponse {

    @SerializedName("success")
    private Boolean success;

    @SerializedName("error_code")
    private int errorCode;
    @SerializedName("provider")
    private Provider provider;

    @SerializedName("message")
    private String message;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }
    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public class Provider {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("unique_id")
        @Expose
        private Integer uniqueId;
        @SerializedName("service_type")
        @Expose
        private Object serviceType;
        @SerializedName("cityid")
        @Expose
        private String cityid;
        @SerializedName("country_id")
        @Expose
        private String countryId;
        @SerializedName("admintypeid")
        @Expose
        private Object admintypeid;
        @SerializedName("provider_type")
        @Expose
        private Integer providerType;
        @SerializedName("provider_type_id")
        @Expose
        private Object providerTypeId;
        @SerializedName("providerLocation")
        @Expose
        private List<Double> providerLocation = null;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("last_transferred_date")
        @Expose
        private String lastTransferredDate;
        @SerializedName("location_updated_time")
        @Expose
        private String locationUpdatedTime;
        @SerializedName("zone_queue_no")
        @Expose
        private Integer zoneQueueNo;
        @SerializedName("in_zone_queue")
        @Expose
        private Boolean inZoneQueue;
        @SerializedName("taxi_service_documents")
        @Expose
        private List<Object> taxiServiceDocuments = null;
        @SerializedName("food_delivery_documents")
        @Expose
        private List<Object> foodDeliveryDocuments = null;
        @SerializedName("parcel_delivery_documents")
        @Expose
        private List<Object> parcelDeliveryDocuments = null;
        @SerializedName("vehicle_detail")
        @Expose
        private List<Object> vehicleDetail = null;
        @SerializedName("services")
        @Expose
        private AddServiceResponse.Services services;
        @SerializedName("service_status")
        @Expose
        private Boolean serviceStatus;
        @SerializedName("document_status")
        @Expose
        private Boolean documentStatus;
        @SerializedName("vehicle_status")
        @Expose
        private Boolean vehicleStatus;
        @SerializedName("register_status")
        @Expose
        private Boolean registerStatus;
        @SerializedName("rate_count")
        @Expose
        private double rateCount;
        @SerializedName("rate")
        @Expose
        private double rate;
        @SerializedName("device_unique_code")
        @Expose
        private String deviceUniqueCode;
        @SerializedName("is_document_uploaded")
        @Expose
        private Integer isDocumentUploaded;
        @SerializedName("is_partner_approved_by_admin")
        @Expose
        private Integer isPartnerApprovedByAdmin;
        @SerializedName("is_approved")
        @Expose
        private Integer isApproved;
        @SerializedName("is_active")
        @Expose
        private Integer isActive;
        @SerializedName("rejected_request")
        @Expose
        private Integer rejectedRequest;
        @SerializedName("cancelled_request")
        @Expose
        private Integer cancelledRequest;
        @SerializedName("completed_request")
        @Expose
        private Integer completedRequest;
        @SerializedName("accepted_request")
        @Expose
        private Integer acceptedRequest;
        @SerializedName("total_request")
        @Expose
        private Integer totalRequest;
        @SerializedName("is_available")
        @Expose
        private Integer isAvailable;
        @SerializedName("providerPreviousLocation")
        @Expose
        private List<Double> providerPreviousLocation = null;
        @SerializedName("is_use_google_distance")
        @Expose
        private Boolean isUseGoogleDistance;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("bearing")
        @Expose
        private Integer bearing;
        @SerializedName("device_timezone")
        @Expose
        private String deviceTimezone;
        @SerializedName("login_by")
        @Expose
        private String loginBy;
        @SerializedName("social_unique_id")
        @Expose
        private String socialUniqueId;
        @SerializedName("zipcode")
        @Expose
        private String zipcode;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("bio")
        @Expose
        private String bio;
        @SerializedName("app_version")
        @Expose
        private String appVersion;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("car_number")
        @Expose
        private String carNumber;
        @SerializedName("car_model")
        @Expose
        private String carModel;
        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("is_vehicle_document_uploaded")
        @Expose
        private Boolean isVehicleDocumentUploaded;
        @SerializedName("total_commission_amount")
        @Expose
        private Object totalCommissionAmount;
        @SerializedName("bank_id")
        @Expose
        private String bankId;
        @SerializedName("account_id")
        @Expose
        private String accountId;
        @SerializedName("is_documents_expired")
        @Expose
        private Boolean isDocumentsExpired;
        @SerializedName("country_phone_code")
        @Expose
        private String countryPhoneCode;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("wallet_currency_code")
        @Expose
        private String walletCurrencyCode;
        @SerializedName("wallet")
        @Expose
        private String wallet;
        @SerializedName("is_trip")
        @Expose
        private List<Object> isTrip = null;
        @SerializedName("received_trip_from_gender")
        @Expose
        private List<Object> receivedTripFromGender = null;
        @SerializedName("languages")
        @Expose
        private List<Object> languages = null;
        @SerializedName("first_name")
        @Expose
        private String firstName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Integer getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(Integer uniqueId) {
            this.uniqueId = uniqueId;
        }

        public Object getServiceType() {
            return serviceType;
        }

        public void setServiceType(Object serviceType) {
            this.serviceType = serviceType;
        }

        public String getCityid() {
            return cityid;
        }

        public void setCityid(String cityid) {
            this.cityid = cityid;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public Object getAdmintypeid() {
            return admintypeid;
        }

        public void setAdmintypeid(Object admintypeid) {
            this.admintypeid = admintypeid;
        }

        public Integer getProviderType() {
            return providerType;
        }

        public void setProviderType(Integer providerType) {
            this.providerType = providerType;
        }

        public Object getProviderTypeId() {
            return providerTypeId;
        }

        public void setProviderTypeId(Object providerTypeId) {
            this.providerTypeId = providerTypeId;
        }

        public List<Double> getProviderLocation() {
            return providerLocation;
        }

        public void setProviderLocation(List<Double> providerLocation) {
            this.providerLocation = providerLocation;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getLastTransferredDate() {
            return lastTransferredDate;
        }

        public void setLastTransferredDate(String lastTransferredDate) {
            this.lastTransferredDate = lastTransferredDate;
        }

        public String getLocationUpdatedTime() {
            return locationUpdatedTime;
        }

        public void setLocationUpdatedTime(String locationUpdatedTime) {
            this.locationUpdatedTime = locationUpdatedTime;
        }

        public Integer getZoneQueueNo() {
            return zoneQueueNo;
        }

        public void setZoneQueueNo(Integer zoneQueueNo) {
            this.zoneQueueNo = zoneQueueNo;
        }

        public Boolean getInZoneQueue() {
            return inZoneQueue;
        }

        public void setInZoneQueue(Boolean inZoneQueue) {
            this.inZoneQueue = inZoneQueue;
        }

        public List<Object> getTaxiServiceDocuments() {
            return taxiServiceDocuments;
        }

        public void setTaxiServiceDocuments(List<Object> taxiServiceDocuments) {
            this.taxiServiceDocuments = taxiServiceDocuments;
        }

        public List<Object> getFoodDeliveryDocuments() {
            return foodDeliveryDocuments;
        }

        public void setFoodDeliveryDocuments(List<Object> foodDeliveryDocuments) {
            this.foodDeliveryDocuments = foodDeliveryDocuments;
        }

        public List<Object> getParcelDeliveryDocuments() {
            return parcelDeliveryDocuments;
        }

        public void setParcelDeliveryDocuments(List<Object> parcelDeliveryDocuments) {
            this.parcelDeliveryDocuments = parcelDeliveryDocuments;
        }

        public List<Object> getVehicleDetail() {
            return vehicleDetail;
        }

        public void setVehicleDetail(List<Object> vehicleDetail) {
            this.vehicleDetail = vehicleDetail;
        }

        public AddServiceResponse.Services getServices() {
            return services;
        }

        public void setServices(AddServiceResponse.Services services) {
            this.services = services;
        }

        public Boolean getServiceStatus() {
            return serviceStatus;
        }

        public void setServiceStatus(Boolean serviceStatus) {
            this.serviceStatus = serviceStatus;
        }

        public Boolean getDocumentStatus() {
            return documentStatus;
        }

        public void setDocumentStatus(Boolean documentStatus) {
            this.documentStatus = documentStatus;
        }

        public Boolean getVehicleStatus() {
            return vehicleStatus;
        }

        public void setVehicleStatus(Boolean vehicleStatus) {
            this.vehicleStatus = vehicleStatus;
        }

        public Boolean getRegisterStatus() {
            return registerStatus;
        }

        public void setRegisterStatus(Boolean registerStatus) {
            this.registerStatus = registerStatus;
        }

        public double getRateCount() {
            return rateCount;
        }

        public void setRateCount(double rateCount) {
            this.rateCount = rateCount;
        }

        public double getRate() {
            return rate;
        }

        public void setRate(double rate) {
            this.rate = rate;
        }

        public String getDeviceUniqueCode() {
            return deviceUniqueCode;
        }

        public void setDeviceUniqueCode(String deviceUniqueCode) {
            this.deviceUniqueCode = deviceUniqueCode;
        }

        public Integer getIsDocumentUploaded() {
            return isDocumentUploaded;
        }

        public void setIsDocumentUploaded(Integer isDocumentUploaded) {
            this.isDocumentUploaded = isDocumentUploaded;
        }

        public Integer getIsPartnerApprovedByAdmin() {
            return isPartnerApprovedByAdmin;
        }

        public void setIsPartnerApprovedByAdmin(Integer isPartnerApprovedByAdmin) {
            this.isPartnerApprovedByAdmin = isPartnerApprovedByAdmin;
        }

        public Integer getIsApproved() {
            return isApproved;
        }

        public void setIsApproved(Integer isApproved) {
            this.isApproved = isApproved;
        }

        public Integer getIsActive() {
            return isActive;
        }

        public void setIsActive(Integer isActive) {
            this.isActive = isActive;
        }

        public Integer getRejectedRequest() {
            return rejectedRequest;
        }

        public void setRejectedRequest(Integer rejectedRequest) {
            this.rejectedRequest = rejectedRequest;
        }

        public Integer getCancelledRequest() {
            return cancelledRequest;
        }

        public void setCancelledRequest(Integer cancelledRequest) {
            this.cancelledRequest = cancelledRequest;
        }

        public Integer getCompletedRequest() {
            return completedRequest;
        }

        public void setCompletedRequest(Integer completedRequest) {
            this.completedRequest = completedRequest;
        }

        public Integer getAcceptedRequest() {
            return acceptedRequest;
        }

        public void setAcceptedRequest(Integer acceptedRequest) {
            this.acceptedRequest = acceptedRequest;
        }

        public Integer getTotalRequest() {
            return totalRequest;
        }

        public void setTotalRequest(Integer totalRequest) {
            this.totalRequest = totalRequest;
        }

        public Integer getIsAvailable() {
            return isAvailable;
        }

        public void setIsAvailable(Integer isAvailable) {
            this.isAvailable = isAvailable;
        }

        public List<Double> getProviderPreviousLocation() {
            return providerPreviousLocation;
        }

        public void setProviderPreviousLocation(List<Double> providerPreviousLocation) {
            this.providerPreviousLocation = providerPreviousLocation;
        }

        public Boolean getIsUseGoogleDistance() {
            return isUseGoogleDistance;
        }

        public void setIsUseGoogleDistance(Boolean isUseGoogleDistance) {
            this.isUseGoogleDistance = isUseGoogleDistance;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public Integer getBearing() {
            return bearing;
        }

        public void setBearing(Integer bearing) {
            this.bearing = bearing;
        }

        public String getDeviceTimezone() {
            return deviceTimezone;
        }

        public void setDeviceTimezone(String deviceTimezone) {
            this.deviceTimezone = deviceTimezone;
        }

        public String getLoginBy() {
            return loginBy;
        }

        public void setLoginBy(String loginBy) {
            this.loginBy = loginBy;
        }

        public String getSocialUniqueId() {
            return socialUniqueId;
        }

        public void setSocialUniqueId(String socialUniqueId) {
            this.socialUniqueId = socialUniqueId;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getAppVersion() {
            return appVersion;
        }

        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getCarNumber() {
            return carNumber;
        }

        public void setCarNumber(String carNumber) {
            this.carNumber = carNumber;
        }

        public String getCarModel() {
            return carModel;
        }

        public void setCarModel(String carModel) {
            this.carModel = carModel;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Boolean getIsVehicleDocumentUploaded() {
            return isVehicleDocumentUploaded;
        }

        public void setIsVehicleDocumentUploaded(Boolean isVehicleDocumentUploaded) {
            this.isVehicleDocumentUploaded = isVehicleDocumentUploaded;
        }

        public Object getTotalCommissionAmount() {
            return totalCommissionAmount;
        }

        public void setTotalCommissionAmount(Object totalCommissionAmount) {
            this.totalCommissionAmount = totalCommissionAmount;
        }

        public String getBankId() {
            return bankId;
        }

        public void setBankId(String bankId) {
            this.bankId = bankId;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public Boolean getIsDocumentsExpired() {
            return isDocumentsExpired;
        }

        public void setIsDocumentsExpired(Boolean isDocumentsExpired) {
            this.isDocumentsExpired = isDocumentsExpired;
        }

        public String getCountryPhoneCode() {
            return countryPhoneCode;
        }

        public void setCountryPhoneCode(String countryPhoneCode) {
            this.countryPhoneCode = countryPhoneCode;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getWalletCurrencyCode() {
            return walletCurrencyCode;
        }

        public void setWalletCurrencyCode(String walletCurrencyCode) {
            this.walletCurrencyCode = walletCurrencyCode;
        }

        public String getWallet() {
            return wallet;
        }

        public void setWallet(String wallet) {
            this.wallet = wallet;
        }

        public List<Object> getIsTrip() {
            return isTrip;
        }

        public void setIsTrip(List<Object> isTrip) {
            this.isTrip = isTrip;
        }

        public List<Object> getReceivedTripFromGender() {
            return receivedTripFromGender;
        }

        public void setReceivedTripFromGender(List<Object> receivedTripFromGender) {
            this.receivedTripFromGender = receivedTripFromGender;
        }

        public List<Object> getLanguages() {
            return languages;
        }

        public void setLanguages(List<Object> languages) {
            this.languages = languages;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }
    }
    public class Documents {

        @SerializedName("document_type7")

        private String documentType7;
        @SerializedName("documentExt7")

        private String documentExt7;
        @SerializedName("documentData7")

        private String documentData7;
        @SerializedName("document_type6")

        private String documentType6;
        @SerializedName("documentExt6")

        private String documentExt6;
        @SerializedName("documentData6")

        private String documentData6;
        @SerializedName("documentData5")

        private String documentData5;
        @SerializedName("document_type5")
        private String documentType5;

        @SerializedName("documentExt5")
        private String documentExt5;
        @SerializedName("document_type4")
        private String documentType4;
        @SerializedName("documentData4")
        private String documentData4;
        @SerializedName("documentExt4")
        private String documentExt4;
        @SerializedName("document_type3")
        private String documentType3;
        @SerializedName("documentData3")

        private String documentData3;
        @SerializedName("documentExt3")

        private String documentExt3;
        @SerializedName("document_type2")

        private String documentType2;
        @SerializedName("documentExt2")

        private String documentExt2;
        @SerializedName("documentData2")

        private String documentData2;
        @SerializedName("document_type")

        private String documentType;
        @SerializedName("documentExt")

        private String documentExt;
        @SerializedName("documentData")

        private String documentData;

        public String getDocumentType7() {
            return documentType7;
        }

        public void setDocumentType7(String documentType7) {
            this.documentType7 = documentType7;
        }

        public String getDocumentExt7() {
            return documentExt7;
        }

        public void setDocumentExt7(String documentExt7) {
            this.documentExt7 = documentExt7;
        }

        public String getDocumentData7() {
            return documentData7;
        }

        public void setDocumentData7(String documentData7) {
            this.documentData7 = documentData7;
        }

        public String getDocumentType6() {
            return documentType6;
        }

        public void setDocumentType6(String documentType6) {
            this.documentType6 = documentType6;
        }

        public String getDocumentExt6() {
            return documentExt6;
        }

        public void setDocumentExt6(String documentExt6) {
            this.documentExt6 = documentExt6;
        }

        public String getDocumentData6() {
            return documentData6;
        }

        public void setDocumentData6(String documentData6) {
            this.documentData6 = documentData6;
        }

        public String getDocumentData5() {
            return documentData5;
        }

        public void setDocumentData5(String documentData5) {
            this.documentData5 = documentData5;
        }

        public String getDocumentType5() {
            return documentType5;
        }

        public void setDocumentType5(String documentType5) {
            this.documentType5 = documentType5;
        }

        public String getDocumentExt5() {
            return documentExt5;
        }

        public void setDocumentExt5(String documentExt5) {
            this.documentExt5 = documentExt5;
        }

        public String getDocumentType4() {
            return documentType4;
        }

        public void setDocumentType4(String documentType4) {
            this.documentType4 = documentType4;
        }

        public String getDocumentData4() {
            return documentData4;
        }

        public void setDocumentData4(String documentData4) {
            this.documentData4 = documentData4;
        }

        public String getDocumentExt4() {
            return documentExt4;
        }

        public void setDocumentExt4(String documentExt4) {
            this.documentExt4 = documentExt4;
        }

        public String getDocumentType3() {
            return documentType3;
        }

        public void setDocumentType3(String documentType3) {
            this.documentType3 = documentType3;
        }

        public String getDocumentData3() {
            return documentData3;
        }

        public void setDocumentData3(String documentData3) {
            this.documentData3 = documentData3;
        }

        public String getDocumentExt3() {
            return documentExt3;
        }

        public void setDocumentExt3(String documentExt3) {
            this.documentExt3 = documentExt3;
        }

        public String getDocumentType2() {
            return documentType2;
        }

        public void setDocumentType2(String documentType2) {
            this.documentType2 = documentType2;
        }

        public String getDocumentExt2() {
            return documentExt2;
        }

        public void setDocumentExt2(String documentExt2) {
            this.documentExt2 = documentExt2;
        }

        public String getDocumentData2() {
            return documentData2;
        }

        public void setDocumentData2(String documentData2) {
            this.documentData2 = documentData2;
        }

        public String getDocumentType() {
            return documentType;
        }

        public void setDocumentType(String documentType) {
            this.documentType = documentType;
        }

        public String getDocumentExt() {
            return documentExt;
        }

        public void setDocumentExt(String documentExt) {
            this.documentExt = documentExt;
        }

        public String getDocumentData() {
            return documentData;
        }

        public void setDocumentData(String documentData) {
            this.documentData = documentData;
        }

    }



    public class Service1 {

        @SerializedName("name")
         private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }


    public class VehicleDetail {

        @SerializedName("place")
        private String place;
        @SerializedName("km")
        private String km;
        @SerializedName("image")
        private String image;
        @SerializedName("is_document_expired")
        private Boolean isDocumentExpired;
        @SerializedName("is_document_uploaded")

        private Boolean isDocumentUploaded;
        @SerializedName("is_selected")

        private Boolean isSelected;
        @SerializedName("admin_type_id")

        private Object adminTypeId;
        @SerializedName("service_type")

        private Object serviceType;
        @SerializedName("passing_year")

        private String passingYear;
        @SerializedName("color")

        private String color;
        @SerializedName("model")

        private String model;
        @SerializedName("plate_no")

        private String plateNo;
        @SerializedName("name")

        private String name;
        @SerializedName("_id")

        private String id;

        public String getPlace() {
            return place;
        }

        public void setPlace(String place) {
            this.place = place;
        }

        public String getKm() {
            return km;
        }

        public void setKm(String km) {
            this.km = km;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Boolean getIsDocumentExpired() {
            return isDocumentExpired;
        }

        public void setIsDocumentExpired(Boolean isDocumentExpired) {
            this.isDocumentExpired = isDocumentExpired;
        }

        public Boolean getIsDocumentUploaded() {
            return isDocumentUploaded;
        }

        public void setIsDocumentUploaded(Boolean isDocumentUploaded) {
            this.isDocumentUploaded = isDocumentUploaded;
        }

        public Boolean getIsSelected() {
            return isSelected;
        }

        public void setIsSelected(Boolean isSelected) {
            this.isSelected = isSelected;
        }

        public Object getAdminTypeId() {
            return adminTypeId;
        }

        public void setAdminTypeId(Object adminTypeId) {
            this.adminTypeId = adminTypeId;
        }

        public Object getServiceType() {
            return serviceType;
        }

        public void setServiceType(Object serviceType) {
            this.serviceType = serviceType;
        }

        public String getPassingYear() {
            return passingYear;
        }

        public void setPassingYear(String passingYear) {
            this.passingYear = passingYear;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getPlateNo() {
            return plateNo;
        }

        public void setPlateNo(String plateNo) {
            this.plateNo = plateNo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

    }

    public class Services {

        @SerializedName("service3")

        private List<Service3> service3 = null;
        @SerializedName("service2")

        private List<Service2> service2 = null;

        private List<Service1> service1 = null;

        public List<Service3> getService3() {
            return service3;
        }

        public void setService3(List<Service3> service3) {
            this.service3 = service3;
        }

        public List<Service2> getService2() {
            return service2;
        }

        public void setService2(List<Service2> service2) {
            this.service2 = service2;
        }

        public List<Service1> getService1() {
            return service1;
        }

        public void setService1(List<Service1> service1) {
            this.service1 = service1;
        }

    }
}
