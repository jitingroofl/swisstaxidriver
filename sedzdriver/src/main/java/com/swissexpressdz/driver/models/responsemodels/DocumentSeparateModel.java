package com.swissexpressdz.driver.models.responsemodels;

import android.net.Uri;

public class DocumentSeparateModel {

    String  document_name;
    String document_type;

    String str_document_ext1;
    String str_document_file1;
    Uri picuri;


    public String getStr_document_ext1() {
        return str_document_ext1;
    }

    public void setStr_document_ext1(String str_document_ext1) {
        this.str_document_ext1 = str_document_ext1;
    }

    public String getStr_document_file1() {
        return str_document_file1;
    }

    public void setStr_document_file1(String str_document_file1) {
        this.str_document_file1 = str_document_file1;
    }



    public Uri getPicuri() {
        return picuri;
    }

    public void setPicuri(Uri picuri) {
        this.picuri = picuri;
    }



    public String getDocument_name() {
        return document_name;
    }

    public void setDocument_name(String document_name) {
        this.document_name = document_name;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }







}
