package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddVehicleResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("provider")
    @Expose
    private Provider provider;
    @SerializedName("error_code")
    private int errorCode;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public Boolean getSuccess() {
        return success;
    }


    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public class Provider {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("unique_id")
        @Expose
        private Integer uniqueId;
        @SerializedName("service_type")
        @Expose
        private Object serviceType;
        @SerializedName("cityid")
        @Expose
        private String cityid;
        @SerializedName("country_id")
        @Expose
        private String countryId;
        @SerializedName("admintypeid")
        @Expose
        private Object admintypeid;
        @SerializedName("provider_type")
        @Expose
        private Integer providerType;
        @SerializedName("provider_type_id")
        @Expose
        private Object providerTypeId;
        @SerializedName("providerLocation")
        @Expose
        private List<Integer> providerLocation = null;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("last_transferred_date")
        @Expose
        private String lastTransferredDate;
        @SerializedName("location_updated_time")
        @Expose
        private String locationUpdatedTime;
        @SerializedName("zone_queue_no")
        @Expose
        private Integer zoneQueueNo;
        @SerializedName("in_zone_queue")
        @Expose
        private Boolean inZoneQueue;
        @SerializedName("taxi_service_documents")
        @Expose
        private List<Object> taxiServiceDocuments = null;
        @SerializedName("food_delivery_documents")
        @Expose
        private List<Object> foodDeliveryDocuments = null;
        @SerializedName("parcel_delivery_documents")
        @Expose
        private List<Object> parcelDeliveryDocuments = null;
        @SerializedName("vehicle_detail")
        @Expose
        private List<VehicleDetail> vehicleDetail = null;
        @SerializedName("services")
        @Expose
        private Services services;
        @SerializedName("service_status")
        @Expose
        private Boolean serviceStatus;
        @SerializedName("document_status")
        @Expose
        private Boolean documentStatus;
        @SerializedName("vehicle_status")
        @Expose
        private Boolean vehicleStatus;
        @SerializedName("register_status")
        @Expose
        private Boolean registerStatus;
        @SerializedName("rate_count")
        @Expose
        private Integer rateCount;
        @SerializedName("rate")
        @Expose
        private Integer rate;
        @SerializedName("device_unique_code")
        @Expose
        private String deviceUniqueCode;
        @SerializedName("is_document_uploaded")
        @Expose
        private Integer isDocumentUploaded;
        @SerializedName("is_partner_approved_by_admin")
        @Expose
        private Integer isPartnerApprovedByAdmin;
        @SerializedName("is_approved")
        @Expose
        private Integer isApproved;
        @SerializedName("is_active")
        @Expose
        private Integer isActive;
        @SerializedName("rejected_request")
        @Expose
        private Integer rejectedRequest;
        @SerializedName("cancelled_request")
        @Expose
        private Integer cancelledRequest;
        @SerializedName("completed_request")
        @Expose
        private Integer completedRequest;
        @SerializedName("accepted_request")
        @Expose
        private Integer acceptedRequest;
        @SerializedName("total_request")
        @Expose
        private Integer totalRequest;
        @SerializedName("is_available")
        @Expose
        private Integer isAvailable;
        @SerializedName("providerPreviousLocation")
        @Expose
        private List<Integer> providerPreviousLocation = null;
        @SerializedName("is_use_google_distance")
        @Expose
        private Boolean isUseGoogleDistance;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("bearing")
        @Expose
        private Integer bearing;
        @SerializedName("device_timezone")
        @Expose
        private String deviceTimezone;
        @SerializedName("login_by")
        @Expose
        private String loginBy;
        @SerializedName("social_unique_id")
        @Expose
        private String socialUniqueId;
        @SerializedName("zipcode")
        @Expose
        private String zipcode;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("bio")
        @Expose
        private String bio;
        @SerializedName("app_version")
        @Expose
        private String appVersion;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("car_number")
        @Expose
        private String carNumber;
        @SerializedName("car_model")
        @Expose
        private String carModel;
        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("is_vehicle_document_uploaded")
        @Expose
        private Boolean isVehicleDocumentUploaded;
        @SerializedName("total_commission_amount")
        @Expose
        private Object totalCommissionAmount;
        @SerializedName("bank_id")
        @Expose
        private String bankId;
        @SerializedName("account_id")
        @Expose
        private String accountId;
        @SerializedName("is_documents_expired")
        @Expose
        private Boolean isDocumentsExpired;
        @SerializedName("country_phone_code")
        @Expose
        private String countryPhoneCode;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("wallet_currency_code")
        @Expose
        private String walletCurrencyCode;
        @SerializedName("wallet")
        @Expose
        private Integer wallet;
        @SerializedName("is_trip")
        @Expose
        private List<Object> isTrip = null;
        @SerializedName("received_trip_from_gender")
        @Expose
        private List<Object> receivedTripFromGender = null;
        @SerializedName("languages")
        @Expose
        private List<Object> languages = null;
        @SerializedName("first_name")
        @Expose
        private String firstName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Integer getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(Integer uniqueId) {
            this.uniqueId = uniqueId;
        }

        public Object getServiceType() {
            return serviceType;
        }

        public void setServiceType(Object serviceType) {
            this.serviceType = serviceType;
        }

        public String getCityid() {
            return cityid;
        }

        public void setCityid(String cityid) {
            this.cityid = cityid;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public Object getAdmintypeid() {
            return admintypeid;
        }

        public void setAdmintypeid(Object admintypeid) {
            this.admintypeid = admintypeid;
        }

        public Integer getProviderType() {
            return providerType;
        }

        public void setProviderType(Integer providerType) {
            this.providerType = providerType;
        }

        public Object getProviderTypeId() {
            return providerTypeId;
        }

        public void setProviderTypeId(Object providerTypeId) {
            this.providerTypeId = providerTypeId;
        }

        public List<Integer> getProviderLocation() {
            return providerLocation;
        }

        public void setProviderLocation(List<Integer> providerLocation) {
            this.providerLocation = providerLocation;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getLastTransferredDate() {
            return lastTransferredDate;
        }

        public void setLastTransferredDate(String lastTransferredDate) {
            this.lastTransferredDate = lastTransferredDate;
        }

        public String getLocationUpdatedTime() {
            return locationUpdatedTime;
        }

        public void setLocationUpdatedTime(String locationUpdatedTime) {
            this.locationUpdatedTime = locationUpdatedTime;
        }

        public Integer getZoneQueueNo() {
            return zoneQueueNo;
        }

        public void setZoneQueueNo(Integer zoneQueueNo) {
            this.zoneQueueNo = zoneQueueNo;
        }

        public Boolean getInZoneQueue() {
            return inZoneQueue;
        }

        public void setInZoneQueue(Boolean inZoneQueue) {
            this.inZoneQueue = inZoneQueue;
        }

        public List<Object> getTaxiServiceDocuments() {
            return taxiServiceDocuments;
        }

        public void setTaxiServiceDocuments(List<Object> taxiServiceDocuments) {
            this.taxiServiceDocuments = taxiServiceDocuments;
        }

        public List<Object> getFoodDeliveryDocuments() {
            return foodDeliveryDocuments;
        }

        public void setFoodDeliveryDocuments(List<Object> foodDeliveryDocuments) {
            this.foodDeliveryDocuments = foodDeliveryDocuments;
        }

        public List<Object> getParcelDeliveryDocuments() {
            return parcelDeliveryDocuments;
        }

        public void setParcelDeliveryDocuments(List<Object> parcelDeliveryDocuments) {
            this.parcelDeliveryDocuments = parcelDeliveryDocuments;
        }

        public List<VehicleDetail> getVehicleDetail() {
            return vehicleDetail;
        }

        public void setVehicleDetail(List<VehicleDetail> vehicleDetail) {
            this.vehicleDetail = vehicleDetail;
        }

        public Services getServices() {
            return services;
        }

        public void setServices(Services services) {
            this.services = services;
        }

        public Boolean getServiceStatus() {
            return serviceStatus;
        }

        public void setServiceStatus(Boolean serviceStatus) {
            this.serviceStatus = serviceStatus;
        }

        public Boolean getDocumentStatus() {
            return documentStatus;
        }

        public void setDocumentStatus(Boolean documentStatus) {
            this.documentStatus = documentStatus;
        }

        public Boolean getVehicleStatus() {
            return vehicleStatus;
        }

        public void setVehicleStatus(Boolean vehicleStatus) {
            this.vehicleStatus = vehicleStatus;
        }

        public Boolean getRegisterStatus() {
            return registerStatus;
        }

        public void setRegisterStatus(Boolean registerStatus) {
            this.registerStatus = registerStatus;
        }

        public Integer getRateCount() {
            return rateCount;
        }

        public void setRateCount(Integer rateCount) {
            this.rateCount = rateCount;
        }

        public Integer getRate() {
            return rate;
        }

        public void setRate(Integer rate) {
            this.rate = rate;
        }

        public String getDeviceUniqueCode() {
            return deviceUniqueCode;
        }

        public void setDeviceUniqueCode(String deviceUniqueCode) {
            this.deviceUniqueCode = deviceUniqueCode;
        }

        public Integer getIsDocumentUploaded() {
            return isDocumentUploaded;
        }

        public void setIsDocumentUploaded(Integer isDocumentUploaded) {
            this.isDocumentUploaded = isDocumentUploaded;
        }

        public Integer getIsPartnerApprovedByAdmin() {
            return isPartnerApprovedByAdmin;
        }

        public void setIsPartnerApprovedByAdmin(Integer isPartnerApprovedByAdmin) {
            this.isPartnerApprovedByAdmin = isPartnerApprovedByAdmin;
        }

        public Integer getIsApproved() {
            return isApproved;
        }

        public void setIsApproved(Integer isApproved) {
            this.isApproved = isApproved;
        }

        public Integer getIsActive() {
            return isActive;
        }

        public void setIsActive(Integer isActive) {
            this.isActive = isActive;
        }

        public Integer getRejectedRequest() {
            return rejectedRequest;
        }

        public void setRejectedRequest(Integer rejectedRequest) {
            this.rejectedRequest = rejectedRequest;
        }

        public Integer getCancelledRequest() {
            return cancelledRequest;
        }

        public void setCancelledRequest(Integer cancelledRequest) {
            this.cancelledRequest = cancelledRequest;
        }

        public Integer getCompletedRequest() {
            return completedRequest;
        }

        public void setCompletedRequest(Integer completedRequest) {
            this.completedRequest = completedRequest;
        }

        public Integer getAcceptedRequest() {
            return acceptedRequest;
        }

        public void setAcceptedRequest(Integer acceptedRequest) {
            this.acceptedRequest = acceptedRequest;
        }

        public Integer getTotalRequest() {
            return totalRequest;
        }

        public void setTotalRequest(Integer totalRequest) {
            this.totalRequest = totalRequest;
        }

        public Integer getIsAvailable() {
            return isAvailable;
        }

        public void setIsAvailable(Integer isAvailable) {
            this.isAvailable = isAvailable;
        }

        public List<Integer> getProviderPreviousLocation() {
            return providerPreviousLocation;
        }

        public void setProviderPreviousLocation(List<Integer> providerPreviousLocation) {
            this.providerPreviousLocation = providerPreviousLocation;
        }

        public Boolean getIsUseGoogleDistance() {
            return isUseGoogleDistance;
        }

        public void setIsUseGoogleDistance(Boolean isUseGoogleDistance) {
            this.isUseGoogleDistance = isUseGoogleDistance;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public Integer getBearing() {
            return bearing;
        }

        public void setBearing(Integer bearing) {
            this.bearing = bearing;
        }

        public String getDeviceTimezone() {
            return deviceTimezone;
        }

        public void setDeviceTimezone(String deviceTimezone) {
            this.deviceTimezone = deviceTimezone;
        }

        public String getLoginBy() {
            return loginBy;
        }

        public void setLoginBy(String loginBy) {
            this.loginBy = loginBy;
        }

        public String getSocialUniqueId() {
            return socialUniqueId;
        }

        public void setSocialUniqueId(String socialUniqueId) {
            this.socialUniqueId = socialUniqueId;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getAppVersion() {
            return appVersion;
        }

        public void setAppVersion(String appVersion) {
            this.appVersion = appVersion;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getCarNumber() {
            return carNumber;
        }

        public void setCarNumber(String carNumber) {
            this.carNumber = carNumber;
        }

        public String getCarModel() {
            return carModel;
        }

        public void setCarModel(String carModel) {
            this.carModel = carModel;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Boolean getIsVehicleDocumentUploaded() {
            return isVehicleDocumentUploaded;
        }

        public void setIsVehicleDocumentUploaded(Boolean isVehicleDocumentUploaded) {
            this.isVehicleDocumentUploaded = isVehicleDocumentUploaded;
        }

        public Object getTotalCommissionAmount() {
            return totalCommissionAmount;
        }

        public void setTotalCommissionAmount(Object totalCommissionAmount) {
            this.totalCommissionAmount = totalCommissionAmount;
        }

        public String getBankId() {
            return bankId;
        }

        public void setBankId(String bankId) {
            this.bankId = bankId;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public Boolean getIsDocumentsExpired() {
            return isDocumentsExpired;
        }

        public void setIsDocumentsExpired(Boolean isDocumentsExpired) {
            this.isDocumentsExpired = isDocumentsExpired;
        }

        public String getCountryPhoneCode() {
            return countryPhoneCode;
        }

        public void setCountryPhoneCode(String countryPhoneCode) {
            this.countryPhoneCode = countryPhoneCode;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getWalletCurrencyCode() {
            return walletCurrencyCode;
        }

        public void setWalletCurrencyCode(String walletCurrencyCode) {
            this.walletCurrencyCode = walletCurrencyCode;
        }

        public Integer getWallet() {
            return wallet;
        }

        public void setWallet(Integer wallet) {
            this.wallet = wallet;
        }

        public List<Object> getIsTrip() {
            return isTrip;
        }

        public void setIsTrip(List<Object> isTrip) {
            this.isTrip = isTrip;
        }

        public List<Object> getReceivedTripFromGender() {
            return receivedTripFromGender;
        }

        public void setReceivedTripFromGender(List<Object> receivedTripFromGender) {
            this.receivedTripFromGender = receivedTripFromGender;
        }

        public List<Object> getLanguages() {
            return languages;
        }

        public void setLanguages(List<Object> languages) {
            this.languages = languages;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

    }

    public class Service1 {

        @SerializedName("name")
        @Expose
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
    public class Service2 {

        @SerializedName("car_rent")
        @Expose
        private String carRent;
        @SerializedName("vtc")
        @Expose
        private String vtc;
        @SerializedName("taxi")
        @Expose
        private String taxi;
        @SerializedName("name")
        @Expose
        private String name;

        public String getCarRent() {
            return carRent;
        }

        public void setCarRent(String carRent) {
            this.carRent = carRent;
        }

        public String getVtc() {
            return vtc;
        }

        public void setVtc(String vtc) {
            this.vtc = vtc;
        }

        public String getTaxi() {
            return taxi;
        }

        public void setTaxi(String taxi) {
            this.taxi = taxi;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
    public class Service3 {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("name")
        @Expose
        private String name;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
    public class Services {

        @SerializedName("service3")
        @Expose
        private List<Service3> service3 = null;
        @SerializedName("service2")
        @Expose
        private List<Service2> service2 = null;
        @SerializedName("service1")
        @Expose
        private List<Service1> service1 = null;

        public List<Service3> getService3() {
            return service3;
        }

        public void setService3(List<Service3> service3) {
            this.service3 = service3;
        }

        public List<Service2> getService2() {
            return service2;
        }

        public void setService2(List<Service2> service2) {
            this.service2 = service2;
        }

        public List<Service1> getService1() {
            return service1;
        }

        public void setService1(List<Service1> service1) {
            this.service1 = service1;
        }

    }
    public class VehicleDetail {

        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("is_document_expired")
        @Expose
        private Boolean isDocumentExpired;
        @SerializedName("is_document_uploaded")
        @Expose
        private Boolean isDocumentUploaded;
        @SerializedName("is_selected")
        @Expose
        private Boolean isSelected;
        @SerializedName("admin_type_id")
        @Expose
        private Object adminTypeId;
        @SerializedName("service_type")
        @Expose
        private Object serviceType;
        @SerializedName("passing_year")
        @Expose
        private String passingYear;
        @SerializedName("color")
        @Expose
        private String color;
        @SerializedName("model")
        @Expose
        private String model;
        @SerializedName("plate_no")
        @Expose
        private String plateNo;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("_id")
        @Expose
        private String id;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Boolean getIsDocumentExpired() {
            return isDocumentExpired;
        }

        public void setIsDocumentExpired(Boolean isDocumentExpired) {
            this.isDocumentExpired = isDocumentExpired;
        }

        public Boolean getIsDocumentUploaded() {
            return isDocumentUploaded;
        }

        public void setIsDocumentUploaded(Boolean isDocumentUploaded) {
            this.isDocumentUploaded = isDocumentUploaded;
        }

        public Boolean getIsSelected() {
            return isSelected;
        }

        public void setIsSelected(Boolean isSelected) {
            this.isSelected = isSelected;
        }

        public Object getAdminTypeId() {
            return adminTypeId;
        }

        public void setAdminTypeId(Object adminTypeId) {
            this.adminTypeId = adminTypeId;
        }

        public Object getServiceType() {
            return serviceType;
        }

        public void setServiceType(Object serviceType) {
            this.serviceType = serviceType;
        }

        public String getPassingYear() {
            return passingYear;
        }

        public void setPassingYear(String passingYear) {
            this.passingYear = passingYear;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getPlateNo() {
            return plateNo;
        }

        public void setPlateNo(String plateNo) {
            this.plateNo = plateNo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

    }
}
