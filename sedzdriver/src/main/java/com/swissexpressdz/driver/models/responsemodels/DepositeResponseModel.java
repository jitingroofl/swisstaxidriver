package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.SerializedName;

public class DepositeResponseModel {

    @SerializedName("success")
    private Boolean success;

    @SerializedName("deposit")
    private Deposit deposit;

    @SerializedName("error_code")
    private int errorCode;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Deposit getDeposit() {
        return deposit;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    public class Deposit {

        @SerializedName("__v")

        private Integer v;
        @SerializedName("user_id")

        private String userId;
        @SerializedName("provider_id")

        private String providerId;
        @SerializedName("_id")

        private String id;
        @SerializedName("payment_status")

        private String paymentStatus;
        @SerializedName("is_damage")

        private Boolean isDamage;
        @SerializedName("apply_date")

        private String applyDate;
        @SerializedName("deposit_amount_pay_to_user")

        private Integer depositAmountPayToUser;
        @SerializedName("damage_amount")

        private Integer damageAmount;
        @SerializedName("user_name")

        private String userName;
        @SerializedName("provider_name")

        private String providerName;
        @SerializedName("vehicle_damage_image")

        private String vehicleDamageImage;

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getProviderId() {
            return providerId;
        }

        public void setProviderId(String providerId) {
            this.providerId = providerId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public Boolean getIsDamage() {
            return isDamage;
        }

        public void setIsDamage(Boolean isDamage) {
            this.isDamage = isDamage;
        }

        public String getApplyDate() {
            return applyDate;
        }

        public void setApplyDate(String applyDate) {
            this.applyDate = applyDate;
        }

        public Integer getDepositAmountPayToUser() {
            return depositAmountPayToUser;
        }

        public void setDepositAmountPayToUser(Integer depositAmountPayToUser) {
            this.depositAmountPayToUser = depositAmountPayToUser;
        }

        public Integer getDamageAmount() {
            return damageAmount;
        }

        public void setDamageAmount(Integer damageAmount) {
            this.damageAmount = damageAmount;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getProviderName() {
            return providerName;
        }

        public void setProviderName(String providerName) {
            this.providerName = providerName;
        }

        public String getVehicleDamageImage() {
            return vehicleDamageImage;
        }

        public void setVehicleDamageImage(String vehicleDamageImage) {
            this.vehicleDamageImage = vehicleDamageImage;
        }

    }
}
