package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Service2 {
    @SerializedName("car_rent")
    @Expose
    private String carRent;
    @SerializedName("vtc")
    @Expose
    private String vtc;
    @SerializedName("taxi")
    @Expose
    private String taxi;
    @SerializedName("name")
    @Expose
    private String name;

    public String getCarRent() {
        return carRent;
    }

    public void setCarRent(String carRent) {
        this.carRent = carRent;
    }

    public String getVtc() {
        return vtc;
    }

    public void setVtc(String vtc) {
        this.vtc = vtc;
    }

    public String getTaxi() {
        return taxi;
    }

    public void setTaxi(String taxi) {
        this.taxi = taxi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
