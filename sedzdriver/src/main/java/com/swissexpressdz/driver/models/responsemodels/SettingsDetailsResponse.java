package com.swissexpressdz.driver.models.responsemodels;

import com.swissexpressdz.driver.models.datamodels.AdminSettings;
import com.swissexpressdz.driver.models.datamodels.ProviderData;
import com.google.gson.annotations.SerializedName;

public class SettingsDetailsResponse {


    @SerializedName("trip_detail")
    private TripsResponse tripsResponse;
    @SerializedName("error_code")
    private int errorCode;
    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private String message;
    @SerializedName("setting_detail")
    private AdminSettings adminSettings;
    @SerializedName("provider_detail")
    private ProviderData providerData;

    @SerializedName("contactUsEmail")
    private String contactUsEmail;

    @SerializedName("admin_phone")
    private String adminPhone;

    public String getContactUsEmail() {
        return contactUsEmail;
    }

    public void setContactUsEmail(String contactUsEmail) {
        this.contactUsEmail = contactUsEmail;
    }

    public String getAdminPhone() {
        return adminPhone;
    }

    public void setAdminPhone(String adminPhone) {
        this.adminPhone = adminPhone;
    }




    public TripsResponse getTripsResponse() {
        return tripsResponse;
    }

    public void setTripsResponse(TripsResponse tripsResponse) {
        this.tripsResponse = tripsResponse;
    }

    public ProviderData getProviderData() {
        return providerData;
    }

    public void setProviderData(ProviderData providerData) {
        this.providerData = providerData;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AdminSettings getAdminSettings() {
        return adminSettings;
    }

    public void setAdminSettings(AdminSettings adminSettings) {
        this.adminSettings = adminSettings;
    }


}
