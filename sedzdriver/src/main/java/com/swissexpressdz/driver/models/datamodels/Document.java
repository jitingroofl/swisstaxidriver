package com.swissexpressdz.driver.models.datamodels;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Document {

    @SerializedName("is_document_uploaded")
    @Expose
    private int isDocumentUploaded;

    @SerializedName("error_code")
    @Expose
    private int errorCode;

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("is_unique_code")
    @Expose
    private boolean isUniqueCode;

    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("document_picture")
    @Expose
    private String documentPicture;

    @SerializedName("document_id")
    @Expose
    private String documentId;

    @SerializedName("expired_date")
    @Expose
    private String expiredDate = "";

    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("provider_id")
    @Expose
    private String userId;

    @SerializedName("name")
    private String name;

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("is_document_expired")
    @Expose
    private boolean isDocumentExpired;

    @SerializedName("is_expired_date")
    @Expose
    private boolean isExpiredDate;

    @SerializedName("unique_code")
    @Expose
    private String uniqueCode = "";

    @SerializedName("is_uploaded")
    @Expose
    private int isUploaded;

    @SerializedName("option")
    @Expose
    private int option;

    //For First Document
    @SerializedName("documentData")
    @Expose
    private String documentData;

    @SerializedName("documentExt")
    @Expose
    private String documentExt;

    @SerializedName("document_type")
    @Expose
    private String document_type;

    //For Second Document
    @SerializedName("documentData2")
    @Expose
    private String documentData2;

    @SerializedName("documentExt2")
    @Expose
    private String documentExt2;

    @SerializedName("document_type2")
    @Expose
    private String document_type2;

    //For Third Document
    @SerializedName("documentData3")
    @Expose
    private String documentData3;

    @SerializedName("documentExt3")
    @Expose
    private String documentExt3;

    @SerializedName("document_type3")
    @Expose
    private String document_type3;

    //For Fourth Document
    @SerializedName("documentData4")
    @Expose
    private String documentData4;

    @SerializedName("documentExt4")
    @Expose
    private String documentExt4;

    @SerializedName("document_type4")
    @Expose
    private String document_type4;

    //For Fifth Document
    @SerializedName("documentData5")
    @Expose
    private String documentData5;

    @SerializedName("documentExt5")
    @Expose
    private String documentExt5;

    @SerializedName("document_type5")
    @Expose
    private String document_type5;


    public String getDocumentData() {
        return documentData;
    }

    public void setDocumentData(String documentData) {
        this.documentData = documentData;
    }

    public String getDocumentExt() {
        return documentExt;
    }

    public void setDocumentExt(String documentExt) {
        this.documentExt = documentExt;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public String getDocumentData2() {
        return documentData2;
    }

    public void setDocumentData2(String documentData2) {
        this.documentData2 = documentData2;
    }

    public String getDocumentExt2() {
        return documentExt2;
    }

    public void setDocumentExt2(String documentExt2) {
        this.documentExt2 = documentExt2;
    }

    public String getDocument_type2() {
        return document_type2;
    }

    public void setDocument_type2(String document_type2) {
        this.document_type2 = document_type2;
    }

    public String getDocumentData3() {
        return documentData3;
    }

    public void setDocumentData3(String documentData3) {
        this.documentData3 = documentData3;
    }

    public String getDocumentExt3() {
        return documentExt3;
    }

    public void setDocumentExt3(String documentExt3) {
        this.documentExt3 = documentExt3;
    }

    public String getDocument_type3() {
        return document_type3;
    }

    public void setDocument_type3(String document_type3) {
        this.document_type3 = document_type3;
    }

    public String getDocumentData4() {
        return documentData4;
    }

    public void setDocumentData4(String documentData4) {
        this.documentData4 = documentData4;
    }

    public String getDocumentExt4() {
        return documentExt4;
    }

    public void setDocumentExt4(String documentExt4) {
        this.documentExt4 = documentExt4;
    }

    public String getDocument_type4() {
        return document_type4;
    }

    public void setDocument_type4(String document_type4) {
        this.document_type4 = document_type4;
    }

    public String getDocumentData5() {
        return documentData5;
    }

    public void setDocumentData5(String documentData5) {
        this.documentData5 = documentData5;
    }

    public String getDocumentExt5() {
        return documentExt5;
    }

    public void setDocumentExt5(String documentExt5) {
        this.documentExt5 = documentExt5;
    }

    public String getDocument_type5() {
        return document_type5;
    }

    public void setDocument_type5(String document_type5) {
        this.document_type5 = document_type5;
    }



    public int getIsDocumentUploaded() {
        return isDocumentUploaded;
    }

    public void setIsDocumentUploaded(int isDocumentUploaded) {
        this.isDocumentUploaded = isDocumentUploaded;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isIsUniqueCode() {
        return isUniqueCode;
    }

    public void setIsUniqueCode(boolean isUniqueCode) {
        this.isUniqueCode = isUniqueCode;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDocumentPicture() {
        return documentPicture;
    }

    public void setDocumentPicture(String documentPicture) {
        this.documentPicture = documentPicture;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getExpiredDate() {
        return TextUtils.isEmpty(expiredDate) ? "" : expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isIsDocumentExpired() {
        return isDocumentExpired;
    }

    public void setIsDocumentExpired(boolean isDocumentExpired) {
        this.isDocumentExpired = isDocumentExpired;
    }

    public boolean isIsExpiredDate() {
        return isExpiredDate;
    }

    public void setIsExpiredDate(boolean isExpiredDate) {
        this.isExpiredDate = isExpiredDate;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public int getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(int isUploaded) {
        this.isUploaded = isUploaded;
    }

    public int getOption() {
        return option;
    }

    public void setOption(int option) {
        this.option = option;
    }

    @Override
    public String toString() {
        return
                "Document{" +
                        "is_unique_code = '" + isUniqueCode + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",document_picture = '" + documentPicture + '\'' +
                        ",document_id = '" + documentId + '\'' +
                        ",expired_date = '" + expiredDate + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",user_id = '" + userId + '\'' +
                        ",name = '" + name + '\'' +
                        ",_id = '" + id + '\'' +
                        ",is_document_expired = '" + isDocumentExpired + '\'' +
                        ",is_expired_date = '" + isExpiredDate + '\'' +
                        ",unique_code = '" + uniqueCode + '\'' +
                        ",is_uploaded = '" + isUploaded + '\'' +
                        ",option = '" + option + '\'' +
                        "}";
    }
}