package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.SerializedName;

public class ProviderSeriveSelctionModel {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("vtc")
    private Boolean vtc;
    @SerializedName("car_rent")
    private Boolean carRent;

    @SerializedName("taxi")
    private Boolean taxi;

    public Boolean getTaxi() {
        return taxi;
    }

    public void setTaxi(Boolean taxi) {
        this.taxi = taxi;
    }



    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Boolean getVtc() {
        return vtc;
    }

    public void setVtc(Boolean vtc) {
        this.vtc = vtc;
    }

    public Boolean getCarRent() {
        return carRent;
    }

    public void setCarRent(Boolean carRent) {
        this.carRent = carRent;
    }

}
