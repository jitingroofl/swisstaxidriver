package com.swissexpressdz.driver.models.responsemodels;

import com.swissexpressdz.driver.models.datamodels.VehicleDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleDetailResponse {

    @SerializedName("error_code")
    private int errorCode;
    @SerializedName("success")
    private boolean success;
//    @SerializedName("document_list")
//    private DocumentNew documentList;
    @SerializedName("vehicle_detail")
    private VehicleDetail vehicleDetail;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

//    public DocumentNew getDocumentList() {
//        return documentList;
//    }
//
//    public void setDocumentList(DocumentNew documentList) {
//        this.documentList = documentList;
//    }

    public VehicleDetail getVehicleDetail() {
        return vehicleDetail;
    }

    public void setVehicleDetail(VehicleDetail vehicleDetail) {
        this.vehicleDetail = vehicleDetail;
    }

    @Override
    public String toString() {
        return
                "VehicleDetailResponse{" +
                        "success = '" + success + '\'' +
                      //  ",document_list = '" + documentList + '\'' +
                        ",vehicle_detail = '" + vehicleDetail + '\'' +
                        "}";
    }
}