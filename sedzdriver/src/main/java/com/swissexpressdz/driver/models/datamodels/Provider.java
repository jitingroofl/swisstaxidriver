package com.swissexpressdz.driver.models.datamodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.swissexpressdz.driver.models.responsemodels.AddVehicleResponse;
import com.swissexpressdz.driver.models.responsemodels.Service1;
import com.swissexpressdz.driver.models.responsemodels.Service2;
import com.swissexpressdz.driver.models.responsemodels.Service3;

import java.util.List;

public class Provider {

    @SerializedName("_id")
    private String id;
    @SerializedName("unique_id")
    private Integer uniqueId;
    @SerializedName("service_type")
    private String serviceType;
    @SerializedName("cityid")
    private String cityid;
    @SerializedName("country_id")
    private String countryId;
    @SerializedName("admintypeid")
    private String admintypeid;
    @SerializedName("provider_type")
    private Integer providerType;
    @SerializedName("provider_type_id")
    private Object providerTypeId;
    @SerializedName("providerLocation")
    private List<Double> providerLocation = null;
    @SerializedName("__v")
    private Integer v;
    @SerializedName("service_vtc_id")
    private String serviceVtcId;
    @SerializedName("start_online_time")
    private String startOnlineTime;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("last_transferred_date")
    private String lastTransferredDate;
    @SerializedName("location_updated_time")
    private String locationUpdatedTime;
    @SerializedName("zone_queue_no")
    private Integer zoneQueueNo;
    @SerializedName("in_zone_queue")
    private Boolean inZoneQueue;
    @SerializedName("documents")
    private Documents documents;
    @SerializedName("vehicle_detail")
    private List<VehicleDetail> vehicleDetail = null;
    @SerializedName("services")
    private Services services;
    @SerializedName("service_status")
    private Boolean serviceStatus;
    @SerializedName("document_status")
    private Boolean documentStatus;
    @SerializedName("vehicle_status")
    private Boolean vehicleStatus;
    @SerializedName("register_status")
    private Boolean registerStatus;
    @SerializedName("seats")
    private Integer seats;
    @SerializedName("km")
    private Integer km;
    @SerializedName("rate_count")
    private Integer rateCount;
    @SerializedName("rate")
    private double rate;
    @SerializedName("device_unique_code")
    private String deviceUniqueCode;
    @SerializedName("is_document_uploaded")
    private Integer isDocumentUploaded;
    @SerializedName("is_partner_approved_by_admin")
    private Integer isPartnerApprovedByAdmin;
    @SerializedName("is_approved")
    private Integer isApproved;
    @SerializedName("is_active")
    private Integer isActive;
    @SerializedName("rejected_request")
    private Integer rejectedRequest;
    @SerializedName("cancelled_request")
    private Integer cancelledRequest;
    @SerializedName("completed_request")
    private Integer completedRequest;
    @SerializedName("accepted_request")
    private Integer acceptedRequest;
    @SerializedName("total_request")
    private Integer totalRequest;
    @SerializedName("is_available")
    private Integer isAvailable;
    @SerializedName("providerPreviousLocation")
    private List<Double> providerPreviousLocation = null;
    @SerializedName("is_use_google_distance")
    private Boolean isUseGoogleDistance;
    @SerializedName("country")

    private String country;
    @SerializedName("city")

    private String city;
    @SerializedName("bearing")

    private Integer bearing;
    @SerializedName("device_timezone")

    private String deviceTimezone;
    @SerializedName("login_by")

    private String loginBy;
    @SerializedName("social_unique_id")

    private String socialUniqueId;
    @SerializedName("zipcode")

    private String zipcode;
    @SerializedName("address")

    private String address;
    @SerializedName("bio")

    private String bio;
    @SerializedName("app_version")
    private String appVersion;
    @SerializedName("device_type")

    private String deviceType;
    @SerializedName("device_token")
    private String deviceToken;
    @SerializedName("car_number")
    private String carNumber;
    @SerializedName("car_model")

    private String carModel;
    @SerializedName("token")
    private String token;
    @SerializedName("picture")
    private String picture;
    @SerializedName("password")
    private String password;
    @SerializedName("phone")
    private String phone;
    @SerializedName("is_vehicle_document_uploaded")
    private Boolean isVehicleDocumentUploaded;
    @SerializedName("total_commission_amount")
    private Object totalCommissionAmount;
    @SerializedName("bank_id")
    private String bankId;
    @SerializedName("account_id")
    private String accountId;
    @SerializedName("is_documents_expired")
    private Boolean isDocumentsExpired;
    @SerializedName("country_phone_code")
    private String countryPhoneCode;
    @SerializedName("gender")
    private String gender;
    @SerializedName("email")
    private String email;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("wallet_currency_code")
    private String walletCurrencyCode;
    @SerializedName("wallet")

    private Integer wallet;
    @SerializedName("is_trip")
    private List<Object> isTrip = null;
    @SerializedName("received_trip_from_gender")

    private List<Object> receivedTripFromGender = null;
    @SerializedName("languages")

    private List<String> languages = null;
    @SerializedName("first_name")
    private String firstName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getAdmintypeid() {
        return admintypeid;
    }

    public void setAdmintypeid(String admintypeid) {
        this.admintypeid = admintypeid;
    }

    public Integer getProviderType() {
        return providerType;
    }

    public void setProviderType(Integer providerType) {
        this.providerType = providerType;
    }

    public Object getProviderTypeId() {
        return providerTypeId;
    }

    public void setProviderTypeId(Object providerTypeId) {
        this.providerTypeId = providerTypeId;
    }

    public List<Double> getProviderLocation() {
        return providerLocation;
    }

    public void setProviderLocation(List<Double> providerLocation) {
        this.providerLocation = providerLocation;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getServiceVtcId() {
        return serviceVtcId;
    }

    public void setServiceVtcId(String serviceVtcId) {
        this.serviceVtcId = serviceVtcId;
    }

    public String getStartOnlineTime() {
        return startOnlineTime;
    }

    public void setStartOnlineTime(String startOnlineTime) {
        this.startOnlineTime = startOnlineTime;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getLastTransferredDate() {
        return lastTransferredDate;
    }

    public void setLastTransferredDate(String lastTransferredDate) {
        this.lastTransferredDate = lastTransferredDate;
    }

    public String getLocationUpdatedTime() {
        return locationUpdatedTime;
    }

    public void setLocationUpdatedTime(String locationUpdatedTime) {
        this.locationUpdatedTime = locationUpdatedTime;
    }

    public Integer getZoneQueueNo() {
        return zoneQueueNo;
    }

    public void setZoneQueueNo(Integer zoneQueueNo) {
        this.zoneQueueNo = zoneQueueNo;
    }

    public Boolean getInZoneQueue() {
        return inZoneQueue;
    }

    public void setInZoneQueue(Boolean inZoneQueue) {
        this.inZoneQueue = inZoneQueue;
    }

    public Documents getDocuments() {
        return documents;
    }

    public void setDocuments(Documents documents) {
        this.documents = documents;
    }

    public List<VehicleDetail> getVehicleDetail() {
        return vehicleDetail;
    }

    public void setVehicleDetail(List<VehicleDetail> vehicleDetail) {
        this.vehicleDetail = vehicleDetail;
    }

    public Services getServices() {
        return services;
    }

    public void setServices(Services services) {
        this.services = services;
    }

    public Boolean getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(Boolean serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public Boolean getDocumentStatus() {
        return documentStatus;
    }

    public void setDocumentStatus(Boolean documentStatus) {
        this.documentStatus = documentStatus;
    }

    public Boolean getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(Boolean vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public Boolean getRegisterStatus() {
        return registerStatus;
    }

    public void setRegisterStatus(Boolean registerStatus) {
        this.registerStatus = registerStatus;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Integer getKm() {
        return km;
    }

    public void setKm(Integer km) {
        this.km = km;
    }

    public Integer getRateCount() {
        return rateCount;
    }

    public void setRateCount(Integer rateCount) {
        this.rateCount = rateCount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getDeviceUniqueCode() {
        return deviceUniqueCode;
    }

    public void setDeviceUniqueCode(String deviceUniqueCode) {
        this.deviceUniqueCode = deviceUniqueCode;
    }

    public Integer getIsDocumentUploaded() {
        return isDocumentUploaded;
    }

    public void setIsDocumentUploaded(Integer isDocumentUploaded) {
        this.isDocumentUploaded = isDocumentUploaded;
    }

    public Integer getIsPartnerApprovedByAdmin() {
        return isPartnerApprovedByAdmin;
    }

    public void setIsPartnerApprovedByAdmin(Integer isPartnerApprovedByAdmin) {
        this.isPartnerApprovedByAdmin = isPartnerApprovedByAdmin;
    }

    public Integer getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(Integer isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getRejectedRequest() {
        return rejectedRequest;
    }

    public void setRejectedRequest(Integer rejectedRequest) {
        this.rejectedRequest = rejectedRequest;
    }

    public Integer getCancelledRequest() {
        return cancelledRequest;
    }

    public void setCancelledRequest(Integer cancelledRequest) {
        this.cancelledRequest = cancelledRequest;
    }

    public Integer getCompletedRequest() {
        return completedRequest;
    }

    public void setCompletedRequest(Integer completedRequest) {
        this.completedRequest = completedRequest;
    }

    public Integer getAcceptedRequest() {
        return acceptedRequest;
    }

    public void setAcceptedRequest(Integer acceptedRequest) {
        this.acceptedRequest = acceptedRequest;
    }

    public Integer getTotalRequest() {
        return totalRequest;
    }

    public void setTotalRequest(Integer totalRequest) {
        this.totalRequest = totalRequest;
    }

    public Integer getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Integer isAvailable) {
        this.isAvailable = isAvailable;
    }

    public List<Double> getProviderPreviousLocation() {
        return providerPreviousLocation;
    }

    public void setProviderPreviousLocation(List<Double> providerPreviousLocation) {
        this.providerPreviousLocation = providerPreviousLocation;
    }

    public Boolean getIsUseGoogleDistance() {
        return isUseGoogleDistance;
    }

    public void setIsUseGoogleDistance(Boolean isUseGoogleDistance) {
        this.isUseGoogleDistance = isUseGoogleDistance;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getBearing() {
        return bearing;
    }

    public void setBearing(Integer bearing) {
        this.bearing = bearing;
    }

    public String getDeviceTimezone() {
        return deviceTimezone;
    }

    public void setDeviceTimezone(String deviceTimezone) {
        this.deviceTimezone = deviceTimezone;
    }

    public String getLoginBy() {
        return loginBy;
    }

    public void setLoginBy(String loginBy) {
        this.loginBy = loginBy;
    }

    public String getSocialUniqueId() {
        return socialUniqueId;
    }

    public void setSocialUniqueId(String socialUniqueId) {
        this.socialUniqueId = socialUniqueId;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getIsVehicleDocumentUploaded() {
        return isVehicleDocumentUploaded;
    }

    public void setIsVehicleDocumentUploaded(Boolean isVehicleDocumentUploaded) {
        this.isVehicleDocumentUploaded = isVehicleDocumentUploaded;
    }

    public Object getTotalCommissionAmount() {
        return totalCommissionAmount;
    }

    public void setTotalCommissionAmount(Object totalCommissionAmount) {
        this.totalCommissionAmount = totalCommissionAmount;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Boolean getIsDocumentsExpired() {
        return isDocumentsExpired;
    }

    public void setIsDocumentsExpired(Boolean isDocumentsExpired) {
        this.isDocumentsExpired = isDocumentsExpired;
    }

    public String getCountryPhoneCode() {
        return countryPhoneCode;
    }

    public void setCountryPhoneCode(String countryPhoneCode) {
        this.countryPhoneCode = countryPhoneCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWalletCurrencyCode() {
        return walletCurrencyCode;
    }

    public void setWalletCurrencyCode(String walletCurrencyCode) {
        this.walletCurrencyCode = walletCurrencyCode;
    }

    public Integer getWallet() {
        return wallet;
    }

    public void setWallet(Integer wallet) {
        this.wallet = wallet;
    }

    public List<Object> getIsTrip() {
        return isTrip;
    }

    public void setIsTrip(List<Object> isTrip) {
        this.isTrip = isTrip;
    }

    public List<Object> getReceivedTripFromGender() {
        return receivedTripFromGender;
    }

    public void setReceivedTripFromGender(List<Object> receivedTripFromGender) {
        this.receivedTripFromGender = receivedTripFromGender;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }





    @Override
    public String toString() {
        return
                "Provider{" +
                        "provider_type = '" + providerType + '\'' +
                        ",provider_type_id = '" + providerTypeId + '\'' +
                        ",is_partner_approved_by_admin = '" + isPartnerApprovedByAdmin + '\'' +
                        ",country = '" + country + '\'' +
                        ",total_request = '" + totalRequest + '\'' +
                        ",app_version = '" + appVersion + '\'' +
                        ",received_trip_from_gender = '" + receivedTripFromGender + '\'' +
                        ",is_trip = '" + isTrip + '\'' +
                        ",bio = '" + bio + '\'' +
                        ",device_type = '" + deviceType + '\'' +
                        ",is_vehicle_document_uploaded = '" + isVehicleDocumentUploaded + '\'' +
                        ",last_transfered_date = '" + lastTransferredDate + '\'' +
                        ",location_updated_time = '" + locationUpdatedTime + '\'' +
                        ",password = '" + password + '\'' +
                        ",cancelled_request = '" + cancelledRequest + '\'' +
                        ",country_phone_code = '" + countryPhoneCode + '\'' +
                        ",bank_id = '" + bankId + '\'' +
                        ",__v = '" + v + '\'' +
                        ",device_unique_code = '" + deviceUniqueCode + '\'' +
                        ",is_use_google_distance = '" + isUseGoogleDistance + '\'' +
                        ",start_online_time = '" + startOnlineTime + '\'' +
                        ",vehicle_detail = '" + vehicleDetail + '\'' +
                        ",unique_id = '" + uniqueId + '\'' +
                        ",providerLocation = '" + providerLocation + '\'' +
                        ",last_transferred_date = '" + lastTransferredDate + '\'' +
                        ",bearing = '" + bearing + '\'' +
                        ",cityid = '" + cityid + '\'' +
                        ",zipcode = '" + zipcode + '\'' +
                        ",rate_count = '" + rateCount + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",rejected_request = '" + rejectedRequest + '\'' +
                        ",_id = '" + id + '\'' +
                        ",country_id = '" + countryId + '\'' +
                        ",gender = '" + gender + '\'' +
                        ",city = '" + city + '\'' +
                        ",device_timezone = '" + deviceTimezone + '\'' +
                        ",is_documents_expired = '" + isDocumentsExpired + '\'' +
                        ",is_document_uploaded = '" + isDocumentUploaded + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",providerPreviousLocation = '" + providerPreviousLocation + '\'' +
                        ",wallet_currency_code = '" + walletCurrencyCode + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",rate = '" + rate + '\'' +
                        ",social_unique_id = '" + socialUniqueId + '\'' +
                        ",first_name = '" + firstName + '\'' +
                        ",email = '" + email + '\'' +
                        ",car_model = '" + carModel + '\'' +
                        ",is_active = '" + isActive + '\'' +
                        ",address = '" + address + '\'' +
                        ",wallet = '" + wallet + '\'' +
                        ",languages = '" + languages + '\'' +
                        ",last_name = '" + lastName + '\'' +
                        ",completed_request = '" + completedRequest + '\'' +
                        ",picture = '" + picture + '\'' +
                        ",is_available = '" + isAvailable + '\'' +
                        ",token = '" + token + '\'' +
                        ",service_type = '" + serviceType + '\'' +
                        ",account_id = '" + accountId + '\'' +
                        ",accepted_request = '" + acceptedRequest + '\'' +
                        ",admintypeid = '" + admintypeid + '\'' +
                        ",device_token = '" + deviceToken + '\'' +
                        ",car_number = '" + carNumber + '\'' +
                        ",is_approved = '" + isApproved + '\'' +
                        ",login_by = '" + loginBy + '\'' +
                        "}";
    }



public class Services {

    @SerializedName("service3")
    @Expose
    private List<Service3> service3 = null;
    @SerializedName("service2")
    @Expose
    private List<Service2> service2 = null;
    @SerializedName("service1")
    @Expose
    private List<Service1> service1 = null;

    public List<Service3> getService3() {
        return service3;
    }

    public void setService3(List<Service3> service3) {
        this.service3 = service3;
    }

    public List<Service2> getService2() {
        return service2;
    }

    public void setService2(List<Service2> service2) {
        this.service2 = service2;
    }

    public List<Service1> getService1() {
        return service1;
    }

    public void setService1(List<Service1> service1) {
        this.service1 = service1;
    }
}

    public class Documents {

        @SerializedName("document_type7")
        @Expose
        private String documentType7;
        @SerializedName("documentExt7")
        @Expose
        private String documentExt7;
        @SerializedName("documentData7")
        @Expose
        private String documentData7;
        @SerializedName("document_type6")
        @Expose
        private String documentType6;
        @SerializedName("documentExt6")
        @Expose
        private String documentExt6;
        @SerializedName("documentData6")
        @Expose
        private String documentData6;
        @SerializedName("documentData5")
        @Expose
        private String documentData5;
        @SerializedName("document_type5")
        @Expose
        private String documentType5;
        @SerializedName("documentExt5")
        @Expose
        private String documentExt5;
        @SerializedName("document_type4")
        @Expose
        private String documentType4;
        @SerializedName("documentData4")
        @Expose
        private String documentData4;
        @SerializedName("documentExt4")
        @Expose
        private String documentExt4;
        @SerializedName("document_type3")
        @Expose
        private String documentType3;
        @SerializedName("documentData3")
        @Expose
        private String documentData3;
        @SerializedName("documentExt3")
        @Expose
        private String documentExt3;
        @SerializedName("document_type2")
        @Expose
        private String documentType2;
        @SerializedName("documentExt2")
        @Expose
        private String documentExt2;
        @SerializedName("documentData2")
        @Expose
        private String documentData2;
        @SerializedName("document_type")
        @Expose
        private String documentType;
        @SerializedName("documentExt")
        @Expose
        private String documentExt;
        @SerializedName("documentData")
        @Expose
        private String documentData;

        public String getDocumentType7() {
            return documentType7;
        }

        public void setDocumentType7(String documentType7) {
            this.documentType7 = documentType7;
        }

        public String getDocumentExt7() {
            return documentExt7;
        }

        public void setDocumentExt7(String documentExt7) {
            this.documentExt7 = documentExt7;
        }

        public String getDocumentData7() {
            return documentData7;
        }

        public void setDocumentData7(String documentData7) {
            this.documentData7 = documentData7;
        }

        public String getDocumentType6() {
            return documentType6;
        }

        public void setDocumentType6(String documentType6) {
            this.documentType6 = documentType6;
        }

        public String getDocumentExt6() {
            return documentExt6;
        }

        public void setDocumentExt6(String documentExt6) {
            this.documentExt6 = documentExt6;
        }

        public String getDocumentData6() {
            return documentData6;
        }

        public void setDocumentData6(String documentData6) {
            this.documentData6 = documentData6;
        }

        public String getDocumentData5() {
            return documentData5;
        }

        public void setDocumentData5(String documentData5) {
            this.documentData5 = documentData5;
        }

        public String getDocumentType5() {
            return documentType5;
        }

        public void setDocumentType5(String documentType5) {
            this.documentType5 = documentType5;
        }

        public String getDocumentExt5() {
            return documentExt5;
        }

        public void setDocumentExt5(String documentExt5) {
            this.documentExt5 = documentExt5;
        }

        public String getDocumentType4() {
            return documentType4;
        }

        public void setDocumentType4(String documentType4) {
            this.documentType4 = documentType4;
        }

        public String getDocumentData4() {
            return documentData4;
        }

        public void setDocumentData4(String documentData4) {
            this.documentData4 = documentData4;
        }

        public String getDocumentExt4() {
            return documentExt4;
        }

        public void setDocumentExt4(String documentExt4) {
            this.documentExt4 = documentExt4;
        }

        public String getDocumentType3() {
            return documentType3;
        }

        public void setDocumentType3(String documentType3) {
            this.documentType3 = documentType3;
        }

        public String getDocumentData3() {
            return documentData3;
        }

        public void setDocumentData3(String documentData3) {
            this.documentData3 = documentData3;
        }

        public String getDocumentExt3() {
            return documentExt3;
        }

        public void setDocumentExt3(String documentExt3) {
            this.documentExt3 = documentExt3;
        }

        public String getDocumentType2() {
            return documentType2;
        }

        public void setDocumentType2(String documentType2) {
            this.documentType2 = documentType2;
        }

        public String getDocumentExt2() {
            return documentExt2;
        }

        public void setDocumentExt2(String documentExt2) {
            this.documentExt2 = documentExt2;
        }

        public String getDocumentData2() {
            return documentData2;
        }

        public void setDocumentData2(String documentData2) {
            this.documentData2 = documentData2;
        }

        public String getDocumentType() {
            return documentType;
        }

        public void setDocumentType(String documentType) {
            this.documentType = documentType;
        }

        public String getDocumentExt() {
            return documentExt;
        }

        public void setDocumentExt(String documentExt) {
            this.documentExt = documentExt;
        }

        public String getDocumentData() {
            return documentData;
        }

        public void setDocumentData(String documentData) {
            this.documentData = documentData;
        }

    }
}


