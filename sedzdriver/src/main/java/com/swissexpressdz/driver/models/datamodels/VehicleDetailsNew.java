package com.swissexpressdz.driver.models.datamodels;

import com.google.gson.annotations.SerializedName;

public class VehicleDetailsNew {




        @SerializedName("hotel_commission")

        private Integer hotelCommission;
        @SerializedName("provider_commission")

        private Integer providerCommission;
        @SerializedName("provider_tax")

        private Integer providerTax;
        @SerializedName("user_tax")

        private Integer userTax;
        @SerializedName("provider_miscellaneous_fee")

        private Integer providerMiscellaneousFee;
        @SerializedName("user_miscellaneous_fee")

        private Integer userMiscellaneousFee;
        @SerializedName("max_space")

        private Integer maxSpace;
        @SerializedName("min_fare")

        private Object minFare;
        @SerializedName("tax")

        private Integer tax;
        @SerializedName("price_for_total_time")

        private Integer priceForTotalTime;
        @SerializedName("is_surge_hours")

        private Integer isSurgeHours;
        @SerializedName("surge_end_hour")

        private Integer surgeEndHour;
        @SerializedName("surge_start_hour")

        private Integer surgeStartHour;
        @SerializedName("is_car_rental_business")

        private Integer isCarRentalBusiness;
        @SerializedName("is_buiesness")

        private Integer isBuiesness;
        @SerializedName("provider_profit")

        private Object providerProfit;
        @SerializedName("day_price_car_rent")

        private String dayPriceCarRent;
        @SerializedName("hourly_price_car_rent")

        private String hourlyPriceCarRent;
        @SerializedName("cancellation_fee")

        private String cancellationFee;
        @SerializedName("hourly_price")
        private Object hourlyPrice;
        @SerializedName("price_for_waiting_time")
        private String priceForWaitingTime;
        @SerializedName("waiting_time_start_after_minute")
        private String waitingTimeStartAfterMinute;
        @SerializedName("price_per_unit_distance")

        private String pricePerUnitDistance;
        @SerializedName("base_price")
        private String basePrice;
        @SerializedName("base_price_distance")
        private String basePriceDistance;
        @SerializedName("deposit")
        private Integer deposit;
        @SerializedName("cityname")
        private String cityname;
        @SerializedName("countryname")

        private String countryname;
        @SerializedName("typename")

        private String typename;
        @SerializedName("typeid")

        private String typeid;
        @SerializedName("cityid")

        private Object cityid;
        @SerializedName("countryid")

        private String countryid;
        @SerializedName("citytypevtcid")

        private String citytypevtcid;
        @SerializedName("providerid")

        private String providerid;
        @SerializedName("place")

        private String place;
        @SerializedName("km")

        private String km;
        @SerializedName("image")

        private String image;
        @SerializedName("is_document_expired")

        private Boolean isDocumentExpired;
        @SerializedName("is_document_uploaded")

        private Boolean isDocumentUploaded;
        @SerializedName("is_selected")

        private Boolean isSelected;
        @SerializedName("admin_type_id")

        private String adminTypeId;
        @SerializedName("service_type")

        private String serviceType;
        @SerializedName("passing_year")

        private String passingYear;

        @SerializedName("brand")
        private String brand;


    @SerializedName("seats")
        private String seats;


        @SerializedName("color")
        private String color;

        @SerializedName("model")
        private String model;

        @SerializedName("plate_no")
        private String plateNo;

        @SerializedName("name")
        private String name;
        @SerializedName("_id")

        private String id;
        @SerializedName("type_image_url")

        private String typeImageUrl;

        public Integer getHotelCommission() {
            return hotelCommission;
        }

        public void setHotelCommission(Integer hotelCommission) {
            this.hotelCommission = hotelCommission;
        }

        public Integer getProviderCommission() {
            return providerCommission;
        }

        public void setProviderCommission(Integer providerCommission) {
            this.providerCommission = providerCommission;
        }

        public Integer getProviderTax() {
            return providerTax;
        }

        public void setProviderTax(Integer providerTax) {
            this.providerTax = providerTax;
        }

        public Integer getUserTax() {
            return userTax;
        }

        public void setUserTax(Integer userTax) {
            this.userTax = userTax;
        }

        public Integer getProviderMiscellaneousFee() {
            return providerMiscellaneousFee;
        }

        public void setProviderMiscellaneousFee(Integer providerMiscellaneousFee) {
            this.providerMiscellaneousFee = providerMiscellaneousFee;
        }

        public Integer getUserMiscellaneousFee() {
            return userMiscellaneousFee;
        }

        public void setUserMiscellaneousFee(Integer userMiscellaneousFee) {
            this.userMiscellaneousFee = userMiscellaneousFee;
        }
        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public Integer getMaxSpace() {
            return maxSpace;
        }

        public void setMaxSpace(Integer maxSpace) {
            this.maxSpace = maxSpace;
        }

        public Object getMinFare() {
            return minFare;
        }

        public void setMinFare(Object minFare) {
            this.minFare = minFare;
        }

        public Integer getTax() {
            return tax;
        }

        public void setTax(Integer tax) {
            this.tax = tax;
        }

        public Integer getPriceForTotalTime() {
            return priceForTotalTime;
        }

        public void setPriceForTotalTime(Integer priceForTotalTime) {
            this.priceForTotalTime = priceForTotalTime;
        }

        public Integer getIsSurgeHours() {
            return isSurgeHours;
        }

        public void setIsSurgeHours(Integer isSurgeHours) {
            this.isSurgeHours = isSurgeHours;
        }

        public Integer getSurgeEndHour() {
            return surgeEndHour;
        }

        public void setSurgeEndHour(Integer surgeEndHour) {
            this.surgeEndHour = surgeEndHour;
        }

        public Integer getSurgeStartHour() {
            return surgeStartHour;
        }

        public void setSurgeStartHour(Integer surgeStartHour) {
            this.surgeStartHour = surgeStartHour;
        }

        public Integer getIsCarRentalBusiness() {
            return isCarRentalBusiness;
        }

        public void setIsCarRentalBusiness(Integer isCarRentalBusiness) {
            this.isCarRentalBusiness = isCarRentalBusiness;
        }

        public Integer getIsBuiesness() {
            return isBuiesness;
        }

        public void setIsBuiesness(Integer isBuiesness) {
            this.isBuiesness = isBuiesness;
        }

        public Object getProviderProfit() {
            return providerProfit;
        }

        public void setProviderProfit(Object providerProfit) {
            this.providerProfit = providerProfit;
        }

        public String getDayPriceCarRent() {
            return dayPriceCarRent;
        }

        public void setDayPriceCarRent(String dayPriceCarRent) {
            this.dayPriceCarRent = dayPriceCarRent;
        }

        public String getHourlyPriceCarRent() {
            return hourlyPriceCarRent;
        }

        public void setHourlyPriceCarRent(String hourlyPriceCarRent) {
            this.hourlyPriceCarRent = hourlyPriceCarRent;
        }

        public String getCancellationFee() {
            return cancellationFee;
        }

        public void setCancellationFee(String cancellationFee) {
            this.cancellationFee = cancellationFee;
        }

        public Object getHourlyPrice() {
            return hourlyPrice;
        }

        public void setHourlyPrice(Object hourlyPrice) {
            this.hourlyPrice = hourlyPrice;
        }

        public String getPriceForWaitingTime() {
            return priceForWaitingTime;
        }

        public void setPriceForWaitingTime(String priceForWaitingTime) {
            this.priceForWaitingTime = priceForWaitingTime;
        }

        public String getWaitingTimeStartAfterMinute() {
            return waitingTimeStartAfterMinute;
        }

        public void setWaitingTimeStartAfterMinute(String waitingTimeStartAfterMinute) {
            this.waitingTimeStartAfterMinute = waitingTimeStartAfterMinute;
        }

        public String getPricePerUnitDistance() {
            return pricePerUnitDistance;
        }

        public void setPricePerUnitDistance(String pricePerUnitDistance) {
            this.pricePerUnitDistance = pricePerUnitDistance;
        }

        public String getBasePrice() {
            return basePrice;
        }

        public void setBasePrice(String basePrice) {
            this.basePrice = basePrice;
        }

        public String getBasePriceDistance() {
            return basePriceDistance;
        }

        public void setBasePriceDistance(String basePriceDistance) {
            this.basePriceDistance = basePriceDistance;
        }

        public Integer getDeposit() {
            return deposit;
        }

        public void setDeposit(Integer deposit) {
            this.deposit = deposit;
        }

        public String getCityname() {
            return cityname;
        }

        public void setCityname(String cityname) {
            this.cityname = cityname;
        }

        public String getCountryname() {
            return countryname;
        }

        public void setCountryname(String countryname) {
            this.countryname = countryname;
        }

        public String getTypename() {
            return typename;
        }

        public void setTypename(String typename) {
            this.typename = typename;
        }

        public String getTypeid() {
            return typeid;
        }

        public void setTypeid(String typeid) {
            this.typeid = typeid;
        }

        public Object getCityid() {
            return cityid;
        }

        public void setCityid(Object cityid) {
            this.cityid = cityid;
        }

        public String getCountryid() {
            return countryid;
        }

        public void setCountryid(String countryid) {
            this.countryid = countryid;
        }

        public String getCitytypevtcid() {
            return citytypevtcid;
        }

        public void setCitytypevtcid(String citytypevtcid) {
            this.citytypevtcid = citytypevtcid;
        }

        public String getProviderid() {
            return providerid;
        }

        public void setProviderid(String providerid) {
            this.providerid = providerid;
        }

        public String getPlace() {
            return place;
        }

        public void setPlace(String place) {
            this.place = place;
        }

        public String getKm() {
            return km;
        }

        public void setKm(String km) {
            this.km = km;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Boolean getIsDocumentExpired() {
            return isDocumentExpired;
        }

        public void setIsDocumentExpired(Boolean isDocumentExpired) {
            this.isDocumentExpired = isDocumentExpired;
        }

        public Boolean getIsDocumentUploaded() {
            return isDocumentUploaded;
        }

        public void setIsDocumentUploaded(Boolean isDocumentUploaded) {
            this.isDocumentUploaded = isDocumentUploaded;
        }

        public Boolean getIsSelected() {
            return isSelected;
        }

        public void setIsSelected(Boolean isSelected) {
            this.isSelected = isSelected;
        }

        public String getAdminTypeId() {
            return adminTypeId;
        }

        public void setAdminTypeId(String adminTypeId) {
            this.adminTypeId = adminTypeId;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public String getPassingYear() {
            return passingYear;
        }

        public void setPassingYear(String passingYear) {
            this.passingYear = passingYear;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getPlateNo() {
            return plateNo;
        }

        public void setPlateNo(String plateNo) {
            this.plateNo = plateNo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTypeImageUrl() {
            return typeImageUrl;
        }

        public void setTypeImageUrl(String typeImageUrl) {
            this.typeImageUrl = typeImageUrl;
        }
    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }


        @Override
        public String toString() {
            return
                    "VehicleDetail{" +
                            "passing_year = '" + passingYear + '\'' +
                            ",service_type = '" + serviceType + '\'' +
                            ",color = '" + color + '\'' +
                            //    ",accessibility = '" + accessibility + '\'' +
                            ",is_selected = '" + isSelected + '\'' +
                            ",is_documents_expired = '" + isDocumentExpired + '\'' +
                            ",plate_no = '" + plateNo + '\'' +
                            ",admin_type_id = '" + adminTypeId + '\'' +
                            ",name = '" + name + '\'' +
                            ",model = '" + model + '\'' +
                            ",_id = '" + id + '\'' +
                            ",is_vehicle_documents_expired = '" + '\'' +
                            "}";
        }

}
