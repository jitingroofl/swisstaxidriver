package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EditVTCResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("vtc_type")
    @Expose
    private VtcType vtcType;
    @SerializedName("error_code")
    private int errorCode;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public VtcType getVtcType() {
        return vtcType;
    }

    public void setVtcType(VtcType vtcType) {
        this.vtcType = vtcType;
    }
    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public class SurgeHour {

        @SerializedName("is_surge")
        @Expose
        private Boolean isSurge;
        @SerializedName("day")
        @Expose
        private String day;
        @SerializedName("day_time")
        @Expose
        private List<Object> dayTime = null;

        public Boolean getIsSurge() {
            return isSurge;
        }

        public void setIsSurge(Boolean isSurge) {
            this.isSurge = isSurge;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public List<Object> getDayTime() {
            return dayTime;
        }

        public void setDayTime(List<Object> dayTime) {
            this.dayTime = dayTime;
        }

    }

    public class VtcType {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("countryid")
        @Expose
        private String countryid;
        @SerializedName("typeid")
        @Expose
        private String typeid;
        @SerializedName("hourly_price_min")
        @Expose
        private double hourlyPriceMin;
        @SerializedName("hourly_price_max")
        @Expose
        private double hourlyPriceMax;
        @SerializedName("hourly_price_car_rent_min")
        @Expose
        private double hourlyPriceCarRentMin;
        @SerializedName("hourly_price_car_rent_max")
        @Expose
        private double hourlyPriceCarRentMax;
        @SerializedName("day_price_car_rent_min")
        @Expose
        private double dayPriceCarRentMin;
        @SerializedName("day_price_car_rent_max")
        @Expose
        private double dayPriceCarRentMax;
        @SerializedName("__v")
        @Expose
        private double v;
        @SerializedName("deposit")
        @Expose
        private double deposit;
        @SerializedName("hotel_commission")
        @Expose
        private double hotelCommission;
        @SerializedName("provider_commission")
        @Expose
        private double providerCommission;
        @SerializedName("zone_ids")
        @Expose
        private List<Object> zoneIds = null;
        @SerializedName("total_provider_in_zone_queue")
        @Expose
        private List<Object> totalProviderInZoneQueue = null;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("provider_tax")
        @Expose
        private double providerTax;
        @SerializedName("user_tax")
        @Expose
        private double userTax;
        @SerializedName("provider_miscellaneous_fee")
        @Expose
        private double providerMiscellaneousFee;
        @SerializedName("user_miscellaneous_fee")
        @Expose
        private double userMiscellaneousFee;
        @SerializedName("cancellation_fee_max")
        @Expose
        private double cancellationFeeMax;
        @SerializedName("cancellation_fee_min")
        @Expose
        private double cancellationFeeMin;
        @SerializedName("max_space")
        @Expose
        private double maxSpace;
        @SerializedName("tax")
        @Expose
        private double tax;
        @SerializedName("price_for_waiting_time_max")
        @Expose
        private Double priceForWaitingTimeMax;
        @SerializedName("price_for_waiting_time_min")
        @Expose
        private Double priceForWaitingTimeMin;
        @SerializedName("waiting_time_start_after_minute_max")
        @Expose
        private double waitingTimeStartAfterMinuteMax;
        @SerializedName("waiting_time_start_after_minute_min")
        @Expose
        private double waitingTimeStartAfterMinuteMin;
        @SerializedName("price_for_total_time")
        @Expose
        private double priceForTotalTime;
        @SerializedName("price_per_unit_distance_max")
        @Expose
        private Double pricePerUnitDistanceMax;
        @SerializedName("price_per_unit_distance_min")
        @Expose
        private Double pricePerUnitDistanceMin;
        @SerializedName("base_price_max")
        @Expose
        private double basePriceMax;
        @SerializedName("base_price_min")
        @Expose
        private double basePriceMin;
        @SerializedName("base_price_time")
        @Expose
        private double basePriceTime;
        @SerializedName("base_price_distance_max")
        @Expose
        private double basePriceDistanceMax;
        @SerializedName("base_price_distance_min")
        @Expose
        private double basePriceDistanceMin;
        @SerializedName("car_rental_ids")
        @Expose
        private List<Object> carRentalIds = null;
        @SerializedName("is_car_rental_business")
        @Expose
        private double isCarRentalBusiness;
        @SerializedName("typename")
        @Expose
        private String typename;
        @SerializedName("provider_profit_max")
        @Expose
        private double providerProfitMax;
        @SerializedName("provider_profit_min")
        @Expose
        private double providerProfitMin;
        @SerializedName("min_fare_max")
        @Expose
        private double minFareMax;
        @SerializedName("min_fare_min")
        @Expose
        private double minFareMin;
        @SerializedName("type_image")
        @Expose
        private String typeImage;
        @SerializedName("cityname")
        @Expose
        private String cityname;
        @SerializedName("countryname")
        @Expose
        private String countryname;
        @SerializedName("is_buiesness")
        @Expose
        private double isBuiesness;
        @SerializedName("surge_hours")
        @Expose
        private List<SurgeHour> surgeHours = null;
        @SerializedName("rich_area_surge")
        @Expose
        private List<Object> richAreaSurge = null;
        @SerializedName("is_zone")
        @Expose
        private double isZone;
        @SerializedName("is_surge_hours")
        @Expose
        private double isSurgeHours;
        @SerializedName("surge_end_hour")
        @Expose
        private double surgeEndHour;
        @SerializedName("surge_start_hour")
        @Expose
        private double surgeStartHour;
        @SerializedName("surge_multiplier")
        @Expose
        private double surgeMultiplier;
        @SerializedName("is_hide")
        @Expose
        private double isHide;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCountryid() {
            return countryid;
        }

        public void setCountryid(String countryid) {
            this.countryid = countryid;
        }

        public String getTypeid() {
            return typeid;
        }

        public void setTypeid(String typeid) {
            this.typeid = typeid;
        }

        public double getHourlyPriceMin() {
            return hourlyPriceMin;
        }

        public void setHourlyPriceMin(double hourlyPriceMin) {
            this.hourlyPriceMin = hourlyPriceMin;
        }

        public double getHourlyPriceMax() {
            return hourlyPriceMax;
        }

        public void setHourlyPriceMax(double hourlyPriceMax) {
            this.hourlyPriceMax = hourlyPriceMax;
        }

        public double getHourlyPriceCarRentMin() {
            return hourlyPriceCarRentMin;
        }

        public void setHourlyPriceCarRentMin(double hourlyPriceCarRentMin) {
            this.hourlyPriceCarRentMin = hourlyPriceCarRentMin;
        }

        public double getHourlyPriceCarRentMax() {
            return hourlyPriceCarRentMax;
        }

        public void setHourlyPriceCarRentMax(double hourlyPriceCarRentMax) {
            this.hourlyPriceCarRentMax = hourlyPriceCarRentMax;
        }

        public double getDayPriceCarRentMin() {
            return dayPriceCarRentMin;
        }

        public void setDayPriceCarRentMin(double dayPriceCarRentMin) {
            this.dayPriceCarRentMin = dayPriceCarRentMin;
        }

        public double getDayPriceCarRentMax() {
            return dayPriceCarRentMax;
        }

        public void setDayPriceCarRentMax(double dayPriceCarRentMax) {
            this.dayPriceCarRentMax = dayPriceCarRentMax;
        }

        public double getV() {
            return v;
        }

        public void setV(double v) {
            this.v = v;
        }

        public double getDeposit() {
            return deposit;
        }

        public void setDeposit(double deposit) {
            this.deposit = deposit;
        }

        public double getHotelCommission() {
            return hotelCommission;
        }

        public void setHotelCommission(double hotelCommission) {
            this.hotelCommission = hotelCommission;
        }

        public double getProviderCommission() {
            return providerCommission;
        }

        public void setProviderCommission(double providerCommission) {
            this.providerCommission = providerCommission;
        }

        public List<Object> getZoneIds() {
            return zoneIds;
        }

        public void setZoneIds(List<Object> zoneIds) {
            this.zoneIds = zoneIds;
        }

        public List<Object> getTotalProviderInZoneQueue() {
            return totalProviderInZoneQueue;
        }

        public void setTotalProviderInZoneQueue(List<Object> totalProviderInZoneQueue) {
            this.totalProviderInZoneQueue = totalProviderInZoneQueue;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public double getProviderTax() {
            return providerTax;
        }

        public void setProviderTax(double providerTax) {
            this.providerTax = providerTax;
        }

        public double getUserTax() {
            return userTax;
        }

        public void setUserTax(double userTax) {
            this.userTax = userTax;
        }

        public double getProviderMiscellaneousFee() {
            return providerMiscellaneousFee;
        }

        public void setProviderMiscellaneousFee(double providerMiscellaneousFee) {
            this.providerMiscellaneousFee = providerMiscellaneousFee;
        }

        public double getUserMiscellaneousFee() {
            return userMiscellaneousFee;
        }

        public void setUserMiscellaneousFee(double userMiscellaneousFee) {
            this.userMiscellaneousFee = userMiscellaneousFee;
        }

        public double getCancellationFeeMax() {
            return cancellationFeeMax;
        }

        public void setCancellationFeeMax(double cancellationFeeMax) {
            this.cancellationFeeMax = cancellationFeeMax;
        }

        public double getCancellationFeeMin() {
            return cancellationFeeMin;
        }

        public void setCancellationFeeMin(double cancellationFeeMin) {
            this.cancellationFeeMin = cancellationFeeMin;
        }

        public double getMaxSpace() {
            return maxSpace;
        }

        public void setMaxSpace(double maxSpace) {
            this.maxSpace = maxSpace;
        }

        public double getTax() {
            return tax;
        }

        public void setTax(double tax) {
            this.tax = tax;
        }

        public Double getPriceForWaitingTimeMax() {
            return priceForWaitingTimeMax;
        }

        public void setPriceForWaitingTimeMax(Double priceForWaitingTimeMax) {
            this.priceForWaitingTimeMax = priceForWaitingTimeMax;
        }

        public Double getPriceForWaitingTimeMin() {
            return priceForWaitingTimeMin;
        }

        public void setPriceForWaitingTimeMin(Double priceForWaitingTimeMin) {
            this.priceForWaitingTimeMin = priceForWaitingTimeMin;
        }

        public double getWaitingTimeStartAfterMinuteMax() {
            return waitingTimeStartAfterMinuteMax;
        }

        public void setWaitingTimeStartAfterMinuteMax(double waitingTimeStartAfterMinuteMax) {
            this.waitingTimeStartAfterMinuteMax = waitingTimeStartAfterMinuteMax;
        }

        public double getWaitingTimeStartAfterMinuteMin() {
            return waitingTimeStartAfterMinuteMin;
        }

        public void setWaitingTimeStartAfterMinuteMin(double waitingTimeStartAfterMinuteMin) {
            this.waitingTimeStartAfterMinuteMin = waitingTimeStartAfterMinuteMin;
        }

        public double getPriceForTotalTime() {
            return priceForTotalTime;
        }

        public void setPriceForTotalTime(double priceForTotalTime) {
            this.priceForTotalTime = priceForTotalTime;
        }

        public Double getPricePerUnitDistanceMax() {
            return pricePerUnitDistanceMax;
        }

        public void setPricePerUnitDistanceMax(Double pricePerUnitDistanceMax) {
            this.pricePerUnitDistanceMax = pricePerUnitDistanceMax;
        }

        public Double getPricePerUnitDistanceMin() {
            return pricePerUnitDistanceMin;
        }

        public void setPricePerUnitDistanceMin(Double pricePerUnitDistanceMin) {
            this.pricePerUnitDistanceMin = pricePerUnitDistanceMin;
        }

        public double getBasePriceMax() {
            return basePriceMax;
        }

        public void setBasePriceMax(double basePriceMax) {
            this.basePriceMax = basePriceMax;
        }

        public double getBasePriceMin() {
            return basePriceMin;
        }

        public void setBasePriceMin(double basePriceMin) {
            this.basePriceMin = basePriceMin;
        }

        public double getBasePriceTime() {
            return basePriceTime;
        }

        public void setBasePriceTime(double basePriceTime) {
            this.basePriceTime = basePriceTime;
        }

        public double getBasePriceDistanceMax() {
            return basePriceDistanceMax;
        }

        public void setBasePriceDistanceMax(double basePriceDistanceMax) {
            this.basePriceDistanceMax = basePriceDistanceMax;
        }

        public double getBasePriceDistanceMin() {
            return basePriceDistanceMin;
        }

        public void setBasePriceDistanceMin(double basePriceDistanceMin) {
            this.basePriceDistanceMin = basePriceDistanceMin;
        }

        public List<Object> getCarRentalIds() {
            return carRentalIds;
        }

        public void setCarRentalIds(List<Object> carRentalIds) {
            this.carRentalIds = carRentalIds;
        }

        public double getIsCarRentalBusiness() {
            return isCarRentalBusiness;
        }

        public void setIsCarRentalBusiness(double isCarRentalBusiness) {
            this.isCarRentalBusiness = isCarRentalBusiness;
        }

        public String getTypename() {
            return typename;
        }

        public void setTypename(String typename) {
            this.typename = typename;
        }

        public double getProviderProfitMax() {
            return providerProfitMax;
        }

        public void setProviderProfitMax(double providerProfitMax) {
            this.providerProfitMax = providerProfitMax;
        }

        public double getProviderProfitMin() {
            return providerProfitMin;
        }

        public void setProviderProfitMin(double providerProfitMin) {
            this.providerProfitMin = providerProfitMin;
        }

        public double getMinFareMax() {
            return minFareMax;
        }

        public void setMinFareMax(double minFareMax) {
            this.minFareMax = minFareMax;
        }

        public double getMinFareMin() {
            return minFareMin;
        }

        public void setMinFareMin(double minFareMin) {
            this.minFareMin = minFareMin;
        }

        public String getTypeImage() {
            return typeImage;
        }

        public void setTypeImage(String typeImage) {
            this.typeImage = typeImage;
        }

        public String getCityname() {
            return cityname;
        }

        public void setCityname(String cityname) {
            this.cityname = cityname;
        }

        public String getCountryname() {
            return countryname;
        }

        public void setCountryname(String countryname) {
            this.countryname = countryname;
        }

        public double getIsBuiesness() {
            return isBuiesness;
        }

        public void setIsBuiesness(double isBuiesness) {
            this.isBuiesness = isBuiesness;
        }

        public List<SurgeHour> getSurgeHours() {
            return surgeHours;
        }

        public void setSurgeHours(List<SurgeHour> surgeHours) {
            this.surgeHours = surgeHours;
        }

        public List<Object> getRichAreaSurge() {
            return richAreaSurge;
        }

        public void setRichAreaSurge(List<Object> richAreaSurge) {
            this.richAreaSurge = richAreaSurge;
        }

        public double getIsZone() {
            return isZone;
        }

        public void setIsZone(double isZone) {
            this.isZone = isZone;
        }

        public double getIsSurgeHours() {
            return isSurgeHours;
        }

        public void setIsSurgeHours(double isSurgeHours) {
            this.isSurgeHours = isSurgeHours;
        }

        public double getSurgeEndHour() {
            return surgeEndHour;
        }

        public void setSurgeEndHour(double surgeEndHour) {
            this.surgeEndHour = surgeEndHour;
        }

        public double getSurgeStartHour() {
            return surgeStartHour;
        }

        public void setSurgeStartHour(double surgeStartHour) {
            this.surgeStartHour = surgeStartHour;
        }

        public double getSurgeMultiplier() {
            return surgeMultiplier;
        }

        public void setSurgeMultiplier(double surgeMultiplier) {
            this.surgeMultiplier = surgeMultiplier;
        }

        public double getIsHide() {
            return isHide;
        }

        public void setIsHide(double isHide) {
            this.isHide = isHide;
        }
    }
}
