package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentNew {
    @SerializedName("vehicle_documentData")
    @Expose
    private String vehicleDocumentData;
    @SerializedName("vehicle_documentExt")
    @Expose
    private String vehicleDocumentExt;
    @SerializedName("vehicle_document_type")
    @Expose
    private String vehicleDocumentType;
    @SerializedName("vehicle_documentData2")
    @Expose
    private String vehicleDocumentData2;
    @SerializedName("vehicle_documentExt2")
    @Expose
    private String vehicleDocumentExt2;
    @SerializedName("vehicle_document_type2")
    @Expose
    private String vehicleDocumentType2;
    @SerializedName("vehicle_documentData3")
    @Expose
    private String vehicleDocumentData3;
    @SerializedName("vehicle_documentExt3")
    @Expose
    private String vehicleDocumentExt3;
    @SerializedName("vehicle_document_type3")
    @Expose
    private String vehicleDocumentType3;
    @SerializedName("vehicle_documentData4")
    @Expose
    private String vehicleDocumentData4;
    @SerializedName("vehicle_documentExt4")
    @Expose
    private String vehicleDocumentExt4;
    @SerializedName("vehicle_document_type4")
    @Expose
    private String vehicleDocumentType4;

    public String getVehicleDocumentData() {
        return vehicleDocumentData;
    }

    public void setVehicleDocumentData(String vehicleDocumentData) {
        this.vehicleDocumentData = vehicleDocumentData;
    }

    public String getVehicleDocumentExt() {
        return vehicleDocumentExt;
    }

    public void setVehicleDocumentExt(String vehicleDocumentExt) {
        this.vehicleDocumentExt = vehicleDocumentExt;
    }

    public String getVehicleDocumentType() {
        return vehicleDocumentType;
    }

    public void setVehicleDocumentType(String vehicleDocumentType) {
        this.vehicleDocumentType = vehicleDocumentType;
    }

    public String getVehicleDocumentData2() {
        return vehicleDocumentData2;
    }

    public void setVehicleDocumentData2(String vehicleDocumentData2) {
        this.vehicleDocumentData2 = vehicleDocumentData2;
    }

    public String getVehicleDocumentExt2() {
        return vehicleDocumentExt2;
    }

    public void setVehicleDocumentExt2(String vehicleDocumentExt2) {
        this.vehicleDocumentExt2 = vehicleDocumentExt2;
    }

    public String getVehicleDocumentType2() {
        return vehicleDocumentType2;
    }

    public void setVehicleDocumentType2(String vehicleDocumentType2) {
        this.vehicleDocumentType2 = vehicleDocumentType2;
    }

    public String getVehicleDocumentData3() {
        return vehicleDocumentData3;
    }

    public void setVehicleDocumentData3(String vehicleDocumentData3) {
        this.vehicleDocumentData3 = vehicleDocumentData3;
    }

    public String getVehicleDocumentExt3() {
        return vehicleDocumentExt3;
    }

    public void setVehicleDocumentExt3(String vehicleDocumentExt3) {
        this.vehicleDocumentExt3 = vehicleDocumentExt3;
    }

    public String getVehicleDocumentType3() {
        return vehicleDocumentType3;
    }

    public void setVehicleDocumentType3(String vehicleDocumentType3) {
        this.vehicleDocumentType3 = vehicleDocumentType3;
    }

    public String getVehicleDocumentData4() {
        return vehicleDocumentData4;
    }

    public void setVehicleDocumentData4(String vehicleDocumentData4) {
        this.vehicleDocumentData4 = vehicleDocumentData4;
    }

    public String getVehicleDocumentExt4() {
        return vehicleDocumentExt4;
    }

    public void setVehicleDocumentExt4(String vehicleDocumentExt4) {
        this.vehicleDocumentExt4 = vehicleDocumentExt4;
    }

    public String getVehicleDocumentType4() {
        return vehicleDocumentType4;
    }

    public void setVehicleDocumentType4(String vehicleDocumentType4) {
        this.vehicleDocumentType4 = vehicleDocumentType4;
    }
}


