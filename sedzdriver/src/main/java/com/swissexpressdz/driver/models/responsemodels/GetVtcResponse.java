package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.swissexpressdz.driver.AddVTCActivity;
import com.swissexpressdz.driver.utils.Utils;

import java.util.List;

public class GetVtcResponse {
    @Expose
    private Boolean success;
    @SerializedName("provider_city_type_vtc")
    @Expose
    private List<ProviderCityTypeVtc> providerCityTypeVtc = null;

    @SerializedName("error_code")
    private int errorCode;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<ProviderCityTypeVtc> getProviderCityTypeVtc() {
        return providerCityTypeVtc;
    }

    public void setProviderCityTypeVtc(List<ProviderCityTypeVtc> providerCityTypeVtc) {
        this.providerCityTypeVtc = providerCityTypeVtc;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public class ProviderCityTypeVtc {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("providerid")
        @Expose
        private String providerid;
        @SerializedName("citytypevtcid")
        @Expose
        private String citytypevtcid;
        @SerializedName("countryid")
        @Expose
        private String countryid;
        @SerializedName("cityid")
        @Expose
        private String cityid;
        @SerializedName("typeid")
        @Expose
        private String typeid;
        @SerializedName("hourly_price")
        @Expose
        private Integer hourlyPrice;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("surge_hours")
        @Expose
        private List<SurgeHour> surgeHours = null;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("rich_area_surge")
        @Expose
        private List<Object> richAreaSurge = null;
        @SerializedName("is_zone")
        @Expose
        private Integer isZone;
        @SerializedName("is_surge_hours")
        @Expose
        private Integer isSurgeHours;
        @SerializedName("surge_end_hour")
        @Expose
        private Integer surgeEndHour;
        @SerializedName("surge_start_hour")
        @Expose
        private Integer surgeStartHour;
        @SerializedName("surge_multiplier")
        @Expose
        private Integer surgeMultiplier;
        @SerializedName("is_hide")
        @Expose
        private Integer isHide;
        @SerializedName("hotel_commission")
        @Expose
        private Integer hotelCommission;
        @SerializedName("provider_commission")
        @Expose
        private Object providerCommission;
        @SerializedName("zone_ids")
        @Expose
        private List<Object> zoneIds = null;
        @SerializedName("total_provider_in_zone_queue")
        @Expose
        private List<Object> totalProviderInZoneQueue = null;
        @SerializedName("provider_tax")
        @Expose
        private Integer providerTax;
        @SerializedName("user_tax")
        @Expose
        private Integer userTax;
        @SerializedName("provider_miscellaneous_fee")
        @Expose
        private Integer providerMiscellaneousFee;
        @SerializedName("user_miscellaneous_fee")
        @Expose
        private Integer userMiscellaneousFee;
        @SerializedName("cancellation_fee")
        @Expose
        private Integer cancellationFee;
        @SerializedName("max_space")
        @Expose
        private Integer maxSpace;
        @SerializedName("tax")
        @Expose
        private Integer tax;
        @SerializedName("price_for_waiting_time")
        @Expose
        private Integer priceForWaitingTime;
        @SerializedName("waiting_time_start_after_minute")
        @Expose
        private Integer waitingTimeStartAfterMinute;
        @SerializedName("price_for_total_time")
        @Expose
        private Integer priceForTotalTime;
        @SerializedName("price_per_unit_distance")
        @Expose
        private Integer pricePerUnitDistance;
        @SerializedName("base_price")
        @Expose
        private Integer basePrice;
        @SerializedName("base_price_time")
        @Expose
        private Integer basePriceTime;
        @SerializedName("base_price_distance")
        @Expose
        private Integer basePriceDistance;
        @SerializedName("car_rental_ids")
        @Expose
        private List<Object> carRentalIds = null;
        @SerializedName("is_car_rental_business")
        @Expose
        private Integer isCarRentalBusiness;
        @SerializedName("typename")
        @Expose
        private String typename;
        @SerializedName("provider_profit")
        @Expose
        private Integer providerProfit;
        @SerializedName("min_fare")
        @Expose
        private Integer minFare;
        @SerializedName("type_image")
        @Expose
        private String typeImage;
        @SerializedName("cityname")
        @Expose
        private String cityname;
        @SerializedName("countryname")
        @Expose
        private String countryname;
        @SerializedName("is_buiesness")
        @Expose
        private Integer isBuiesness;
        @SerializedName("sub_type_status")
        @Expose
        private Boolean subTypeStatus;
        @SerializedName("day_price_car_rent")
        @Expose
        private Integer dayPriceCarRent;
        @SerializedName("hourly_price_car_rent")
        @Expose
        private Integer hourlyPriceCarRent;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProviderid() {
            return providerid;
        }

        public void setProviderid(String providerid) {
            this.providerid = providerid;
        }

        public String getCitytypevtcid() {
            return citytypevtcid;
        }

        public void setCitytypevtcid(String citytypevtcid) {
            this.citytypevtcid = citytypevtcid;
        }

        public String getCountryid() {
            return countryid;
        }

        public void setCountryid(String countryid) {
            this.countryid = countryid;
        }

        public String getCityid() {
            return cityid;
        }

        public void setCityid(String cityid) {
            this.cityid = cityid;
        }

        public String getTypeid() {
            return typeid;
        }

        public void setTypeid(String typeid) {
            this.typeid = typeid;
        }

        public Integer getHourlyPrice() {
            return hourlyPrice;
        }

        public void setHourlyPrice(Integer hourlyPrice) {
            this.hourlyPrice = hourlyPrice;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public List<SurgeHour> getSurgeHours() {
            return surgeHours;
        }

        public void setSurgeHours(List<SurgeHour> surgeHours) {
            this.surgeHours = surgeHours;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public List<Object> getRichAreaSurge() {
            return richAreaSurge;
        }

        public void setRichAreaSurge(List<Object> richAreaSurge) {
            this.richAreaSurge = richAreaSurge;
        }

        public Integer getIsZone() {
            return isZone;
        }

        public void setIsZone(Integer isZone) {
            this.isZone = isZone;
        }

        public Integer getIsSurgeHours() {
            return isSurgeHours;
        }

        public void setIsSurgeHours(Integer isSurgeHours) {
            this.isSurgeHours = isSurgeHours;
        }

        public Integer getSurgeEndHour() {
            return surgeEndHour;
        }

        public void setSurgeEndHour(Integer surgeEndHour) {
            this.surgeEndHour = surgeEndHour;
        }

        public Integer getSurgeStartHour() {
            return surgeStartHour;
        }

        public void setSurgeStartHour(Integer surgeStartHour) {
            this.surgeStartHour = surgeStartHour;
        }

        public Integer getSurgeMultiplier() {
            return surgeMultiplier;
        }

        public void setSurgeMultiplier(Integer surgeMultiplier) {
            this.surgeMultiplier = surgeMultiplier;
        }

        public Integer getIsHide() {
            return isHide;
        }

        public void setIsHide(Integer isHide) {
            this.isHide = isHide;
        }

        public Integer getHotelCommission() {
            return hotelCommission;
        }

        public void setHotelCommission(Integer hotelCommission) {
            this.hotelCommission = hotelCommission;
        }

        public Object getProviderCommission() {
            return providerCommission;
        }

        public void setProviderCommission(Object providerCommission) {
            this.providerCommission = providerCommission;
        }

        public List<Object> getZoneIds() {
            return zoneIds;
        }

        public void setZoneIds(List<Object> zoneIds) {
            this.zoneIds = zoneIds;
        }

        public List<Object> getTotalProviderInZoneQueue() {
            return totalProviderInZoneQueue;
        }

        public void setTotalProviderInZoneQueue(List<Object> totalProviderInZoneQueue) {
            this.totalProviderInZoneQueue = totalProviderInZoneQueue;
        }

        public Integer getProviderTax() {
            return providerTax;
        }

        public void setProviderTax(Integer providerTax) {
            this.providerTax = providerTax;
        }

        public Integer getUserTax() {
            return userTax;
        }

        public void setUserTax(Integer userTax) {
            this.userTax = userTax;
        }

        public Integer getProviderMiscellaneousFee() {
            return providerMiscellaneousFee;
        }

        public void setProviderMiscellaneousFee(Integer providerMiscellaneousFee) {
            this.providerMiscellaneousFee = providerMiscellaneousFee;
        }

        public Integer getUserMiscellaneousFee() {
            return userMiscellaneousFee;
        }

        public void setUserMiscellaneousFee(Integer userMiscellaneousFee) {
            this.userMiscellaneousFee = userMiscellaneousFee;
        }

        public Integer getCancellationFee() {
            return cancellationFee;
        }

        public void setCancellationFee(Integer cancellationFee) {
            this.cancellationFee = cancellationFee;
        }

        public Integer getMaxSpace() {
            return maxSpace;
        }

        public void setMaxSpace(Integer maxSpace) {
            this.maxSpace = maxSpace;
        }

        public Integer getTax() {
            return tax;
        }

        public void setTax(Integer tax) {
            this.tax = tax;
        }

        public Integer getPriceForWaitingTime() {
            return priceForWaitingTime;
        }

        public void setPriceForWaitingTime(Integer priceForWaitingTime) {
            this.priceForWaitingTime = priceForWaitingTime;
        }

        public Integer getWaitingTimeStartAfterMinute() {
            return waitingTimeStartAfterMinute;
        }

        public void setWaitingTimeStartAfterMinute(Integer waitingTimeStartAfterMinute) {
            this.waitingTimeStartAfterMinute = waitingTimeStartAfterMinute;
        }

        public Integer getPriceForTotalTime() {
            return priceForTotalTime;
        }

        public void setPriceForTotalTime(Integer priceForTotalTime) {
            this.priceForTotalTime = priceForTotalTime;
        }

        public Integer getPricePerUnitDistance() {
            return pricePerUnitDistance;
        }

        public void setPricePerUnitDistance(Integer pricePerUnitDistance) {
            this.pricePerUnitDistance = pricePerUnitDistance;
        }

        public Integer getBasePrice() {
            return basePrice;
        }

        public void setBasePrice(Integer basePrice) {
            this.basePrice = basePrice;
        }

        public Integer getBasePriceTime() {
            return basePriceTime;
        }

        public void setBasePriceTime(Integer basePriceTime) {
            this.basePriceTime = basePriceTime;
        }

        public Integer getBasePriceDistance() {
            return basePriceDistance;
        }

        public void setBasePriceDistance(Integer basePriceDistance) {
            this.basePriceDistance = basePriceDistance;
        }

        public List<Object> getCarRentalIds() {
            return carRentalIds;
        }

        public void setCarRentalIds(List<Object> carRentalIds) {
            this.carRentalIds = carRentalIds;
        }

        public Integer getIsCarRentalBusiness() {
            return isCarRentalBusiness;
        }

        public void setIsCarRentalBusiness(Integer isCarRentalBusiness) {
            this.isCarRentalBusiness = isCarRentalBusiness;
        }

        public String getTypename() {
            return typename;
        }

        public void setTypename(String typename) {
            this.typename = typename;
        }

        public Integer getProviderProfit() {
            return providerProfit;
        }

        public void setProviderProfit(Integer providerProfit) {
            this.providerProfit = providerProfit;
        }

        public Integer getMinFare() {
            return minFare;
        }

        public void setMinFare(Integer minFare) {
            this.minFare = minFare;
        }

        public String getTypeImage() {
            return typeImage;
        }

        public void setTypeImage(String typeImage) {
            this.typeImage = typeImage;
        }

        public String getCityname() {
            return cityname;
        }

        public void setCityname(String cityname) {
            this.cityname = cityname;
        }

        public String getCountryname() {
            return countryname;
        }

        public void setCountryname(String countryname) {
            this.countryname = countryname;
        }

        public Integer getIsBuiesness() {
            return isBuiesness;
        }

        public void setIsBuiesness(Integer isBuiesness) {
            this.isBuiesness = isBuiesness;
        }

        public Boolean getSubTypeStatus() {
            return subTypeStatus;
        }

        public void setSubTypeStatus(Boolean subTypeStatus) {
            this.subTypeStatus = subTypeStatus;
        }

        public Integer getDayPriceCarRent() {
            return dayPriceCarRent;
        }

        public void setDayPriceCarRent(Integer dayPriceCarRent) {
            this.dayPriceCarRent = dayPriceCarRent;
        }

        public Integer getHourlyPriceCarRent() {
            return hourlyPriceCarRent;
        }

        public void setHourlyPriceCarRent(Integer hourlyPriceCarRent) {
            this.hourlyPriceCarRent = hourlyPriceCarRent;
        }

    }
    public class SurgeHour {

        @SerializedName("is_surge")
        @Expose
        private Boolean isSurge;
        @SerializedName("day")
        @Expose
        private String day;
        @SerializedName("day_time")
        @Expose
        private List<Object> dayTime = null;

        public Boolean getIsSurge() {
            return isSurge;
        }

        public void setIsSurge(Boolean isSurge) {
            this.isSurge = isSurge;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public List<Object> getDayTime() {
            return dayTime;
        }

        public void setDayTime(List<Object> dayTime) {
            this.dayTime = dayTime;
        }

    }
}
