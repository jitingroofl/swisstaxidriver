package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarrentalResponse {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("trips")
    private List<Trip> trips = null;


    @SerializedName("error_code")
    private int errorCode;



    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public class Trip {


        @SerializedName("_id")
        private String id;

        @SerializedName("unique_id")
        private double uniqueId;

        @SerializedName("current_provider")
        private String currentProvider;

        @SerializedName("provider_trip_end_time")
        private String providerTripEndTime;

        @SerializedName("user_create_time")
        private String userCreateTime;
        @SerializedName("provider_service_fees")
        private double providerServiceFees;


        @SerializedName("is_damage_apply")
        private boolean is_damage_apply;


        @SerializedName("total")
        private double total;
        @SerializedName("total_time")
        private double totalTime;
        @SerializedName("total_distance")
        private double totalDistance;
        @SerializedName("timezone")
        private String timezone;
        @SerializedName("unit")
        private double unit;
        @SerializedName("currencycode")
        private String currencycode;
        @SerializedName("currency")
        private String currency;
        @SerializedName("destination_address")
        private String destinationAddress;
        @SerializedName("source_address")
        private String sourceAddress;
        @SerializedName("is_provider_rated")
        private double isProviderRated;
        @SerializedName("is_user_rated")
        private double isUserRated;
        @SerializedName("is_trip_cancelled_by_provider")
        private double isTripCancelledByProvider;
        @SerializedName("is_trip_cancelled_by_user")
        private double isTripCancelledByUser;
        @SerializedName("is_trip_completed")
        private double isTripCompleted;
        @SerializedName("invoice_number")
        private String invoiceNumber;
        @SerializedName("trip_id")
        private String tripId;
        @SerializedName("first_name")
        private String firstName;
        @SerializedName("last_name")
        private String lastName;
        @SerializedName("picture")
        private String picture;
        @SerializedName("service_selection")
        private String serviceSelection;
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public double getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(double uniqueId) {
            this.uniqueId = uniqueId;
        }

        public String getCurrentProvider() {
            return currentProvider;
        }

        public void setCurrentProvider(String currentProvider) {
            this.currentProvider = currentProvider;
        }

        public String getProviderTripEndTime() {
            return providerTripEndTime;
        }

        public void setProviderTripEndTime(String providerTripEndTime) {
            this.providerTripEndTime = providerTripEndTime;
        }
        public boolean isIs_damage_apply() {
            return is_damage_apply;
        }

        public void setIs_damage_apply(boolean is_damage_apply) {
            this.is_damage_apply = is_damage_apply;
        }

        public String getUserCreateTime() {
            return userCreateTime;
        }

        public void setUserCreateTime(String userCreateTime) {
            this.userCreateTime = userCreateTime;
        }

        public double getProviderServiceFees() {
            return providerServiceFees;
        }

        public void setProviderServiceFees(double providerServiceFees) {
            this.providerServiceFees = providerServiceFees;
        }

        public double getTotal() {
            return total;
        }

        public void setTotal(double total) {
            this.total = total;
        }

        public double getTotalTime() {
            return totalTime;
        }

        public void setTotalTime(double totalTime) {
            this.totalTime = totalTime;
        }

        public double getTotalDistance() {
            return totalDistance;
        }

        public void setTotalDistance(double totalDistance) {
            this.totalDistance = totalDistance;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

        public double getUnit() {
            return unit;
        }

        public void setUnit(double unit) {
            this.unit = unit;
        }

        public String getCurrencycode() {
            return currencycode;
        }

        public void setCurrencycode(String currencycode) {
            this.currencycode = currencycode;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getDestinationAddress() {
            return destinationAddress;
        }

        public void setDestinationAddress(String destinationAddress) {
            this.destinationAddress = destinationAddress;
        }

        public String getSourceAddress() {
            return sourceAddress;
        }

        public void setSourceAddress(String sourceAddress) {
            this.sourceAddress = sourceAddress;
        }

        public double getIsProviderRated() {
            return isProviderRated;
        }

        public void setIsProviderRated(double isProviderRated) {
            this.isProviderRated = isProviderRated;
        }

        public double getIsUserRated() {
            return isUserRated;
        }

        public void setIsUserRated(double isUserRated) {
            this.isUserRated = isUserRated;
        }

        public double getIsTripCancelledByProvider() {
            return isTripCancelledByProvider;
        }

        public void setIsTripCancelledByProvider(double isTripCancelledByProvider) {
            this.isTripCancelledByProvider = isTripCancelledByProvider;
        }

        public double getIsTripCancelledByUser() {
            return isTripCancelledByUser;
        }

        public void setIsTripCancelledByUser(double isTripCancelledByUser) {
            this.isTripCancelledByUser = isTripCancelledByUser;
        }

        public double getIsTripCompleted() {
            return isTripCompleted;
        }

        public void setIsTripCompleted(double isTripCompleted) {
            this.isTripCompleted = isTripCompleted;
        }

        public String getInvoiceNumber() {
            return invoiceNumber;
        }

        public void setInvoiceNumber(String invoiceNumber) {
            this.invoiceNumber = invoiceNumber;
        }

        public String getTripId() {
            return tripId;
        }

        public void setTripId(String tripId) {
            this.tripId = tripId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getServiceSelection() {
            return serviceSelection;
        }

        public void setServiceSelection(String serviceSelection) {
            this.serviceSelection = serviceSelection;
        }




    }
}
