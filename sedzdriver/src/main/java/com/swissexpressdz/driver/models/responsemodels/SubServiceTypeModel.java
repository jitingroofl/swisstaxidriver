package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubServiceTypeModel {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("sub_type")
    private List<SubType> subType = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<SubType> getSubType() {
        return subType;
    }

    public void setSubType(List<SubType> subType) {
        this.subType = subType;
    }
    public class SubType {

        @SerializedName("_id")
        private String id;
        @SerializedName("service_type")
        private Integer serviceType;
        @SerializedName("__v")
        private Integer v;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("is_default_selected")
        private Boolean isDefaultSelected;
        @SerializedName("is_business")
        private Integer isBusiness;
        @SerializedName("priority")
        private Integer priority;
        @SerializedName("map_pin_image_url")
        private String mapPinImageUrl;
        @SerializedName("type_image_url")
        private String typeImageUrl;
        @SerializedName("description_gr")
        private String descriptionGr;
        @SerializedName("description_fr")
        private String descriptionFr;
        @SerializedName("description")
        private String description;
        @SerializedName("subtypename")
        private String subtypename;
        @SerializedName("type_id")
        private String typeId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Integer getServiceType() {
            return serviceType;
        }

        public void setServiceType(Integer serviceType) {
            this.serviceType = serviceType;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Boolean getIsDefaultSelected() {
            return isDefaultSelected;
        }

        public void setIsDefaultSelected(Boolean isDefaultSelected) {
            this.isDefaultSelected = isDefaultSelected;
        }

        public Integer getIsBusiness() {
            return isBusiness;
        }

        public void setIsBusiness(Integer isBusiness) {
            this.isBusiness = isBusiness;
        }

        public Integer getPriority() {
            return priority;
        }

        public void setPriority(Integer priority) {
            this.priority = priority;
        }

        public String getMapPinImageUrl() {
            return mapPinImageUrl;
        }

        public void setMapPinImageUrl(String mapPinImageUrl) {
            this.mapPinImageUrl = mapPinImageUrl;
        }

        public String getTypeImageUrl() {
            return typeImageUrl;
        }

        public void setTypeImageUrl(String typeImageUrl) {
            this.typeImageUrl = typeImageUrl;
        }

        public String getDescriptionGr() {
            return descriptionGr;
        }

        public void setDescriptionGr(String descriptionGr) {
            this.descriptionGr = descriptionGr;
        }

        public String getDescriptionFr() {
            return descriptionFr;
        }

        public void setDescriptionFr(String descriptionFr) {
            this.descriptionFr = descriptionFr;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSubtypename() {
            return subtypename;
        }

        public void setSubtypename(String subtypename) {
            this.subtypename = subtypename;
        }

        public String getTypeId() {
            return typeId;
        }

        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }

    }
}
