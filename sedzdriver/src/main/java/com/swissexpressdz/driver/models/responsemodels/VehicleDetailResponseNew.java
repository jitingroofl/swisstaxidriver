package com.swissexpressdz.driver.models.responsemodels;

import com.google.gson.annotations.SerializedName;
import com.swissexpressdz.driver.models.datamodels.VehicleDetail;
import com.swissexpressdz.driver.models.datamodels.VehicleDetailsNew;

public class VehicleDetailResponseNew {

    @SerializedName("error_code")
    private int errorCode;
    @SerializedName("success")
    private boolean success;
    //    @SerializedName("document_list")
//    private DocumentNew documentList;
    @SerializedName("vehicle_detail")
    private VehicleDetailsNew vehicleDetail;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

//    public DocumentNew getDocumentList() {
//        return documentList;
//    }
//
//    public void setDocumentList(DocumentNew documentList) {
//        this.documentList = documentList;
//    }

    public VehicleDetailsNew getVehicleDetail() {
        return vehicleDetail;
    }

    public void setVehicleDetail(VehicleDetailsNew vehicleDetail) {
        this.vehicleDetail = vehicleDetail;
    }

    @Override
    public String toString() {
        return
                "VehicleDetailResponse{" +
                        "success = '" + success + '\'' +
                        //  ",document_list = '" + documentList + '\'' +
                        ",vehicle_detail = '" + vehicleDetail + '\'' +
                        "}";
    }
}
