package com.swissexpressdz.driver;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.swissexpressdz.driver.adapter.LanguageAdaptor;
import com.swissexpressdz.driver.components.MyAppTitleFontTextView;
import com.swissexpressdz.driver.interfaces.ClickListener;
import com.swissexpressdz.driver.interfaces.RecyclerTouchListener;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.LanguageHelper;
import com.swissexpressdz.driver.utils.PreferenceHelper;


public class SelectLanguageActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "SelectLanguageActivity";
    private RecyclerView rcvCountryCode;
    private MyAppTitleFontTextView tvCountryDialogTitle;
    private LanguageAdaptor languageAdaptor;
    RelativeLayout rr_continue;
    private Context context;
    private TypedArray langCode;
    private TypedArray langName;
    public PreferenceHelper preferenceHelper;
    int language_cod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        preferenceHelper = PreferenceHelper.getInstance(this);

        initUi();

    }


    private void initUi() {
        langCode = getResources().obtainTypedArray(R.array.language_code);
        langName = getResources().obtainTypedArray(R.array.language_name);
        rr_continue=(RelativeLayout)findViewById(R.id.rr_continue);

        setLanguageName();

        rr_continue.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        AppLog.Log(TAG, PreferenceHelper.getInstance(newBase).getLanguageCode());
        super.attachBaseContext(LanguageHelper.wrapper(newBase, PreferenceHelper.getInstance
                (newBase).getLanguageCode()));
    }

    private void setLanguageName() {

        TypedArray array = getResources().obtainTypedArray(R.array.language_code);
        TypedArray array2 = getResources().obtainTypedArray(R.array.language_name);
        int size = array.length();

        System.out.println("Language Code  "+preferenceHelper.getLanguageCode());
        for (int i = 0; i < size; i++) {

            if (TextUtils.equals(preferenceHelper.getLanguageCode(), array.getString(i))) {
//                tvLanguage.setText(array2.getString(i));
                Log.e("Language Code  ",i+"");
                language_cod=i;
                break;
            }
        }

        rcvCountryCode = (RecyclerView) findViewById(R.id.rcvCountryCode);

        LinearLayoutManager llm = new LinearLayoutManager(SelectLanguageActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);



        languageAdaptor = new LanguageAdaptor(getApplicationContext(),language_cod);
        rcvCountryCode.setAdapter(languageAdaptor);
        rcvCountryCode.setLayoutManager(llm);

        //        rcvCountryCode.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager
//                .VERTICAL));


        rcvCountryCode.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rcvCountryCode,
                new ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        // onSelect(langName.getString(position), langCode.getString(position));
                        Log.e("Lang name  ",langName.getString(position));
                        Log.e("Lang code  ",langCode.getString(position));

                        onSelect(langName.getString(position), langCode.getString(position));

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

    }
    public  void onSelect(String languageName, String languageCode)
    {
        if (!TextUtils.equals(preferenceHelper.getLanguageCode(), languageCode)) {
//            AppLog.Log(TAG, languageCode);
            preferenceHelper.putLanguageCode(languageCode);
//            finishAffinity();

        }
    }

    @Override
    public void onClick(View view) {

        if(view==rr_continue)
        {
            System.out.println("Language Code   "+preferenceHelper.getLanguageCode());
            System.out.println("Session Token    "+preferenceHelper.getSessionToken());
            System.out.println("Register Status    "+preferenceHelper.getIsRegisterStatus());
            System.out.println("Vehicle Status    "+preferenceHelper.getIsVehicleStatus());
            System.out.println("Document Status    "+preferenceHelper.getIsDocumentstatus());



            if(!TextUtils.isEmpty(preferenceHelper.getSessionToken())) {

            if (preferenceHelper.getIsRegisterStatus() == true) {
                if (preferenceHelper.getIsServiceStatus() == true) {
                    if (preferenceHelper.getIsVehicleStatus() == true) {

                        if(preferenceHelper.getIsDocumentstatus() == true)
                        {
                            Intent intent = new Intent(SelectLanguageActivity.this, MainDrawerActivity.class);
                            startActivity(intent);
                        }

                        else if (preferenceHelper.getIsDocumentstatus() == false) {
                            Intent intent = new Intent(SelectLanguageActivity.this, DocumentNewActivity.class);
                            startActivity(intent);
                        }

                    } else if (preferenceHelper.getIsVehicleStatus() == false) {
                        Intent intent = new Intent(SelectLanguageActivity.this, AddVehicleActivity.class);
                        startActivity(intent);
                    }

                } else if (preferenceHelper.getIsServiceStatus() == false) {
                    Intent intent = new Intent(SelectLanguageActivity.this, Choose_Service_TypeActivity.class);
                    intent.putExtra("beforelogin","beforelogin");

                    startActivity(intent);
                }

            } else if (preferenceHelper.getIsRegisterStatus() == false) {
                Intent intent = new Intent(SelectLanguageActivity.this, SignInActivity_Driver.class);
                startActivity(intent);
            }


        }
        else
        {
            Intent intent = new Intent(SelectLanguageActivity.this, SignInActivity_Driver.class);
            startActivity(intent);
        }


//            Intent ii=new Intent(SelectLanguageActivity.this, SignInActivity_Driver.class);
//            startActivity(ii);
        }
    }



    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
