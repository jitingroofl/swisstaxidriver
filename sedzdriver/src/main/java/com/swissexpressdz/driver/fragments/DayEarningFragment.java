package com.swissexpressdz.driver.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.swissexpressdz.driver.EarningActivity_Driver;
import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.adapter.TripAnalyticAdapter;
import com.swissexpressdz.driver.adapter.TripDayEarningAdaptor;
import com.swissexpressdz.driver.adapter.TripEarningAdapter;
import com.swissexpressdz.driver.components.MyAppTitleFontTextView;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.models.datamodels.Analytic;
import com.swissexpressdz.driver.models.datamodels.EarningData;
import com.swissexpressdz.driver.models.datamodels.TripsEarning;
import com.swissexpressdz.driver.models.responsemodels.EarningResponse;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by elluminati on 27-Jun-17.
 */

public class DayEarningFragment extends Fragment implements View.OnClickListener {


    public MyFontTextView tvTripDate;
    private MyAppTitleFontTextView tvTripTotal;
    private RecyclerView rcvOrderEarning, rcvProviderAnalytic, rcvOrders;
    private ArrayList<ArrayList<EarningData>> arrayListForEarning;
    private ArrayList<Analytic> arrayListProviderAnalytic;
    private List<TripsEarning> tripPaymentsItemList;
    private LinearLayout llData, ivEmpty;
    private Calendar calendar;
    private int day;
    private int month;
    private int year;
    private DatePickerDialog.OnDateSetListener fromDateSet;
    private EarningActivity_Driver earningActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        View fragEarView = inflater.inflate(R.layout.fragment_day_earning, container, false);

        tvTripDate = fragEarView.findViewById(R.id.tvTripDate);
        tvTripTotal = fragEarView.findViewById(R.id.tvTripTotal);
        rcvOrderEarning = (RecyclerView) fragEarView.findViewById(R.id.rcvOrderEarning);
        rcvProviderAnalytic = (RecyclerView) fragEarView.findViewById(R.id.rcvProviderAnalytic);
        rcvOrders = (RecyclerView) fragEarView.findViewById(R.id.rcvOrders);
        llData = (LinearLayout) fragEarView.findViewById(R.id.llData);
        return fragEarView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        earningActivity = (EarningActivity_Driver) getActivity();
        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");

        String formattedDate = df.format(c);
        System.out.println("Date  "+formattedDate);

        arrayListForEarning = new ArrayList<>();
        arrayListProviderAnalytic = new ArrayList<>();
        tripPaymentsItemList = new ArrayList<>();
//        getDailyEarning(earningActivity.parseContent.dateFormat.format(new Date()
//        ));
        getDailyEarning(formattedDate);

        setDate(new Date());
        tvTripDate.setOnClickListener(this);
        fromDateSet = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                calendar.clear();
                calendar.set(year, monthOfYear, dayOfMonth);
                setDate(calendar.getTime());
                getDailyEarning(earningActivity.parseContent.dateFormat.format
                        (calendar.getTime()));
            }
        };

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTripDate:
                openFromDatePicker();
                break;

            default:
                // do with default
                break;
        }
    }


    private void initEarningOrderRcv() {
        rcvOrderEarning.setLayoutManager(new LinearLayoutManager(earningActivity));
        rcvOrderEarning.setAdapter(new TripEarningAdapter(arrayListForEarning));
        rcvOrderEarning.setNestedScrollingEnabled(false);
    }

    private void initAnalyticRcv() {
        rcvProviderAnalytic.setLayoutManager(new GridLayoutManager(earningActivity, 2,
                LinearLayoutManager.VERTICAL, false));
        rcvProviderAnalytic.addItemDecoration(new DividerItemDecoration(earningActivity,
                DividerItemDecoration.HORIZONTAL));
        rcvProviderAnalytic.setAdapter(new TripAnalyticAdapter(arrayListProviderAnalytic));
        rcvProviderAnalytic.setNestedScrollingEnabled(false);
    }

    private void initOrdersRcv() {
        rcvOrders.setLayoutManager(new LinearLayoutManager(earningActivity));
        rcvOrders.addItemDecoration(new DividerItemDecoration(earningActivity,
                DividerItemDecoration.VERTICAL));
        rcvOrders.setAdapter(new TripDayEarningAdaptor(earningActivity, tripPaymentsItemList));
        rcvOrders.setNestedScrollingEnabled(false);
    }

    private void openFromDatePicker() {
        DatePickerDialog fromPiker = new DatePickerDialog(earningActivity, fromDateSet, year,
                month, day);
        fromPiker.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
        fromPiker.show();
    }

    public void setDate(Date date) {
        tvTripDate.setText(Utils.getDayOfMonthSuffix(Integer.valueOf
                (earningActivity.parseContent.day.format(date))) + earningActivity
                .parseContent.dateFormatMonth.format(date));
    }


    public void getDailyEarning(String earningDate) {
        Utils.showCustomProgressDialog(earningActivity, "", false, null);

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate(Const.Params.PROVIDER_ID, earningActivity.preferenceHelper
                    .getProviderId());
            jsonObject.accumulate(Const.Params.TOKEN, earningActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.accumulate(Const.Params.DATE, earningDate);


            Call<EarningResponse> call = ApiClient.getClient(getActivity()).create(ApiInterface.class)
                    .getProviderDailyEarningDetail(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<EarningResponse>() {
                             @Override
                             public void onResponse(Call<EarningResponse> call,
                                                    Response<EarningResponse> response) {
                                 if (response.isSuccessful()) {
                                     EarningResponse earningResponse = response.body();
                                     if (earningResponse.isSuccess()) {

                                         arrayListForEarning.clear();
                                         arrayListProviderAnalytic.clear();
                                         tripPaymentsItemList.clear();
                                         tripPaymentsItemList.addAll(earningResponse.getTrips());

                                         NumberFormat numberFormat =
                                                 earningActivity.currencyHelper.getCurrencyFormat(earningActivity.preferenceHelper.getCurrencyCode());

                                         earningActivity.parseContent.parseEarning(earningResponse, arrayListForEarning, arrayListProviderAnalytic, false);
                                         initEarningOrderRcv();
                                         initAnalyticRcv();
                                         initOrdersRcv();


                                         tvTripTotal.setText(numberFormat.format(earningResponse
                                                         .getProviderDayEarning()
                                                         .getTotalProviderServiceFees()));



                                      /*   String str_rate= ""+earningResponse
                                                 .getProviderDayEarning()
                                                 .getTotalProviderServiceFees();

                                         System.out.println("total earning::::::"+str_rate);

                                         tvTripTotal.setText(earningResponse.getCurrency()+""+str_rate);*/

                                         llData.setVisibility(View.VISIBLE);

                                     } else {
                                         Utils.showErrorToast(earningResponse.getErrorCode(),
                                                 earningActivity);
                                         llData.setVisibility(View.GONE);
                                     }
                                     Utils.hideCustomProgressDialog();
                                 }

                             }

                             @Override
                             public void onFailure(Call<EarningResponse> call, Throwable t) {
                                 AppLog.handleThrowable(EarningActivity_Driver.class.getSimpleName(), t);
                             }
                         }
            );
        } catch (JSONException e) {
            AppLog.handleException("DailyEarning", e);
        }
    }
}
