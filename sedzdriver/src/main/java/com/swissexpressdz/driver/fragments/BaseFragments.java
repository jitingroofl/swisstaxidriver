package com.swissexpressdz.driver.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.swissexpressdz.driver.MainDrawerActivity;


/**
 * Created by elluminati on 14-06-2016.
 */
public abstract class BaseFragments extends Fragment implements View.OnClickListener {

    protected MainDrawerActivity drawerActivity;
    public String TAG = this.getClass().getSimpleName();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        drawerActivity = (MainDrawerActivity) getActivity();
    }
}
