package com.swissexpressdz.driver.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.components.MyFontButton;
import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.components.MyFontTextView;

public class SuccessFragment extends BaseFragments {



    private MyFontTextView tvTripDistance, tvTripTime, tvDriverFullName;
    private MyFontEdittextView etComment;
    private ImageView ivDriverImage;
    private RatingBar ratingBar;
    private MyFontButton btnsuccessDone;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable
                                     Bundle savedInstanceState) {

        View feedFrag = inflater.inflate(R.layout.fragment_success, container, false);
        drawerActivity.setTitleOnToolbar(drawerActivity.getResources().getString(R.string
                .text_feed_back));
        btnsuccessDone = (MyFontButton) feedFrag.findViewById(R.id.btn_done);

        return feedFrag;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        drawerActivity.setToolbarBackgroundAndElevation(true,
                R.drawable.toolbar_bg_rounded_bottom, R
                        .dimen.toolbar_elevation);
        drawerActivity.setScheduleListener(null);
        drawerActivity.setTitleOnToolbar(drawerActivity.getResources().getString(R.string
                .text_payment_success));
    /*    tvTripTime.setText(CurrentTrip.getInstance().getTime() + " " + getResources().getString(R
                .string
                .text_unit_mins));
        tvTripDistance.setText(drawerActivity.parseContent.twoDigitDecimalFormat.format
                (CurrentTrip.getInstance()
                        .getDistance()) + " " + Utils
                .showUnit
                        (drawerActivity,
                                CurrentTrip.getInstance().getUnit()));

        Glide.with(drawerActivity).load(drawerActivity.currentTrip.getUserProfileImage())
                .fallback(R.drawable.ellipse)
                .placeholder(R
                        .drawable.ellipse).override(200, 200).dontAnimate()
                .into(ivDriverImage);

        tvDriverFullName.setText(drawerActivity.currentTrip.getUserFirstName()
                + " " + drawerActivity.currentTrip.getUserLastName());*/

        btnsuccessDone.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

               drawerActivity.goToFeedBackFragment();


    }
}
