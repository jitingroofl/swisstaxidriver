package com.swissexpressdz.driver.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.RegisterActivity_Driver;
import com.swissexpressdz.driver.activity.applyfordamage.ApplyForDamageActivity;
import com.swissexpressdz.driver.adapter.CircularProgressViewAdapter;
import com.swissexpressdz.driver.adapter.InvoiceAdapter;
import com.swissexpressdz.driver.components.CustomCircularProgressView;
import com.swissexpressdz.driver.components.CustomPhotoDialog;
import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.components.MyFontTextViewMedium;
import com.swissexpressdz.driver.models.datamodels.CityType;
import com.swissexpressdz.driver.models.datamodels.Trip;
import com.swissexpressdz.driver.models.responsemodels.DepositeResponseModel;
import com.swissexpressdz.driver.models.responsemodels.InvoiceResponse;
import com.swissexpressdz.driver.models.responsemodels.IsSuccessResponse;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextViewRegular;
import com.swissexpressdz.driver.utils.ImageCompression;
import com.swissexpressdz.driver.utils.ImageHelper;
import com.swissexpressdz.driver.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by elluminati on 26-07-2016.
 */
public class InvoiceFragment extends BaseFragments {


    private TextView tvPaymentWith, tvInvoiceNumber, tvInvoiceDistance, tvInvoiceTripTime,
            tvInvoiceTotal, tvTotalText;
    private String unit;
    private ImageView ivPaymentImage;
    private MyFontTextViewMedium tvInvoiceTripType;
    private MyFontTextView tvInvoiceMinFareApplied,tvTitle,tvSubPriceValue,txt_numofday_numofhrs,tvnumofday_numofhrsValue;
    private Dialog dialogProgress;
    private CustomCircularProgressView ivProgressBar;
    private RecyclerView rcvInvoice;
    private NumberFormat currencyFormat;
LinearLayout ll_numofday_numofhrs;
    CircleImageView profile_icon;
    CustomTextViewRegular user_name;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable
                                     Bundle savedInstanceState) {
        View invoiceFrag = inflater.inflate(R.layout.fragment_invoice, container, false);

        getProviderInvoice();
        ivPaymentImage = (ImageView) invoiceFrag.findViewById(R.id.ivPaymentImage);
        tvPaymentWith = invoiceFrag.findViewById(R.id.tvPaymentWith);
        tvInvoiceNumber = invoiceFrag.findViewById(R.id.tvInvoiceNumber);
        profile_icon=(CircleImageView) invoiceFrag.findViewById(R.id.profile_icon);
        user_name=(CustomTextViewRegular)invoiceFrag.findViewById(R.id.user_name);

        tvTitle = (MyFontTextView) invoiceFrag.findViewById(R.id.tvTitle);
        tvSubPriceValue = (MyFontTextView) invoiceFrag.findViewById(R.id.tvSubPriceValue);

        ll_numofday_numofhrs = (LinearLayout) invoiceFrag.findViewById(R.id.ll_numofday_numofhrs);
        txt_numofday_numofhrs = (MyFontTextView) invoiceFrag.findViewById(R.id.txt_numofday_numofhrs);
        tvnumofday_numofhrsValue = (MyFontTextView) invoiceFrag.findViewById(R.id.tvnumofday_numofhrsValue);

        tvInvoiceTripType = (MyFontTextViewMedium) invoiceFrag.findViewById(R.id.tvInvoiceTripType);
        tvInvoiceMinFareApplied = (MyFontTextView) invoiceFrag.findViewById(R.id
                .tvInvoiceMinFareApplied);
        tvInvoiceDistance = invoiceFrag.findViewById(R.id.tvInvoiceDistance);
        tvInvoiceTripTime = invoiceFrag.findViewById(R.id.tvInvoiceTripTime);
        tvInvoiceTotal = invoiceFrag.findViewById(R.id.tvInvoiceTotal);
        tvTotalText = invoiceFrag.findViewById(R.id.tvTotalText);
        rcvInvoice = invoiceFrag.findViewById(R.id.rcvInvoice);
        return invoiceFrag;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        currencyFormat =
                drawerActivity.currencyHelper.getCurrencyFormat(drawerActivity.preferenceHelper.getCurrencyCode());
        tvTotalText.setVisibility(View.GONE);
        tvInvoiceTotal.setVisibility(View.GONE);
       // drawerActivity.setToolbarBackgroundAndElevation(false, R.color.color_white, 0);
        drawerActivity.setScheduleListener(null);
        drawerActivity.preferenceHelper.putIsHaveTrip(false);
        drawerActivity.preferenceHelper.putLocationUniqueId(0);
        drawerActivity.setTitleOnToolbar(drawerActivity.getString(R.string.text_invoice));
        drawerActivity.setToolbarIcon(AppCompatResources.getDrawable(drawerActivity, R
                        .drawable.ic_done_black_24dp),
                new View
                        .OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        providerSubmitInvoice();
                    }
                });
        rcvInvoice.setLayoutManager(new LinearLayoutManager(drawerActivity));
        rcvInvoice.setNestedScrollingEnabled(false);

        PicassoTrustAll.getInstance(drawerActivity)
                .load(drawerActivity.currentTrip.getUserProfileImage())
                .error(R.drawable.ellipse)
                .resize(200,200)
                .into(profile_icon);

        user_name.setText(drawerActivity.currentTrip.getUserFirstName()
                + " " + drawerActivity.currentTrip.getUserLastName());
    }


    private void getProviderInvoice() {
        Utils.hideCustomProgressDialog();
        showCustomProgressDialog(drawerActivity);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.TRIP_ID, drawerActivity.preferenceHelper.getTripId
                    ());
            jsonObject.put(Const.Params.TOKEN, drawerActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.PROVIDER_ID, drawerActivity.preferenceHelper
                    .getProviderId());

            Call<InvoiceResponse> call = ApiClient.getClient(getActivity()).create(ApiInterface.class)
                    .getInvoice(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<InvoiceResponse>() {
                @Override
                public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse>
                        response) {
                    if (ParseContent.getInstance().isSuccessful(response)) {
                        hideCustomProgressDialog();
                        if (response.body().isSuccess()) {
                            setInvoiceData(response.body().getTrip(), response.body()
                                    .getTripService());
                        } else {
                            Utils.showErrorToast(response.body().getErrorCode(), drawerActivity);
                        }

                    }
                }

                @Override
                public void onFailure(Call<InvoiceResponse> call, Throwable t) {
                    AppLog.handleThrowable(InvoiceFragment.class.getSimpleName(), t);

                }
            });

        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.INVOICE_FRAGMENT, e);
        }
    }


    @Override
    public void onClick(View v) {

    }

    public File getPathOfPdfFile() {
        File storageDir = new File(drawerActivity.imageHelper.getAlbumDir().getAbsolutePath(),
                getResources().getString(R.string.text_invoice));

        if (storageDir != null) {
            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    Log.d("PdfSample", "failed to create directory");
                    return null;
                }
            }
        }
        Date date = new Date();
        String timeStamp = drawerActivity.parseContent.dateFormat.format(date);
        timeStamp = timeStamp + "_" + drawerActivity.parseContent.timeFormat.format(date);
        String imageFileName = "Invoice_" + timeStamp + ".pdf";
        File imageF = new File(storageDir.getAbsolutePath(), imageFileName);
        return imageF;
    }

    private void createPdfFile(View view) {

        PdfDocument document = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            document = new PdfDocument();
            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(view.getWidth(),
                    view.getHeight(), 1)
                    .create();
            PdfDocument.Page page = document.startPage(pageInfo);
            view.draw(page.getCanvas());
            document.finishPage(page);
            try {
                document.writeTo(new FileOutputStream(getPathOfPdfFile()));
            } catch (IOException e) {
                AppLog.handleException(Const.Tag.INVOICE_FRAGMENT, e);
            }
            document.close();
        }
    }


    private void providerSubmitInvoice() {
        Utils.showCustomProgressDialog(drawerActivity, getResources()
                        .getString(R.string.msg_waiting_for_invoice), false,
                null);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.TRIP_ID, drawerActivity.preferenceHelper.getTripId
                    ());
            jsonObject.put(Const.Params.TOKEN, drawerActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.put(Const.Params.PROVIDER_ID, drawerActivity.preferenceHelper
                    .getProviderId());
            Call<IsSuccessResponse> call = ApiClient.getClient(getActivity()).create(ApiInterface.class)
                    .submitInvoice(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {

                    if (ParseContent.getInstance().isSuccessful(response)) {
                        Utils.hideCustomProgressDialog();
                        if (response.body().isSuccess()) {
                            drawerActivity.goToFeedBackFragment();
                          //  drawerActivity.goToSuccessFragmet();
                        } else {
                            Utils.showErrorToast(response.body().getErrorCode(), drawerActivity);
                        }
                    }

                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(InvoiceFragment.class.getSimpleName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.INVOICE_FRAGMENT, e);
        }
    }


    public void showCustomProgressDialog(Context context) {
        AppCompatActivity appCompatActivity = (AppCompatActivity) context;
        if (!appCompatActivity.isFinishing()) {
            if (dialogProgress != null && dialogProgress.isShowing()) {
                return;
            }
            dialogProgress = new Dialog(context);
            dialogProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogProgress.setContentView(R.layout.circuler_progerss_bar_two);
            dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            ivProgressBar = (CustomCircularProgressView) dialogProgress.findViewById(R.id
                    .ivProgressBarTwo);
            ivProgressBar.addListener(new CircularProgressViewAdapter() {
                @Override
                public void onProgressUpdate(float currentProgress) {
                    Log.d("CPV", "onProgressUpdate: " + currentProgress);
                }

                @Override
                public void onProgressUpdateEnd(float currentProgress) {
                    Log.d("CPV", "onProgressUpdateEnd: " + currentProgress);
                }

                @Override
                public void onAnimationReset() {
                    Log.d("CPV", "onAnimationReset");
                }

                @Override
                public void onModeChanged(boolean isIndeterminate) {
                    Log.d("CPV", "onModeChanged: " + (isIndeterminate ? "indeterminate" :
                            "determinate"));
                }
            });
            ivProgressBar.startAnimation();
            dialogProgress.setCancelable(false);
            WindowManager.LayoutParams params = dialogProgress.getWindow().getAttributes();
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            dialogProgress.getWindow().setAttributes(params);
            dialogProgress.getWindow().setDimAmount(0);
            dialogProgress.show();
        }
    }

    public void hideCustomProgressDialog() {
        try {
            if (dialogProgress != null && ivProgressBar != null) {

                dialogProgress.dismiss();

            }
        } catch (Exception e) {
            AppLog.handleException(TAG, e);
        }
    }

    private void setInvoiceData(Trip trip, CityType tripService) {
        unit = Utils.showUnit(drawerActivity, trip.getUnit());
        if (trip.getIsMinFareUsed() == Const.TRUE && trip.getTripType() == Const.TripType
                .NORMAL) {
            tvInvoiceMinFareApplied.setVisibility(View.VISIBLE);
            tvInvoiceMinFareApplied.setText(String.valueOf(getResources().getString(R.string
                    .start_min_fare) + " " + currencyFormat.format(tripService.getMinFare()) + " " +
                    getResources
                            ().getString(R.string.text_applied)));
        }

        if (trip.getPaymentMode() == Const.CASH) {
            ivPaymentImage.setImageDrawable(AppCompatResources.getDrawable(drawerActivity,
                    R.drawable.cash));
            tvPaymentWith.setText(this.getResources().getString(R.string
                    .text_payment_with_cash));
        }

        else if(trip.getPaymentMode() == Const.STRIPE)
        {
            ivPaymentImage.setImageDrawable(AppCompatResources.getDrawable(drawerActivity,
                    R.drawable.stripe_icon));
            tvPaymentWith.setText(getActivity().getResources().getString(R.string
                    .stripe));
        }
        else if(trip.getPaymentMode() == Const.PAYPAL)
        {
            ivPaymentImage.setImageDrawable(AppCompatResources.getDrawable(drawerActivity,
                    R.drawable.paypal_icon));
            tvPaymentWith.setText(getActivity().getResources().getString(R.string
                    .Paypal));
        }
        else {
            ivPaymentImage.setImageDrawable(AppCompatResources.getDrawable(drawerActivity,
                    R.drawable.card));
            tvPaymentWith.setText(this.getResources().getString(R.string
                    .text_payment_with_card));
        }





     //   if(trip.getServiceSelection().equalsIgnoreCase("CarRent_perHour") || trip.getServiceSelection().equalsIgnoreCase("CarRent_perDay")) {

        if(trip.getServiceSelection().equalsIgnoreCase("CarRent")) {
            ll_numofday_numofhrs.setVisibility(View.VISIBLE);
            SimpleDateFormat dateFormat = drawerActivity.parseContent.dateFormat;

            Date parseHistoryDate = new Date();

            try {
                parseHistoryDate = ParseContent.getInstance().webFormat.parse(trip.getPickUpDate());
                String historyDate = dateFormat.format(parseHistoryDate);
                tvInvoiceDistance.setText(historyDate + "");
                try {
                    CurrentTrip.getInstance().setDistance(Double.parseDouble(trip.getPickUpDate()));

                }
                catch (NumberFormatException e)
                {

                }



            } catch (ParseException e) {
                AppLog.handleException(TripFragment.class.getSimpleName(), e);
            }
            tvInvoiceTripTime.setText(trip.getPickUpTime()+"");

            //"per_day":null,
            //"per_hour":4,
            if (trip.getPerDay() != null) {
                txt_numofday_numofhrs.setText(getResources().getString(R.string.str_number_of_days));

                tvnumofday_numofhrsValue.setText(trip.getPerDay() + "");

            } else if (trip.getPerHour() != null) {
                txt_numofday_numofhrs.setText(getResources().getString(R.string.str_number_of_hours));

                tvnumofday_numofhrsValue.setText(trip.getPerHour() + "");

            }
            try {
                CurrentTrip.getInstance().setTotalTime(Integer.parseInt(trip.getPickUpTime()));

            }
catch (NumberFormatException e)
{

}
            CurrentTrip.getInstance().setPer_day(trip.getPerDay()+"");
            CurrentTrip.getInstance().setPer_hour(trip.getPerHour()+"");


        }

        else
        {
            ll_numofday_numofhrs.setVisibility(View.GONE);

            tvInvoiceNumber.setText(trip.getInvoiceNumber());

            tvInvoiceDistance.setText((trip
                    .getTotalDistance()) + " " + unit);
            tvInvoiceTripTime.setText(trip.getTotalTime() + " " + this.getResources().getString(R
                    .string.text_unit_mins));

            CurrentTrip.getInstance().setTotalTime((int) trip.getTotalTime());
            CurrentTrip.getInstance().setDistance(trip.getTotalDistance());

            CurrentTrip.getInstance().setPer_day(trip.getPerDay()+"");
            CurrentTrip.getInstance().setPer_hour(trip.getPerHour()+"");


        }
        CurrentTrip.getInstance().setService_selection(trip.getServiceSelection());

        if(trip.getServiceSelection().equalsIgnoreCase("taxi"))
        {
            tvSubPriceValue.setText(getResources().getString(R.string.taxi));
        }
       else if(trip.getServiceSelection().equalsIgnoreCase("vtc"))
        {
            tvSubPriceValue.setText(getResources().getString(R.string.vtc));
        }
        else if(trip.getServiceSelection().equalsIgnoreCase("CarRent"))
        {
            tvSubPriceValue.setText(getResources().getString(R.string.text_rent_trip));
        }


        tvInvoiceTotal.setText(currencyFormat
                .format(trip
                        .getTotal()));


    //   tvInvoiceTotal.setText(trip.getCurrency()+" "+trip.getTotal()+"");

        tvInvoiceTotal.setVisibility(View.VISIBLE);
        tvTotalText.setVisibility(View.VISIBLE);

        CurrentTrip.getInstance().setUnit(trip.getUnit());
        switch (trip.getTripType()) {
            case Const.TripType.AIRPORT:
                tvInvoiceTripType.setVisibility(View.VISIBLE);
                tvInvoiceTripType.setText(this.getResources().getString(R.string
                        .text_airport_trip));
                break;
            case Const.TripType.ZONE:
                tvInvoiceTripType.setVisibility(View.VISIBLE);
                tvInvoiceTripType.setText(this.getResources().getString(R.string
                        .text_zone_trip));
                break;
            case Const.TripType.CITY:
                tvInvoiceTripType.setVisibility(View.VISIBLE);
                tvInvoiceTripType.setText(this.getResources().getString(R.string
                        .text_city_trip));
                break;
            default:
                //Default case here..
                if (trip.isFixedFare()) {
                    tvInvoiceTripType.setVisibility(View.VISIBLE);
                    tvInvoiceTripType.setText(this.getResources().getString(R.string
                            .text_fixed_price));
                } else {
                    tvInvoiceTripType.setVisibility(View.GONE);
                }
                break;
        }



        if (rcvInvoice != null) {
            rcvInvoice.setAdapter(new InvoiceAdapter(drawerActivity.parseContent.parseInvoice(drawerActivity, trip, tripService, currencyFormat)));
        }


    }

}