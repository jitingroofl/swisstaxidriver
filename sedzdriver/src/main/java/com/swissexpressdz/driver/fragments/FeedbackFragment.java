package com.swissexpressdz.driver.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.components.MyFontButton;
import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.models.responsemodels.IsSuccessResponse;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by elluminati on 04-07-2016.
 */
public class FeedbackFragment extends BaseFragments {


    private MyFontTextView tvTripDistance, tvTripTime, tvDriverFullName,tvSubPriceValue,txt_numofday_numofhrs,tvnumofday_numofhrsValue;
    private MyFontEdittextView etComment;
    private ImageView ivDriverImage;
    private RatingBar ratingBar;
    private MyFontButton btnFeedBackDone;
    LinearLayout ll_numofday_numofhrs;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable
                                     Bundle savedInstanceState) {

        View feedFrag = inflater.inflate(R.layout.fragment_feedback, container, false);
        drawerActivity.setTitleOnToolbar(drawerActivity.getResources().getString(R.string
                .text_feed_back));
        tvTripDistance = (MyFontTextView) feedFrag.findViewById(R.id.tvTripDistance);
        tvTripTime = (MyFontTextView) feedFrag.findViewById(R.id.tvTripTime);
        ivDriverImage = (ImageView) feedFrag.findViewById(R.id.ivDriverImage);
        etComment = (MyFontEdittextView) feedFrag.findViewById(R.id.etComment);
        ratingBar = (RatingBar) feedFrag.findViewById(R.id.ratingBar);
        tvDriverFullName = (MyFontTextView) feedFrag.findViewById(R.id.tvDriverFullName);
        btnFeedBackDone = (MyFontButton) feedFrag.findViewById(R.id.btnFeedBackDone);

        tvSubPriceValue = (MyFontTextView) feedFrag.findViewById(R.id.tvSubPriceValue);

        ll_numofday_numofhrs = (LinearLayout) feedFrag.findViewById(R.id.ll_numofday_numofhrs);
        txt_numofday_numofhrs = (MyFontTextView) feedFrag.findViewById(R.id.txt_numofday_numofhrs);
        tvnumofday_numofhrsValue = (MyFontTextView) feedFrag.findViewById(R.id.tvnumofday_numofhrsValue);
        return feedFrag;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        drawerActivity.setToolbarBackgroundAndElevation(true,
                R.drawable.toolbar_bg_rounded_bottom, R
                .dimen.toolbar_elevation);
        drawerActivity.setScheduleListener(null);
        drawerActivity.setTitleOnToolbar(drawerActivity.getResources().getString(R.string
                .text_feed_back));
//        tvTripTime.setText(CurrentTrip.getInstance().getTime() + " " + getResources().getString(R
//                .string
//                .text_unit_mins));


//        Glide.with(drawerActivity).load(drawerActivity.currentTrip.getUserProfileImage())
//                .fallback(R.drawable.ellipse)
//                .placeholder(R
//                        .drawable.ellipse).override(200, 200).dontAnimate()
//                .into(ivDriverImage);
        PicassoTrustAll.getInstance(drawerActivity)
                .load(drawerActivity.currentTrip.getUserProfileImage())
                .error(R.drawable.ellipse)
                .resize(200,200)
                .into(ivDriverImage);

        tvDriverFullName.setText(drawerActivity.currentTrip.getUserFirstName()
                + " " + drawerActivity.currentTrip.getUserLastName());

        btnFeedBackDone.setOnClickListener(this);

        if(drawerActivity.currentTrip.getService_selection().equalsIgnoreCase("CarRent")) {


            ll_numofday_numofhrs.setVisibility(View.VISIBLE);
            SimpleDateFormat dateFormat = drawerActivity.parseContent.dateFormat;

            Date parseHistoryDate = new Date();

            try {
                parseHistoryDate = ParseContent.getInstance().webFormat.parse(String.valueOf(drawerActivity.currentTrip.getDistance()));
                String historyDate = dateFormat.format(parseHistoryDate);
                tvTripDistance.setText(historyDate + "");




            } catch (ParseException e) {
                AppLog.handleException(TripFragment.class.getSimpleName(), e);
            }
            tvTripTime.setText(drawerActivity.currentTrip.getTotalTime()+"");

            //"per_day":null,
            //"per_hour":4,
            if (drawerActivity.currentTrip.getPer_day() != null) {
                txt_numofday_numofhrs.setText(getResources().getString(R.string.str_number_of_days));

                tvnumofday_numofhrsValue.setText(drawerActivity.currentTrip.getPer_day() + "");

            } else if (drawerActivity.currentTrip.getPer_hour() != null) {
                txt_numofday_numofhrs.setText(getResources().getString(R.string.str_number_of_hours));

                tvnumofday_numofhrsValue.setText(drawerActivity.currentTrip.getPer_hour() + "");

            }

        }

        else
        {
            ll_numofday_numofhrs.setVisibility(View.GONE);


            tvTripDistance.setText((drawerActivity.currentTrip
                    .getTotalDistance()) + " " + drawerActivity.currentTrip.getUnit());
            tvTripTime.setText(drawerActivity.currentTrip.getTotalTime() + " " + this.getResources().getString(R
                    .string.text_unit_mins));



        }

        if(drawerActivity.currentTrip.getService_selection().equalsIgnoreCase("taxi"))
        {
            tvSubPriceValue.setText(getResources().getString(R.string.taxi));
        }
        else if(drawerActivity.currentTrip.getService_selection().equalsIgnoreCase("vtc"))
        {
            tvSubPriceValue.setText(getResources().getString(R.string.vtc));
        }
        else if(drawerActivity.currentTrip.getService_selection().equalsIgnoreCase("CarRent"))
        {
            tvSubPriceValue.setText(getResources().getString(R.string.text_rent_trip));
        }





    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnFeedBackDone:
                if (ratingBar.getRating() != 0) {
                    giveFeedBack();
                } else {
                    Utils.showToast(drawerActivity.getResources().getString(R.string
                                    .msg_give_ratting),
                            drawerActivity);
                }
                break;
            default:
                // do with default
                break;
        }
    }

    public void giveFeedBack() {

        JSONObject jsonObject = new JSONObject();
        Utils.showCustomProgressDialog(drawerActivity, drawerActivity.getResources().getString(R
                .string
                .msg_waiting_for_ratting), false, null);

        try {
            jsonObject.accumulate(Const.Params.PROVIDER_ID, drawerActivity.preferenceHelper
                    .getProviderId());
            jsonObject.accumulate(Const.Params.TRIP_ID, drawerActivity.preferenceHelper.getTripId
                    ());
            jsonObject.accumulate(Const.Params.REVIEW, etComment.getText().toString());
            jsonObject.accumulate(Const.Params.TOKEN, drawerActivity.preferenceHelper
                    .getSessionToken());
            jsonObject.accumulate(Const.Params.RATING, ratingBar.getRating());


            Call<IsSuccessResponse> call = ApiClient.getClient(getActivity()).create(ApiInterface.class)
                    .giveRating(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    if (ParseContent.getInstance().isSuccessful(response)) {
                        if (response.body().isSuccess()) {
                            drawerActivity.currentTrip.clearData();
                            drawerActivity.goToMapFragment();
                            Utils.hideCustomProgressDialog();
                        } else {
                            Utils.showErrorToast(response.body().getErrorCode(), drawerActivity);
                            Utils.hideCustomProgressDialog();
                        }
                    }

                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(FeedbackFragment.class.getSimpleName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.FEEDBACK_FRAGMENT, e);
        }

    }

}
