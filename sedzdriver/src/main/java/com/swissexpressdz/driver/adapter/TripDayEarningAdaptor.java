package com.swissexpressdz.driver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.models.datamodels.TripsEarning;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CurrencyHelper;
import com.swissexpressdz.driver.utils.PreferenceHelper;

import java.text.NumberFormat;
import java.util.List;



public class TripDayEarningAdaptor extends RecyclerView.Adapter<TripDayEarningAdaptor
        .OrderDayView> {

    private List<TripsEarning> tripsEarnings;
    private Context context;
    private ParseContent parseContent;
    private NumberFormat numberFormat;

    public TripDayEarningAdaptor(Context context, List<TripsEarning> tripsEarnings) {
        this.tripsEarnings = tripsEarnings;
        this.context = context;
        parseContent = ParseContent.getInstance();
        numberFormat =
                CurrencyHelper.getInstance(context).getCurrencyFormat(PreferenceHelper.getInstance(context).getCurrencyCode());
    }

    @Override
    public OrderDayView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                        .item_payment_earning,
                parent, false);
        return new OrderDayView(view);
    }

    @Override
    public void onBindViewHolder(OrderDayView holder, int position) {
        TripsEarning tripsEarning = (TripsEarning) tripsEarnings.get(position);
        holder.tvTripNo.setText(String.valueOf(tripsEarning.getUniqueId()));
        holder.tvTotal.setText(numberFormat.format(tripsEarning.getTotal()));
    //   holder.tvTotal.setText(PreferenceHelper.getInstance(context).getCurrencyCode()+""+ tripsEarning.getTotal());

    /* holder.tvProfit.setText(""+tripsEarning
                .getProviderServiceFees());*/

      holder.tvProfit.setText(numberFormat.format(tripsEarning.getProviderServiceFees()));
      //  holder.tvCash.setText(numberFormat.format(tripsEarning.getProviderHaveCash()));

        holder.tvCash.setText(numberFormat.format(tripsEarning.getProviderHaveCash()));

/*
        holder.tvCash.setText(""+tripsEarning
                .getProviderHaveCash());*/
        holder.tvEarn.setText(numberFormat.format(tripsEarning.getPayToProvider()));
        holder.tvWallet.setText(numberFormat.format(tripsEarning.getProviderIncomeSetInWallet()));

        if (tripsEarning.getPaymentMode() == Const.CASH) {
            holder.tvPaymentMode.setText(context.getResources().getString(R.string.text_cash));
        } else {
            holder.tvPaymentMode.setText(context.getResources().getString(R.string.text_card));
        }


    }

    @Override
    public int getItemCount() {
        return tripsEarnings.size();
    }


    protected class OrderDayView extends RecyclerView.ViewHolder {

        MyFontTextView tvTripNo, tvPaymentMode, tvTotal, tvProfit, tvWallet,
                tvCash, tvEarn;

        public OrderDayView(View itemView) {
            super(itemView);
            tvTripNo = itemView.findViewById(R.id.tvTripNo);
            tvPaymentMode = itemView.findViewById(R.id.tvPaymentMode);
            tvTotal = itemView.findViewById(R.id.tvTotal);
            tvProfit = itemView.findViewById(R.id.tvProfit);
            tvCash = itemView.findViewById(R.id.tvCash);
            tvWallet = itemView.findViewById(R.id.tvWallet);
            tvEarn = itemView.findViewById(R.id.tvEarn);

        }
    }
}
