package com.swissexpressdz.driver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.models.datamodels.Country;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;

import java.util.ArrayList;


public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder>
        implements Filterable {

    private ArrayList<Country> countryList;
    private Filter filter;
    Context context;
    String login="";


    public CountryAdapter(Context context, ArrayList<Country> countryList) {
        this.countryList = countryList;
        this.context=context;

    }
    public CountryAdapter(Context context, ArrayList<Country> countryList,String login) {
        this.countryList = countryList;
        this.context=context;
        this.login=login;
    }
    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_country_code,
                parent, false);
        CountryViewHolder countryViewHolder = new CountryViewHolder(view);

        return countryViewHolder;
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, int position) {
        holder.tvCountryCodeDigit.setText(countryList.get(position).getCountryphonecode());
        holder.tvCountryName.setText(countryList.get(position).getCountryname());

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            holder.tvFlag.setText(countryList.get(position).getFlagUrl());
//        }

        if(login!=null && !login.equalsIgnoreCase("") &&
                !login.equalsIgnoreCase("null"))
        {
            System.out.println(""+countryList.get(position).getFlagUrl());

            holder.tvFlag.setVisibility(View.GONE);
            holder.tvFlag_txt.setText(countryList.get(position).getFlagUrl());
        }

        else
        {
            holder.tvFlag_txt.setVisibility(View.GONE);

            PicassoTrustAll.getInstance(context)
                    .load(ApiClient.Base_URL+countryList.get(position).getFlagUrl())
                    // .error(R.drawable.ellipse)
                    .into(holder.tvFlag);
        }




    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new AppFilter(countryList);
        return filter;
    }

    public ArrayList<Country> getFilterResult() {
        return countryList;
    }

    protected class CountryViewHolder extends RecyclerView.ViewHolder {
        TextView tvCountryCodeDigit, tvCountryName,tvFlag_txt;
        ImageView tvFlag;
        View viewDive;


        public CountryViewHolder(View itemView) {
            super(itemView);

            tvCountryCodeDigit = itemView.findViewById(R.id.tvCountryCodeDigit);
            tvCountryName = itemView.findViewById(R.id.tvCountryName);
            tvFlag = itemView.findViewById(R.id.tvFlag);
            tvFlag_txt=itemView.findViewById(R.id.tvFlag_txt);
            viewDive = itemView.findViewById(R.id.viewDiveCountry);

        }
    }

    private class AppFilter extends Filter {

        private ArrayList<Country> sourceObjects;

        public AppFilter(ArrayList<Country> objects) {

            sourceObjects = new ArrayList<Country>();
            synchronized (this) {
                sourceObjects.addAll(objects);
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence chars) {
            String filterSeq = chars.toString();
            FilterResults result = new FilterResults();
            if (filterSeq != null && filterSeq.length() > 0) {
                ArrayList<Country> filter = new ArrayList<Country>();
                for (Country countryCode : sourceObjects) {
                    // the filtering itself:
                    if (countryCode.getCountryname().toUpperCase().startsWith(filterSeq
                            .toUpperCase()))
                        filter.add(countryCode);
                }
                result.count = filter.size();
                result.values = filter;
            } else {
                // add all objects
                synchronized (this) {
                    result.values = sourceObjects;
                    result.count = sourceObjects.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            // NOTE: this function is *always* called from the UI thread.

            if (results.count != 0) {
                countryList = (ArrayList<Country>) results.values;
                notifyDataSetChanged();
            }


        }

    }
}
