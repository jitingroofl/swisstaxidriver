package com.swissexpressdz.driver.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.components.MyAppTitleFontTextView;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.models.datamodels.Analytic;

import java.util.ArrayList;



public class TripAnalyticAdapter extends RecyclerView.Adapter<TripAnalyticAdapter
        .OrderAnalyticView> {

    private ArrayList<Analytic> arrayListProviderAnalytic;

    public TripAnalyticAdapter(ArrayList<Analytic> arrayListProviderAnalytic) {
        this.arrayListProviderAnalytic = arrayListProviderAnalytic;
    }

    @Override
    public OrderAnalyticView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_analitic_item,
                parent, false);
        return new OrderAnalyticView(view);
    }

    @Override
    public void onBindViewHolder(OrderAnalyticView holder, int position) {
        holder.tvAnalyticName.setText(arrayListProviderAnalytic.get(position).getTitle());

        System.out.println("analy value::::::"+arrayListProviderAnalytic.get(position).getValue());
        holder.tvAnalyticValue.setText(arrayListProviderAnalytic.get(position).getValue());
    }

    @Override
    public int getItemCount() {
        return arrayListProviderAnalytic.size();
    }

    protected class OrderAnalyticView extends RecyclerView.ViewHolder {

        MyFontTextView tvAnalyticValue;
        MyAppTitleFontTextView tvAnalyticName;

        public OrderAnalyticView(View itemView) {
            super(itemView);
            tvAnalyticValue = itemView.findViewById(R.id.tvAnalyticValue);
            tvAnalyticName = itemView.findViewById(R.id.tvAnalyticName);
        }
    }
}
