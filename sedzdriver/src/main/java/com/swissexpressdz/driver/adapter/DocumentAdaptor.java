package com.swissexpressdz.driver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.swissexpressdz.driver.R;

import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.models.datamodels.Document;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.Const;

import java.util.ArrayList;


public class DocumentAdaptor extends RecyclerView.Adapter<DocumentAdaptor
        .DocumentViewHolder> {

    private ArrayList<Document> docList;
    private Context context;

    public DocumentAdaptor(Context context, ArrayList<Document> docList) {
        this.docList = docList;
        this.context = context;
    }


    @Override
    public DocumentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .item_document, parent, false);

        return new DocumentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DocumentViewHolder holder, int position) {

        Document document = docList.get(position);

//        Glide.with(context).load(ApiClient.Base_URL + document.getDocumentPicture()).dontAnimate()
//                .fallback(R.drawable
//                        .ellipse)
//                .override(200, 200).placeholder(R.drawable.ellipse).into(holder.ivDocumentImage);

        PicassoTrustAll.getInstance(context)
                .load(ApiClient.Base_URL + document.getDocumentPicture())
                .error(R.drawable.ellipse)
                .resize(200,200)
                .into(holder.ivDocumentImage);
/*        if (document.isIsExpiredDate()) {
            holder.tvExpireDate.setVisibility(View.VISIBLE);
            String date = context.getResources().getString(R.string.text_expire_date);
            try {
                if (!TextUtils.isEmpty(document.getExpiredDate())) {
                    date = date + " " + ParseContent.getInstance().dateFormat.format(ParseContent
                            .getInstance()
                            .webFormatWithLocalTimeZone
                            .parse(document.getExpiredDate()));
                }
            } catch (ParseException e) {
                AppLog.handleException(DocumentActivity_Driver.class.getSimpleName(), e);
            }
            holder.tvExpireDate.setText(date);
        } else {
            holder.tvExpireDate.setVisibility(View.GONE);
        }
        if (document.isIsUniqueCode()) {
            holder.tvIdNumber.setVisibility(View.VISIBLE);
            String date = context.getResources().getString(R.string.text_id_number) + " " +
                    "" + document.getUniqueCode();
            holder.tvIdNumber.setText(date);
        } else {
            holder.tvIdNumber.setVisibility(View.GONE);
        }*/
        holder.tvDocumentTittle.setText(document.getName());
        if (document.getOption() == Const.TRUE) {
            holder.tvOption.setVisibility(View.VISIBLE);
        } else {
            holder.tvOption.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return docList.size();
    }


    protected class DocumentViewHolder extends RecyclerView.ViewHolder {

        ImageView ivDocumentImage;
        MyFontTextView tvIdNumber, tvExpireDate, tvDocumentTittle, tvOption;
        LinearLayout llDocumentUpload;

        public DocumentViewHolder(View itemView) {
            super(itemView);
            ivDocumentImage = (ImageView) itemView.findViewById(R.id.ivDocumentImage);
            tvDocumentTittle = (MyFontTextView) itemView.findViewById(R.id.tvDocumentTittle);
            llDocumentUpload = (LinearLayout) itemView.findViewById(R.id.llDocumentUpload);
            tvIdNumber = (MyFontTextView) itemView.findViewById(R.id.tvIdNumber);
            tvExpireDate = (MyFontTextView) itemView.findViewById(R.id.tvExpireDate);
            tvOption = (MyFontTextView) itemView.findViewById(R.id.tvOption);
        }


    }
}
