package com.swissexpressdz.driver.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.PaymentActivity_Driver;
import com.swissexpressdz.driver.models.datamodels.Card;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;

import java.util.ArrayList;


public abstract class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    public static final String TAG = "CardAdapter";
    private PaymentActivity_Driver paymentActivity;
    private ArrayList<Card> cardList;
    private ParseContent parseContent;

    public CardAdapter(PaymentActivity_Driver paymentActivity, ArrayList<Card> cardList) {
        this.cardList = cardList;
        this.paymentActivity = paymentActivity;
        parseContent = ParseContent.getInstance();
        parseContent.getContext(paymentActivity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(paymentActivity).inflate(R.layout.item_payment_card_list,
                parent,
                false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        Card card = cardList.get(position);
        String cardNo = card.getLastFour();
        String cardType = card.getCardType().toUpperCase();
        AppLog.Log("cardNo", cardNo + " " + cardType);
        holder.iVCardNo.setText("****" + cardNo);
        holder.llCreditCard.setVisibility(View.VISIBLE);
        holder.ivSelected.setVisibility(card.getIsDefault() == Const.TRUE ? View
                .VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return cardList.size();

    }

    public abstract void onSelected(int position);

    public abstract void onClickRemove(int position);

    /**
     * Holder for item
     */

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView iVCardNo;
        LinearLayout llCreditCard;
        ImageView ivDelete, ivSelected;

        public ViewHolder(View itemView) {
            super(itemView);

            iVCardNo = (TextView) itemView.findViewById(R.id.tvCardNo);
            llCreditCard = itemView.findViewById(R.id.llCreditCard);
            llCreditCard.setOnClickListener(this);
            ivDelete = itemView.findViewById(R.id.ivDelete);
            ivSelected = itemView.findViewById(R.id.ivSelected);
            ivDelete.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivDelete:
                    onClickRemove(getAdapterPosition());
                    break;
                case R.id.llCreditCard:
                    onSelected(getAdapterPosition());
                    break;
            }

        }

    }
}