package com.swissexpressdz.driver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.models.datamodels.VehicleDetail;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.PreferenceHelper;

import java.util.ArrayList;


/**
 * Created by elluminati on 15-Sep-17.
 */

public abstract class VehicleSelectionAdapter extends RecyclerView
        .Adapter<VehicleSelectionAdapter.MyViewHolder> {

    private ArrayList<VehicleDetail> listVehicle;
    private Context context;
    private boolean isEnable = true;
    PreferenceHelper preferenceHelper;

    public VehicleSelectionAdapter(Context context, ArrayList<VehicleDetail> listVehicle) {

        this.context = context;
        this.listVehicle = listVehicle;
        preferenceHelper = PreferenceHelper.getInstance(context);

    }
    public VehicleSelectionAdapter(Context context, ArrayList<VehicleDetail> listVehicle, PreferenceHelper preferenceHelper) {

        this.context = context;
        this.listVehicle = listVehicle;


    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vehicle_list,
                parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final VehicleDetail vehicleDetail = listVehicle.get(position);

        holder.tvVehicleListName.setText(vehicleDetail.getName() + " " + vehicleDetail
                .getModel());
        holder.tvVehicleListPlateNo.setText(vehicleDetail.getPlateNo());


        holder.rbSelectVehicle.setChecked(vehicleDetail.isIsSelected());
        if (!vehicleDetail.getIsDocumentUploaded()) {
            holder.tvExpireMsg.setText(context.getResources().getString(R.string.text_document_not_uploded));
            holder.tvExpireMsg.setVisibility(View.VISIBLE);
        }
        else if (vehicleDetail.isIsDocumentsExpired()) {
            holder.tvExpireMsg.setText(context.getResources().getString(R.string.text_document_expire));
            holder.tvExpireMsg.setVisibility(View.VISIBLE);
        } else {
            holder.tvExpireMsg.setVisibility(View.GONE);
        }
//        Glide.with(context).load(ApiClient.Base_URL + vehicleDetail.getTypeImageUrl()
//        ).fallback(R.drawable.car_placeholder).placeholder(R.drawable
//                .car_placeholder).override(100, 100)
//                .into(holder.ivCarVehicleList);

        PicassoTrustAll.getInstance(context)
                .load(ApiClient.Base_URL + vehicleDetail.getTypeImageUrl())
                .error(R.drawable.ellipse)
                .resize(100,100)
                .into(holder.ivCarVehicleList);

        holder.rbSelectVehicle.setEnabled(isEnable);

        holder.rbSelectVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(vehicleDetail.getServiceType()) || vehicleDetail
                        .isIsDocumentsExpired()) {
                    onVehicleSelect(position, vehicleDetail.getId(), false);
                    holder.rbSelectVehicle.setChecked(false);
                } else {
                    onVehicleSelect(position, vehicleDetail.getId(), true);
                }

            }
        });


        holder.llEditVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onVehicleClick(vehicleDetail.getId());
            }
        });


    }

    @Override
    public int getItemCount() {
        return listVehicle.size();
    }

    public abstract void onVehicleSelect(int position, String vehicleId, boolean
            isHaveServiceTypeID);

    public abstract void onVehicleClick(String vehicleId);

    public void changeSelection(int position) {

        for (int i = 0; i < listVehicle.size(); i++) {
            listVehicle.get(i).setIsSelected(false);
        }
        preferenceHelper.putTypeid(listVehicle.get(position).getAdminTypeId());
        listVehicle.get(position).setIsSelected(true);

        notifyDataSetChanged();
    }

    public void setVehicleChangeEnable(boolean isEnable) {
        this.isEnable = isEnable;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivCarVehicleList;
        private MyFontTextView tvVehicleListName;
        private MyFontTextView tvVehicleListPlateNo, tvExpireMsg;
        private RadioButton rbSelectVehicle;
        private LinearLayout llEditVehicle;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivCarVehicleList = (ImageView) itemView.findViewById(R.id.ivCarVehicleList);
            tvVehicleListName = (MyFontTextView) itemView.findViewById(R.id.tvVehicleListName);
            tvVehicleListPlateNo = (MyFontTextView) itemView.findViewById(R.id
                    .tvVehicleListPlateNo);
            rbSelectVehicle = (RadioButton) itemView.findViewById(R.id.rbSelectVehicle);
            llEditVehicle = (LinearLayout) itemView.findViewById(R.id.llEditVehicle);
            tvExpireMsg = itemView.findViewById(R.id.tvExpireMsg);
        }
    }

}
