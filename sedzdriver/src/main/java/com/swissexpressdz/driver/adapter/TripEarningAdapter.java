package com.swissexpressdz.driver.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.components.MyAppTitleFontTextView;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.models.datamodels.EarningData;
import com.swissexpressdz.driver.utils.SectionedRecyclerViewAdapter;

import java.util.ArrayList;



public class TripEarningAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    private ArrayList<ArrayList<EarningData>> arrayListForEarning;

    public TripEarningAdapter(ArrayList<ArrayList<EarningData>> arrayListForEarning) {
        this.arrayListForEarning = arrayListForEarning;
    }

    @Override
    public int getSectionCount() {
        return arrayListForEarning.size();
    }

    @Override
    public int getItemCount(int section) {
        return arrayListForEarning.get(section).size();
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {

        OrderEarningHeading heading = (OrderEarningHeading) holder;
        heading.tvEarningHeader.setText(arrayListForEarning.get(section).get(0)
                .getTitleMain());

        System.out.println("Title Header is###"+arrayListForEarning.get(section).get(0)
                .getTitleMain());
        /*if (arrayListForEarning.size() - 1 == section) {
            heading.tvEarningHeader.setText("");
            heading.tvEarningHeader.setPadding(0, 5, 0, 0);
            heading.tvEarningHeader.setVisibility(View.INVISIBLE);
        } else {
            heading.tvEarningHeader.setVisibility(View.VISIBLE);
        }*/

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, int
            relativePosition, int absolutePosition) {
        OrderEarningItem item = (OrderEarningItem) holder;

        System.out.println("Title Item  is###"+arrayListForEarning.get(section).get(relativePosition).getTitle());
        item.tvName.setText(arrayListForEarning.get(section).get(relativePosition).getTitle());

        String value=arrayListForEarning.get(section).get(relativePosition).getPrice();


        item.tvPrice.setText(arrayListForEarning.get(section).get(relativePosition).getPrice());

      /*  double dub_price= Double.parseDouble(value);
        String str_main_value=String.format("%.2f", dub_price);
        System.out.println("price show:::::"+str_main_value);


        item.tvPrice.setText(value);*/
        //item.tvName.setAllCaps(arrayListForEarning.size() - 1 == section);

        //String.format("%.2f", provider.getRate())
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                return new OrderEarningHeading(LayoutInflater.from(parent.getContext()).inflate(R
                        .layout.item_earning_header, parent, false));
            case VIEW_TYPE_ITEM:
                return new OrderEarningItem(LayoutInflater.from(parent.getContext()).inflate(R
                        .layout.item_earning_item, parent, false));
            default:
                // do with default
                break;
        }
        return null;
    }


    protected class OrderEarningHeading extends RecyclerView.ViewHolder {
        MyAppTitleFontTextView tvEarningHeader;

        public OrderEarningHeading(View itemView) {
            super(itemView);
            tvEarningHeader = itemView.findViewById(R.id.tvEarningHeader);
        }
    }

    protected class OrderEarningItem extends RecyclerView.ViewHolder {
        MyFontTextView tvName, tvPrice;

        public OrderEarningItem(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
        }
    }
}
