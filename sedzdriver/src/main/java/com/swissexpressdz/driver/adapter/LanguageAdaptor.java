package com.swissexpressdz.driver.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.components.MyFontTextView;


public class LanguageAdaptor extends RecyclerView.Adapter<LanguageAdaptor.LanguageViewHolder> {
    private TypedArray langCode;
    private TypedArray langName;
    private Context context;
    private int focusedItem = 0;

    public LanguageAdaptor(Context context) {
        this.context = context;
        langCode = context.getResources().obtainTypedArray(R.array.language_code);
        langName = context.getResources().obtainTypedArray(R.array.language_name);
        Log.e("Language Code  ",langCode+"");
        Log.e("Language Name  ",langName+"");


    }
    public LanguageAdaptor(Context context, int post) {
        this.context = context;
        langCode = context.getResources().obtainTypedArray(R.array.language_code);
        langName = context.getResources().obtainTypedArray(R.array.language_name);
        focusedItem=post;
        Log.e("Language Code  ",langCode+"");
        Log.e("Language Name  ",langName+"");
        Log.e("Language Name  ",post+"");


    }
    @Override
    public LanguageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city_name, parent, false);
        return new LanguageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LanguageViewHolder holder, final int position) {
        holder.tvCityName.setText(langName.getString(position));

        holder.ll_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusedItem = position;
                notifyDataSetChanged();


            }
        });

        if(focusedItem==position){
            //  viewHolder.rr_layout.setBackgroundColor(Color.parseColor("#567845"));
            Drawable new_image= context.getResources().getDrawable(R.drawable.language_check_bg);
            holder.rr_check.setBackgroundDrawable(new_image);

            holder.rr_check.setVisibility(View.VISIBLE);
            holder.rr_uncheck.setVisibility(View.GONE);


        }
        else
        {
            holder.ll_language.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.rr_check.setVisibility(View.GONE);
            holder.rr_uncheck.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public int getItemCount() {
        return langName.length();
    }

    protected class LanguageViewHolder extends RecyclerView.ViewHolder {
        MyFontTextView tvCityName;
        RelativeLayout rr_check,rr_uncheck;
        LinearLayout ll_language;
        public LanguageViewHolder(View itemView) {
            super(itemView);
            tvCityName = (MyFontTextView) itemView.findViewById(R.id.tvItemCityName);
            rr_check=(RelativeLayout)itemView.findViewById(R.id.rr_check);
            rr_uncheck=(RelativeLayout)itemView.findViewById(R.id.rr_uncheck);
            ll_language=(LinearLayout)itemView.findViewById(R.id.ll_language);
        }
    }


}
