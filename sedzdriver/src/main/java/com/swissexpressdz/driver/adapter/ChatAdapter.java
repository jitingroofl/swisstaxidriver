package com.swissexpressdz.driver.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swissexpressdz.driver.R;
import com.swissexpressdz.driver.ChatActivity_Driver;
import com.swissexpressdz.driver.models.datamodels.Message;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.picasso.PicassoTrustAll;
import com.swissexpressdz.driver.utils.Const;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.swissexpressdz.driver.utils.PreferenceHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends FirebaseRecyclerAdapter<Message, ChatAdapter
        .MessageViewHolder> {



    private android.content.Context context;
    private SimpleDateFormat dateFormat, webFormat;
    private ParseContent parseContent;
    private ChatActivity_Driver chatActivity;

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public ChatAdapter(ChatActivity_Driver chatActivity, @NonNull FirebaseRecyclerOptions<Message>
            options) {
        super(options);
        webFormat = new SimpleDateFormat(Const.DATE_TIME_FORMAT_WEB, Locale.US);
        webFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateFormat = new SimpleDateFormat("dd-MM-yyyy, HH:mm", Locale.US);
        parseContent = ParseContent.getInstance();
        this.chatActivity = chatActivity;
    }

    @Override
    protected void onBindViewHolder(@NonNull MessageViewHolder holder, int position, @NonNull
            Message model) {

        if (model.getType() == Const.PROVIDER_UNIQUE_NUMBER) {
            holder.rrSent.setVisibility(View.VISIBLE);
            holder.rrReceive.setVisibility(View.GONE);
//            holder.llSent.setVisibility(View.VISIBLE);
//            holder.llReceive.setVisibility(View.GONE);
            holder.tvSentMessage.setText(model.getMessage());
            try {
                Date date = webFormat.parse(model.getTime());
                holder.tvSentTime.setText(dateFormat.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (model.isIs_read()) {
                //holder.tvRead.setText(R.string.text_read);
                holder.tvRead.setText("");
            } else {
                holder.tvRead.setText("");
            }

            PicassoTrustAll.getInstance(chatActivity)
                    .load(PreferenceHelper.getInstance(chatActivity).getProfilePic())
                    .error(R.drawable.ellipse)
                    .resize(50,50)
                    .into(holder.sender_img);


        } else {
//            holder.llSent.setVisibility(View.GONE);
//            holder.llReceive.setVisibility(View.VISIBLE);
            holder.rrSent.setVisibility(View.GONE);
            holder.rrReceive.setVisibility(View.VISIBLE);
            holder.tvReceiveMessage.setText(model.getMessage());
            try {
                Date date = parseContent.webFormat.parse(model.getTime());
                holder.tvReceiveTime.setText(dateFormat.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (!model.isIs_read()) {
                chatActivity.setAsReadMessage(model.getId());
            }

            PicassoTrustAll.getInstance(chatActivity)
                    .load(CurrentTrip.getInstance().getUserProfileImage())
                    .error(R.drawable.ellipse)
                    .resize(50,50)
                    .into(holder.reciver_img);


        }


    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new MessageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout
                .item_message, parent, false));
    }

    protected class MessageViewHolder extends RecyclerView.ViewHolder {

        TextView tvReceiveMessage, tvSentMessage, tvSentTime, tvReceiveTime, tvRead;
        LinearLayout llReceive, llSent;
        RelativeLayout rrReceive, rrSent;
        CircleImageView reciver_img,sender_img;

        public MessageViewHolder(View itemView) {
            super(itemView);
            tvReceiveMessage = itemView.findViewById(R.id.tvReceiveMessage);
            tvSentMessage = itemView.findViewById(R.id.tvSentMessage);
            tvRead = itemView.findViewById(R.id.tvRead);
            tvReceiveTime = itemView.findViewById(R.id.tvReceiveTime);
            tvSentTime = itemView.findViewById(R.id.tvSentTime);
            llReceive = itemView.findViewById(R.id.llReceive);
            llSent = itemView.findViewById(R.id.llSent);
            rrReceive = itemView.findViewById(R.id.rrReceive);
            rrSent = itemView.findViewById(R.id.rrSent);
            sender_img=itemView.findViewById(R.id.sender_img);
            reciver_img=itemView.findViewById(R.id.reciver_img);

        }
    }
}
