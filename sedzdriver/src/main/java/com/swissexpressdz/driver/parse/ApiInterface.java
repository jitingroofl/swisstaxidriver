package com.swissexpressdz.driver.parse;


import com.swissexpressdz.driver.models.datamodels.Document;
import com.swissexpressdz.driver.models.responsemodels.AddServiceResponse;
import com.swissexpressdz.driver.models.responsemodels.AddVTCResponse;
import com.swissexpressdz.driver.models.responsemodels.AddVehicleResponse;
import com.swissexpressdz.driver.models.responsemodels.AddWalletResponse;
import com.swissexpressdz.driver.models.responsemodels.AppKeyResponse;
import com.swissexpressdz.driver.models.responsemodels.BankDetailResponse;
import com.swissexpressdz.driver.models.responsemodels.CardsResponse;
import com.swissexpressdz.driver.models.responsemodels.CarrentalResponse;
import com.swissexpressdz.driver.models.responsemodels.CitiesResponse;
import com.swissexpressdz.driver.models.responsemodels.CountriesResponse;
import com.swissexpressdz.driver.models.responsemodels.DepositListModel;
import com.swissexpressdz.driver.models.responsemodels.DepositeResponseModel;
import com.swissexpressdz.driver.models.responsemodels.DocumentNewResponse;
import com.swissexpressdz.driver.models.responsemodels.DocumentResponse;
import com.swissexpressdz.driver.models.responsemodels.ETAResponse;
import com.swissexpressdz.driver.models.responsemodels.EarningResponse;
import com.swissexpressdz.driver.models.responsemodels.EditVTCResponse;
import com.swissexpressdz.driver.models.responsemodels.GetVtcResponse;
import com.swissexpressdz.driver.models.responsemodels.HeatMapResponse;
import com.swissexpressdz.driver.models.responsemodels.InvoiceResponse;
import com.swissexpressdz.driver.models.responsemodels.IsSuccessResponse;
import com.swissexpressdz.driver.models.responsemodels.LanguageResponse;
import com.swissexpressdz.driver.models.responsemodels.NewDocumentResponse;
import com.swissexpressdz.driver.models.responsemodels.ProviderDataResponse;
import com.swissexpressdz.driver.models.responsemodels.ProviderDetailResponse;
import com.swissexpressdz.driver.models.responsemodels.ProviderLocationResponse;
import com.swissexpressdz.driver.models.responsemodels.ProviderSeriveSelctionModel;
import com.swissexpressdz.driver.models.responsemodels.ServiceTypeModel;
import com.swissexpressdz.driver.models.responsemodels.SettingsDetailsResponse;
import com.swissexpressdz.driver.models.responsemodels.SubServiceTypeModel;
import com.swissexpressdz.driver.models.responsemodels.TripHistoryDetailResponse;
import com.swissexpressdz.driver.models.responsemodels.TripHistoryResponse;
import com.swissexpressdz.driver.models.responsemodels.TripPathResponse;
import com.swissexpressdz.driver.models.responsemodels.TripStatusResponse;
import com.swissexpressdz.driver.models.responsemodels.TripsResponse;
import com.swissexpressdz.driver.models.responsemodels.TypesResponse;
import com.swissexpressdz.driver.models.responsemodels.VehicleDetailResponse;
import com.swissexpressdz.driver.models.responsemodels.VehicleDetailResponseNew;
import com.swissexpressdz.driver.models.responsemodels.VehicleDocumentResponse;
import com.swissexpressdz.driver.models.responsemodels.VehiclesResponse;
import com.swissexpressdz.driver.models.responsemodels.VerificationResponse;
import com.swissexpressdz.driver.models.responsemodels.WalletHistoryResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;

public interface ApiInterface {

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("providerslogin")
    Call<ProviderDataResponse> login(@Body RequestBody requestBody);

    @Multipart
    @POST("providerupdatedetail")
    Call<ProviderDataResponse> updateProfile(@Part MultipartBody.Part file, @PartMap() Map<String,
            RequestBody> partMap);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("update_password")
    Call<IsSuccessResponse> updatePassword(@Body RequestBody requestBody);

    @Multipart
    @POST("providerregister")
    Call<ProviderDataResponse> register(@Part MultipartBody.Part[] file, @PartMap() Map<String,
            RequestBody> partMap);

    @Multipart
    @POST("provider_add_document")
    Call<DocumentNewResponse> provider_add_document(@Part MultipartBody.Part[] file, @PartMap() Map<String,
            RequestBody> partMap);


    @Multipart
    @POST("provider_vehicle")
    Call<AddVehicleResponse> provider_vehicle(@Part MultipartBody.Part[] file, @PartMap() Map<String,
                    RequestBody> partMap);

    @Multipart
    @POST("provider_add_vehicle")
    Call<AddVehicleResponse> addVehicleDetail_Doc2(@Part MultipartBody.Part[] file, @PartMap() Map<String,
            RequestBody> partMap);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @GET("service_category")
    Call<ServiceTypeModel> getServiceType();

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("sub_service_category")
    Call<SubServiceTypeModel> getSubServiceType(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_vehicle_status")
    Call<ProviderSeriveSelctionModel> provider_vehicle_status(@Body RequestBody requestBody);


    @Multipart
    @POST("provider_add_city_type_vtc_value")
    Call<AddVTCResponse> provider_add_city_type_vtc_value(@Part MultipartBody.Part[] file, @PartMap() Map<String,
            RequestBody> partMap);

    @Multipart
    @POST("provider_edit_city_type_vtc_value")
    Call<AddVTCResponse> provider_edit_city_type_vtc_value(@Part MultipartBody.Part[] file, @PartMap() Map<String,
            RequestBody> partMap);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_get_city_type_vtc_value")
    Call<GetVtcResponse> provider_get_city_type_vtc_value(@Body RequestBody requestBody);

    @Multipart
    @POST("deposit")
    Call<DepositeResponseModel> deposit(@Part MultipartBody.Part[] file, @PartMap() Map<String,
            RequestBody> partMap);




    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_get_vtc_city_type")
    Call<EditVTCResponse> provider_get_vtc_city_type(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("forgotpassword")
    Call<IsSuccessResponse> forgotPassword(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("get_provider_setting_detail")
    Call<SettingsDetailsResponse> getProviderSettingDetail(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("getappkeys")
    Call<AppKeyResponse> getAppKeys(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("cards")
    Call<CardsResponse> getCards(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("addcard")
    Call<IsSuccessResponse> addCard(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("delete_card")
    Call<IsSuccessResponse> deleteCard(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("providerlogout")
    Call<IsSuccessResponse> logout(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("apply_referral_code")
    Call<IsSuccessResponse> applyReferralCode(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("providergiverating")
    Call<IsSuccessResponse> giveRating(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("updateproviderdevicetoken")
    Call<IsSuccessResponse> updateDeviceToken(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("add_wallet_amount")
    Call<AddWalletResponse> addWalletAmount(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("get_wallet_history")
    Call<WalletHistoryResponse> getWalletHistory(@Body RequestBody requestBody);

    @GET("api/distancematrix/json")
    Call<ResponseBody> getGoogleDistanceMatrix(@QueryMap Map<String, String> stringMap);

    @GET("api/geocode/json")
    Call<ResponseBody> getGoogleGeocode(@QueryMap Map<String, String> stringMap);

    @GET("api/directions/json")
    Call<ResponseBody> getGoogleDirection(@QueryMap Map<String, String> stringMap);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("verification")
    Call<VerificationResponse> verification(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("getlanguages")
    Call<LanguageResponse> getLanguageForTrip(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("tripcancelbyprovider")
    Call<IsSuccessResponse> cancelTrip(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("get_country_city_list")
    Call<CountriesResponse> getCountries();

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_get_document")
    Call<NewDocumentResponse> providerGetdocument(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("getproviderdocument")
    Call<DocumentResponse> getDocuments(@Body RequestBody requestBody);

    @Multipart
    @POST("uploaddocument")
    Call<Document> uploadDocument(@Part MultipartBody.Part file, @PartMap()
            Map<String,
                    RequestBody> partMap);
    @Multipart
    @POST("updatedocument")
    Call<Document> updateDocument(@Part MultipartBody.Part file, @PartMap()
            Map<String,
                    RequestBody> partMap);

    @Multipart
    @POST("upload_vehicle_document")
    Call<VehicleDocumentResponse> uploadVehicleDocument(@Part MultipartBody.Part file, @PartMap()
            Map<String,
                    RequestBody> partMap);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_location")
    Call<ProviderLocationResponse> uploadProviderLocation(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("togglestate")
    Call<IsSuccessResponse> toggleState(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("gettrips")
    Call<TripsResponse> getTrips(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("respondstrip")
    Call<IsSuccessResponse> respondsTrip(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("providergettripstatus")
    Call<TripStatusResponse> getTripStatus(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("providerhistory")
    Call<TripHistoryResponse> getTripHistory(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("trips_car_rent")
    Call<CarrentalResponse> trips_car_rent(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("deposit_list")
    Call<DepositListModel> getdeposit_list(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("providertripdetail")
    Call<TripHistoryDetailResponse> getTripHistoryDetail(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("get_provider_detail")
    Call<ProviderDetailResponse> getProviderDetail(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("get_provider_vehicle_list")
    Call<VehiclesResponse> getVehicles(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("get_provider_vehicle_detail")
    Call<VehicleDetailResponse> getVehicleDetail(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("get_provider_vehicle_detail")
    Call<VehicleDetailResponseNew> getVehicleDetailnew(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_update_vehicle_detail")
    Call<IsSuccessResponse> updateVehicle(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("change_current_vehicle")
    Call<IsSuccessResponse> changeCurrentVehicle(@Body RequestBody requestBody);

//    @Headers("Content-Type:application/json;charset=UTF-8")
//    @POST("typelist_selectedcountrycity")
//    Call<TypesResponse> getVehicleTypes(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("typelist")
    Call<TypesResponse> getVehicleTypes(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("typelistwithoutlogin_selectedcountrycity")
    Call<TypesResponse> withoutlogin_getVehicleTypes(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("providerupdatetype")
    Call<IsSuccessResponse> updateType(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("getgooglemappath")
    Call<TripPathResponse> getTripPath(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("setgooglemappath")
    Call<IsSuccessResponse> setTripPath(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("completetrip")
    Call<IsSuccessResponse> completeTrip(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("getproviderinvoice")
    Call<InvoiceResponse> getInvoice(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_submit_invoice")
    Call<IsSuccessResponse> submitInvoice(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("card_selection")
    Call<IsSuccessResponse> setSelectedCard(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_add_vehicle")
    Call<VehicleDetailResponse> addVehicleDetail(@Body RequestBody requestBody);



    @Multipart
    @POST("provider_add_vehicle")
    Call<VehicleDetailResponse> addVehicleDetail_Doc(@Part MultipartBody.Part[] file, @PartMap() Map<String,
            RequestBody> partMap);




    @Multipart
    @POST("provider_update_vehicle_detail")
    Call<IsSuccessResponse> updateVehicleDetailNew(@Part MultipartBody.Part[] file, @PartMap() Map<String,
            RequestBody> partMap);



    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_update_vehicle_detail")
    Call<IsSuccessResponse> updateVehicleDetail(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("get_bank_detail")
    Call<BankDetailResponse> getBankDetail(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("add_bank_detail")
    Call<IsSuccessResponse> addBankDetail(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_add_service_type")
    Call<AddServiceResponse> providerAddservicetype(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("delete_bank_detail")
    Call<IsSuccessResponse> deleteBankDetail(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("citilist_selectedcountry")
    Call<CitiesResponse> getCities(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("settripstatus")
    Call<TripStatusResponse> setProviderStatus(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("pay_payment")
    Call<IsSuccessResponse> payPayment(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("check_destination")
    Call<IsSuccessResponse> checkDestination(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_createtrip")
    Call<IsSuccessResponse> providerCreateTrip(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("getfareestimate")
    Call<ETAResponse> getETAForeTrip(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("update_provider_setting")
    Call<IsSuccessResponse> updateProviderSetting(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("get_provider_weekly_earning_detail")
    Call<EarningResponse> getProviderWeeklyEarningDetail(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("get_provider_daily_earning_detail")
    Call<EarningResponse> getProviderDailyEarningDetail(@Body RequestBody requestBody);


    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("provider_heat_map")
    Call<HeatMapResponse> getProviderHeatMap(@Body RequestBody requestBody);

    @Headers("Content-Type:application/json;charset=UTF-8")
    @POST("twilio_voice_call")
    Call<IsSuccessResponse> twilioCall(@Body RequestBody requestBody);
}