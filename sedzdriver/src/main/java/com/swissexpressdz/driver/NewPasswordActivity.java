package com.swissexpressdz.driver;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.swissexpressdz.driver.activity.font.AppLog;
import com.swissexpressdz.driver.models.responsemodels.IsSuccessResponse;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.parse.ParseContent;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomEditTextRegular;
import com.swissexpressdz.driver.utils.PreferenceHelper;
import com.swissexpressdz.driver.utils.Utils;


import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private CustomEditTextRegular ext_password, etNewPassword, etConfirmPassword;
    private String countryCode, contactNumber;
RelativeLayout back_icon,btnUpdatePassword;
    public ParseContent parseContent;

    ImageView water_ball,water_ball_2,water_ball_3,water_ball_4,water_ball_5,water_ball_6,water_ball_7,water_ball_8,Water_ball_9,Water_ball_10;
    ImageView water_ball1,water_ball_21,water_ball_31,water_ball_41,water_ball_51,water_ball_61,water_ball_71,water_ball_81,Water_ball_91,Water_ball_101;
    public PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_change__password);
        loadExtrasData();

        preferenceHelper = PreferenceHelper.getInstance(this);

        parseContent = ParseContent.getInstance();
        parseContent.getContext(this);
        back_icon=(RelativeLayout)findViewById(R.id.back_icon);
        btnUpdatePassword=(RelativeLayout)findViewById(R.id.btnUpdatePassword);

        //   initToolBar();
        // change toolbar color here
      /*  toolbar.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.color_white,
                null));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(0);
        }
        setTitleOnToolbar("");
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.color_black),
                PorterDuff.Mode.SRC_ATOP);*/


        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        ext_password= findViewById(R.id.ext_password);

        btnUpdatePassword.setOnClickListener(this);

        back_icon.setOnClickListener(this);

        randomWaterBallAnimation();

    }

 /*   @Override
    public void goWithBackArrow() {
        hideKeyBord();
        onBackPressed();
    }*/
 private void randomWaterBallAnimation(){

     water_ball =  findViewById(R.id.water_ball);
     water_ball1 =  findViewById(R.id.water_ball1);


     // For First First

     RotateAnimation anim = new RotateAnimation(0f, 350f, 15f, 15f);
     //   RotateAnimation anim = new RotateAnimation(0f, 0.5f, 1.1f, -0.3f);

     anim.setInterpolator(new LinearInterpolator());
     anim.setRepeatCount(Animation.INFINITE);
     anim.setDuration(9000);

     TranslateAnimation mAnimation ;
     mAnimation = new TranslateAnimation(
             TranslateAnimation.RELATIVE_TO_PARENT, 0f,
             TranslateAnimation.RELATIVE_TO_PARENT,0.5f,
             TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
             TranslateAnimation.RELATIVE_TO_PARENT, -0.3f);
     mAnimation.setDuration(25000);
     mAnimation.setRepeatCount(-1);
     mAnimation.setRepeatMode(Animation.REVERSE);
     mAnimation.setInterpolator(new LinearInterpolator());
     water_ball.setAnimation(mAnimation);
     water_ball.startAnimation(anim);
     water_ball1.setAnimation(mAnimation);
     water_ball1.startAnimation(anim);


     // For Second Second
     water_ball_2 =  findViewById(R.id.water_ball_2);
     water_ball_21 =  findViewById(R.id.water_ball_21);
     TranslateAnimation mAnimation2 ;
     mAnimation2 = new TranslateAnimation(
             TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
             TranslateAnimation.RELATIVE_TO_PARENT,0.7f,
             TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
             TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
     mAnimation2.setDuration(30000);
     mAnimation2.setRepeatCount(-1);
     mAnimation2.setRepeatMode(Animation.REVERSE);
     mAnimation2.setInterpolator(new LinearInterpolator());
     water_ball_2.setAnimation(mAnimation2);

     water_ball_2.animate().rotation(1800f).setDuration(25000).start();
     water_ball_2.getAnimation().setRepeatCount(-1);

     water_ball_2.getAnimation().setRepeatMode(Animation.REVERSE);
     water_ball_2.getAnimation().setInterpolator(new LinearInterpolator());
     water_ball_21.setAnimation(mAnimation2);

     water_ball_21.animate().rotation(1800f).setDuration(25000).start();
     water_ball_21.getAnimation().setRepeatCount(-1);

     water_ball_21.getAnimation().setRepeatMode(Animation.REVERSE);
     water_ball_21.getAnimation().setInterpolator(new LinearInterpolator());

     water_ball_3 = findViewById(R.id.water_ball_3);
     water_ball_31 = findViewById(R.id.water_ball_31);


     TranslateAnimation mAnimation3 ;
     mAnimation3 = new TranslateAnimation(
             TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
             TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
             TranslateAnimation.RELATIVE_TO_PARENT, 0f,
             TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
     mAnimation3.setDuration(9000);
     mAnimation3.setRepeatCount(-1);
     mAnimation3.setRepeatMode(Animation.REVERSE);
     mAnimation3.setInterpolator(new LinearInterpolator());
     water_ball_3.setAnimation(mAnimation3);
     water_ball_31.setAnimation(mAnimation3);


     water_ball_4 = findViewById(R.id.water_ball_4);
     water_ball_41 = findViewById(R.id.water_ball_41);
     TranslateAnimation mAnimation4 ;
     mAnimation4 = new TranslateAnimation(
             TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
             TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
             TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
             TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
     mAnimation4.setDuration(45000);
     mAnimation4.setRepeatCount(-1);
     mAnimation4.setRepeatMode(Animation.REVERSE);
     mAnimation4.setInterpolator(new LinearInterpolator());
     water_ball_4.setAnimation(mAnimation4);
     water_ball_41.setAnimation(mAnimation4);

     water_ball_5 = findViewById(R.id.water_ball_5);
     water_ball_51 = findViewById(R.id.water_ball_51);
     TranslateAnimation mAnimation5 ;
     mAnimation5 = new TranslateAnimation(
             TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
             TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
             TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
             TranslateAnimation.RELATIVE_TO_PARENT, 1f);
     mAnimation5.setDuration(20000);
     mAnimation5.setRepeatCount(-1);
     mAnimation5.setRepeatMode(Animation.REVERSE);
     mAnimation5.setInterpolator(new LinearInterpolator());
     water_ball_5.setAnimation(mAnimation5);
     water_ball_51.setAnimation(mAnimation5);

     water_ball_6 = findViewById(R.id.water_ball_6);
     water_ball_61= findViewById(R.id.water_ball_61);
     TranslateAnimation mAnimation6 ;
     mAnimation6 = new TranslateAnimation(
             TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
             TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
             TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
             TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
     mAnimation6.setDuration(35000);
     mAnimation6.setRepeatCount(-1);
     mAnimation6.setRepeatMode(Animation.REVERSE);
     mAnimation6.setInterpolator(new LinearInterpolator());
     water_ball_6.setAnimation(mAnimation6);
     water_ball_61.setAnimation(mAnimation6);

     water_ball_7 = findViewById(R.id.water_ball_7);
     water_ball_71 = findViewById(R.id.water_ball_71);
     TranslateAnimation mAnimation7 ;
     mAnimation7 = new TranslateAnimation(
             TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
             TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
             TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
             TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
     mAnimation7.setDuration(9000);
     mAnimation7.setRepeatCount(-1);
     mAnimation7.setRepeatMode(Animation.REVERSE);
     mAnimation7.setInterpolator(new LinearInterpolator());
     water_ball_7.setAnimation(mAnimation7);
     water_ball_71.setAnimation(mAnimation7);

     water_ball_8 = findViewById(R.id.water_ball_8);
     water_ball_81 = findViewById(R.id.water_ball_81);
     TranslateAnimation mAnimation8 ;
     mAnimation8 = new TranslateAnimation(
             TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
             TranslateAnimation.RELATIVE_TO_PARENT,-0.1f,
             TranslateAnimation.RELATIVE_TO_PARENT, 0f,
             TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
     mAnimation8.setDuration(21000);
     mAnimation8.setRepeatCount(-1);
     mAnimation8.setRepeatMode(Animation.REVERSE);
     mAnimation8.setInterpolator(new LinearInterpolator());
     water_ball_8.setAnimation(mAnimation8);
     water_ball_81.setAnimation(mAnimation8);


     Water_ball_10 = findViewById(R.id.water_ball_10);
     Water_ball_101 = findViewById(R.id.water_ball_101);
     TranslateAnimation mAnimation10 ;
     mAnimation10 = new TranslateAnimation(
             TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
             TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
             TranslateAnimation.RELATIVE_TO_PARENT, 1f,
             TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
     mAnimation10.setDuration(25000);
     mAnimation10.setRepeatCount(-1);
     mAnimation10.setRepeatMode(Animation.REVERSE);
     mAnimation10.setInterpolator(new LinearInterpolator());
     Water_ball_10.setAnimation(mAnimation10);
     Water_ball_101.setAnimation(mAnimation10);

     Water_ball_9 = findViewById(R.id.water_ball_9);
     Water_ball_91 = findViewById(R.id.water_ball_91);
     TranslateAnimation mAnimation9 ;
     mAnimation9 = new TranslateAnimation(
             TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
             TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
             TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
             TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
     mAnimation9.setDuration(10000);
     mAnimation9.setRepeatCount(-1);
     mAnimation9.setRepeatMode(Animation.REVERSE);
     mAnimation9.setInterpolator(new LinearInterpolator());
     Water_ball_9.setAnimation(mAnimation9);
     Water_ball_91.setAnimation(mAnimation9);
 }

    protected boolean isValidate() {
        String msg = null;
        String str_ext_password=ext_password.getText().toString();

        String NewPassword = etNewPassword.getText().toString().trim();
        String ConfirmPassword = etConfirmPassword.getText().toString().trim();

        if(TextUtils.isEmpty(str_ext_password))
        {
            msg = getString(R.string.msg_enter_password);
            ext_password.requestFocus();
            ext_password.setError(msg);
        }
      else   if (TextUtils.isEmpty(NewPassword)) {
            msg = getString(R.string.msg_enter_password);
            etNewPassword.requestFocus();
            etNewPassword.setError(msg);
        } else if (TextUtils.isEmpty(ConfirmPassword)) {
            msg = getString(R.string.msg_enter_valid_password);
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError(msg);
        } else if (!TextUtils.equals(NewPassword, ConfirmPassword)) {
            msg = getString(R.string.msg_incorrect_confirm_password);
            etConfirmPassword.setError(msg);
        }
        return msg == null;
    }

    private void loadExtrasData() {
        if (getIntent().getExtras() != null) {
            contactNumber = getIntent().getExtras().getString(Const.Params.PHONE);
            countryCode = getIntent().getExtras().getString(Const.Params.COUNTRY_PHONE_CODE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUpdatePassword:
                if (isValidate()) {
                    updatePassword(countryCode, contactNumber, etConfirmPassword.getText()
                            .toString().trim());
                }
                break;
            case R.id.back_icon:

onBackPressed();

                break;

        }

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }


/*    @Override
    protected void onResume() {
        super.onResume();
        setAdminApprovedListener(this);
        setConnectivityListener(this);
    }

    @Override
    public void onAdminApproved() {

    }

    @Override
    public void onAdminDeclined() {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            closedEnableDialogInternet();
        } else {
            openInternetDialog();
        }
    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {

    }*/

    private void updatePassword(String countryCode, String contactNumber, String password) {
        Utils.showCustomProgressDialog(this, getResources().getString(R.string.msg_loading),
                false, null);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.PHONE, contactNumber);
            jsonObject.put(Const.Params.COUNTRY_PHONE_CODE, countryCode);
            jsonObject.put(Const.Params.PASSWORD, password);
            Call<IsSuccessResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .updatePassword(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    if (parseContent.isSuccessful(response)) {
                        if (response.body().isSuccess()) {

                            Utils.showMessageToast(response.body().getMessage(),
                                    NewPasswordActivity.this);
                            Utils.hideCustomProgressDialog();
                           logOut();
                        } else {
                            Utils.hideCustomProgressDialog();
                            Utils.showErrorToast(response.body().getErrorCode(),
                                    NewPasswordActivity.this);
                        }
                    }


                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(NewPasswordActivity.class.getSimpleName(), t);
                }
            });
        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.SIGN_IN_ACTIVITY, e);
        }
    }

    public void logOut() {
        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_log_out), false, null);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.USER_ID, preferenceHelper.getProviderId());
            jsonObject.put(Const.Params.TOKEN, preferenceHelper.getSessionToken());

            Call<IsSuccessResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .logout(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    if (parseContent.isSuccessful(response)) {
                        if (response.body().isSuccess()) {
                            Utils.hideCustomProgressDialog();
                            Utils.showMessageToast(response.body().getMessage(),
                                    NewPasswordActivity
                                            .this);
                            preferenceHelper.logout();// clear session token

                            Intent ii=new Intent(NewPasswordActivity.this, SignInActivity_Driver.class);
                            startActivity(ii);
                        } else {
                            Utils.hideCustomProgressDialog();
                            Utils.showErrorToast(response.body()
                                    .getErrorCode(), NewPasswordActivity.this);
                        }
                    }


                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(BaseAppCompatActivity.class.getSimpleName(), t);
                }
            });

        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.MAIN_DRAWER_ACTIVITY, e);
        }
    }

}
