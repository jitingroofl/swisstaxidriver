package com.swissexpressdz.driver.interfaces;

public interface ConnectivityReceiverListener {
    void onNetworkConnectionChanged(boolean isConnected);

    void onGpsConnectionChanged(boolean isConnected);
}

