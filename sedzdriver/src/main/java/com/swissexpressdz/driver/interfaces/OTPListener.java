package com.swissexpressdz.driver.interfaces;

public interface OTPListener {

     void otpReceived(String otp);
}