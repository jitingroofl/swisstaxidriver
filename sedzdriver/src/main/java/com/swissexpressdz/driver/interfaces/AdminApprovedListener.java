package com.swissexpressdz.driver.interfaces;

public interface AdminApprovedListener {
    void onAdminApproved();

    void onAdminDeclined();
}