package com.swissexpressdz.driver;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.santalu.maskedittext.MaskEditText;
import com.swissexpressdz.driver.components.CustomCountryDialog;
import com.swissexpressdz.driver.components.CustomDialogVerifyAccount;
import com.swissexpressdz.driver.components.MyAppTitleFontTextView;
import com.swissexpressdz.driver.components.MyFontButton;
import com.swissexpressdz.driver.components.MyFontEdittextView;
import com.swissexpressdz.driver.components.MyFontTextView;
import com.swissexpressdz.driver.models.datamodels.Country;
import com.swissexpressdz.driver.models.responsemodels.IsSuccessResponse;
import com.swissexpressdz.driver.models.responsemodels.ProviderDataResponse;
import com.swissexpressdz.driver.models.singleton.CurrentTrip;
import com.swissexpressdz.driver.parse.ApiClient;
import com.swissexpressdz.driver.parse.ApiInterface;
import com.swissexpressdz.driver.utils.AppLog;
import com.swissexpressdz.driver.utils.Const;
import com.swissexpressdz.driver.utils.CustomTextViewBold;
import com.swissexpressdz.driver.utils.CustomTextViewRegular;
import com.swissexpressdz.driver.utils.GoogleClientHelper;
import com.swissexpressdz.driver.utils.Utils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity_Driver extends BaseAppCompatActivity implements TextView
        .OnEditorActionListener, GoogleApiClient.OnConnectionFailedListener {
    private MyFontEdittextView etSignInEmail, etSignInPassword;
    private CustomTextViewBold tvForgotPassword;

    private MyFontButton btnSignIn;
    private RelativeLayout rr_login,rr_layout;
    //    private MyAppTitleFontTextView tvGotoRegister;
    LinearLayout rr_create_account,ll_fb_login;
    private CustomDialogVerifyAccount dialogForgotPassword;
    private ImageView ivFacebook, ivGoogle;
    private CallbackManager callbackManager;
    private String socialId, socialEmail;
    private GoogleApiClient googleApiClient;
    Animation slide_bottom_to_top;
    private ArrayList<Country> codeArrayList;
    ImageView water_ball,water_ball_2,water_ball_3,water_ball_4,water_ball_5,water_ball_6,water_ball_7,water_ball_8,Water_ball_9,Water_ball_10;
    ImageView water_ball1,water_ball_21,water_ball_31,water_ball_41,water_ball_51,water_ball_61,water_ball_71,water_ball_81,Water_ball_91,Water_ball_101;
    LinearLayout ll_loginwithemail,ll_loginwithphonenumber,ll_ext_country_code;
    MaskEditText ext_phone;
    String countryCode_forFormate,countryName;
    private Country country;
    MyFontTextView ext_country_code;
    int phoneNumberLength,phoneNumberMinLength;
    TextView tvFlag;
    private CustomCountryDialog customCountryDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signin_driver);

        codeArrayList = parseContent.getRawCountryCodeList();
        //   CurrentTrip.getInstance().setCountryCodes(codeArrayList);

        //codeArrayList = CurrentTrip.getInstance().getCountryCodes();
        System.out.println("Contry Array List  "+codeArrayList);

        //CurrentTrip.getInstance().clear();
        FacebookSdk.sdkInitialize(getApplicationContext());

        slide_bottom_to_top = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bottom_up);
        googleApiClient = new GoogleClientHelper(this).build();
        checkPlayServices();
//        btnSignIn = (MyFontButton) findViewById(R.id.btnSignIn);
        rr_layout=(RelativeLayout)findViewById(R.id.rr_layout);
        // btnSignIn.setOnClickListener(this);
        rr_login=(RelativeLayout)findViewById(R.id.rr_login);
        rr_login.setOnClickListener(this);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setOnClickListener(this);
        tvFlag=(TextView) findViewById(R.id.tvFlag);

        ll_loginwithemail=(LinearLayout)findViewById(R.id.ll_loginwithemail);
        ll_loginwithphonenumber=(LinearLayout)findViewById(R.id.ll_loginwithphonenumber);
        slide_bottom_to_top.setFillAfter(true);
        ll_ext_country_code=(LinearLayout)findViewById(R.id.ll_ext_country_code);

        rr_layout.setVisibility(View.VISIBLE);
        rr_layout.setAnimation(slide_bottom_to_top);

        etSignInEmail = (MyFontEdittextView) findViewById(R.id.ext_email);
        etSignInPassword = (MyFontEdittextView) findViewById(R.id.etSignInPassword);

        ext_country_code=(MyFontTextView)findViewById(R.id.ext_country_code);
        etSignInPassword.setOnEditorActionListener(this);

        ext_phone=(MaskEditText)findViewById(R.id.ext_phone);

        rr_create_account = (LinearLayout) findViewById(R.id.rr_create_account);
        rr_create_account.setOnClickListener(this);

//        ivFacebook = (ImageView) findViewById(R.id.ivFacebook);
//        ivFacebook.setOnClickListener(this);

//        ll_fb_login=(LinearLayout)findViewById(R.id.ll_fb_login);
//        ll_fb_login.setOnClickListener(this);
        ll_ext_country_code=(LinearLayout)findViewById(R.id.ll_ext_country_code);

        ivGoogle = (ImageView) findViewById(R.id.ivGoogle);
        ivGoogle.setOnClickListener(this);


        callbackManager = CallbackManager.Factory.create();
     ll_ext_country_code.setOnClickListener(this);
   tvFlag.setOnClickListener(this);
   ext_country_code.setOnClickListener(this);

        ll_loginwithphonenumber.setOnClickListener(this);


        getHashKey();
        setCountry(0);



//        randomWaterBallAnimation();
//        randomWaterBallAnimation1();


    }
    private void setCountry(int position) {
        country = codeArrayList.get(position);
        ext_country_code.setText(country.getCountryphonecode());
        countryName = country.getCountryname();

        System.out.println("Country Name is###"+countryName);
        System.out.println("Country Name is###"+country.getCountryphonecode());
        System.out.println("Country Flag is###"+country.getFlagUrl());

        phoneNumberLength = country.getPhoneNumberLength();
        phoneNumberMinLength = country.getPhoneNumberMinLength();



        if(country.getCountryphonecode().equalsIgnoreCase("+33"))
        {
            ext_phone.setMask("# ## ## ## ##");

        }

        else  if(country.getCountryphonecode().equalsIgnoreCase("+91"))
        {
            ext_phone.setMask("## #### ####");

        }

        else if(country.getCountryphonecode().equalsIgnoreCase("+971"))
        {
            ext_phone.setMask("## ### ####");

        }

        else
        {
            ext_phone.setMask("# ## ## ## ##");

        }

        tvFlag.setText(country.getFlagUrl());
//        PicassoTrustAll.getInstance(SignInActivity_Driver.this)
//                .load(ApiClient.Base_URL+country.getFlagUrl())
//                // .error(R.drawable.ellipse)
//                .into(tvFlag);


    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.ll_login_with_email:
                if (checked)
                    ll_loginwithemail.setVisibility(View.VISIBLE);
                ll_loginwithphonenumber.setVisibility(View.GONE);

                // Pirates are the best
                break;
            case R.id.ll_login_with_phonenumber:
                if (checked)
                    ll_loginwithemail.setVisibility(View.GONE);
                ll_loginwithphonenumber.setVisibility(View.VISIBLE);
                // Ninjas rule
                break;
        }
    }

    public  void getHashKey()
    {
        try {
            PackageInfo info = getApplicationContext().getPackageManager().getPackageInfo(
                    "com.alberdriver.driver",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", "KeyHash:" + Base64.encodeToString(md.digest(),
                        Base64.DEFAULT));
//                Toast.makeText(getApplicationContext().getApplicationContext(), Base64.encodeToString(md.digest(),
//                        Base64.DEFAULT), Toast.LENGTH_LONG).show();
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        if (googleApiClient != null && !googleApiClient.isConnected()) {
            googleApiClient.connect(GoogleApiClient.SIGN_IN_MODE_OPTIONAL);
        }

    }

    @Override
    protected void onStop() {
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
        super.onStop();

    }
    private boolean checkPlayServices() {

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 12).show();
            } else {
                AppLog.Log("Google Play Service", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setConnectivityListener(this);
        setAdminApprovedListener(this);
    }


    /**
     * Facebook login part
     */
    private void registerCallForFacebook() {
        LoginManager.getInstance().registerCallback(callbackManager, new
                FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Utils.showCustomProgressDialog(SignInActivity_Driver.this, "", false, null);
                        getFacebookProfileDetail(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        AppLog.Log("Facebook :", "" + error);
                        Utils.showToast(getString(R.string.message_can_not_signin_facebook),
                                SignInActivity_Driver.this);
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(this, Collections.singletonList
                ("email"));
    }


    private void getFacebookProfileDetail(AccessToken accessToken) {

        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest
                .GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Utils.hideCustomProgressDialog();
                try {
                    if (object.has("email")) {
                        socialEmail = object.getString("email");
                    }
                    socialId = object.getString("id");
                    signIn(Const.SOCIAL_FACEBOOK);

                } catch (JSONException e) {
                    AppLog.handleException(Const.Tag.SIGN_IN_ACTIVITY, e);
                }
                AppLog.Log("fb response", object.toString());
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();

    }


    /**
     * Google signIn part...
     */
    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, Const.google.RC_SIGN_IN);
    }

    private void handleGoogleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            try {
                GoogleSignInAccount account = result.getSignInAccount();
                socialId = account.getId();
                socialEmail = account.getEmail();
                signIn(Const.SOCIAL_GOOGLE);
            } catch (NullPointerException e) {
                AppLog.handleException(Const.Tag.SIGN_IN_ACTIVITY, e);
            }
        } else {
            AppLog.Log("Error", result.getStatus().toString());
            Utils.showToast(getString(R.string.message_can_not_signin_google), SignInActivity_Driver.this);
        }
    }


    private void signOutFromGoogle() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {

                    @Override
                    public void onResult(@NonNull Status status) {
                        AppLog.Log("Google SignOut", "" + status);
                    }
                });
    }


    private void signIn(String loginType) {

        AppLog.Log(Const.Tag.SIGN_IN_ACTIVITY, "SingIn valid");
        Utils.showCustomProgressDialog(this, getResources().getString(R.string
                .msg_waiting_for_sing_in), false, null);

        JSONObject jsonObject = new JSONObject();


        try {
            if (loginType.equalsIgnoreCase(Const.MANUAL)) {
                jsonObject.put(Const.Params.PASSWORD, etSignInPassword.getText()
                        .toString());

                if(ll_loginwithphonenumber.getVisibility()==View.VISIBLE)
                {
                    jsonObject.put(Const.Params.EMAIL, ext_phone.getText().toString());

                }

                else if(ll_loginwithemail.getVisibility()==View.VISIBLE)
                {
                    jsonObject.put(Const.Params.EMAIL, etSignInEmail.getText().toString());

                }
                //   jsonObject.put(Const.Params.EMAIL, etSignInEmail.getText().toString());
                FirebaseAuth.getInstance().signOut();
            } else {
                jsonObject.put(Const.Params.PASSWORD, "");
                jsonObject.put(Const.Params.SOCIAL_UNIQUE_ID, socialId);
                jsonObject.put(Const.Params.EMAIL, socialEmail);
            }
            jsonObject.put(Const.Params.DEVICE_TYPE, Const.DEVICE_TYPE_ANDROID);
            Log.e("FCM Token Login  ", preferenceHelper.getDeviceToken()+"");
            jsonObject.put(Const.Params.DEVICE_TOKEN, preferenceHelper.getDeviceToken());
            Log.e("LoginType  ", loginType);

            jsonObject.put(Const.Params.LOGIN_BY, loginType);
            jsonObject.put(Const.Params.APP_VERSION, getAppVersion());

            Call<ProviderDataResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .login(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<ProviderDataResponse>() {
                @Override
                public void onResponse(Call<ProviderDataResponse> call,
                                       Response<ProviderDataResponse> response) {
                    if (parseContent.isSuccessful(response)) {
                        if (parseContent.saveProviderData(response.body(), true)) {
                            AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "LogIn Success");
                            preferenceHelper.putUserPassword(etSignInPassword.getText().toString());
                            CurrentTrip.getInstance().clear();
                            Utils.hideCustomProgressDialog();
                            Intent ii =new Intent(SignInActivity_Driver.this,MainDrawerActivity.class);
                            startActivity(ii);
                        } else {
                            Utils.hideCustomProgressDialog();
                            AppLog.Log(Const.Tag.REGISTER_ACTIVITY, "LogIn Failed");
                        }
                    }



                }

                @Override
                public void onFailure(Call<ProviderDataResponse> call, Throwable t) {
                    AppLog.handleThrowable(SignInActivity_Driver.class.getSimpleName(), t);
                }
            });

        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.SIGN_IN_ACTIVITY, e);
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rr_login:
                if (isValidate()) {
                    signIn(Const.MANUAL);
                }
                break;
            case R.id.rr_create_account:
                goToRegisterActivity();
                break;
            case R.id.tvForgotPassword:
                openForgotPasswordDialog();
                break;
            case R.id.ivGoogle:
                signOutFromGoogle();
                googleSignIn();
                break;
//            case R.id.ll_fb_login:
//                LoginManager.getInstance().logOut();
//                registerCallForFacebook();
//                break;
            case  R.id.ll_loginwithphonenumber:
                openCountryCodeDialog();
                break;
            case R.id.tvFlag:
                openCountryCodeDialog();
                break;
            case R.id.ext_country_code:
                openCountryCodeDialog();
                break;
            case R.id.ll_ext_country_code:
                openCountryCodeDialog();
                break;

            default:
                // do wti default
                break;
        }

    }
    private void openCountryCodeDialog() {

        customCountryDialog = null;

        customCountryDialog = new CustomCountryDialog(SignInActivity_Driver.this, codeArrayList,"login") {
            @Override
            public void onSelect(int position, ArrayList<Country> filterList) {
                System.out.println("Country Position is####"+position);
                codeArrayList = filterList;
                setCountry(position);
                customCountryDialog.dismiss();

            }

        };
        customCountryDialog.show();

    }


    @Override
    protected boolean isValidate() {
        String msg = null;
        String user_phonenumber=ext_phone.getText().toString();

        if(ll_loginwithemail.getVisibility()==View.VISIBLE)
        {
            if (TextUtils.isEmpty(etSignInEmail.getText().toString().trim())) {
                msg = getString(R.string.msg_enter_email);
                etSignInEmail.requestFocus();
            }
            else if (!Utils.eMailValidation((etSignInEmail.getText().toString().trim()))) {
                msg = getString(R.string.msg_enter_valid_email);
                etSignInEmail.requestFocus();
            }
        }
        else if(ll_loginwithphonenumber.getVisibility()==View.VISIBLE)

        {
            if(user_phonenumber==null || user_phonenumber.equalsIgnoreCase("") || user_phonenumber.equalsIgnoreCase("null")) {
                msg = getString(R.string.msg_enter_number);
                ext_phone.setError(getResources().getString(R.string.ce_champ_est_requis));


            }


            else {
                ext_phone.setError(null);
            }
        }
        else if (TextUtils.isEmpty(etSignInPassword.getText().toString().trim())) {
            msg = getString(R.string.msg_enter_password);
            etSignInPassword.requestFocus();
        }
        else if (etSignInPassword.getText().toString().trim().length() < 6) {
            msg = getString(R.string.msg_enter_valid_password);
            etSignInPassword.requestFocus();
        }

        if (msg != null) {
            Toast.makeText(SignInActivity_Driver.this, msg, Toast.LENGTH_SHORT).show();
            return false;

        }
        return true;
    }

    @Override
    public void goWithBackArrow() {
        // do with back arrow
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backToMainActivity();
    }

    private void backToMainActivity() {
       /* Intent sigInIntent = new Intent(this, MainActivity_Driver.class);

        sigInIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sigInIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(sigInIntent);*/
        finishAffinity();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    private void openForgotPasswordDialog() {
        dialogForgotPassword = new CustomDialogVerifyAccount(this, getResources().getString(R.string
                .text_forgot_password), getResources().getString(R.string
                .text_send), getResources().getString(R.string
                .text_cancel), getResources().getString(R.string
                .text_hint_enter_email), true) {
            @Override
            public void doWithEnable(EditText editText) {
                if (Utils.eMailValidation(editText
                        .getText().toString())) {
                    forgotPassword((editText
                            .getText().toString()));
                } else {
                    Utils.showToast(getResources().getString(R.string.msg_enter_valid_email),
                            SignInActivity_Driver
                                    .this);
                }
            }

            @Override
            public void doWithDisable() {
                dismiss();
            }

            @Override
            public void clickOnText() {
                // do with click
            }
        };
        dialogForgotPassword.show();
    }

    private void forgotPassword(String email) {
        Utils.showCustomProgressDialog(this, getResources().getString(R.string.msg_loading),
                false, null);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Const.Params.EMAIL, email);
            jsonObject.put(Const.Params.TYPE, Const.PROVIDER);
            Call<IsSuccessResponse> call = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class)
                    .forgotPassword(ApiClient.makeJSONRequestBody(jsonObject));
            call.enqueue(new Callback<IsSuccessResponse>() {
                @Override
                public void onResponse(Call<IsSuccessResponse> call, Response<IsSuccessResponse>
                        response) {
                    if (parseContent.isSuccessful(response)) {
                        if (response.body().isSuccess()) {
                            if (dialogForgotPassword != null && dialogForgotPassword.isShowing()) {
                                dialogForgotPassword.dismiss();
                            }
                            Utils.showMessageToast(response.body().getMessage(), SignInActivity_Driver
                                    .this);
                            Utils.hideCustomProgressDialog();
                        } else {
                            Utils.hideCustomProgressDialog();
                            Utils.showErrorToast(response.body().getErrorCode(), SignInActivity_Driver
                                    .this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<IsSuccessResponse> call, Throwable t) {
                    AppLog.handleThrowable(SignInActivity_Driver.class.getSimpleName(), t);
                }
            });

        } catch (JSONException e) {
            AppLog.handleException(Const.Tag.SIGN_IN_ACTIVITY, e);
        }


    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        switch (v.getId()) {
            case R.id.etSignInPassword:
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (isValidate()) {
                        signIn(Const.MANUAL);
                    }
                    return true;
                }
                break;
            default:
                // do with default
                break;

        }
        return false;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            closedEnableDialogInternet();
        } else {
            openInternetDialog();
        }
    }

    @Override
    public void onGpsConnectionChanged(boolean isConnected) {
        if (isConnected) {
            closedEnableDialogGps();
        } else {
            openGpsDialog();

        }
    }

    @Override
    public void onAdminApproved() {
        goWithAdminApproved();
    }

    @Override
    public void onAdminDeclined() {
        goWithAdminDecline();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Const.google.RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void randomWaterBallAnimation(){

        water_ball1 =  findViewById(R.id.water_ball);


        // For First First

        RotateAnimation anim = new RotateAnimation(0f, 350f, 15f, 15f);
        //   RotateAnimation anim = new RotateAnimation(0f, 0.5f, 1.1f, -0.3f);

        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(9000);

        TranslateAnimation mAnimation ;
        mAnimation = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.3f);
        mAnimation.setDuration(25000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());

        water_ball1.setAnimation(mAnimation);
        water_ball1.startAnimation(anim);


        // For Second Second  water_ball_2 =  findViewById(R.id.water_ball_2);
        water_ball_21 =  findViewById(R.id.water_ball_2);
        TranslateAnimation mAnimation2 ;
        mAnimation2 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.7f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation2.setDuration(30000);
        mAnimation2.setRepeatCount(-1);
        mAnimation2.setRepeatMode(Animation.REVERSE);
        mAnimation2.setInterpolator(new LinearInterpolator());

        water_ball_21.setAnimation(mAnimation2);

        water_ball_21.animate().rotation(1800f).setDuration(25000).start();
        water_ball_21.getAnimation().setRepeatCount(-1);

        water_ball_21.getAnimation().setRepeatMode(Animation.REVERSE);
        water_ball_21.getAnimation().setInterpolator(new LinearInterpolator());




        water_ball_3 = findViewById(R.id.water_ball_3);


        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(19000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());
        water_ball_3.setAnimation(mAnimation3);

        water_ball_4 = findViewById(R.id.water_ball_4);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());
        water_ball_4.setAnimation(mAnimation4);

        water_ball_5 = findViewById(R.id.water_ball_5);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());
        water_ball_5.setAnimation(mAnimation5);

        water_ball_6 = findViewById(R.id.water_ball_6);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF,0.0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0.5f,
                TranslateAnimation.RELATIVE_TO_SELF, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());
        water_ball_6.setAnimation(mAnimation6);

        water_ball_7 = findViewById(R.id.water_ball_7);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.1f);
        mAnimation7.setDuration(8000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());
        water_ball_7.setAnimation(mAnimation7);

        water_ball_8 = findViewById(R.id.water_ball_8);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());
        water_ball_8.setAnimation(mAnimation8);


        Water_ball_10 = findViewById(R.id.water_ball_10);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());
        Water_ball_10.setAnimation(mAnimation10);

        Water_ball_9 = findViewById(R.id.water_ball_9);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(10000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_9.setAnimation(mAnimation9);
    }

    private void randomWaterBallAnimation1(){




        water_ball_31 = findViewById(R.id.water_ball_31);


        TranslateAnimation mAnimation3 ;
        mAnimation3 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.8f);
        mAnimation3.setDuration(9000);
        mAnimation3.setRepeatCount(-1);
        mAnimation3.setRepeatMode(Animation.REVERSE);
        mAnimation3.setInterpolator(new LinearInterpolator());

        water_ball_31.setAnimation(mAnimation3);



        water_ball_41 = findViewById(R.id.water_ball_41);
        TranslateAnimation mAnimation4 ;
        mAnimation4 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation4.setDuration(45000);
        mAnimation4.setRepeatCount(-1);
        mAnimation4.setRepeatMode(Animation.REVERSE);
        mAnimation4.setInterpolator(new LinearInterpolator());

        water_ball_41.setAnimation(mAnimation4);


        water_ball_51 = findViewById(R.id.water_ball_51);
        TranslateAnimation mAnimation5 ;
        mAnimation5 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.9f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f);
        mAnimation5.setDuration(20000);
        mAnimation5.setRepeatCount(-1);
        mAnimation5.setRepeatMode(Animation.REVERSE);
        mAnimation5.setInterpolator(new LinearInterpolator());

        water_ball_51.setAnimation(mAnimation5);


        water_ball_61= findViewById(R.id.water_ball_61);
        TranslateAnimation mAnimation6 ;
        mAnimation6 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation6.setDuration(35000);
        mAnimation6.setRepeatCount(-1);
        mAnimation6.setRepeatMode(Animation.REVERSE);
        mAnimation6.setInterpolator(new LinearInterpolator());

        water_ball_61.setAnimation(mAnimation6);


        water_ball_71 = findViewById(R.id.water_ball_71);
        TranslateAnimation mAnimation7 ;
        mAnimation7 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation7.setDuration(9000);
        mAnimation7.setRepeatCount(-1);
        mAnimation7.setRepeatMode(Animation.REVERSE);
        mAnimation7.setInterpolator(new LinearInterpolator());

        water_ball_71.setAnimation(mAnimation7);


        water_ball_81 = findViewById(R.id.water_ball_81);
        TranslateAnimation mAnimation8 ;
        mAnimation8 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,-0.1f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f);
        mAnimation8.setDuration(21000);
        mAnimation8.setRepeatCount(-1);
        mAnimation8.setRepeatMode(Animation.REVERSE);
        mAnimation8.setInterpolator(new LinearInterpolator());

        water_ball_81.setAnimation(mAnimation8);



        Water_ball_101 = findViewById(R.id.water_ball_101);
        TranslateAnimation mAnimation10 ;
        mAnimation10 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.0f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.2f);
        mAnimation10.setDuration(25000);
        mAnimation10.setRepeatCount(-1);
        mAnimation10.setRepeatMode(Animation.REVERSE);
        mAnimation10.setInterpolator(new LinearInterpolator());

        Water_ball_101.setAnimation(mAnimation10);


        Water_ball_91 = findViewById(R.id.water_ball_91);
        TranslateAnimation mAnimation9 ;
        mAnimation9 = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_PARENT, 0.4f,
                TranslateAnimation.RELATIVE_TO_PARENT,0.3f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.2f,
                TranslateAnimation.RELATIVE_TO_PARENT, -0.1f);
        mAnimation9.setDuration(9000);
        mAnimation9.setRepeatCount(-1);
        mAnimation9.setRepeatMode(Animation.REVERSE);
        mAnimation9.setInterpolator(new LinearInterpolator());
        Water_ball_91.setAnimation(mAnimation9);
    }

}
